/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package android.support.compat;

public final class R {
    public static final class attr {
        public static final int alpha = 0x7f0101bd;
        public static final int font = 0x7f010251;
        public static final int fontProviderAuthority = 0x7f01024a;
        public static final int fontProviderCerts = 0x7f01024d;
        public static final int fontProviderFetchStrategy = 0x7f01024e;
        public static final int fontProviderFetchTimeout = 0x7f01024f;
        public static final int fontProviderPackage = 0x7f01024b;
        public static final int fontProviderQuery = 0x7f01024c;
        public static final int fontStyle = 0x7f010250;
        public static final int fontVariationSettings = 0x7f010253;
        public static final int fontWeight = 0x7f010252;
        public static final int ttcIndex = 0x7f010254;
    }
    public static final class color {
        public static final int notification_action_color_filter = 0x7f0f0003;
        public static final int notification_icon_bg_color = 0x7f0f00bc;
        public static final int ripple_material_light = 0x7f0f00d8;
        public static final int secondary_text_default_material_light = 0x7f0f00da;
    }
    public static final class dimen {
        public static final int compat_button_inset_horizontal_material = 0x7f0b0335;
        public static final int compat_button_inset_vertical_material = 0x7f0b0336;
        public static final int compat_button_padding_horizontal_material = 0x7f0b0337;
        public static final int compat_button_padding_vertical_material = 0x7f0b0338;
        public static final int compat_control_corner_material = 0x7f0b0339;
        public static final int compat_notification_large_icon_max_height = 0x7f0b033a;
        public static final int compat_notification_large_icon_max_width = 0x7f0b033b;
        public static final int notification_action_icon_size = 0x7f0b03b8;
        public static final int notification_action_text_size = 0x7f0b03b9;
        public static final int notification_big_circle_margin = 0x7f0b03ba;
        public static final int notification_content_margin_start = 0x7f0b02da;
        public static final int notification_large_icon_height = 0x7f0b03bb;
        public static final int notification_large_icon_width = 0x7f0b03bc;
        public static final int notification_main_column_padding_top = 0x7f0b02db;
        public static final int notification_media_narrow_margin = 0x7f0b02dc;
        public static final int notification_right_icon_size = 0x7f0b03bd;
        public static final int notification_right_side_padding_top = 0x7f0b02d8;
        public static final int notification_small_icon_background_padding = 0x7f0b03be;
        public static final int notification_small_icon_size_as_large = 0x7f0b03bf;
        public static final int notification_subtext_size = 0x7f0b03c0;
        public static final int notification_top_pad = 0x7f0b03c1;
        public static final int notification_top_pad_large_text = 0x7f0b03c2;
    }
    public static final class drawable {
        public static final int notification_action_background = 0x7f020306;
        public static final int notification_bg = 0x7f020307;
        public static final int notification_bg_low = 0x7f020308;
        public static final int notification_bg_low_normal = 0x7f020309;
        public static final int notification_bg_low_pressed = 0x7f02030a;
        public static final int notification_bg_normal = 0x7f02030b;
        public static final int notification_bg_normal_pressed = 0x7f02030c;
        public static final int notification_icon_background = 0x7f02030d;
        public static final int notification_template_icon_bg = 0x7f02038f;
        public static final int notification_template_icon_low_bg = 0x7f020390;
        public static final int notification_tile_bg = 0x7f02030e;
        public static final int notify_panel_notification_icon_bg = 0x7f02030f;
    }
    public static final class id {
        public static final int action_container = 0x7f1103d5;
        public static final int action_divider = 0x7f1103dc;
        public static final int action_image = 0x7f1103d6;
        public static final int action_text = 0x7f1103d7;
        public static final int actions = 0x7f1103e4;
        public static final int async = 0x7f1100b8;
        public static final int blocking = 0x7f1100b9;
        public static final int chronometer = 0x7f1103e1;
        public static final int forever = 0x7f1100ba;
        public static final int icon = 0x7f110113;
        public static final int icon_group = 0x7f1103e5;
        public static final int info = 0x7f1103e2;
        public static final int italic = 0x7f1100bb;
        public static final int line1 = 0x7f110017;
        public static final int line3 = 0x7f110018;
        public static final int normal = 0x7f110053;
        public static final int notification_background = 0x7f1103e3;
        public static final int notification_main_column = 0x7f1103de;
        public static final int notification_main_column_container = 0x7f1103dd;
        public static final int right_icon = 0x7f1103e6;
        public static final int right_side = 0x7f1103df;
        public static final int tag_transition_group = 0x7f110024;
        public static final int tag_unhandled_key_event_manager = 0x7f110025;
        public static final int tag_unhandled_key_listeners = 0x7f110026;
        public static final int text = 0x7f110027;
        public static final int text2 = 0x7f110028;
        public static final int time = 0x7f1103e0;
        public static final int title = 0x7f11002c;
    }
    public static final class integer {
        public static final int status_bar_notification_info_maxnum = 0x7f0e0014;
    }
    public static final class layout {
        public static final int notification_action = 0x7f04008b;
        public static final int notification_action_tombstone = 0x7f04008c;
        public static final int notification_template_custom_big = 0x7f040093;
        public static final int notification_template_icon_group = 0x7f040094;
        public static final int notification_template_part_chronometer = 0x7f040098;
        public static final int notification_template_part_time = 0x7f040099;
    }
    public static final class string {
        public static final int status_bar_notification_info_overflow = 0x7f0a0081;
    }
    public static final class style {
        public static final int TextAppearance_Compat_Notification = 0x7f0c0089;
        public static final int TextAppearance_Compat_Notification_Info = 0x7f0c008a;
        public static final int TextAppearance_Compat_Notification_Line2 = 0x7f0c014c;
        public static final int TextAppearance_Compat_Notification_Time = 0x7f0c008d;
        public static final int TextAppearance_Compat_Notification_Title = 0x7f0c008f;
        public static final int Widget_Compat_NotificationActionContainer = 0x7f0c0096;
        public static final int Widget_Compat_NotificationActionText = 0x7f0c0097;
    }
    public static final class styleable {
        public static final int[] ColorStateListItem = { 0x010101a5, 0x0101031f, 0x7f0101bd };
        public static final int ColorStateListItem_android_color = 0;
        public static final int ColorStateListItem_android_alpha = 1;
        public static final int ColorStateListItem_alpha = 2;
        public static final int[] FontFamily = { 0x7f01024a, 0x7f01024b, 0x7f01024c, 0x7f01024d, 0x7f01024e, 0x7f01024f };
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderPackage = 1;
        public static final int FontFamily_fontProviderQuery = 2;
        public static final int FontFamily_fontProviderCerts = 3;
        public static final int FontFamily_fontProviderFetchStrategy = 4;
        public static final int FontFamily_fontProviderFetchTimeout = 5;
        public static final int[] FontFamilyFont = { 0x01010532, 0x01010533, 0x0101053f, 0x0101056f, 0x01010570, 0x7f010250, 0x7f010251, 0x7f010252, 0x7f010253, 0x7f010254 };
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_ttcIndex = 3;
        public static final int FontFamilyFont_android_fontVariationSettings = 4;
        public static final int FontFamilyFont_fontStyle = 5;
        public static final int FontFamilyFont_font = 6;
        public static final int FontFamilyFont_fontWeight = 7;
        public static final int FontFamilyFont_fontVariationSettings = 8;
        public static final int FontFamilyFont_ttcIndex = 9;
        public static final int[] GradientColor = { 0x0101019d, 0x0101019e, 0x010101a1, 0x010101a2, 0x010101a3, 0x010101a4, 0x01010201, 0x0101020b, 0x01010510, 0x01010511, 0x01010512, 0x01010513 };
        public static final int GradientColor_android_startColor = 0;
        public static final int GradientColor_android_endColor = 1;
        public static final int GradientColor_android_type = 2;
        public static final int GradientColor_android_centerX = 3;
        public static final int GradientColor_android_centerY = 4;
        public static final int GradientColor_android_gradientRadius = 5;
        public static final int GradientColor_android_tileMode = 6;
        public static final int GradientColor_android_centerColor = 7;
        public static final int GradientColor_android_startX = 8;
        public static final int GradientColor_android_startY = 9;
        public static final int GradientColor_android_endX = 10;
        public static final int GradientColor_android_endY = 11;
        public static final int[] GradientColorItem = { 0x010101a5, 0x01010514 };
        public static final int GradientColorItem_android_color = 0;
        public static final int GradientColorItem_android_offset = 1;
    }
}
