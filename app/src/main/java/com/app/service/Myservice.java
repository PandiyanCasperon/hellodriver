package com.app.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.android.volley.Request;

import java.lang.reflect.Method;
import java.util.HashMap;


public class Myservice extends IntentService {
    private String Ack_ride_id = "", driver_id = "", Sride_type = "";
    private ServiceRequest mRequest;


    /*public Myservice(String name) {
        super(name);
    }*/

    public Myservice() {
        super("DisplayNotification");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        Ack_ride_id = intent.getStringExtra("Ack_id");

        Sride_type = intent.getStringExtra("ride_type");

        if (Sride_type.equalsIgnoreCase("accept")) {
            posrequestUpdate_ride(ServiceConstant.acknowledge_ride);
        } else {
            posrequestUpdate_ride(ServiceConstant.delete_ride);
        }


    }

    private void posrequestUpdate_ride(String url) {
        HashMap<String, String> jsonparams = new HashMap<>();
        jsonparams.put("ack_id", Ack_ride_id);
        System.out.println("---------------Myservice_params-----------" + jsonparams);

        mRequest = new ServiceRequest(Myservice.this);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonparams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------Myservice_response----------" + response);


            }

            @Override
            public void onErrorListener() {

            }
        });

    }
}
