package com.app.sqliteDb;

/**
 * Created by user144 on 8/28/2018.
 */

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class documentdb extends SQLiteOpenHelper {


    public static String DATABASE_NAME="documenxsftllrsddaxve";
    public static final String TABLE_NAME="doddcudgaxxmrkjenttable";


    public static final String KEY_SAVEID="saveid";
    public static final String KEY_NAME="name";
    public static final String KEY_SHORTNAME="shortname";

    public static final String KEY_TIME="title";
    public static final String KEY_DESCRIPTION="description";
    public static final String KEY_IS_REQ="is_req";
    public static final String KEY_IS_EXP="is_exp";
    public static final String KEY_PATTERN="pattern";

    public static final String KEY_IMAGEADDED="imagesadded";
    public static final String KEY_IMAGESNAME="imagesName";
    public static final String KEY_EXPIRE_DATE="expireDate";



    public static final String KEY_ID="id";


    public documentdb(Context context) {
        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE="CREATE TABLE "+TABLE_NAME+" ("+KEY_ID+" INTEGER PRIMARY KEY, "+KEY_SAVEID+" TEXT, "+KEY_NAME+" TEXT, "+KEY_SHORTNAME+" TEXT, "+KEY_TIME+" TEXT, "+KEY_DESCRIPTION+" TEXT, "+KEY_IS_REQ+" TEXT, "+KEY_IS_EXP+" TEXT, "+KEY_PATTERN+" TEXT, "+KEY_IMAGEADDED+" TEXT, "+KEY_EXPIRE_DATE+" TEXT, "+KEY_IMAGESNAME+" TEXT)";
        db.execSQL(CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);

    }


    public int getProfilesCount() {
        String countQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public void clearTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_NAME, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int count = cursor.getInt(0);
            if (count > 0) {
                db.delete(TABLE_NAME, null, null);
                System.out.println("*****************************Chat Table Cleared*****************************");
                db.close();
            }
            System.out.println("*****************************Chat Table empty*****************************");
            cursor.close();
        }
        System.out.println("*****************************Chat Table cursor empty*****************************");

    }





}
