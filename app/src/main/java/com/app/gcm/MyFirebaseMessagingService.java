package com.app.gcm;


import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.service.ServiceConstant;
import com.app.service.ServiceRequest;
import com.app.sqliteDb.ChatDatabaseHelper;
import com.hellotaxidriver.driver.NewTripAlert;
import com.hellotaxidriver.driver.Pojo.ChatPojo;
import com.hellotaxidriver.driver.PushNotificationAlert;
import com.hellotaxidriver.driver.R;
import com.hellotaxidriver.driver.Splash;
import com.hellotaxidriver.driver.Utils.ConnectionDetector;
import com.hellotaxidriver.driver.Utils.SessionManager;
import com.hellotaxidriver.driver.newdriverrequestpage;
import com.hellotaxidriver.driver.foregroundservice.sqlitedb.ridelaternotificationdb;
import com.hellotaxidriver.driver.foregroundservice.sqlitedb.riderequestnotificationdb;
import com.hellotaxidriver.driver.tripdetailspackage.tripsummaryutils.TripSummarCancelRidesDetailsUtil;
import com.hellotaxidriver.driver.waitingservice;
import com.hellotaxidriver.driver.widgets.PkDialog;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by user145 on 8/4/2017.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    int no = 114;
    SessionManager Session;
    //---------------------------Request dublication and time stamp
    private riderequestnotificationdb mHelper;
    //---------------------------Request dublication and time stamp end
    private SQLiteDatabase dataBase, dataBase1;
    private String DeviceLanguage;
    private ridelaternotificationdb mHelper1;
    private String Job_status_message = "", actionnew = "";
    private String key1 = "", key2 = "", key3 = "", key5 = "", key8 = "", key9 = "", message = "", action = "", key4 = "", key6 = "", key7 = "", key10 = "", key11 = "", key12 = "", msg1, title, banner;
    private Boolean isInternetPresent = false;
    private String driver_id = "";
    private ServiceRequest mRequest;
    private boolean isAppInfoAvailable = false;
    private ConnectionDetector cd;
    private String driverID = "", driver_image = "", driverName = "",ride_id="",message_desc="",timestamppush="";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());


        Session = new SessionManager(getApplicationContext());
        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Data Payload: " + remoteMessage.getData().toString());
            try
            {
                Map<String, String> params = remoteMessage.getData();
                JSONObject object = new JSONObject(params);
                Log.d("JSON_OBJECT", object.toString());
                handleDataMessage(object);
            } catch (Exception e) {
                Log.d(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotification(String message) {
        Job_status_message = message;
    }

    private void handleDataMessage(JSONObject json) {


        try {
            if (json.has("action")) {
                action = json.getString("action");
            }
            if (json.has("message")) {
                message = json.getString("message");
            }

            if (json.has("key1")) {
                key1 = json.getString("key1");
            }
            if (json.has("key2")) {
                key2 = json.getString("key2");
            }
            if (json.has("key3")) {
                key3 = json.getString("key3");
            }

            if (json.has("message_desc")) {
                message_desc = json.getString("message_desc");
            }
            if (json.has("timestamp")) {
                timestamppush = json.getString("timestamp");
            }
            if (json.has("ride_id")) {
                ride_id = json.getString("ride_id");
            }



            if (json.has("key4")) {
                key4 = json.getString("key4");
            }
            if (json.has("key5")) {
                key5 = json.getString("key5");
            }
            if (json.has("key6")) {
                key6 = json.getString("key6");
                Session.set_ack_id(key6);
            }
            if (json.has("key7")) {
                key7 = json.getString("key7");
            }
            if (json.has("key8")) {
                key8 = json.getString("key8");
            }
            if (json.has("key9")) {
                key9 = json.getString("key9");
            }
            if (json.has("key10")) {
                key10 = json.getString("key10");
            }

            if (json.has("key11")) {
                key11 = json.getString("key11");
            }

            if (json.has("key12")) {
                key12 = json.getString("key12");
            }

            if (action.equalsIgnoreCase(ServiceConstant.pushNotification_Ads)) {
                title = json.getString("key1");
                msg1 = json.getString("key2");
                banner = json.getString("key3");

            }


            JSONObject jsonObject = new JSONObject();
            jsonObject.put("message", message);
            jsonObject.put("action", action);
            jsonObject.put("key1", key1);
            jsonObject.put("key2", key2);
            jsonObject.put("key3", key3);
            jsonObject.put("key4", key4);
            jsonObject.put("key5", key5);
            jsonObject.put("key6", key6);
            jsonObject.put("key7", key7);
            jsonObject.put("key8", key8);
            jsonObject.put("key9", key9);
            jsonObject.put("key10", key10);
            jsonObject.put("key11", key11);
            jsonObject.put("key12", key12);


            if (action.equalsIgnoreCase(ServiceConstant.ACTION_TAG_RIDE_CANCELLED)) {

                ChatDatabaseHelper db = new ChatDatabaseHelper(getApplicationContext());
                HashMap<String, String> ride = Session.getrideType();
                String rideType = ride.get(SessionManager.KEY_RIDE_TYPE);
                if (rideType.equalsIgnoreCase("Normal")) {
                    db.clearTable();
                } else {
                    db.clearShareRide(key1);
                }
                Session.setchatNotify("0");

                Intent intent = new Intent(getApplicationContext(), PushNotificationAlert.class);
                intent.putExtra("Message", message);
                intent.putExtra("Action", action);
                intent.putExtra("RideId", key1);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }

            sendNotification(message.toString(), jsonObject);


            if (ServiceConstant.ACTION_TAG_PAYMENT_PAID.equalsIgnoreCase(action))
            {
                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction("com.finish.OtpPage");
                sendOrderedBroadcast(broadcastIntent, null);
                Intent broadcastIntent_payment = new Intent();
                broadcastIntent_payment.setAction("com.finish.PaymentPage");
                sendOrderedBroadcast(broadcastIntent_payment, null);
                Intent broadcastIntent_paymenttrip = new Intent();
                broadcastIntent_paymenttrip.setAction("com.finish.tripsummerydetail");
                sendOrderedBroadcast(broadcastIntent_paymenttrip, null);
                Intent broadcastIntent_pushnotification = new Intent();
                broadcastIntent_pushnotification.setAction("com.finish.PushNotificationAlert");
                sendOrderedBroadcast(broadcastIntent_pushnotification, null);
                Intent intent1 = new Intent(this, PushNotificationAlert.class);
                intent1.putExtra("Message", jsonObject.getString("message"));
                intent1.putExtra("Action", jsonObject.getString("action"));
                intent1.putExtra("RideId", jsonObject.getString("key1"));
                startActivity(intent1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void sendNotification(String msg, JSONObject data) {



        Intent notificationIntent = null;

        int id = createID();


        Session = new SessionManager(MyFirebaseMessagingService.this);

        Session.setNotificationStatus("");

        if (ServiceConstant.ACTION_CHAT_RECEIVED.equalsIgnoreCase(action))
        {
            if(!timestamppush.equals(""))
            {
                if(!ride_id.equals(""))
                {
                    String timestamp="";
                    ChatDatabaseHelper db;
                    SessionManager session;
                    session = new SessionManager(getApplicationContext());
                    db = new ChatDatabaseHelper(getApplicationContext());

                    SimpleDateFormat df_time = new SimpleDateFormat("hh:mm a");
                    Calendar cal = Calendar.getInstance();
                    String sCurrentTime = df_time.format(cal.getTime());
                    timestamp = sCurrentTime;

                    HashMap<String, String> state = session.getAppStatus();
                    String Str_driverState = state.get(SessionManager.KEY_APP_STATUS);
                    String sMessage1 = "";
                    sMessage1 = message_desc;

                    if(!timestamppush.equals(""))
                    {
                        timestamp = timestamppush;
                    }

                    ChatDatabaseHelper  mHelper = new ChatDatabaseHelper(getApplicationContext());
                    SQLiteDatabase dataBase = mHelper.getWritableDatabase();
                    int okl = 0;
                    Cursor mCursor = dataBase.rawQuery("SELECT * FROM " + mHelper.TABLE_CHAT + " WHERE chattimestamp ='" + timestamp + "'" + " AND chatMessage='" + sMessage1 + "'", null);
                    if (mCursor.moveToFirst())
                    {
                        do
                        {
                            okl++;
                        } while (mCursor.moveToNext());
                    }
                    if (okl == 0)
                    {
                        try
                        {
                            db.addChat(new ChatPojo(sMessage1, "OTHER", sCurrentTime, "0", Integer.parseInt(ride_id),timestamp),getApplicationContext());

                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        if (ServiceConstant.ACTION_TAG_RIDE_CANCELLED.equalsIgnoreCase(action)) {
            HashMap<String, String> user = Session.getUserDetails();
            driver_id = user.get(SessionManager.KEY_DRIVERID);
            TripSummarCancelRidesDetailsUtil tripSummarCancelRidesDetailsUtil = new TripSummarCancelRidesDetailsUtil(MyFirebaseMessagingService.this, driver_id, key1);
            if (isMyServiceRunning(waitingservice.class))
            {
                try
                {
                    Intent startIntent = new Intent(getApplicationContext(), waitingservice.class);
                    stopService(startIntent);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }

        if (ServiceConstant.pushNotification_Ads.equalsIgnoreCase(action)) {
            notificationIntent = new Intent(MyFirebaseMessagingService.this, Splash.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            notificationIntent.putExtra("title", title);
            notificationIntent.putExtra("msg", msg1);
            notificationIntent.putExtra("banner", banner);
            notificationIntent.putExtra("ad", "true");
            notificationIntent.putExtra("push", "false");
        } else {
            notificationIntent = new Intent(MyFirebaseMessagingService.this, Splash.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            notificationIntent.putExtra("ad", "false");
            notificationIntent.putExtra("push", "false");
        }

        if (action.equalsIgnoreCase(ServiceConstant.ACTION_TAG_RIDE_REQUEST)) {

            int okl = 0;
            mHelper = new riderequestnotificationdb(this);
            dataBase = mHelper.getWritableDatabase();
            Cursor mCursor = dataBase.rawQuery("SELECT * FROM " + mHelper.TABLE_NAME + " WHERE uniqueid ='" + key1 + "'" + "AND timestamp='" + key5 + "'", null);

            if (mCursor.moveToFirst()) {
                do {
                    okl++;
                    System.out.println("Sara response--->fcm-okl" + okl);
                } while (mCursor.moveToNext());
            }
            if (okl == 0) {
                System.out.println("Sara response--->fcm-record not exists" + mHelper.getProfilesCount());
                mHelper = new riderequestnotificationdb(this);
                dataBase = mHelper.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(riderequestnotificationdb.KEY_FNAME, "");
                values.put(riderequestnotificationdb.KEY_LNAME, key1);
                values.put(riderequestnotificationdb.KEY_TIMESTAMP, key5);
                dataBase.insert(riderequestnotificationdb.TABLE_NAME, null, values);
                System.out.println("-----------data.toString()--------" + data.toString());
                try {
                    HashMap<String, String> language = Session.getLanaguage();
                    DeviceLanguage = language.get(SessionManager.KEY_Language);
                    long tsCurrentLong = System.currentTimeMillis() / 1000;
                    Long tsRequestLong = Long.valueOf(data.getString("key5"));

                    Intent intent1 = new Intent(MyFirebaseMessagingService.this, newdriverrequestpage.class);
                    intent1.putExtra("EXTRA", data.toString());
                    intent1.putExtra("ad", "false");
                    intent1.putExtra("push", "true");
                    intent1.putExtra("data", data.toString());
                    intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent1);


                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                System.out.println("Sara response--->fcm-record exists" + mHelper.getProfilesCount());
            }



        } else if (ServiceConstant.ACTION_TAG_NEW_TRIP.equalsIgnoreCase(action)) {

            int okl = 0;
            mHelper1 = new ridelaternotificationdb(this);
            dataBase1 = mHelper1.getWritableDatabase();
            Cursor mCursor = dataBase1.rawQuery("SELECT * FROM " + mHelper1.TABLE_NAME + " WHERE uniqueid ='" + key6 + "'", null);

            if (mCursor.moveToFirst()) {
                do {
                    okl++;
                    System.out.println("Sara response--->fcm-okl" + okl);
                } while (mCursor.moveToNext());
            }

            if (okl == 0) {
                mHelper1 = new ridelaternotificationdb(this);
                dataBase1 = mHelper1.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(ridelaternotificationdb.KEY_FNAME, "");
                values.put(ridelaternotificationdb.KEY_LNAME, key6);
                dataBase1.insert(ridelaternotificationdb.TABLE_NAME, null, values);
                System.out.println("---------------inside new trip--------gcm----------");
                notificationIntent = new Intent(MyFirebaseMessagingService.this, NewTripAlert.class);
                notificationIntent.putExtra("Message", message);
                notificationIntent.putExtra("Action", action);
                notificationIntent.putExtra("Username", key1);
                notificationIntent.putExtra("Mobilenumber", key3);
                notificationIntent.putExtra("UserImage", key4);
                notificationIntent.putExtra("UserRating", key5);
                notificationIntent.putExtra("RideId", key6);
                notificationIntent.putExtra("UserPickuplocation", key7);
                notificationIntent.putExtra("UserPickupTime", key10);
                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(notificationIntent);
                //   timestamp = messageObject.getString(ServiceConstant.key5);
            }

        } else {


           if (ServiceConstant.ACTION_CHAT_RECEIVED.equalsIgnoreCase(action) || ServiceConstant.ACTION_CHAT_RECEIVEDs.equalsIgnoreCase(action))
           {
           }
            else
           {
               if (Build.VERSION.SDK_INT >= 26) {
                   final NotificationManager mNotific =
                           (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                   final int ncode = 101;
                   final String ChannelID = "my_channel_01";
                   CharSequence name = "Ragffav";
                   String desc = "this is notific";
                   int imp = NotificationManager.IMPORTANCE_HIGH;

                   @SuppressLint("WrongConstant") NotificationChannel mChannel = new NotificationChannel(ChannelID, name,
                           imp);
                   mChannel.setDescription(desc);
                   mChannel.setLightColor(Color.CYAN);
                   mChannel.canShowBadge();
                   mChannel.setShowBadge(true);
                   mNotific.createNotificationChannel(mChannel);

                   Intent intent = new Intent(getApplicationContext(), Splash.class);
                   PendingIntent pendingIntent = PendingIntent.getActivity(this, id, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
                   @SuppressLint("WrongConstant") Notification n = new Notification.Builder(this, ChannelID)
                           .setContentTitle(msg)
                           .setContentText(Job_status_message)
                           .setBadgeIconType(R.drawable.applogo)
                           .setNumber(5)
                           .setColor(getResources().getColor(R.color.app_color))
                           .setLights(0xffff0000, 100, 2000)
                           .setWhen(System.currentTimeMillis())
                           .setPriority(Notification.DEFAULT_SOUND)
                           .setContentIntent(pendingIntent)
                           .setSmallIcon(R.drawable.applogo)
                           .setAutoCancel(true)
                           .build();
                   mNotific.notify(ncode, n);


               } else {
                   PendingIntent contentIntent = PendingIntent.getActivity(MyFirebaseMessagingService.this, id, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
                   NotificationManager nm = (NotificationManager) MyFirebaseMessagingService.this.getSystemService(Context.NOTIFICATION_SERVICE);
                   Resources res = MyFirebaseMessagingService.this.getResources();
                   NotificationCompat.Builder builder = new NotificationCompat.Builder(MyFirebaseMessagingService.this);
                   builder.setContentIntent(contentIntent)
                           .setSmallIcon(R.drawable.applogo)
                           .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.applogo))
                           .setTicker(msg)
                           .setColor(getResources().getColor(R.color.app_color))
                           .setWhen(System.currentTimeMillis())
                           .setAutoCancel(true)
                           .setContentTitle(getResources().getString(R.string.app_name))
                           .setLights(0xffff0000, 100, 2000)
                           .setPriority(Notification.DEFAULT_SOUND)
                           .setContentText(msg);

                   Notification n = builder.getNotification();
                   if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                       int smallIconViewId = getResources().getIdentifier("right_icon", "id", android.R.class.getPackage().getName());

                       if (smallIconViewId != 0) {
                           if (n.contentView != null)
                               n.contentView.setViewVisibility(smallIconViewId, View.INVISIBLE);

                           if (n.headsUpContentView != null)
                               n.headsUpContentView.setViewVisibility(smallIconViewId, View.INVISIBLE);

                           if (n.bigContentView != null)
                               n.bigContentView.setViewVisibility(smallIconViewId, View.INVISIBLE);
                       }
                   }

                   n.defaults |= Notification.DEFAULT_ALL;
                   nm.notify(id, n);
               }
           }



        }


    }

    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(MyFirebaseMessagingService.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.alert_label_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    public int createID() {
        Date now = new Date();
        int id = Integer.parseInt(new SimpleDateFormat("ddHHmmss", Locale.US).format(now));
        return id;
    }

    private void postRequest_applaunch(String Url) {


        HashMap<String, String> user = Session.getUserDetails();
        driver_id = user.get(SessionManager.KEY_DRIVERID);


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_type", "driver");
        jsonParams.put("id", driver_id);
        mRequest = new ServiceRequest(MyFirebaseMessagingService.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                String server_mode = "", site_mode = "", site_string = "", site_url = "", app_identity_name = "", Language_code = "";

                String Str_status = "", sContact_mail = "", sCustomerServiceNumber = "", sSiteUrl = "", sXmppHostUrl = "", sHostName = "", sFacebookId = "", sGooglePlusId = "", sPhoneMasking = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            JSONObject info_object = response_object.getJSONObject("info");
                            if (info_object.length() > 0) {
                                sContact_mail = info_object.getString("site_contact_mail");
                                sCustomerServiceNumber = info_object.getString("customer_service_number");
                                sSiteUrl = info_object.getString("site_url");
                                sXmppHostUrl = info_object.getString("xmpp_host_url");
                                sHostName = info_object.getString("xmpp_host_name");
                              /*sFacebookId = info_object.getString("facebook_app_id");
                                sGooglePlusId = info_object.getString("google_plus_app_id");
                                sPhoneMasking = info_object.getString("phone_masking_status");*/

                                app_identity_name = info_object.getString("app_identity_name");
                                server_mode = info_object.getString("server_mode");
                                site_mode = info_object.getString("site_mode");
                                site_string = info_object.getString("site_mode_string");
                                site_url = info_object.getString("site_url");
                                /* Language_code="ta";*/
                                driver_image = info_object.getString("driver_image");
                                driverName = info_object.getString("driver_name");
                                Language_code = info_object.getString("lang_code");
                                isAppInfoAvailable = true;
                            } else {
                                isAppInfoAvailable = false;
                            }

                           /* sPendingRideId= response_object.getString("pending_rideid");
                            sRatingStatus= response_object.getString("ride_status");*/

                        } else {
                            isAppInfoAvailable = false;
                        }
                    } else {
                        isAppInfoAvailable = false;
                    }
                    if (Str_status.equalsIgnoreCase("1") && isAppInfoAvailable) {

                        HashMap<String, String> language = Session.getLanaguage();
                        Locale locale = null;

                        switch (Language_code) {

                            case "en":
                                locale = new Locale("en");
                                Session.setlamguage("en", "en");
                                //
                                //  Intent in=new Intent(ProfilePage.this,NavigationDrawer.class);
                                //   finish();
                                //  startActivity(in);

//                        Intent bi = new Intent();
//                        bi.setAction("homepage");
//                        sendBroadcast(bi);
//                        finish();

                                break;
                            case "es":
                                locale = new Locale("es");
                                Session.setlamguage("es", "es");
                                //
                                //     Intent i=new Intent(ProfilePage.this,NavigationDrawer.class);
                                //     finish();
                                //     startActivity(i);

//                        Intent bii = new Intent();
//                        bii.setAction("homepage");
//                        sendBroadcast(bii);
//                        finish();
                                break;

                            default:
                                locale = new Locale("en");
                                Session.setlamguage("en", "en");
                                break;
                        }

                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;
                        getApplicationContext().getResources().updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());

                        Session.setXmpp(sXmppHostUrl, sHostName);
                        Session.setAgent(app_identity_name);
                        Session.setdriver_image(driver_image);
                        Session.setdriverNameUpdate(driverName);


                    } else {
                        Toast.makeText(getApplicationContext(), "BAD URL", Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                Toast.makeText(getApplicationContext(), ServiceConstant.MAIN_URL, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        boolean b = false;
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                System.out.println("1 already running");
                b = true;
                break;
            } else {
                System.out.println("2 not running");
                b = false;
            }
        }
        System.out.println("3 not running");
        return b;
    }
}











