package com.hellotaxidriver.driver.adapter;

/**
 * Created by Prem Kumar on 10/1/2015.
 */

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hellotaxidriver.driver.R;
import com.hellotaxidriver.driver.Utils.RoundedImageView;
import com.hellotaxidriver.driver.Utils.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.HashMap;


/**
 * Created by Prem Kumar on 9/21/2015.
 */
public class HomeMenuListAdapter extends BaseAdapter {
    private Context context;
    private String[] mTitle;
    private int[] mIcon;
    private static final String TAG = "HomeMenuListAdapter";
    private String User_vehicle_model, User_fullname, driver_image, category_name, reviews;

    public HomeMenuListAdapter(Context context, String[] title, int[] icon) {
        this.context = context;
        this.mTitle = title;
        this.mIcon = icon;
        SessionManager session = new SessionManager(context);
        HashMap<String, String> user = session.getUserDetails();
        User_vehicle_model = session.getUserVehicle();
        User_fullname = user.get(SessionManager.KEY_DRIVER_NAME);
        driver_image = user.get(SessionManager.KEY_DRIVER_IMAGE);
        HashMap<String, String> driver_details = session.get_driverdetails();
        category_name = driver_details.get(SessionManager.KEY_CATEGORY);
        reviews = driver_details.get(SessionManager.KEY_REVIEWS);
    }

    @Override
    public int getCount() {
        return mTitle.length;
    }

    @Override
    public Object getItem(int position) {
        return mTitle[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Declare Variables


        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder holder = new ViewHolder();
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.drawer_list_item, parent, false);
            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
            holder.profile_mobile = (TextView) convertView.findViewById(R.id.profile_mobile_number);
            holder.driver_rating = (RatingBar) convertView.findViewById(R.id.driver_dashboard_ratting);
            holder.Tv_category_name = (TextView) convertView.findViewById(R.id.category_name);
            holder.Tv_car_number = (TextView) convertView.findViewById(R.id.car_number);
            holder.Tv_rating = (TextView) convertView.findViewById(R.id.rating);
            holder.imgIcon = (ImageView) convertView.findViewById(R.id.icon);
            holder.profile_name = (TextView) convertView.findViewById(R.id.profile_name);
            holder.profile_icon = (RoundedImageView) convertView.findViewById(R.id.profile_icon);
            holder.general_layout = (RelativeLayout) convertView.findViewById(R.id.drawer_list_item_normal_layout);
            holder.profile_layout = (RelativeLayout) convertView.findViewById(R.id.drawer_list_item_profile_layout);
            holder.drawer_view = (View) convertView.findViewById(R.id.drawer_list_view);
            holder.Tv_star = (TextView) convertView.findViewById(R.id.star_textview);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (position == 0) {
            holder.profile_layout.setVisibility(View.VISIBLE);
            holder.general_layout.setVisibility(View.GONE);
            holder.drawer_view.setVisibility(View.GONE);

            Picasso.with(context).load(String.valueOf(driver_image)).placeholder(R.drawable.placeholder_icon).into(holder.profile_icon);
            holder.profile_name.setText(User_fullname);
            // profile_mobile.setText(User_vehicle_model);

            holder.Tv_category_name.setText(category_name);
            holder.Tv_car_number.setText(User_vehicle_model);
            if (!reviews.equalsIgnoreCase("")) {
                holder.driver_rating.setRating(Float.parseFloat(reviews));
            }
            if (reviews.equalsIgnoreCase("")) {
                reviews="0";
            }
            holder.Tv_rating.setText("(" + reviews + "/5)");
            holder.Tv_star.setText("(" + reviews + "/5)");
        } else {

            if (position == 3) {
                holder.drawer_view.setVisibility(View.VISIBLE);
            } else {
                holder.drawer_view.setVisibility(View.GONE);
            }

            holder.profile_layout.setVisibility(View.GONE);
            holder.general_layout.setVisibility(View.VISIBLE);


            holder.imgIcon.setImageResource(mIcon[position]);
            int color = Color.parseColor("#000000"); //The color u want
            holder.imgIcon.setColorFilter(color);
            holder.txtTitle.setText(mTitle[position]);
        }
        Log.d(TAG, "getView: ");
        return convertView;
    }

    static class ViewHolder {
        TextView txtTitle, profile_name, profile_mobile, Tv_category_name, Tv_car_number, Tv_rating, Tv_star;
        RatingBar driver_rating;
        RoundedImageView profile_icon;
        ImageView imgIcon;
        RelativeLayout general_layout, profile_layout;
        View drawer_view;
    }
}

