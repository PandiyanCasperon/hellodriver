package com.hellotaxidriver.driver;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.service.Myservice;
import com.app.service.ServiceConstant;
import com.app.service.ServiceManager;
import com.hellotaxidriver.driver.Helper.GEODBHelper;
import com.hellotaxidriver.driver.Utils.GPSTracker;
import com.hellotaxidriver.driver.Utils.SessionManager;
import com.hellotaxidriver.driver.widgets.PkDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import cn.iwgang.countdownview.CountdownView;
import cn.iwgang.familiarrecyclerview.FamiliarRecyclerView;


public class newdriverrequestpage extends AppCompatActivity {
    public static String EXTRA = "EXTRA";
    public static MediaPlayer mediaPlayer;
    MyAdapter mMyAdapter;
    LinearLayoutManager horizontalLayoutManager;
    FamiliarRecyclerView cvFamiliarRecyclerView;
    GEODBHelper myDBHelper;
    Dialog dialog;
    GPSTracker gps;
    PowerManager.WakeLock screenLock;
    ServiceManager.ServiceListener acceptServicelistener = new ServiceManager.ServiceListener() {

        @Override
        public void onCompleteListener(String object) {
            String Sstatus = "", SResponse = "", Sride_Type = "";

            if (object instanceof String) {

                String jsonString = (String) object;
                try {
                    JSONObject object1 = new JSONObject(jsonString);
                    Sstatus = object1.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        dismissDialog();
                        JSONObject jobject = object1.getJSONObject("response");
                        JSONObject jobject2 = jobject.getJSONObject("user_profile");
                        String userID = jobject2.getString("user_id");

                        myDBHelper.insertuser_id(userID);
                    }
                    if (Sstatus.equalsIgnoreCase("0")) {
                        SResponse = object1.getString("response");
                    }
                    if (Sstatus.equalsIgnoreCase("1")) {


                        SessionManager session = new SessionManager(getApplicationContext());
                        session.setRequestCount(0);
                        sessionManager.setupcomming_backoption("0");
                        session.setmove_upcomming("0");
                        stopPlayer();
                        Intent broadcastIntent_trip = new Intent();
                        broadcastIntent_trip.setAction("com.finish.tripPage");
                        sendBroadcast(broadcastIntent_trip);

                        Intent trip_intent = new Intent(getApplicationContext(), TripPage.class);
                        trip_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(trip_intent);
                        finish();


                    } else {
                        dismissDialog();
                        if (Sstatus.equalsIgnoreCase("0")) {
                            SessionManager session = new SessionManager(getApplicationContext());
                            session.setRequestCount(0);
                            stopPlayer();
                            sessionManager.setupcomming_backoption("0");

                            Intent broadcastIntent_trip = new Intent();
                            broadcastIntent_trip.setAction("com.finish.tripPage");
                            sendBroadcast(broadcastIntent_trip);

                            Alert(getString(R.string.alert_sorry_label_title), SResponse);

                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    dismissDialog();
                    e.printStackTrace();
                } catch (Exception e) {
                    dismissDialog();
                    e.printStackTrace();
                }


            }
        }

        @Override
        public void onErrorListener(Object error) {
            dismissDialog();

        }
    };
    ServiceManager.ServiceListener passServicelistener = new ServiceManager.ServiceListener() {

        @Override
        public void onCompleteListener(String object) {
            String Sstatus = "", SResponse = "";

            if (object instanceof String) {

                String jsonString = (String) object;
                try {
                    JSONObject object1 = new JSONObject(jsonString);
                    Sstatus = object1.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        dismissDialog();
                        sessionManager.setupcomming_backoption("0");
                        sessionManager.setmove_upcomming("0");
                        /*mDataList.remove(postion);
                        mMyAdapter.notifyDataSetChanged();
                        if(mDataList.size()==0) {
                            if (mediaPlayer != null) {
                                if (media Player.isPlaying()) {
                                    mediaPlayer.stop();
                                }
                            }
                        }*/

                        Intent intss=new Intent(newdriverrequestpage.this,NavigationDrawerNew.class);
                        startActivity(intss);
                        laststack();
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    dismissDialog();
                    e.printStackTrace();
                } catch (Exception e) {
                    dismissDialog();
                    e.printStackTrace();
                }


            }
        }

        @Override
        public void onErrorListener(Object error) {
            dismissDialog();

        }
    };
    private String estimationprice = "";
    private List<ItemInfo> mDataList;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.newdriverrequestmainpage);

        gps = new GPSTracker(getApplicationContext());

        myDBHelper = new GEODBHelper(getApplicationContext());
        sessionManager = new SessionManager(getApplicationContext());

        sessionManager.setfirsttimeloader("0");

        int MAX_VOLUME = 100;
        final float volume = (float) (1 - (Math.log(MAX_VOLUME - 80) / Math.log(MAX_VOLUME)));
        mediaPlayer = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI);
//        mediaPlayer = MediaPlayer.create(this, R.raw.emergency);

        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 20, 0);

       /* if (isMyServiceRunning(FloatingWidgetService.class) || isMyServiceRunning(FloatingWidgetService.class)) {
            stopfloatingservice();
        } else {

        }*/

        screenLock = ((PowerManager) getSystemService(POWER_SERVICE)).newWakeLock(
                PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
        screenLock.acquire();
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Activity.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE);
        lock.disableKeyguard();

        try {
            mediaPlayer.setVolume(volume, volume);
        } catch (NullPointerException e) {

        } catch (Exception e) {

        }

        if (mediaPlayer != null) {
            if (!mediaPlayer.isPlaying()) {
                mediaPlayer.start();
                mediaPlayer.setLooping(true);
            }
        }


        cvFamiliarRecyclerView = (FamiliarRecyclerView) findViewById(R.id.cv_familiarRecyclerView);
        mDataList = new ArrayList<>();
        Intent intent = getIntent();
        if (intent != null) {
            callcommonintent(intent);
        }
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            callcommonintent(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (null != mMyAdapter) {
            mMyAdapter.startRefreshTime();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (null != mMyAdapter) {
            mMyAdapter.cancelRefreshTime();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                newdriverrequestpage.mediaPlayer.stop();
                newdriverrequestpage.mediaPlayer.release();
                newdriverrequestpage.mediaPlayer =null;
            }
        }


        if (null != mMyAdapter) {
            mMyAdapter.cancelRefreshTime();
        }
    }

    private void callcommonintent(Intent intent) {

        if (intent != null) {
            Bundle extra = intent.getExtras();
            if (extra != null) {
                String data = (String) extra.get(EXTRA);
                try {
                    if (data != null) {
                        String decodeData = URLDecoder.decode(data, "UTF-8");
                        System.out.println("decode" + decodeData);
                        JSONObject dataObject = new JSONObject(decodeData);
                        String pickupaddress = "", dropaddress = "", rideidstore = "", pickupdistancedricer = "", triptype = "", timetoplay = "50";
                        if (dataObject.has("key3")) {
                            pickupaddress = dataObject.getString("key3");
                        }
                        if (dataObject.has("key4")) {
                            dropaddress = dataObject.getString("key4");
                        }
                        if (dataObject.has("key8")) {
                            pickupdistancedricer = dataObject.getString("key8");
                        }
                        if (dataObject.has("key8")) {
                            pickupdistancedricer = dataObject.getString("key8");
                        }
                        if (dataObject.has("key1")) {
                            rideidstore = dataObject.getString("key1");
                        }
                        if (dataObject.has("key5")) {
                            triptype = dataObject.getString("key5");
                        }
                        if (dataObject.has("key6")) {
                            estimationprice = dataObject.getString("key6");
                            sessionManager.set_ack_id(estimationprice);

                            intent_service();
                        }
                        if (dataObject.has("key2")) {
                            timetoplay = dataObject.getString("key2");
                        }

                        int timeto = 30;
                        try {
                            timeto = (Integer.parseInt(timetoplay));
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }

                        int dontadd = 0;
                        if (pickupaddress.length() > 4) {
                            for (int jk = 0; jk < mDataList.size(); jk++) {
                                String rideidd = "" + mDataList.get(jk).getId();
                                if (rideidd.equals(rideidstore)) {
                                    dontadd++;
                                }

                            }

                            if (dontadd == 0) {
                                /*Snackbar snackbar = Snackbar
                                        .make(cvFamiliarRecyclerView, getResources().getString(R.string.alert_tape_toaccept), Snackbar.LENGTH_LONG);
                                TextView snackbarActionTextView = (TextView) snackbar.getView().findViewById( android.support.design.R.id.snackbar_action );
                                snackbarActionTextView.setTextSize( 20 );
                                Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Roboto-Medium.ttf");
                                snackbarActionTextView.setTypeface(tf, Typeface.BOLD);
                                snackbar.show();*/

                                long curTime = System.currentTimeMillis();
                                mDataList.add(new ItemInfo(rideidstore, pickupaddress, dropaddress, triptype, estimationprice, timeto * 1000, (curTime + (timeto * 1000)), timeto));


                                if (mDataList.size() == 1) {
                                    horizontalLayoutManager = new LinearLayoutManager(newdriverrequestpage.this, LinearLayoutManager.HORIZONTAL, false);
                                    cvFamiliarRecyclerView.setLayoutManager(horizontalLayoutManager);
                                    cvFamiliarRecyclerView.setAdapter(mMyAdapter = new MyAdapter(this, mDataList));
                                } else {
                                    if (mMyAdapter != null) {
                                        mMyAdapter.notifyDataSetChanged();

                                    }
                                }


                            } else {
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    private void intent_service() {
        Intent intent_service = new Intent(getApplicationContext(), Myservice.class);
        intent_service.putExtra("Ack_id", estimationprice);
        intent_service.putExtra("ride_type", "accept");
        startService(intent_service);
    }

    public void stopPlayer() {
        try {
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                newdriverrequestpage.mediaPlayer.stop();
                newdriverrequestpage.mediaPlayer.release();
                newdriverrequestpage.mediaPlayer =null;            }
        } catch (Exception e) {
        }
    }


    public void showDialog() {
        /*dialog = new Dialog(newdriverrequestpage.this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();*/
        dialog = new Dialog(newdriverrequestpage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void dismissDialog() {
        try {
            if (dialog != null)
                dialog.dismiss();
        } catch (Exception e) {
        }
    }

    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(newdriverrequestpage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getString(R.string.alert_label_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                finish();
            }
        });
        mDialog.show();
    }

    private void laststack() {
        int returuncount = 0;
        ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);
        returuncount = taskList.get(0).numActivities;
        if (returuncount == 1) {
            Intent intent = new Intent(getApplicationContext(), Splash.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        } else {
            finish();
        }

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        boolean b = false;
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                System.out.println("1 already running");
                b = true;
                break;
            } else {
                System.out.println("2 not running");
                b = false;
            }
        }
        System.out.println("3 not running");
        return b;
    }

    private void stopfloatingservice() {
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent startIntent = new Intent(newdriverrequestpage.this, FloatingWidgetService.class);
            startIntent.putExtra("Stop","yes");
            startService(startIntent);
        } else {
            stopService(new Intent(newdriverrequestpage.this, FloatingWidgetService.class));
        }*/
    }

    static class ItemInfo {
        private String id;
        private String title;
        private long countdown;
        private long endTime;
        private String dropaddress;
        private String triptype;
        private String estimationprice;

        private int maxtime;

        public ItemInfo(String id, String title, String dropaddress, String triptype, String estimationprice, long countdown, long endTime, int maxtime) {
            this.id = id;
            this.title = title;
            this.dropaddress = dropaddress;
            this.triptype = triptype;
            this.estimationprice = estimationprice;
            this.countdown = countdown;
            this.endTime = endTime;
            this.maxtime = maxtime;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getMaxTime() {
            return maxtime;
        }

        public void setMaxTime(int maxtime) {
            this.maxtime = maxtime;
        }

        public String getdropaddress() {
            return dropaddress;
        }

        public void setdropaddress(String dropaddress) {
            this.dropaddress = dropaddress;
        }

        public String getTriptype() {
            return triptype;
        }

        public void setTriptype(String triptype) {
            this.triptype = triptype;
        }

        public String getEstimationprice() {
            return estimationprice;
        }

        public void setEstimationprice(String estimationprice) {
            this.estimationprice = estimationprice;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public long getCountdown() {
            return countdown;
        }

        public void setCountdown(long countdown) {
            this.countdown = countdown;
        }

        public long getEndTime() {
            return endTime;
        }

        public void setEndTime(long endTime) {
            this.endTime = endTime;
        }
    }

    class MyAdapter extends RecyclerView.Adapter<MyViewHolder> {
        private final SparseArray<MyViewHolder> mCountdownVHList;
        private Context mContext;
        private List<ItemInfo> mDatas;
        private Handler mHandler = new Handler();
        private Timer mTimer;
        private boolean isCancel = true;
        private Runnable mRefreshTimeRunnable = new Runnable() {
            @Override
            public void run() {
                if (mCountdownVHList.size() == 0) return;

                synchronized (mCountdownVHList) {
                    long currentTime = System.currentTimeMillis();
                    int key;
                    for (int i = 0; i < mCountdownVHList.size(); i++) {
                        key = mCountdownVHList.keyAt(i);
                        MyViewHolder curMyViewHolder = mCountdownVHList.get(key);
                        if (currentTime >= curMyViewHolder.getBean().getEndTime()) {
                            curMyViewHolder.getBean().setCountdown(0);
                            String removedid = "" + curMyViewHolder.getBean().getId();
                            mCountdownVHList.remove(key);
                            int removepostion = 0;
                            for (int jk = 0; jk < mDatas.size(); jk++) {
                                String id = "" + mDatas.get(jk).getId();
                                if (id.equals(removedid)) {
                                    removepostion = jk;
                                }
                            }
                            mDatas.remove(removepostion);
                            notifyDataSetChanged();


                            if (mDatas.size() == 0) {
                                if (mediaPlayer != null) {
                                    if (mediaPlayer.isPlaying()) {
                                        newdriverrequestpage.mediaPlayer.stop();
                                        newdriverrequestpage.mediaPlayer.release();
                                        newdriverrequestpage.mediaPlayer =null;                                    }
                                }
                                laststack();


                            }

                        } else {
                            curMyViewHolder.refreshTime(currentTime);
                        }

                    }
                }
            }
        };

        public MyAdapter(Context context, List<ItemInfo> datas) {
            this.mContext = context;
            this.mDatas = datas;
            mCountdownVHList = new SparseArray<>();
            startRefreshTime();
        }

        public void startRefreshTime() {
            if (!isCancel) return;

            if (null != mTimer) {
                mTimer.cancel();
            }

            isCancel = false;
            mTimer = new Timer();
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    mHandler.post(mRefreshTimeRunnable);
                }
            }, 0, 10);
        }

        public void cancelRefreshTime() {
            isCancel = true;
            if (null != mTimer) {
                mTimer.cancel();
            }
            mHandler.removeCallbacks(mRefreshTimeRunnable);
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.requestpagelayout, parent, false));
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            ItemInfo curItemInfo = mDatas.get(position);
            holder.bindData(curItemInfo, position, mContext);

            if (curItemInfo.getCountdown() > 0) {
                synchronized (mCountdownVHList) {
                    mCountdownVHList.put(Integer.parseInt(curItemInfo.getId()), holder);
                }
            }
        }

        @Override
        public int getItemCount() {
            return mDatas.size();
        }

        @Override
        public void onViewRecycled(MyViewHolder holder) {
            super.onViewRecycled(holder);

            ItemInfo curAnnounceGoodsInfo = holder.getBean();
            if (null != curAnnounceGoodsInfo && curAnnounceGoodsInfo.getCountdown() > 0) {
                mCountdownVHList.remove(Integer.parseInt(curAnnounceGoodsInfo.getId()));
            }
        }
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout hidelandmark, dropaddress_layout, estimation_layout;
        private TextView pickup_address, drop_address, trip_type, estimation_price, ctstart, cabily_driver_alert_accept_btn, cabily_driver_alert_reject_btn;
        private CountdownView mCvCountdownView;
        private ItemInfo mItemInfo;

        ProgressBar progressBar;

        public MyViewHolder(View itemView) {
            super(itemView);

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int width = displayMetrics.widthPixels;
            itemView.setLayoutParams(new RecyclerView.LayoutParams((width - 60), RecyclerView.LayoutParams.WRAP_CONTENT));
            pickup_address = (TextView) itemView.findViewById(R.id.pickup_address);
            drop_address = (TextView) itemView.findViewById(R.id.drop_address);
            trip_type = (TextView) itemView.findViewById(R.id.trip_type);
            estimation_price = (TextView) itemView.findViewById(R.id.estimation_price);
            mCvCountdownView = (CountdownView) itemView.findViewById(R.id.cv_countdownView);
            hidelandmark = (LinearLayout) itemView.findViewById(R.id.hidelandmark);
            ctstart = (TextView) itemView.findViewById(R.id.ctstart);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
            dropaddress_layout = (LinearLayout) itemView.findViewById(R.id.dropaddress_layout);
            estimation_layout = (LinearLayout) itemView.findViewById(R.id.estimation_layout);
            cabily_driver_alert_accept_btn = (TextView) itemView.findViewById(R.id.cabily_driver_alert_accept_btn);
            cabily_driver_alert_reject_btn = (TextView) itemView.findViewById(R.id.cabily_driver_alert_reject_btn);
        }

        public void bindData(ItemInfo itemInfo, final int postion, Context ctx) {
            mItemInfo = itemInfo;
            if (itemInfo.getCountdown() > 0) {
                refreshTime(System.currentTimeMillis());
            } else {
                mCvCountdownView.allShowZero();
            }
            pickup_address.setText(itemInfo.getTitle());
            drop_address.setText(itemInfo.getdropaddress());

            progressBar.setMax(itemInfo.getMaxTime());


            cabily_driver_alert_accept_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mediaPlayer != null) {
                        if (mediaPlayer.isPlaying()) {
                            newdriverrequestpage.mediaPlayer.stop();
                            newdriverrequestpage.mediaPlayer.release();
                            newdriverrequestpage.mediaPlayer =null;
                        }
                    }

                    SessionManager session = new SessionManager(getApplicationContext());
                   /* String savedlat= String.valueOf(session.getLatitude());
                    String savedlongg= String.valueOf(session.getLongitude());*/
                    String savedlat = String.valueOf(gps.getLatitude());
                    String savedlongg = String.valueOf(gps.getLongitude());
                    if (savedlat != null && !savedlat.equals("0.0") && !savedlat.equals("") && !savedlat.equals(" ")) {

                        showDialog();
                        String rideid = "" + mDataList.get(postion).getId();
                        stopPlayer();

                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        HashMap<String, String> userDetails = session.getUserDetails();
                        String driverId = userDetails.get(SessionManager.KEY_DRIVERID);


                        try {
                            myDBHelper.insertRide_id(rideid);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (savedlat != null) {
                            jsonParams.put("ride_id", "" + rideid);
                            jsonParams.put("driver_id", "" + driverId);
                            jsonParams.put("driver_lat", "" + savedlat);
                            jsonParams.put("driver_lon", "" + savedlongg);
                            jsonParams.put("distance", "0");
                            jsonParams.put("ack_id", estimationprice);
                        }
                        try {
                            myDBHelper.Delete("");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        ServiceManager manager = new ServiceManager(getApplicationContext(), acceptServicelistener);
                        manager.makeServiceRequest(ServiceConstant.ACCEPTING_RIDE_REQUEST, Request.Method.POST, jsonParams);


                     /*   Intent intent_service = new Intent(getApplicationContext(), Myservice.class);
                        intent_service.putExtra("Ack_id", estimationprice);
                        intent_service.putExtra("ride_type", "accept");
                        startService(intent_service);
*/

                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.app_name), Toast.LENGTH_SHORT).show();
                    }


                }
            });


            cabily_driver_alert_reject_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mediaPlayer != null) {
                        if (mediaPlayer.isPlaying()) {
                            newdriverrequestpage.mediaPlayer.stop();
                            newdriverrequestpage.mediaPlayer.release();
                            newdriverrequestpage.mediaPlayer =null;
                        }
                    }


                    SessionManager session = new SessionManager(getApplicationContext());
                    String savedlat = String.valueOf(session.getLatitude());
                    String savedlongg = String.valueOf(session.getLongitude());

                    showDialog();
                    String rideid = "" + mDataList.get(postion).getId();
                    stopPlayer();

                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    HashMap<String, String> userDetails = session.getUserDetails();
                    String driverId = userDetails.get(SessionManager.KEY_DRIVERID);


                    try {
                        myDBHelper.insertRide_id(rideid);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (savedlat != null) {
                        jsonParams.put("ride_id", "" + rideid);
                        jsonParams.put("driver_id", "" + driverId);
                        jsonParams.put("driver_lat", "" + savedlat);
                        jsonParams.put("driver_lon", "" + savedlongg);
                        jsonParams.put("distance", "0");
                        jsonParams.put("ack_id", estimationprice);
                    }
                    try {
                        myDBHelper.Delete("");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    ServiceManager manager = new ServiceManager(newdriverrequestpage.this, passServicelistener);
                    manager.makeServiceRequest(ServiceConstant.PASS_RIDE_REQUEST, Request.Method.POST, jsonParams);
//                        laststack();
                    Intent intent_service = new Intent(getApplicationContext(), Myservice.class);
                    intent_service.putExtra("Ack_id", estimationprice);
                    intent_service.putExtra("ride_type", "decline");
                    startService(intent_service);

                }
            });


            trip_type.setText(itemInfo.getTriptype());
            if (itemInfo.getdropaddress().length() > 3) {
                estimation_price.setText("₹ " + itemInfo.getEstimationprice());

                dropaddress_layout.setVisibility(View.GONE);
                estimation_layout.setVisibility(View.GONE);
            } else {
                dropaddress_layout.setVisibility(View.GONE);
                estimation_layout.setVisibility(View.GONE);
            }

        }

        public void refreshTime(long curTimeMillis) {
            if (null == mItemInfo || mItemInfo.getCountdown() <= 0) return;
            mCvCountdownView.updateShow(mItemInfo.getEndTime() - curTimeMillis);
            ctstart.setText("" + ((mItemInfo.getEndTime() - curTimeMillis) / 1000));
            int timec = Integer.parseInt("" + ((mItemInfo.getEndTime() - curTimeMillis) / 1000));
            progressBar.setProgress(timec);

        }

        public ItemInfo getBean() {
            return mItemInfo;
        }
    }
}
