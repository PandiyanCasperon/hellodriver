package com.hellotaxidriver.driver;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.app.service.ServiceConstant;
import com.hellotaxidriver.driver.Utils.SessionManager;

/**
 */
public class AboutUs extends Fragment
{
    private TextView Tv_more_info,version_code,Tv_About_us_content,tv_customer_phone,tv_customer_address,tv_terms_condition;

    private String currentVersion;
    SessionManager session;

    private String customerPhoneNo = "";
    final int PERMISSION_REQUEST_CODE = 111;

    private static View rootview;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        try {
            if (rootview == null) {
                rootview = inflater.inflate(R.layout.aboutus, container, false);
            } else {
                ViewGroup parent = (ViewGroup) rootview.getParent();
                if (parent != null)
                    parent.removeView(rootview);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
     /*   if (rootview != null) {
            ViewGroup parent = (ViewGroup) rootview.getParent();
            if (parent != null)
                parent.removeView(rootview);
        }
        try {
            rootview = inflater.inflate(R.layout.aboutus, container, false);
        } catch (InflateException e) {
        }*/
        init(rootview);
        ((NavigationDrawerNew) getActivity()).disableSwipeDrawer();
      /*  NavigationDrawerNew navigationDrawerNew=new NavigationDrawerNew();
        navigationDrawerNew.disableSwipeDrawer();*/
//        NavigationDrawerNew.disableSwipeDrawer();


        rootview.findViewById(R.id.ham_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                if (imm.isAcceptingText()) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                ((NavigationDrawerNew) getActivity()).openDrawer();
           /*     NavigationDrawerNew navigationDrawerNew = new NavigationDrawerNew();
                navigationDrawerNew.openDrawer();*/
//                NavigationDrawerNew.openDrawer();

            }
        });


        //   setUpViews();
        return rootview;

    }




    private void init(View rootview){
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN |
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_FULLSCREEN |
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        //  ChatService.startUserAction(AboutUs.this);
        Tv_more_info = (TextView) rootview.findViewById(R.id.more_info_baseurl);
        Tv_About_us_content = (TextView) rootview.findViewById(R.id.textView);
        version_code = (TextView) rootview.findViewById(R.id.aboutus_versioncode);
        tv_customer_phone = (TextView) rootview.findViewById(R.id.more_phone_baseurl);
        tv_customer_address = (TextView) rootview.findViewById(R.id.more_address_baseurl);
        tv_terms_condition = (TextView) rootview.findViewById(R.id.aboutus_terms_condition);

        session = new SessionManager(getActivity());
        try {
            currentVersion = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        version_code.setText(getResources().getString(R.string.aboutus_lable_version_textview) + " " + currentVersion);



        String second_text = "<font color='#012D8E'>" + getResources().getString(R.string.login_label_Terms_of_service) + " " + "</font>";
        String tired_text = "<font color='#5b5b5b'>" + getResources().getString(R.string.login_label_and) + " " + "</font>";
        String fourth_text = "<font color='#012D8E'>" + getResources().getString(R.string.login_label_Privacy_Policy) + "</font>";
        tv_terms_condition.setText(Html.fromHtml( second_text + tired_text + fourth_text));

        ClickableSpan termsOfServicesClick = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ServiceConstant.terms_of_servie_URL));
                startActivity(browserIntent);

            }
        };

        ClickableSpan privacyPolicyClick = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ServiceConstant.privacy_policy_URL));
                startActivity(browserIntent);

            }
        };


        makeLinks(tv_terms_condition, new String[]{getResources().getString(R.string.login_label_Terms_of_service), getResources().getString(R.string.login_label_Privacy_Policy)}, new ClickableSpan[]{
                termsOfServicesClick, privacyPolicyClick
        });

    }


    public void makeLinks(TextView textView, String[] links, ClickableSpan[] clickableSpans) {
        SpannableString spannableString = new SpannableString(textView.getText());
        for (int i = 0; i < links.length; i++) {
            ClickableSpan clickableSpan = clickableSpans[i];
            String link = links[i];

            int startIndexOfLink = textView.getText().toString().indexOf(link);

            spannableString.setSpan(clickableSpan, startIndexOfLink, startIndexOfLink + link.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.app_color)), startIndexOfLink, startIndexOfLink + link.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spannableString, TextView.BufferType.SPANNABLE);
    }


}


