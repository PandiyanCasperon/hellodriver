package com.hellotaxidriver.driver;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.service.ServiceConstant;
import com.app.service.ServiceRequest;
import com.app.service.UpdateLocationService;
import com.hellotaxidriver.driver.Utils.ConnectionDetector;
import com.hellotaxidriver.driver.Utils.Extras;
import com.hellotaxidriver.driver.Utils.GPSTracker;
import com.hellotaxidriver.driver.Utils.Locationservices;
import com.hellotaxidriver.driver.Utils.PkDialogWithoutButton;
import com.hellotaxidriver.driver.Utils.SessionManager;
import com.hellotaxidriver.driver.widgets.PkDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;

/**
 * Created by user88 on 6/3/2016.
 */
public class Splash extends Activity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final int REQUEST_LOCATION_CODE = 102;
    private static final int REQUEST_LOCATION_MANUEL = 105;
    private final static int REQUEST_LOCATION = 199;
    public static String PHONEMASKINGSTATUS = "";
    Context context;
    PkDialogWithoutButton mInfoDialog;
    ImageView ivSplashImage;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    private boolean isAppInfoAvailable = false;
    private SessionManager session;
    private String driver_id = "", currentVersion = "", sLatitude = "", sLongitude = "";
    private String server_mode, site_mode, site_string, site_url, app_identity_name = "", Language_code = "", driver_image = "", driverName = "";
    private GPSTracker gps;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private PendingResult<LocationSettingsResult> result;
    private String pushtype = "";
    private String onlineVersion = "";
    private Extras extras;
    public static SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        sessionManager = new SessionManager(getApplicationContext());
        sessionManager.setfirsttimeloader("1");
        sessionManager.setmove_upcomming("0");
//        ivSplashImage = findViewById(R.id.ivSplashImg);

     //   Glide.with(Splash.this).load(R.mipmap.splash).listener(new RequestListener<Drawable>() {

         //   @Override
         //   public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
          //      initialization();
            //    return false;
         //   }

       //     @Override
        //    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, com.bumptech.glide.load.DataSource dataSource, boolean isFirstResource) {
        //        ((GifDrawable) resource).setLoopCount(1);
                initialization();


           //     return false;
          //  }

       // }).into(ivSplashImage);

    }

    private void AlertPermission() {


          final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle("Alert Permission");
        builder.setMessage("this app collects location data to share my current location with user even when the app is closed or not in use.Click enable to allow location permission cancel to close..");
        builder.setPositiveButton("Enable",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        requestAppPermissions();
                        // DO TASK
                    }
                });
        builder.setNegativeButton("Close",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                        // DO TASK
                    }
                });

        builder.show();

    }






    private void apiHit() {



        if (!sLatitude.equalsIgnoreCase("") && !sLongitude.equalsIgnoreCase("")) {

            if (isInternetPresent) {
                startService(new Intent(Splash.this, Locationservices.class));
                postRequest_applaunch(ServiceConstant.app_launching_url);

            } else {
                Alert(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));
            }
        } else {
            if (gps.isgpsenabled() && gps.canGetLocation()) {
                gps = new GPSTracker(getApplicationContext());
                sLatitude = String.valueOf(gps.getLatitude());
                sLongitude = String.valueOf(gps.getLongitude());
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        apiHit();
                    }
                }, 500);

            } else {
                enableGpsService();
            }

        }

    }

    private void initialization() {
        context = getApplicationContext();
        cd = new ConnectionDetector(Splash.this);
        isInternetPresent = cd.isConnectingToInternet();
        session = new SessionManager(Splash.this);
        gps = new GPSTracker(getApplicationContext());
        extras = new Extras(getApplicationContext());
        cd = new ConnectionDetector(Splash.this);
        isInternetPresent = cd.isConnectingToInternet();




        mGoogleApiClient = new GoogleApiClient.Builder(Splash.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();


        HashMap<String, String> user = session.getUserDetails();
        driver_id = user.get(SessionManager.KEY_DRIVERID);

        try {
            currentVersion = Splash.this.getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        Intent i = getIntent();
        if (i != null) {
            if (i.hasExtra("ad")) {
                String type = i.getStringExtra("ad");
                pushtype = i.getStringExtra("push");
                if (type.equalsIgnoreCase("true")) {
                    String title = i.getStringExtra("title");
                    String msg = i.getStringExtra("msg");
                    String banner = i.getStringExtra("banner");
                    session.setADS(true);
                    session.setAds(title, msg, banner);
                    System.out.println("-ads jai-------title" + title);
                }
                if (pushtype.equalsIgnoreCase("true")) {
                    String data = i.getStringExtra("data");
                    session.setNotificationStatus(ServiceConstant.ACTION_TAG_RIDE_REQUEST);
                    session.setDriverAlertData(data.toString());
                }
            }


        }

        if (Build.VERSION.SDK_INT >= 21) {
            if (!extras.checkAccessFineLocationPermission() || !extras.checkAccessCoarseLocationPermission()) {

                AlertPermission();

            } else {
                apiHit();
            }
        }


    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void AlertPlayStore(String title, String alert) {
        try {
            final PkDialog mDialog = new PkDialog(Splash.this);
            mDialog.setDialogTitle(title);
            mDialog.setDialogMessage(alert);
            mDialog.setPositiveButton(getResources().getString(R.string.alert_label_ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    finish();
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }
            });
            mDialog.setNegativeButton(getResources().getString(R.string.lbel_begintrip_label_cancel), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    finish();
                }
            });
            mDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(Splash.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.alert_label_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //Enabling Gps Service
    private void enableGpsService() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            status.startResolutionForResult(Splash.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                session = new SessionManager(getApplicationContext());
                                gps = new GPSTracker(Splash.this);

                                HashMap<String, String> user = session.getUserDetails();
                                driver_id = user.get(SessionManager.KEY_DRIVERID);

                                sLatitude = String.valueOf(gps.getLatitude());
                                sLongitude = String.valueOf(gps.getLongitude());
                                apiHit();

                            }
                        }, 2000);
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
//                        finish();
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;

            case REQUEST_LOCATION_MANUEL:

                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {


                   //AlertPermission();
                    LocationPermissionManule();


                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            session = new SessionManager(getApplicationContext());
                            gps = new GPSTracker(Splash.this);

                            HashMap<String, String> user = session.getUserDetails();
                            driver_id = user.get(SessionManager.KEY_DRIVERID);

                            sLatitude = String.valueOf(gps.getLatitude());
                            sLongitude = String.valueOf(gps.getLongitude());
                            apiHit();

                        }
                    }, 2000);
                }


                break;
        }
    }
/*

    private void AlertPermission() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Alert Permissions in Settings");
        alertDialogBuilder.setMessage("" +
                "\nClick SETTINGS to Manually Set\n" + "this app collects location data to share my current location with user even when the app is closed or not in use.Click enable to allow location permission cancel to close.")
                .setCancelable(false)
                .setPositiveButton("SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_LOCATION_MANUEL);     // Comment 3.
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

      */
/*  AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle("Alert dialog title");
        builder.setMessage("this app collects location data to share my current location with user even when the app is closed or not in use.Click enable to allow location permission cancel to close..");
        builder.setPositiveButton("PositiveButton",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        // DO TASK
                    }
                });
        builder.setNegativeButton("NegativeButton",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        // DO TASK
                    }
                });*//*

    }
*/





     /*   AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Alert Permissions in Settings");
        alertDialogBuilder.setMessage("" +
                        "\nClick SETTINGS to Manually Set\n" + "this app collects location data to share my current location with user even when the app is closed or not in use.Click enable to allow location permission cancel to close.")
                .setCancelable(false)
                .setPositiveButton("SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_LOCATION_MANUEL);     // Comment 3.
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }*/





    @Override
    protected void onResume() {
        super.onResume();

        Intent alarmIntent = new Intent(this, UpdateLocationService.class);
        alarmIntent.putExtra("Mode", "available");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 60000, pendingIntent);


    }

    //-----------------------App Information Post Request-----------------
    private void postRequest_applaunch(String Url) {

        System.out.println("-------------Splash App Information Url----------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_type", "driver");
        jsonParams.put("id", driver_id);
        jsonParams.put("latitude", sLatitude);
        jsonParams.put("longitude", sLongitude);
        System.out.println("-------------Splash App Information Url----------------" + jsonParams);
        mRequest = new ServiceRequest(Splash.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Splash App Information Response----------------" + response);

                String Str_status = "", user_img = "", completed_rides_str = "", cancelled_rides_str = "", About_Content = "", customer_service_number = "", customer_service_address = "", sCustomerServiceNumber = "", sSiteUrl = "", sXmppHostUrl = "", sHostName = "", sFacebookId = "", sGooglePlusId = "", sPhoneMasking = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            JSONObject info_object = response_object.getJSONObject("info");
                            if (info_object.length() > 0) {

                                sCustomerServiceNumber = info_object.getString("customer_service_number");
                                sSiteUrl = info_object.getString("site_url");
                                sXmppHostUrl = info_object.getString("xmpp_host_url");
                                sHostName = info_object.getString("xmpp_host_name");
                                app_identity_name = info_object.getString("app_identity_name");
                                server_mode = info_object.getString("server_mode");
                                user_img = info_object.getString("user_image");
                                site_mode = info_object.getString("site_mode");
                                site_string = info_object.getString("site_mode_string");
                                site_url = info_object.getString("site_url");
                                About_Content = info_object.getString("about_content");
                                driver_image = info_object.getString("driver_image");
                                driverName = info_object.getString("driver_name");
                                Language_code = info_object.getString("lang_code");
                                onlineVersion = info_object.getString("driver_version_control");
                                if (info_object.has("completed")) {
                                    completed_rides_str = info_object.getString("completed");
                                }
                                if (info_object.has("cancelled")) {
                                    cancelled_rides_str = info_object.getString("cancelled");
                                }


                                if (!completed_rides_str.isEmpty() && !cancelled_rides_str.isEmpty()) {
                                    int trips_all = Integer.parseInt(completed_rides_str + cancelled_rides_str);
                                    session.setTripsAll(trips_all);
                                }


                                if (info_object.has("customer_service_number")) {
                                    customer_service_number = info_object.getString("customer_service_number");
                                }
                                if (info_object.has("site_contact_address")) {
                                    customer_service_address = info_object.getString("site_contact_address");
                                }
                                if (info_object.has("phone_masking_status")) {
                                    String phoneMaskingStatus = info_object.getString("phone_masking_status");
                                    PHONEMASKINGSTATUS = phoneMaskingStatus;
                                    // set phone masking
                                    session.setKeyPhoneMaskingStatus(PHONEMASKINGSTATUS);
                                    System.out.println("=====>>>===PHONEMASKINGSTATUS ==========>>>>> " + PHONEMASKINGSTATUS);
                                }
                                isAppInfoAvailable = true;
                            } else {
                                isAppInfoAvailable = false;
                            }

                        } else {
                            isAppInfoAvailable = false;
                        }
                    } else {
                        isAppInfoAvailable = false;
                    }
/*
                    if (!onlineVersion.equals("")) {
                        if (onlineVersion != null && !onlineVersion.isEmpty()) {
                            if (Float.valueOf(currentVersion) < Float.valueOf(onlineVersion)) {
                                if (Splash.this != null && !Splash.this.isFinishing()) {

                                    AlertPlayStore(getResources().getString(R.string.app_name), "There is newer version of this application available, click OK to upgrade now?");
                                }
                            } else {
                                if (Str_status.equalsIgnoreCase("1") && isAppInfoAvailable) {

                                    HashMap<String, String> language = session.getLanaguage();
                                    Locale locale = null;

                                    switch (Language_code) {

                                        case "en":
                                            locale = new Locale("en");
                                            session.setlamguage("en", "en");

                                            break;
                                        case "es":
                                            locale = new Locale("es");
                                            session.setlamguage("es", "es");
                                            break;

                                        case "ta":
                                            locale = new Locale("ta");
                                            session.setlamguage("ta", "ta");
                                            break;

                                        default:
                                            locale = new Locale("en");
                                            session.setlamguage("en", "en");
                                            break;
                                    }

                                    Locale.setDefault(locale);
                                    Configuration config = new Configuration();
                                    config.locale = locale;
                                    getApplicationContext().getResources().updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());
                                    session.setDriver_image(driver_image);
                                    session.setcustomerdetail(customer_service_number, customer_service_address);
                                    session.setXmpp(sXmppHostUrl, sHostName);
                                    session.setaboutus(About_Content, sSiteUrl);
                                    session.setAgent(app_identity_name);
                                    session.setdriver_image(driver_image);
                                    session.setdriverNameUpdate(driverName);

                                    if (server_mode.equalsIgnoreCase("0")) {
                                        Toast.makeText(context, site_url, Toast.LENGTH_SHORT).show();
                                    }

                                    if (gps.isgpsenabled() && gps.canGetLocation()) {
                                        sLatitude = String.valueOf(gps.getLatitude());
                                        sLongitude = String.valueOf(gps.getLongitude());

                                        if (session.isLoggedIn()) {
                                            session.setXmppServiceState("online");
                                            Intent i = new Intent(getApplicationContext(), NavigationDrawerNew.class);
                                            if ("true".equalsIgnoreCase(pushtype)) {
                                                i.putExtra("type", "push");
                                            }
                                            startActivity(i);
                                            finish();
                                        } else {
                                            if (site_mode.equalsIgnoreCase("development")) {
                                                mInfoDialog = new PkDialogWithoutButton(Splash.this);
                                                mInfoDialog.setDialogTitle("ALERT");
                                                mInfoDialog.setDialogMessage(site_string);
                                                mInfoDialog.show();
                                            } else {
                                                Intent intent = new Intent(Splash.this, HomePage.class);
                                                startActivity(intent);
//                                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                                overridePendingTransition(R.anim.reg_pull_in_left, R.anim.reg_push_out_right);
                                                finish();
                                            }

                                        }

                                    } else {
                                        enableGpsService();
                                    }


                                } else {
                                    Toast.makeText(context, "BAD URL", Toast.LENGTH_SHORT).show();

                                }
                            }
                        } else {
                            Toast.makeText(context, "Not able to get current version", Toast.LENGTH_SHORT).show();
                        }

                    } else {*/
                    if (Str_status.equalsIgnoreCase("1") && isAppInfoAvailable) {

                        HashMap<String, String> language = session.getLanaguage();
                        Locale locale = null;

                        switch (Language_code) {

                            case "en":
                                locale = new Locale("en");
                                session.setlamguage("en", "en");

                                break;
                            case "es":
                                locale = new Locale("es");
                                session.setlamguage("es", "es");
                                break;

                            case "ta":
                                locale = new Locale("ta");
                                session.setlamguage("ta", "ta");
                                break;

                            default:
                                locale = new Locale("en");
                                session.setlamguage("en", "en");
                                break;
                        }
                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;

                        getApplicationContext().getResources().updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());
                        session.setDriver_image(driver_image);
                        session.setcustomerdetail(customer_service_number, customer_service_address);
                        session.setXmpp(sXmppHostUrl, sHostName);
                        session.setaboutus(About_Content, sSiteUrl);
                        session.setAgent(app_identity_name);
                        session.setdriver_image(driver_image);
                        session.setdriverNameUpdate(driverName);

                        if (server_mode.equalsIgnoreCase("0")) {
                            Toast.makeText(context, site_url, Toast.LENGTH_SHORT).show();
                        }

                        if (gps.isgpsenabled() && gps.canGetLocation()) {
                            sLatitude = String.valueOf(gps.getLatitude());
                            sLongitude = String.valueOf(gps.getLongitude());

                            if (session.isLoggedIn()) {
                                session.setXmppServiceState("online");
                                Intent i = new Intent(getApplicationContext(), NavigationDrawerNew.class);
                                if ("true".equalsIgnoreCase(pushtype)) {
                                    i.putExtra("type", "push");
                                }
                                startActivity(i);
                                finish();
                            } else {
                                if (site_mode.equalsIgnoreCase("development")) {
                                    mInfoDialog = new PkDialogWithoutButton(Splash.this);
                                    mInfoDialog.setDialogTitle("ALERT");
                                    mInfoDialog.setDialogMessage(site_string);
                                    mInfoDialog.show();
                                } else {
                                    Intent intent = new Intent(Splash.this, HomePage.class);
                                    startActivity(intent);
//                                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    overridePendingTransition(R.anim.reg_pull_in_left, R.anim.reg_push_out_right);
                                    finish();
                                }

                            }

                        } else {
                            enableGpsService();
                        }


                    } else {
                        Toast.makeText(context, "BAD URL", Toast.LENGTH_SHORT).show();

                    }
                    //  }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                Toast.makeText(context, ServiceConstant.MAIN_URL, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void requestAppPermissions() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        if (extras.hasReadPermissions() && extras.hasWritePermissions() && extras.hasLocatePermissions()) {
            return;
        }

        ActivityCompat.requestPermissions(this,
                new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION

                }, REQUEST_LOCATION_CODE); // your request code
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

             /*  AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle("Alert dialog title");
        builder.setMessage("this app collects location data to share my current location with user even when the app is closed or not in use.Click enable to allow location permission cancel to close..");
        builder.setPositiveButton("PositiveButton",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        // DO TASK
                    }
                });
        builder.setNegativeButton("NegativeButton",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        // DO TASK
                    }
                });*/


            case REQUEST_LOCATION_CODE:
               /* if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    apiHit();
                } else {
                    finish();
                }*/


                String permission = permissions[0];
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    // user rejected the permission
                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
                    if (!showRationale) {

                        LocationPermissionManule();

                           /* Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.fromParts("package", getPackageName(), null));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);*/
                        // user also CHECKED "never ask again"
                        // you can either enable some fall back,
                        // disable features of your app
                        // or open another dialog explaining
                        // again the permission and directing to
                        // the app setting
                    } else if (Manifest.permission.ACCESS_FINE_LOCATION.equals(permission)) {
                        showRationale(permission);
                        // user did NOT check "never ask again"
                        // this is a good place to explain the user
                        // why you need the permission and ask if he wants
                        // to accept it (the rationale)
                    } else if (Manifest.permission.ACCESS_COARSE_LOCATION.equals(permission)) {
                        showRationale(permission);
                        // user did NOT check "never ask again"
                        // this is a good place to explain the user
                        // why you need the permission and ask if he wants
                        // to accept it (the rationale)
                    }
                } else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            session = new SessionManager(getApplicationContext());
                            gps = new GPSTracker(Splash.this);

                            HashMap<String, String> user = session.getUserDetails();
                            driver_id = user.get(SessionManager.KEY_DRIVERID);

                            sLatitude = String.valueOf(gps.getLatitude());
                            sLongitude = String.valueOf(gps.getLongitude());
                            apiHit();

                        }
                    }, 2000);
                } else {
                    finish();
                }
                break;
        }
    }


    private void showRationale(String permission) {
        ActivityCompat.requestPermissions(this,
                new String[]{
                        permission
                }, REQUEST_LOCATION_CODE); // your request code
    }

    private void LocationPermissionManule() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Change Permissions in Settings");
        alertDialogBuilder
                .setMessage("" +
                        "\nClick SETTINGS to Manually Set\n" + "Permissions to Enable Location")
                .setCancelable(false)
                .setPositiveButton("SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_LOCATION_MANUEL);     // Comment 3.
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}







