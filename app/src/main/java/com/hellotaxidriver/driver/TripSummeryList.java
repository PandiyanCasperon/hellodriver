package com.hellotaxidriver.driver;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.app.service.ServiceConstant;
import com.app.service.ServiceRequest;
import com.hellotaxidriver.driver.Pojo.TripSummaryPojo;
import com.hellotaxidriver.driver.Utils.ConnectionDetector;
import com.hellotaxidriver.driver.Utils.SessionManager;
import com.hellotaxidriver.driver.adapter.TripSummeryAdapter;
import com.hellotaxidriver.driver.tripdetailspackage.localdb.TripDetailsDbHelper;
import com.hellotaxidriver.driver.tripdetailspackage.ridesummarydetail;
import com.hellotaxidriver.driver.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user88 on 10/23/2015.
 */
public class TripSummeryList extends Fragment {


    //----------------------------Local Db Start
    TripDetailsDbHelper tripDetailsDbHelper;
    List<String> trip_details_array = new ArrayList<>();
    //----------------------------Local Db end

    private static View rootview;
    private Context context;
    private View parentView;
    private StringRequest postrequest;
    private SessionManager session;
    private String driver_id = "";
    private String check = null;
    private LinearLayout all_layout, onride_layout, complete_layout;
    private TextView Tv_all, Tv_onride, Tv_complete;
    private ListView listview;
    private ActionBar actionBar;
    private TextView empty_Tv, up_tab_txt;
    Dialog dialog;
    private ArrayList<TripSummaryPojo> tripsummaryListall;
    private ArrayList<TripSummaryPojo> tripsummaryListonride;
    private ArrayList<TripSummaryPojo> tripsummaryListcompleted;
    private ArrayList<TripSummaryPojo> tripsummaryListupcomming;
    private TripSummeryAdapter adapter;
    private TextView no_trip_summary;

    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private boolean show_progress_status = false;

    private ServiceRequest mRequest;

    BroadcastReceiver receiver;
    String ride_position = "all";

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootview == null) {
            rootview = inflater.inflate(R.layout.trip_summary, container, false);
        } else {
//                ((ViewGroup) container.getParent()).removeView(parentView);
            ViewGroup parent = (ViewGroup) rootview.getParent();
            if (parent != null)
                parent.removeView(rootview);
        }
/*        if (rootview != null) {
            ViewGroup parent = (ViewGroup) rootview.getParent();
            if (parent != null)
                parent.removeView(rootview);
        }
        try {
            rootview = inflater.inflate(R.layout.trip_summary, container, false);
        } catch (InflateException e) {
        }*/

        /*ActionBarActivity actionBarActivity = (ActionBarActivity) getActivity();
        actionBar = actionBarActivity.getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.hide();*/
        context = getActivity();

        initialize(rootview);
        all_layout.setBackgroundColor(0xFF2F4052);
        onride_layout.setBackgroundColor(0xFFFFFFFF);
        complete_layout.setBackgroundColor(0xFFFFFFFF);
        Tv_all.setTextColor(0xFFFFFFFF);
        Tv_onride.setTextColor(0xFF000000);
        Tv_complete.setTextColor(0xFF000000);
        ride_position = "all";
        empty_Tv.setVisibility(View.GONE);
//        up_tab_txt.setVisibility(View.VISIBLE);
        listview.setEmptyView(empty_Tv);
        listview.setVisibility(View.GONE);

        //Code for broadcat receive
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.finish.tripsummerylist");

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("com.finish.tripsummerylist")) {
                    getActivity().finish();
                }
            }
        };
        getActivity().registerReceiver(receiver, filter);


        rootview.findViewById(R.id.ham_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((NavigationDrawerNew) getActivity()).openDrawer();
            /*    NavigationDrawerNew navigationDrawerNew = new NavigationDrawerNew();
                navigationDrawerNew.openDrawer();*/
//                NavigationDrawerNew.openDrawer();
                /*if (resideMenu != null) {
                    resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
                }*/
            }
        });
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                if (ride_position.equalsIgnoreCase("all")) {
                    Intent intent = new Intent(getActivity(), TripPage.class);
                    session.setupcomming_backoption("1");
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
                if (ride_position.equalsIgnoreCase("on_ride")) {
                    Intent intent = new Intent(getActivity(), ridesummarydetail.class);
                    intent.putExtra("rideid", tripsummaryListonride.get(position).getride_id());
                    intent.putExtra("type", "tripList");
                    intent.putExtra("ride_status", tripsummaryListall.get(position).getTripDetailsRideStatus());
//                    intent.putExtra("trip_details_json", tripsummaryListall.get(position).getTripDetailsJson());
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
                if (ride_position.equalsIgnoreCase("completed")) {
                    Intent intent = new Intent(getActivity(), ridesummarydetail.class);
                    intent.putExtra("rideid", tripsummaryListcompleted.get(position).getride_id());
                    intent.putExtra("type", "tripList");
                    intent.putExtra("ride_status", tripsummaryListall.get(position).getTripDetailsRideStatus());
//                    intent.putExtra("trip_details_json", tripsummaryListall.get(position).getTripDetailsJson());
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }

            }


        });


        all_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                all_layout.setBackgroundColor(0xFF2F4052);
                onride_layout.setBackgroundColor(0xFFFFFFFF);
                complete_layout.setBackgroundColor(0xFFFFFFFF);
                Tv_all.setTextColor(0xFFFFFFFF);
                Tv_onride.setTextColor(0xFF000000);
                Tv_complete.setTextColor(0xFF000000);
                ride_position = "all";

                adapter = new TripSummeryAdapter(getActivity(), tripsummaryListupcomming);
                listview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                if (tripsummaryListupcomming.size() > 0) {
                    empty_Tv.setVisibility(View.GONE);
                } else {
                    empty_Tv.setVisibility(View.VISIBLE);
                    listview.setEmptyView(empty_Tv);
                }
//                up_tab_txt.setVisibility(View.VISIBLE);
            }
        });

        onride_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                all_layout.setBackgroundColor(0xFFFFFFFF);
                onride_layout.setBackgroundColor(0xFF2F4052);
                complete_layout.setBackgroundColor(0xFFFFFFFF);
                Tv_all.setTextColor(0xFF000000);
                Tv_complete.setTextColor(0xFF000000);
                Tv_onride.setTextColor(0xFFFFFFFF);
                ride_position = "on_ride";
                adapter = new TripSummeryAdapter(getActivity(), tripsummaryListonride);
                listview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                if (tripsummaryListonride.size() > 0) {
                    empty_Tv.setVisibility(View.GONE);
                } else {
                    empty_Tv.setVisibility(View.VISIBLE);
                    listview.setEmptyView(empty_Tv);
                }
//                up_tab_txt.setVisibility(View.GONE);
            }
        });
        complete_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                all_layout.setBackgroundColor(0xFFFFFFFF);
                onride_layout.setBackgroundColor(0xFFFFFFFF);
                complete_layout.setBackgroundColor(0xFF2F4052);

                Tv_all.setTextColor(0xFF000000);
                Tv_onride.setTextColor(0xFF000000);
                Tv_complete.setTextColor(0xFFFFFFFF);


                ride_position = "completed";
                adapter = new TripSummeryAdapter(getActivity(), tripsummaryListcompleted);
                listview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                if (tripsummaryListcompleted.size() > 0) {
                    empty_Tv.setVisibility(View.GONE);
                } else {
                    empty_Tv.setVisibility(View.VISIBLE);
                    listview.setEmptyView(empty_Tv);
                }

//                up_tab_txt.setVisibility(View.GONE);
            }
        });
        //  setUpViews();
        return rootview;
    }


    private void initialize(View rootview) {
        tripsummaryListall = new ArrayList<TripSummaryPojo>();
        tripsummaryListonride = new ArrayList<TripSummaryPojo>();
        tripsummaryListcompleted = new ArrayList<TripSummaryPojo>();
        tripsummaryListupcomming = new ArrayList<TripSummaryPojo>();

        session = new SessionManager(getActivity());
        all_layout = (LinearLayout) rootview.findViewById(R.id.trip_summary_all_layout);
        onride_layout = (LinearLayout) rootview.findViewById(R.id.trip_summary_onride_layout);
        complete_layout = (LinearLayout) rootview.findViewById(R.id.trip_summary_completed_layout);
        listview = (ListView) rootview.findViewById(R.id.trip_summary_listview);
        no_trip_summary = (TextView) rootview.findViewById(R.id.no_trip_summary);
        Tv_all = (TextView) rootview.findViewById(R.id.trip_summary_all);
        Tv_onride = (TextView) rootview.findViewById(R.id.trip_summary_onride);
        Tv_complete = (TextView) rootview.findViewById(R.id.trip_summary_completed_button);
        empty_Tv = (TextView) rootview.findViewById(R.id.no_trip_summary);
//        up_tab_txt = (TextView) rootview.findViewById(R.id.upcomming_job_txt);
        HashMap<String, String> user = session.getUserDetails();
        driver_id = user.get(SessionManager.KEY_DRIVERID);
        all_layout.setBackgroundColor(0xFF2F4052);
        Tv_all.setTextColor(0xFFFFFFFF);
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();
        session.setmove_upcomming("0");

//        dbInitialize(rootview);

        if (isInternetPresent) {
            PostRequest(ServiceConstant.tripsummery_list_url);
            System.out.println("triplists------------------" + ServiceConstant.tripsummery_list_url);
        } else {
            Alert(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));

        }
    }

    private void PostRequest(String Url) {
        dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        System.out.println("-------------triplist----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driver_id);
        jsonParams.put("trip_type", "all");

        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------reponse-------------------" + response);
                Log.e("trip", response);
                String status = "", total_rides = "", type_group = "", Str_response = "";

                try {
                    JSONObject object = new JSONObject(response);
                    status = object.getString("status");

                    System.out.println("triplist--status----" + status);

                    if (status.equalsIgnoreCase("1")) {

                        JSONObject jsonObject = object.getJSONObject("response");

                        total_rides = jsonObject.getString("total_rides");
                        JSONArray jarry = jsonObject.getJSONArray("rides");

                        if (jarry.length() > 0) {

                            for (int i = 0; i < jarry.length(); i++) {
                                JSONObject jobjct = jarry.getJSONObject(i);
                                TripSummaryPojo items = new TripSummaryPojo();
                                items.setpickup((jobjct.getString("pickup")).replaceAll("\n", " "));
                                items.setride_date(jobjct.getString("ride_date"));
                                items.setride_time(jobjct.getString("ride_time"));
                                items.setride_id(jobjct.getString("ride_id"));
                                tripsummaryListall.add(items);

                                if (jobjct.getString("group").equalsIgnoreCase("completed")) {
                                    tripsummaryListcompleted.add(items);
                                } else if (jobjct.getString("group").equalsIgnoreCase("onride")) {
                                    tripsummaryListonride.add(items);
                                } else if (jobjct.getString("group").equalsIgnoreCase("upcoming")) {
                                    tripsummaryListupcomming.add(items);
                                }

                            }
                            show_progress_status = true;
                        } else {
                            show_progress_status = false;
                        }
                    } else {

                        Str_response = object.getString("response");

                        System.out.println("triplist----------response---" + Str_response);

                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();

                if (status.equalsIgnoreCase("1")) {
                    adapter = new TripSummeryAdapter(getActivity(), tripsummaryListupcomming);
                    listview.setAdapter(adapter);
                    if (show_progress_status) {
                        empty_Tv.setVisibility(View.GONE);
                    } else {
                        empty_Tv.setVisibility(View.VISIBLE);
                        listview.setEmptyView(empty_Tv);
                    }
                } else {
                    Alert(getResources().getString(R.string.alert_sorry_label_title), Str_response);
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }

        });

    }

    //--------------Alert Method-----------
    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(getActivity());
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.alert_label_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //----------------------------Local Db Start
    private void dbInitialize(View rootview) {
        tripDetailsDbHelper = new TripDetailsDbHelper(getActivity());
        int trip_details_count = tripDetailsDbHelper.getTripDetailCount();
        int trips_all = session.getTripsAll();
        String trip_db_driver_id = tripDetailsDbHelper.getTripDriverId();
        /*if (trip_details_count != 0 && trip_db_driver_id.equalsIgnoreCase(driver_id)) {
            tripListJsonParserMethod();
        } else {

        }*/
        tripDetailsDbHelper.deleteTripdeatils();
        postRequestRetrieveTripDetails(ServiceConstant.TRIP_DETAILS_DATA_RETRIVE);
    }

    private void tripListJsonParserMethod() {

        tripsummaryListall.clear();
        tripsummaryListcompleted.clear();
        tripsummaryListonride.clear();
        tripsummaryListupcomming.clear();


        dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        trip_details_array = tripDetailsDbHelper.getTripDetailsJson();
        int trip_array_size = trip_details_array.size();
        if (trip_array_size > 0) {
            show_progress_status = true;
        } else {
            show_progress_status = false;
        }
        System.out.println("------------------------------trip_details_array" + trip_details_array.toString());
        for (int i = 0; i < trip_array_size; i++) {
            listview.setVisibility(View.VISIBLE);
            String[] split_str = trip_details_array.get(i).split(";");
            String trip_details_response = split_str[0].replaceAll("\n", " ");

            String trip_status = split_str[1];

            System.out.println("--------------reponse-------------------" + trip_details_response);
            String pick_up_address = "", drop_address = "", ride_date = "", ride_time = "", ride_id = "", ride_status = "", Str_response = "";

            try {
                JSONObject object = new JSONObject(trip_details_response);
                JSONObject ride_info_object = object.getJSONObject("ride_info");


                JSONObject jsonObject = ride_info_object.getJSONObject("response");


                JSONObject pick_up_object = jsonObject.getJSONObject("pickup");
                if (pick_up_object.has("location"))
                    pick_up_address = pick_up_object.getString("location");

                JSONObject drop_object = jsonObject.getJSONObject("drop");
                if (drop_object.has("location"))
                    drop_address = drop_object.getString("location");

                ride_date = jsonObject.getString("ride_date");
                ride_time = jsonObject.getString("pickup_date");
                ride_id = jsonObject.getString("ride_id");
                ride_status = jsonObject.getString("ride_status");

                TripSummaryPojo items = new TripSummaryPojo();
                items.setpickup(pick_up_address);
                items.setride_date(ride_date);
                items.setride_time(ride_time);
                items.setride_id(ride_id);
                items.setTripDetailsJson(trip_details_response);
                items.setTripDetailsRideStatus(trip_status);

                tripsummaryListall.add(items);

                if (trip_status.equalsIgnoreCase("1")) {
                    tripsummaryListcompleted.add(items);
                } else if (trip_status.equalsIgnoreCase("0")) {
                    tripsummaryListonride.add(items);
                } else if (trip_status.equalsIgnoreCase("2")) {
                    tripsummaryListupcomming.add(items);
                }

                adapter = new TripSummeryAdapter(getActivity(), tripsummaryListupcomming);
                listview.setAdapter(adapter);
                if (show_progress_status) {
                    empty_Tv.setVisibility(View.GONE);
                } else {
                    empty_Tv.setVisibility(View.VISIBLE);
                    listview.setEmptyView(empty_Tv);
                }
                dialog.dismiss();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                dialog.dismiss();
            }
        }
    }

    //-----------------------Code for my rides post request-----------------

    private void postRequestRetrieveTripDetails(String Url) {


        dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        System.out.println("-------------triplist----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driver_id);

        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------reponse-------------------" + response);
                Log.e("trip", response);
                String status = "", total_rides = "", type_group = "", Str_response = "";

                try {
                    JSONObject object = new JSONObject(response);
                    status = object.getString("status");

                    System.out.println("triplist--status----" + status);

                    if (status.equalsIgnoreCase("1")) {

                        JSONObject jsonObject = object.getJSONObject("response");
                        JSONArray jarray = jsonObject.getJSONArray("rides");
                        for (int i = 0; i < jarray.length(); i++) {
                            JSONObject j_ride_object = jarray.getJSONObject(i);

                            String finalresponse = "{" + "\"ride_info\": {" + "" + "\"response\": " + j_ride_object.toString() + "}}";

                            String ride_status = "";
                            if (j_ride_object.has("ride_status")) {
                                ride_status = j_ride_object.getString("ride_status");
                            }
                            String rideid = j_ride_object.getString("ride_id");

                            if (ride_status.equalsIgnoreCase("Completed")) {
                                tripDetailsDbHelper.insertTripdeatils(rideid, driver_id, finalresponse, "1");
                            } else if (ride_status.equalsIgnoreCase("Cancelled")) {
                                tripDetailsDbHelper.insertTripdeatils(rideid, driver_id, finalresponse, "0");

                            } else if (ride_status.equalsIgnoreCase("Confirmed")) {
                                tripDetailsDbHelper.insertTripdeatils(rideid, driver_id, finalresponse, "2");
                            }


                        }


                        if (jarray.length() > 0) {
                            dialog.dismiss();
                            tripListJsonParserMethod();
                        } else {
                            empty_Tv.setVisibility(View.VISIBLE);
                            listview.setEmptyView(empty_Tv);
                        }

                    } else {

                        Str_response = object.getString("response");


                    }
                    dialog.dismiss();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }

        });

    }

    //----------------------------Local Db end


}
