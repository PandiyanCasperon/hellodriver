package com.hellotaxidriver.driver;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.service.ServiceConstant;
import com.app.service.ServiceRequest;
import com.hellotaxidriver.driver.Pojo.RegistrationInfoModel;
import com.hellotaxidriver.driver.widgets.SnackFlashBar;
import com.hbb20.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class RegistrationMobileVerificationActivity extends AppCompatActivity {
    private View headerView;
    private RelativeLayout headerBackRl;
    private CollapsingToolbarLayout headerCollapseTL;
    private SnackFlashBar snackFlashBar;

    private RelativeLayout mobileNumberRl, otpVerificationRl, mainRl;
    private TextInputLayout codeTIL, mobileNumberTIl, otpTIl;
    private EditText codeEt, mobileNumberEt, otpEt;
    private TextView locationTypeNextTv, resendTv, otpSendLabelTv;
    private CountryCodePicker ccp;
    private Dialog dialog;
    private ServiceRequest mRequest;
    private RegistrationInfoModel registrationInfoModel;
    //private Handler resendOtpHandler = new Handler();
    private BroadcastReceiver broadcastReceiver;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            if (otpVerificationRl.getVisibility() == View.VISIBLE) {
                otpVerificationRl.setVisibility(View.GONE);
                mobileNumberRl.setVisibility(View.VISIBLE);
                otpEt.setText("");
            } else {
                finish();
                CloseKeyBoard();
                overridePendingTransition(R.anim.reg_pull_in_left, R.anim.reg_push_out_right);
            }
            return true;
        }
        return false;
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (mobileNumberEt.getText().toString().trim().length() == 0) {
                snackFlashBar.SnackBar(getResources().getString(R.string.app_name), getResources().getString(R.string.mobile_verification_mobile_number_empty_error), mainRl, "error");
            } else {
                postRequest_otp(ServiceConstant.RegistrationDriverOtp);
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // resendOtpHandler.removeCallbacks(runnable);
        if (broadcastReceiver!=null)
        {
            unregisterReceiver(broadcastReceiver);
        }

    }

   /* @Override
    protected void onPostResume() {
        super.onPostResume();
        otpVerificationRl.setVisibility(View.GONE);
        mobileNumberRl.setVisibility(View.VISIBLE);
        otpEt.setText("");
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_mobile_verification);
        Init();

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.finish.RegisterMobileVerification");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("com.finish.RegisterMobileVerification")) {
                    finish();
                }
            }
        };
        try {
            registerReceiver(broadcastReceiver, filter);
        } catch (Exception e) {

        }
        headerBackRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (otpVerificationRl.getVisibility() == View.VISIBLE) {
                    otpVerificationRl.setVisibility(View.GONE);
                    mobileNumberRl.setVisibility(View.VISIBLE);
                    otpEt.setText("");
                } else {
                    finish();
                    CloseKeyBoard();
                    overridePendingTransition(R.anim.reg_pull_in_left, R.anim.reg_push_out_right);
                }

            }
        });
        resendTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mobileNumberEt.getText().toString().trim().length() == 0) {
                    snackFlashBar.SnackBar(getResources().getString(R.string.app_name), getResources().getString(R.string.mobile_verification_mobile_number_empty_error), mainRl, "error");
                } else {
                    postRequest_otp(ServiceConstant.RegistrationDriverOtp);
                }
            }
        });

        locationTypeNextTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (otpVerificationRl.getVisibility() == View.VISIBLE) {
                    if (otpEt.getText().toString().trim().length() == 0) {
                        snackFlashBar.SnackBar(getResources().getString(R.string.app_name), getResources().getString(R.string.mobile_verification_otp_empty_error), mainRl, "error");
                    } else if (!otpEt.getText().toString().equals(registrationInfoModel.getDriverMobileReceivedOtp())) {
                        snackFlashBar.SnackBar(getResources().getString(R.string.app_name), getResources().getString(R.string.mobile_verification_otp_invalid_error), mainRl, "error");
                    } else {
                        Intent nextIntent = new Intent(RegistrationMobileVerificationActivity.this, RegistrationLoginDetailsActivity.class);
//                nextIntent.putExtra("modelObject", registrationInfoModel);
                        CloseKeyBoard();
                        startActivity(nextIntent);
                        overridePendingTransition(R.anim.reg_pull_in_left, R.anim.reg_push_out_right);
                    }

                } else {
                    if (mobileNumberEt.getText().toString().trim().length() == 0) {
                        snackFlashBar.SnackBar(getResources().getString(R.string.app_name), getResources().getString(R.string.mobile_verification_mobile_number_empty_error), mainRl, "error");
                    } else {
                        postRequest_otp(ServiceConstant.RegistrationDriverOtp);
                    }
                }

            }
        });

    }

    private void Init() {
//        Intent in = getIntent();
//        registrationInfoModel = (RegistrationInfoModel) in.getSerializableExtra("modelObject");

        headerView = findViewById(R.id.header_registration);
        headerBackRl = (RelativeLayout) headerView.findViewById(R.id.back_layout);
        headerCollapseTL = (CollapsingToolbarLayout) headerView.findViewById(R.id.collapsing_toolbar);
        headerCollapseTL.setTitleEnabled(true);
        headerCollapseTL.setCollapsedTitleTextColor(getResources().getColor(R.color.white_color));
        headerCollapseTL.setTitle(getResources().getString(R.string.mobile_verification_header_label));
        headerCollapseTL.setCollapsedTitleTextAppearance(R.style.coll_toolbar_title);
        headerCollapseTL.setExpandedTitleTextAppearance(R.style.exp_toolbar_title);
        snackFlashBar = new SnackFlashBar(RegistrationMobileVerificationActivity.this);
        mainRl = (RelativeLayout) findViewById(R.id.main_layout);

        registrationInfoModel = RegistrationLoginTypeActivity.registrationInfoModel;
        locationTypeNextTv = (TextView) findViewById(R.id.registration_mobile_next_tv);
        resendTv = (TextView) findViewById(R.id.otp_resend_tv);
        otpSendLabelTv = (TextView) findViewById(R.id.otp_send_label_tv);
        mobileNumberRl = (RelativeLayout) findViewById(R.id.mobile_number_layout);
        otpVerificationRl = (RelativeLayout) findViewById(R.id.main_verification_Rl);
//        codeArrowIv = (ImageView) findViewById(R.id.code_drop_down_arrow);
//        codeTIL = (TextInputLayout) findViewById(R.id.registration_input_layout_code);
        mobileNumberTIl = (TextInputLayout) findViewById(R.id.registration_input_layout_mobile);
        otpTIl = (TextInputLayout) findViewById(R.id.registration_input_layout_otp);
//        codeEt = (EditText) findViewById(R.id.registration_country_code_et);
        mobileNumberEt = (EditText) findViewById(R.id.registration_phone_editText);
        otpEt = (EditText) findViewById(R.id.registration_otp_editText);
        ccp = (CountryCodePicker) findViewById(R.id.registration_input_ccp);
        ccp.setDefaultCountryUsingNameCode("US");
        registrationInfoModel.setDriverMobileDialCode(ccp.getDefaultCountryCodeWithPlus());

    }

    //-----------------------Post Request-----------------
    private void postRequest_otp(String Url) {
        dialog = new Dialog(RegistrationMobileVerificationActivity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        final TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        System.out.println("-------------postRequest_otp url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("dail_code", registrationInfoModel.getDriverMobileDialCode());
        jsonParams.put("mobile_number", mobileNumberEt.getText().toString());

        System.out.println("-------------postRequest_otp---jsonParams-------------" + jsonParams);

        mRequest = new ServiceRequest(RegistrationMobileVerificationActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                String Strstatus = "", Sresponse = "", dial_code = "", mobile_number = "", otp_status = "", otp = "", otp_resend_time;

                System.out.println("postRequest_otp-----------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    Strstatus = object.getString("status");

                    if (Strstatus.equalsIgnoreCase("1")) {

                        Sresponse = object.getString("response");
                        dial_code = object.getString("dail_code");
                        mobile_number = object.getString("mobile_number");
                        otp_status = object.getString("otp_status");
                        otp_resend_time = object.getString("otp_resend_timer");
                        otp = object.getString("otp");

                        registrationInfoModel.setDriverMobileReceivedOtp(otp);
                        registrationInfoModel.setDriverMobileDialCode(ccp.getSelectedCountryCodeWithPlus());
                        registrationInfoModel.setDriverMobileNumber(mobileNumberEt.getText().toString());

                        mobileNumberRl.setVisibility(View.GONE);
                        otpVerificationRl.setVisibility(View.VISIBLE);
                        otpSendLabelTv.setText(getResources().getText(R.string.mobile_verification_otp_label) + "\n " + registrationInfoModel.getDriverMobileDialCode() + " " + registrationInfoModel.getDriverMobileNumber());
                        if (otp_status.equalsIgnoreCase("development")) {
                            otpEt.setText(registrationInfoModel.getDriverMobileReceivedOtp());
                        } else {
                            otpEt.setText("");
                        }
                        int resendTimer = Integer.parseInt(otp_resend_time) * 1000;
                        //resendOtpHandler.postDelayed(runnable, resendTimer);
                        OpenKeyBoard();


                    } else {
                        Sresponse = object.getString("response");
                        Toast.makeText(getApplicationContext(), Sresponse, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {

                dialog.dismiss();

            }

        });


    }

    private void CloseKeyBoard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

    }

    private void OpenKeyBoard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }

    }


}
