package com.hellotaxidriver.driver;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.app.service.ServiceConstant;
import com.app.service.ServiceRequest;
import com.hellotaxidriver.driver.Utils.ConnectionDetector;
import com.hellotaxidriver.driver.Utils.EmojiExcludeFilter;
import com.hellotaxidriver.driver.Utils.SessionManager;
import com.hellotaxidriver.driver.widgets.PkDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by user88 on 3/22/2017.
 */

public class FeedBackPage extends Fragment {

    SessionManager session;
    EditText et_msg, et_subject;
    RelativeLayout submit;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;

    private static View rootview;

    Dialog dialog;
    private ServiceRequest mRequest;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        try {
            if (rootview == null) {
                rootview = inflater.inflate(R.layout.feedback_page, container, false);
            } else {
                ViewGroup parent = (ViewGroup) rootview.getParent();
                if (parent != null)
                    parent.removeView(rootview);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
       /* if (rootview != null) {
            ViewGroup parent = (ViewGroup) rootview.getParent();
            if (parent != null)
                parent.removeView(rootview);
        }
        try {
            rootview = inflater.inflate(R.layout.feedback_page, container, false);
        } catch (InflateException e) {
        }*/
        init(rootview);
        ((NavigationDrawerNew) getActivity()).disableSwipeDrawer();
      /*  NavigationDrawerNew navigationDrawerNew=new NavigationDrawerNew();
        navigationDrawerNew.disableSwipeDrawer();*/
//        NavigationDrawerNew.disableSwipeDrawer();

        rootview.findViewById(R.id.ham_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                if (imm.isAcceptingText()) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                ((NavigationDrawerNew) getActivity()).openDrawer();
           /*     NavigationDrawerNew navigationDrawerNew = new NavigationDrawerNew();
                navigationDrawerNew.openDrawer();*/
//                NavigationDrawerNew.openDrawer();

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_msg.setText(Html.fromHtml(et_msg.getText().toString()).toString());
                et_subject.setText(Html.fromHtml(et_subject.getText().toString()).toString());
                if (et_subject.length() == 0) {
                    erroredit(et_subject, getResources().getString(R.string.action_alert_sub));
                } else if (et_msg.length() == 0) {
                    erroredit(et_msg, getResources().getString(R.string.action_alert_msg));
                } else {
                    cd = new ConnectionDetector(getActivity());
                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        postRequest_saveFeedback(ServiceConstant.send_report);
                    } else {
                        Alert(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));
                    }
                }

            }
        });

        //   setUpViews();
        return rootview;

    }

    private void init(View rootview) {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN |
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_FULLSCREEN |
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        submit = (RelativeLayout) rootview.findViewById(R.id.rl_submit);
        et_msg = (EditText) rootview.findViewById(R.id.et_msg);
        et_subject = (EditText) rootview.findViewById(R.id.et_sub);
        session = new SessionManager(getActivity());

        et_msg.setFilters(new InputFilter[]{new EmojiExcludeFilter()});
        et_subject.setFilters(new InputFilter[]{new EmojiExcludeFilter()});

    }


    private void postRequest_saveFeedback(String Url) {
        dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        HashMap<String, String> user = session.getUserDetails();
        String driver_id = user.get(SessionManager.KEY_DRIVERID);


        final TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_reporting));

        System.out.println("-------------saveFeedback----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("id", driver_id);
        jsonParams.put("user_type", "driver");
        jsonParams.put("subject", et_subject.getText().toString());
        jsonParams.put("message", et_msg.getText().toString());


        System.out.println("id----------" + driver_id);


        System.out.println("subject----------" + et_subject.getText().toString());

        System.out.println("message----------" + et_msg.getText().toString());


        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                String Strstatus = "", Sresponse = "", messhae = "";

                System.out.println("saveFeedback-----------------" + response);

                try {
                    JSONObject object = new JSONObject(response);
                    Strstatus = object.getString("status");

                    if (Strstatus.equalsIgnoreCase("1")) {

                        JSONObject jsonObject = object.getJSONObject("response");

                        messhae = jsonObject.getString("message");

                    } else {
                        Sresponse = object.getString("response");
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (Strstatus.equalsIgnoreCase("1")) {
                    et_msg.setText("");
                    et_subject.setText("");

                    AlertSuccess(getResources().getString(R.string.action_loading_sucess), messhae);


                } else {
                    Alert(getResources().getString(R.string.alert_sorry_label_title), Sresponse);
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {

                dialog.dismiss();

            }

        });


    }


    //--------------Alert Method-----------
    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(getActivity());
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.alert_label_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void AlertSuccess(String title, String message) {
        final PkDialog mDialog = new PkDialog(getActivity());
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.alert_label_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.content_frame, new DashBoardDriver());
                ft.commit();
            }
        });
        mDialog.show();
    }

    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);
        editname.startAnimation(shake);
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }


}
