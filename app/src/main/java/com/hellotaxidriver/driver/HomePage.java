package com.hellotaxidriver.driver;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import com.app.sqliteDb.documentdb;
import com.hellotaxidriver.driver.Fragment.Welcome_slide1;
import com.hellotaxidriver.driver.Fragment.Welcome_slide2;
import com.hellotaxidriver.driver.Fragment.Welcome_slide3;
import com.hellotaxidriver.driver.Utils.SessionManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.Timer;


public class HomePage extends FragmentActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

   private Button mSignIn;
    private Button mRegister;
  final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000;
    private SessionManager session;
    public static HomePage homePage;
    LinearLayout Layout_bars;
    private int numberOfViewPagerChildren = 3;
    private int numberOfViewPagerChildren1 = 3;
    int[] screens;
  //  Timer swipeTimer;
  //  MyAdapter myvpAdapter;
    Timer timer;
  //  ViewPager vp;
    int current_position = 0;
    int currentPage = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage);



        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN |
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_FULLSCREEN |
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        try {
            homePage = HomePage.this;
            // onRequestRunTimePermission();
           mSignIn = (Button) findViewById(R.id.btn_signin);
            mRegister = (Button) findViewById(R.id.btn_register);
            int SDK_INT = android.os.Build.VERSION.SDK_INT;
            if (SDK_INT > 8) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                        .permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        session = new SessionManager(HomePage.this);
        session.createSessionOnline("0");
        session.setRequestCount(0);
        mSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), LoginPage.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }

        });
        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(HomePage.this, RegistrationLoginTypeActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                documentdb db = new documentdb(getApplicationContext());
                db.clearTable();

            }
        });


/*

        vp = (ViewPager) findViewById(R.id.viewPager);
        Layout_bars = (LinearLayout) findViewById(R.id.linearlayout);
        screens = new int[]{
                R.layout.welcome_slide1,
                R.layout.welcome_slide2,
                R.layout.welcome_slider3
        };

        myvpAdapter = new MyAdapter(getSupportFragmentManager());
        vp.setAdapter(myvpAdapter);
        //pageindicator.setViewPager(vp);
*/


/*        ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // ColoredBars(position);
                current_position = position;
                vp.setCurrentItem(position);
      *//*      if (position == screens.length - 1) {
                Next.setText("start");

            } else {
                Next.setText("end");

            }*//*
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {


            }
        };
        vp.addOnPageChangeListener(viewPagerPageChangeListener);

        vp.setPageTransformer(true, new ViewPager.PageTransformer()
        {
            @Override
            public void transformPage(View view, float position) {
                int index = (Integer) view.getTag();
                current_position = index;
               *//* Drawable currentDrawableInLayerDrawable;
                currentDrawableInLayerDrawable = background.getDrawable(index);

                if(position <= -1 || position >= 1) {
                    currentDrawableInLayerDrawable.setAlpha(0);
                } else if( position == 0 ) {
                    currentDrawableInLayerDrawable.setAlpha(255);
                } else {
                    currentDrawableInLayerDrawable.setAlpha((int)(255 - Math.abs(position*255)));
                }*//*
            }
        });

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == numberOfViewPagerChildren1) {

                    vp.setCurrentItem(currentPage, true);
                } else{
                    vp.setCurrentItem(currentPage++, true);
                }

            }
        };*/

/*        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);*/
//        if (!preferenceManager.FirstLaunch()) {
//            launchMain();
//            finish();
//        }

        //ColoredBars(0);
    }

//    public void next(View v) {
//        int i = getItem(+1);
//        if (i < screens.length) {
//            vp.setCurrentItem(i);
//        } else {
//            launchMain();
//        }
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    class MyAdapter extends FragmentStatePagerAdapter
    {

        public MyAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment=null;
            if(i==0)
            {
                fragment=new Welcome_slide1();
               // pageindicator.notifyDataSetChanged();
                //RL.setBackgroundColor(getResources().getColor(R.color.how_it_work_color_yellow));
            }
            if(i==1)
            {
                fragment=new Welcome_slide2();
               // pageindicator.notifyDataSetChanged();
                //   RL.setBackgroundColor(getResources().getColor(R.color.how_it_work_color_red));
            }
            if(i==2)
            {
                fragment=new Welcome_slide3();
               // pageindicator.notifyDataSetChanged();
                //  RL.setBackgroundColor(getResources().getColor(R.color.how_it_work_color_blue));
            }

            return fragment;
        }

        @Override
        public int getCount() {

            return numberOfViewPagerChildren;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            if(object instanceof Welcome_slide1){
                view.setTag(2);
            }
            if(object instanceof Welcome_slide2){
                view.setTag(1);
            }
            if(object instanceof Welcome_slide3){
                view.setTag(0);
            }

            return super.isViewFromObject(view, object);
        }
    }


}
