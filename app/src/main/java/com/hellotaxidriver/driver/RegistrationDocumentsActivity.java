package com.hellotaxidriver.driver;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.service.ServiceConstant;
import com.app.service.ServiceRequest;
import com.app.sqliteDb.documentdb;
import com.hellotaxidriver.driver.Pojo.DriverDocumentPojo;
import com.hellotaxidriver.driver.Pojo.RegistrationInfoModel;
import com.hellotaxidriver.driver.Utils.ConnectionDetector;
import com.hellotaxidriver.driver.Utils.SessionManager;
import com.hellotaxidriver.driver.adapter.DriverRegsitrationDocumentListAdapter;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.hellotaxidriver.driver.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class RegistrationDocumentsActivity extends AppCompatActivity implements DriverRegsitrationDocumentListAdapter.DriverDocumentSelect {

    public static HashMap<String, String> picsMap;
    public static DriverRegsitrationDocumentListAdapter adapter;
    String driver_id = "", sLatitude = "", sLongitude = "", app_identity_name = "", Language_code = "", driver_image = "", driverName = "";
    documentdb mHelper;
    SQLiteDatabase dataBase;
    private boolean isAppInfoAvailable = false;
    private View headerView;
    private RelativeLayout headerBackRl;
    private CollapsingToolbarLayout headerCollapseTL;
    private SessionManager session;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    private CardView documentListCardView;
    private TextView sumbitTv;
    private ExpandableHeightListView driverregdocumetLv;
    private ArrayList<DriverDocumentPojo> documentsListnew;
    private Dialog dialog;
    private RegistrationInfoModel registrationInfoModel;
    private boolean IsValidated = true;

    @Override
    protected void onResume() {
        super.onResume();
        commonreload();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            finish();
            CloseKeyBoard();
            overridePendingTransition(R.anim.reg_pull_in_left, R.anim.reg_push_out_right);

            return true;
        }
        return false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_documents);
        mHelper = new documentdb(RegistrationDocumentsActivity.this);
        Init();
        headerBackRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.reg_pull_in_left, R.anim.reg_push_out_right);
            }
        });
        sumbitTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mHelper = new documentdb(RegistrationDocumentsActivity.this);
                dataBase = mHelper.getWritableDatabase();
                Cursor mCursor = dataBase.rawQuery("SELECT * FROM " + documentdb.TABLE_NAME, null);


                if (mCursor.moveToFirst()) {
                    do {

                        String yesv = mCursor.getString(mCursor.getColumnIndex(documentdb.KEY_IS_REQ));
                        String documentsListnew = mCursor.getString(mCursor.getColumnIndex(documentdb.KEY_IMAGEADDED));
                        String shortname = mCursor.getString(mCursor.getColumnIndex(documentdb.KEY_NAME));

                        if (yesv.equalsIgnoreCase("yes")) {
                            if (documentsListnew.equalsIgnoreCase("") || documentsListnew.equalsIgnoreCase(" ")) {
                                IsValidated = false;
                                Toast.makeText(getApplicationContext(), "Please upload valid " + shortname + " file", Toast.LENGTH_SHORT).show();
//                            AlertError(getResources().getString(R.string.action_error), "Please upload valid " + shortname + " file", regMobileSend);
                                break;
                            } else {
                                IsValidated = true;
                            }

                        }


                    } while (mCursor.moveToNext());
                }

                if (IsValidated) {
                    if (isInternetPresent) {
                        postRequest_Submit_Registration(ServiceConstant.RegistrationSubmit);

                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.alert_nointernet), Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });
    }

    private void Init() {
        headerView = findViewById(R.id.header_registration);
        headerBackRl = (RelativeLayout) headerView.findViewById(R.id.back_layout);
        headerCollapseTL = (CollapsingToolbarLayout) headerView.findViewById(R.id.collapsing_toolbar);
        headerCollapseTL.setTitleEnabled(true);
        headerCollapseTL.setCollapsedTitleTextColor(getResources().getColor(R.color.white_color));
        headerCollapseTL.setTitle(getResources().getString(R.string.documents_header_label));
        headerCollapseTL.setCollapsedTitleTextAppearance(R.style.coll_toolbar_title);
        headerCollapseTL.setExpandedTitleTextAppearance(R.style.exp_toolbar_title);

        session = new SessionManager(getApplicationContext());
        cd = new ConnectionDetector(this);
        isInternetPresent = cd.isConnectingToInternet();
        registrationInfoModel = RegistrationLoginTypeActivity.registrationInfoModel;
        driverregdocumetLv = (ExpandableHeightListView) findViewById(R.id.driverregdocumet_lv);
        documentListCardView = (CardView) findViewById(R.id.register_cardview);
        sumbitTv = (TextView) findViewById(R.id.registration_mobile_next_tv);
        documentsListnew = new ArrayList<>();
        picsMap = new HashMap<>();

        HitUrl();
    }

    private void HitUrl() {
        if (isInternetPresent) {
            int count = mHelper.getProfilesCount();
            if (count == 0) {
                DriverRegistrationDocumentDetails(ServiceConstant.RegistrationDocumentGetList);
            } else {
                commonreload();
            }
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.alert_nointernet), Toast.LENGTH_SHORT).show();
        }

    }


    //    -------------------------postRequest_Submit_Registration
    private void postRequest_Submit_Registration(String Url) {
        dialog = new Dialog(RegistrationDocumentsActivity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        final TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

/*        HashMap<String, String> user = session.getUserDetails();
        String agent_id = user.get(SessionManager.KEY_USERID);*/

        HashMap<String, String> json = new HashMap<>();
        json.put("location", registrationInfoModel.getSelectedLocationId());
        json.put("category", registrationInfoModel.getSelectedCateId());
        json.put("dail_code", registrationInfoModel.getDriverMobileDialCode());
        json.put("mobile_number", registrationInfoModel.getDriverMobileNumber());
        json.put("email", registrationInfoModel.getDriverEmail());
        json.put("driver_name", registrationInfoModel.getDriverName());
        json.put("gender", registrationInfoModel.getGender());
        json.put("image", registrationInfoModel.getDriverImageName());
        json.put("password", registrationInfoModel.getDriverPassword());
        json.put("vehicle_number", registrationInfoModel.getDriverVehicleNumber());
        json.put("vehicle_type", registrationInfoModel.getSelectedTypeId());
        json.put("vehicle_maker", registrationInfoModel.getSelectedMakerId());
        json.put("vehicle_model", registrationInfoModel.getSelectedModelId());

        json.put("vehicle_model_year", registrationInfoModel.getSelectedYear());
        json.put("address", registrationInfoModel.getDriverAddress());
        json.put("city", registrationInfoModel.getDriverCity());

        json.put("state", registrationInfoModel.getDriverState());
        json.put("country", registrationInfoModel.getDriverCountryId());
        json.put("postal_code", registrationInfoModel.getDriverZipCode());
        json.put("gcm_id", registrationInfoModel.getGcmId());
        json.put("accept_other_category", registrationInfoModel.getAdditionalCate());

        documentdb mHelperer = new documentdb(RegistrationDocumentsActivity.this);
        dataBase = mHelperer.getWritableDatabase();
        Cursor mCursorsd = dataBase.rawQuery("SELECT * FROM " + documentdb.TABLE_NAME, null);
        if (mCursorsd.moveToFirst()) {
            do {
                String yesv = mCursorsd.getString(mCursorsd.getColumnIndex(documentdb.KEY_IS_REQ));
                String expDate = mCursorsd.getString(mCursorsd.getColumnIndex(documentdb.KEY_EXPIRE_DATE));
                String saveid = mCursorsd.getString(mCursorsd.getColumnIndex(documentdb.KEY_SAVEID));
                String imageadded = mCursorsd.getString(mCursorsd.getColumnIndex(documentdb.KEY_IMAGESNAME));
                if (imageadded == null) {

                } else {
                    json.put("documents[" + saveid + "]" + "[file_name]", imageadded);
                    if (yesv.equalsIgnoreCase("Yes")) {
                        json.put("documents[" + saveid + "]" + "[expiry_date]", expDate);
                    }
                    System.out.println("imagename2" + imageadded);
                }

            } while (mCursorsd.moveToNext());
        }


        System.out.println("--------------------postRequest_Submit_Registration Url-----------------" + Url);
        System.out.println("--------------------postRequest_Submit_Registration json-----------------" + json);

        mRequest = new ServiceRequest(RegistrationDocumentsActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, json, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                String Strstatus = "", Sresponse = "", action = "", driver_name = "", email = "", vehicle_number = "", vehicle_model = "", sec_key = "", key = "", isalive = "";

                System.out.println("--------------------postRequest_Submit_Registration response----------------" + response);
                try {
                    JSONObject jobject = new JSONObject(response);
                    Strstatus = jobject.getString("status");
                    Sresponse = jobject.getString("response");
                    if (Strstatus.equalsIgnoreCase("1")) {
                        action = jobject.getString("action");
                        String driver_img = "", driver_id = "";
                        if (action.equalsIgnoreCase("login")) {
                            Toast.makeText(getApplicationContext(), Sresponse, Toast.LENGTH_SHORT).show();
                            Intent logintypefinish = new Intent();
                            logintypefinish.setAction("com.finish.RegisterLoginType");
                            sendBroadcast(logintypefinish);

                            Intent mobileVerificationfinish = new Intent();
                            mobileVerificationfinish.setAction("com.finish.RegisterMobileVerification");
                            sendBroadcast(mobileVerificationfinish);

                            Intent logindetailsfinish = new Intent();
                            logindetailsfinish.setAction("com.finish.LoginDetails");
                            sendBroadcast(logindetailsfinish);

                            Intent driverdetailsfinish = new Intent();
                            driverdetailsfinish.setAction("com.finish.RegisteDriverDetails");
                            sendBroadcast(driverdetailsfinish);

                            Intent vehicleinfofinish = new Intent();
                            vehicleinfofinish.setAction("com.finish.vehicleInfo");
                            sendBroadcast(vehicleinfofinish);
                            finish();

                            deleteDatabase(documentdb.DATABASE_NAME);

                        } else if (action.equalsIgnoreCase("dashboard")) {

                            isalive = jobject.getString("is_alive_other");
                            driver_img = jobject.getString("driver_image");
                            driver_id = jobject.getString("driver_id");
                            driver_name = jobject.getString("driver_name");
                            email = jobject.getString("email");
                            vehicle_number = jobject.getString("vehicle_number");
                            vehicle_model = jobject.getString("vehicle_model");
                            sec_key = jobject.getString("sec_key");
                            key = jobject.getString("key");
                            //   lang_code= details.getlang_key();
                            isalive = jobject.getString("is_alive_other");

                            System.out.println("key--------------" + sec_key);

                            System.out.println("driverid--------------" + driver_id);

                            System.out.println("--------gcm id-------------" + key);

                            session.createLoginSession(driver_img, driver_id, driver_name, email, vehicle_number, vehicle_model, key, sec_key, key);
//                            session.setUserVehicle(vehicle_model);

                            if (isalive.equalsIgnoreCase("Yes")) {

                                HomePage.homePage.finish();
                                final PkDialog mDialog = new PkDialog(RegistrationDocumentsActivity.this);
                                mDialog.setDialogTitle(getResources().getString(R.string.app_name));
                                mDialog.setDialogMessage(getResources().getString(R.string.alert_multiple_login));
                                final String finalDriver_id = driver_id;
                                mDialog.setPositiveButton(getResources().getString(R.string.alert_label_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mDialog.dismiss();
                                        session.setApplogout("login");
                                        postRequest_applaunch(ServiceConstant.app_launching_url, finalDriver_id);
                                    }
                                });
                                mDialog.show();
                            } else {
                                postRequest_applaunch(ServiceConstant.app_launching_url, driver_id);
                            }
                        }

                    } else {
//                        erroredit(Et_emailid, Sresponse);
                        Toast.makeText(getApplicationContext(), Sresponse, Toast.LENGTH_SHORT).show();
                    }
                    dialog.dismiss();
                } catch (Exception e) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }

        });


    }

    private void postRequest_applaunch(String Url, String driver_id) {
        System.out.println("-------------Splash App Information Url----------------" + Url);
        HashMap<String, String> user = session.getUserDetails();
        driver_id = user.get(SessionManager.KEY_DRIVERID);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_type", "driver");
        jsonParams.put("id", driver_id);
        jsonParams.put("latitude", sLatitude);
        jsonParams.put("longitude", sLongitude);

        System.out.println("-------------LoginPage App Information jsonParams----------------" + jsonParams);


        mRequest = new ServiceRequest(RegistrationDocumentsActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Splash App Information Response----------------" + response);
                String Str_status = "", sContact_mail = "", completed_rides_str = "", cancelled_rides_str = "", About_Content = "", customer_service_number = "", customer_service_address = "", sCustomerServiceNumber = "", sSiteUrl = "", sXmppHostUrl = "", sHostName = "", sFacebookId = "", sGooglePlusId = "", sPhoneMasking = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            JSONObject info_object = response_object.getJSONObject("info");
                            if (info_object.length() > 0) {
                                sContact_mail = info_object.getString("site_contact_mail");
                                sCustomerServiceNumber = info_object.getString("customer_service_number");
                                sSiteUrl = info_object.getString("site_url");
                                sXmppHostUrl = info_object.getString("xmpp_host_url");
                                sHostName = info_object.getString("xmpp_host_name");
                                app_identity_name = info_object.getString("app_identity_name");
                                About_Content = info_object.getString("about_content");
                                driver_image = info_object.getString("driver_image");
                                driverName = info_object.getString("driver_name");
                                Language_code = info_object.getString("lang_code");
                                completed_rides_str = info_object.getString("completed");
                                cancelled_rides_str = info_object.getString("cancelled");
                                int trips_all = Integer.parseInt(completed_rides_str + cancelled_rides_str);
                                session.setTripsAll(trips_all);

                                if (info_object.has("customer_service_number")) {
                                    customer_service_number = info_object.getString("customer_service_number");
                                }
                                if (info_object.has("site_contact_address")) {
                                    customer_service_address = info_object.getString("site_contact_address");
                                }
                                if (info_object.has("phone_masking_status")) {
                                    String phoneMaskingStatus = info_object.getString("phone_masking_status");
                                    session.setKeyPhoneMaskingStatus(phoneMaskingStatus);
                                    System.out.println("=====>>>===PHONEMASKINGSTATUS ==========>>>>> " + phoneMaskingStatus);
                                }
                                isAppInfoAvailable = true;
                            } else {
                                isAppInfoAvailable = false;
                            }
                        } else {
                            isAppInfoAvailable = false;
                        }
                    } else {
                        isAppInfoAvailable = false;
                    }
                    if (Str_status.equalsIgnoreCase("1") && isAppInfoAvailable) {
                        HashMap<String, String> language = session.getLanaguage();
                        Locale locale = null;

                        switch (Language_code) {

                            case "en":
                                locale = new Locale("en");
                                session.setlamguage("en", "en");
                                break;
                            case "es":
                                locale = new Locale("es");
                                session.setlamguage("es", "es");
                                break;
                            case "ta":
                                locale = new Locale("ta");
                                session.setlamguage("ta", "ta");
                                break;
                            default:
                                locale = new Locale("en");
                                session.setlamguage("en", "en");
                                break;
                        }

                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;
                        getApplicationContext().getResources().updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());
                        session.setcustomerdetail(customer_service_number, customer_service_address);
                        session.setXmpp(sXmppHostUrl, sHostName);

                        session.setaboutus(About_Content, sSiteUrl);
                        session.setAgent(app_identity_name);
                        session.setdriver_image(driver_image);
                        session.setdriverNameUpdate(driverName);

                        HomePage.homePage.finish();
                        session.setXmppServiceState("online");

                        session.setApplogout("login");

                        deleteDatabase(documentdb.DATABASE_NAME);

                        Intent i = new Intent(getApplicationContext(), NavigationDrawerNew.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();

                        /*Intent logintypefinish = new Intent();
                        logintypefinish.setAction("com.finish.RegisterLoginType");
                        sendBroadcast(logintypefinish);

                        Intent mobileVerificationfinish = new Intent();
                        mobileVerificationfinish.setAction("com.finish.RegisterMobileVerification");
                        sendBroadcast(mobileVerificationfinish);

                        Intent logindetailsfinish = new Intent();
                        logindetailsfinish.setAction("com.finish.LoginDetails");
                        sendBroadcast(logindetailsfinish);

                        Intent driverdetailsfinish = new Intent();
                        driverdetailsfinish.setAction("com.finish.RegisteDriverDetails");
                        sendBroadcast(driverdetailsfinish);

                        Intent vehicleinfofinish = new Intent();
                        vehicleinfofinish.setAction("com.finish.vehicleInfo");
                        sendBroadcast(vehicleinfofinish);
                        finish();*/
                    } else {
                        Toast.makeText(RegistrationDocumentsActivity.this, "BAD URL", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                Toast.makeText(RegistrationDocumentsActivity.this, ServiceConstant.baseurl, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void DriverRegistrationDocumentDetails(String Url) {
        dialog = new Dialog(RegistrationDocumentsActivity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        final TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));
        Log.e("gethire_documents", Url.toString());
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        Log.e("gethire_documents", jsonParams.toString());

        mRequest = new ServiceRequest(this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                Log.e("gethire_documents", response);
                System.out.println("--------------gethire_documents response-------------------" + response);
                String Smessage = "", Sstatus = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject resObj = object.getJSONObject("response");

                        if (resObj.length() > 0) {
                            Object intervention = resObj.get("documents");
                            if (intervention instanceof JSONArray) {
                                JSONArray locatioArray = resObj.getJSONArray("documents");
                                documentsListnew.clear();
                                if (locatioArray.length() > 0) {
                                    picsMap.clear();
                                    for (int i = 0; i < locatioArray.length(); i++) {
                                        JSONObject locationobj = locatioArray.getJSONObject(i);
                                        mHelper = new documentdb(RegistrationDocumentsActivity.this);
                                        dataBase = mHelper.getWritableDatabase();
                                        ContentValues values = new ContentValues();
                                        values.put(documentdb.KEY_SAVEID, locationobj.getString("id"));
                                        values.put(documentdb.KEY_NAME, locationobj.getString("name"));
                                        values.put(documentdb.KEY_IS_REQ, locationobj.getString("is_req"));
                                        values.put(documentdb.KEY_IS_EXP, locationobj.getString("is_exp"));
                                        values.put(documentdb.KEY_IMAGEADDED, "");
                                        values.put(documentdb.KEY_EXPIRE_DATE, "");
                                        dataBase.insert(documentdb.TABLE_NAME, null, values);
                                    }
                                    commonreload();

                                } else {

                                }

                            }
                        }
                    } else {
                        Smessage = object.getString("response");
                        Toast.makeText(getApplicationContext(), Smessage, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    dialog.dismiss();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void commonreload() {
        documentListCardView.setVisibility(View.VISIBLE);
        documentsListnew.clear();
        mHelper = new documentdb(RegistrationDocumentsActivity.this);
        dataBase = mHelper.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM " + documentdb.TABLE_NAME, null);

        if (mCursor.moveToFirst()) {
            do {
                DriverDocumentPojo pojo = new DriverDocumentPojo();
                pojo.setId(mCursor.getString(mCursor.getColumnIndex(documentdb.KEY_SAVEID)));
                pojo.setName(mCursor.getString(mCursor.getColumnIndex(documentdb.KEY_NAME)));
                pojo.setIs_req(mCursor.getString(mCursor.getColumnIndex(documentdb.KEY_IS_REQ)));
                pojo.setIs_exp(mCursor.getString(mCursor.getColumnIndex(documentdb.KEY_IS_EXP)));
                pojo.setImageadded(mCursor.getString(mCursor.getColumnIndex(documentdb.KEY_IMAGEADDED)));
                pojo.setTitle(mCursor.getString(mCursor.getColumnIndex(documentdb.KEY_IMAGESNAME)));
                pojo.setExpDate(mCursor.getString(mCursor.getColumnIndex(documentdb.KEY_EXPIRE_DATE)));
                documentsListnew.add(pojo);
            } while (mCursor.moveToNext());
        }
        adapter = new DriverRegsitrationDocumentListAdapter(RegistrationDocumentsActivity.this, documentsListnew, RegistrationDocumentsActivity.this);
        driverregdocumetLv.setAdapter(adapter);
        driverregdocumetLv.setExpanded(true);
        if (documentsListnew.size() > 0) {
            driverregdocumetLv.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void OnSelectedDocument(int Position) {
        startActivity(new Intent(this, DocumentUploadAcitivy.class)
                .putExtra("titleid", documentsListnew.get(Position).getId())
                .putExtra("isExp", documentsListnew.get(Position).getIs_exp())
                .putExtra("name", documentsListnew.get(Position).getName())
                .putExtra("flag", "3"));
        overridePendingTransition(R.anim.reg_pull_in_left, R.anim.reg_push_out_right);
    }

    private void CloseKeyBoard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

    }
}
