package com.hellotaxidriver.driver.Pojo;

import java.util.ArrayList;

/**
 * Created by CAS64 on 8/24/2018.
 */

public class RegistrationCategoryPojo {
    private String categoryId,citySelectedName;
    private ArrayList<String> additionalCate;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    private String category;


    public String getCitySelectedName() {
        return citySelectedName;
    }

    public void setCitySelectedName(String citySelectedName) {
        this.citySelectedName = citySelectedName;
    }

    public ArrayList<String> getAdditionalCate() {
        return additionalCate;
    }

    public void setAdditionalCate(ArrayList<String> additionalCate) {
        this.additionalCate = additionalCate;
    }
}
