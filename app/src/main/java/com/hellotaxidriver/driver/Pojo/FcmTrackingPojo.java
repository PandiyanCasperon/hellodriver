package com.hellotaxidriver.driver.Pojo;

import com.google.firebase.database.IgnoreExtraProperties;


@IgnoreExtraProperties
public class FcmTrackingPojo {

    public String ride_id_str,aCurrentDate,ridestatus;
    public Double currentLat,currentLon;
    public float bearingValue;

    public FcmTrackingPojo() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public FcmTrackingPojo(String ride_id_str, double currentLat, double currentLon, float bearingValue, String aCurrentDate,String ridestatus) {
        this.ride_id_str = ride_id_str;
        this.currentLat = currentLat;
        this.currentLon = currentLon;
        this.bearingValue = bearingValue;
        this.aCurrentDate = aCurrentDate;
        this.ridestatus = ridestatus;
    }

}
