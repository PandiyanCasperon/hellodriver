package com.hellotaxidriver.pojo;

public class placesearchPojo {

    String citytitle,cityaddress;

    String  place_latitude,place_longitude;

    public String getPlace_latitude() {
        return place_latitude;
    }

    public void setPlace_latitude(String place_latitude) {
        this.place_latitude = place_latitude;
    }

    public String getPlace_longitude() {
        return place_longitude;
    }

    public void setPlace_longitude(String place_longitude) {
        this.place_longitude = place_longitude;
    }

    public String getCitytitle() {
        return citytitle;
    }

    public void setCitytitle(String citytitle) {
        this.citytitle = citytitle;
    }

    public String getCityaddress() {
        return cityaddress;
    }

    public void setCityaddress(String cityaddress) {
        this.cityaddress = cityaddress;
    }
}
