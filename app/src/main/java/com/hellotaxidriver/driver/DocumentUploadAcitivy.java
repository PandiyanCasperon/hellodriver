package com.hellotaxidriver.driver;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hellotaxidriver.driver.Utils.AppController;
import com.hellotaxidriver.driver.Utils.ConnectionDetector;
import com.hellotaxidriver.driver.Utils.SessionManager;
import com.hellotaxidriver.driver.widgets.PkDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.app.service.ServiceConstant;
import com.app.service.VolleyMultipartRequest;
import com.app.sqliteDb.documentdb;

import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import id.zelory.compressor.Compressor;

/**
 * Created by user144 on 8/31/2018.
 */

@SuppressLint("Registered")
public class DocumentUploadAcitivy extends Activity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private static final int PERMISSION_REQUEST_CODE = 100;
    private static final int REQUEST_TAKE_PHOTO = 101;
    private static final int REQUEST_GALLERY_PHOTO = 102;
    private View headerView;
    private RelativeLayout headerBackRl;
    private CollapsingToolbarLayout headerCollapseTL;

    private TextView documentUploadNextTv;
    private ImageView uploadedDocumentIv;
    private TextView documentHasExpire;
    private Button uploadDocumentBtn;
    byte[] byteArray;
    Bitmap selectedBitmap;

    private Dialog dialog;
    SessionManager session;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private String Driver_ID = "", gcmID = "", language_code = "", Agent_Name = "", catname = "", isExpire = "", titleid = "";
    String title = "", description = "", flag = "", StrImage = "";
    HashMap<String, String> user;
    AnimatedVectorDrawableCompat avdProgress;
    static HashMap<String, String> docPic;
    static HashMap<String, String> docPicVehiclehire;
    private Dialog photo_dialog;
    private Uri camera_FileUri;
    private static final String TAG = "documentimageupload";
    private static final String IMAGE_DIRECTORY_NAME = "Cabily_document_image";
    private DatePickerDialog pickerDialog;
    private int year, month, day;
    private String selectedYear, selectedMonth, selectedDay;
    private String selectedDate = "";
    private Calendar calendar;


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            finish();
            CloseKeyBoard();
            overridePendingTransition(R.anim.reg_pull_in_left, R.anim.reg_push_out_right);

            return true;
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document_upload);
        findViews();
        clickEvents();
    }

    private void findViews() {
        headerView = findViewById(R.id.header_registration);
        headerBackRl = (RelativeLayout) headerView.findViewById(R.id.back_layout);
        headerCollapseTL = (CollapsingToolbarLayout) headerView.findViewById(R.id.collapsing_toolbar);
        headerCollapseTL.setTitleEnabled(true);
        headerCollapseTL.setCollapsedTitleTextColor(getResources().getColor(R.color.white_color));
        headerCollapseTL.setTitle(getResources().getString(R.string.documents_upload_header_label));
        headerCollapseTL.setCollapsedTitleTextAppearance(R.style.coll_toolbar_title);
        headerCollapseTL.setExpandedTitleTextAppearance(R.style.exp_toolbar_title);

        session = new SessionManager(getApplicationContext());
        cd = new ConnectionDetector(this);
        isInternetPresent = cd.isConnectingToInternet();

        documentUploadNextTv = (TextView) findViewById(R.id.registration_document_upload_next_tv);
        docPic = new HashMap<>();
        docPicVehiclehire = new HashMap<>();
        user = session.getUserDetails();
        Driver_ID = user.get(SessionManager.KEY_DRIVERID);
        gcmID = user.get(SessionManager.KEY_GCM_ID);
        Agent_Name = user.get(SessionManager.KEY_ID_NAME);
        language_code = user.get(SessionManager.KEY_Language_code);
        uploadDocumentBtn = (Button) findViewById(R.id.reg_mobile_send);
        uploadedDocumentIv = (ImageView) findViewById(R.id.document_upload_iv);
        documentHasExpire = (TextView) findViewById(R.id.has_expire_tv);
        session.setfirsttimeloader("0");

        GetData();
    }

    private void GetData() {
        try {
            flag = getIntent().getStringExtra("flag");
            titleid = getIntent().getStringExtra("titleid");
            isExpire = getIntent().getStringExtra("isExp");
            catname = getIntent().getStringExtra("name");
            uploadDocumentBtn.setText("Upload " + catname);

            if (isExpire.equalsIgnoreCase("yes")) {
                if (uploadDocumentBtn.getText().toString().equalsIgnoreCase("Change Document")) {
                    documentHasExpire.setVisibility(View.VISIBLE);
                    ((ImageView) findViewById(R.id.document_view_iv)).setVisibility(View.VISIBLE);
                }

            } else {
                documentHasExpire.setVisibility(View.GONE);
                ((ImageView) findViewById(R.id.document_view_iv)).setVisibility(View.GONE);

            }
            documentdb mHelper = new documentdb(DocumentUploadAcitivy.this);
            SQLiteDatabase dataBase = mHelper.getWritableDatabase();
            Cursor mCursor = dataBase.rawQuery("SELECT * FROM " + documentdb.TABLE_NAME, null);

            if (mCursor.moveToFirst()) {
                do {
                    String imagepa = mCursor.getString(mCursor.getColumnIndex(documentdb.KEY_IMAGEADDED));
                    String titleIdDb = mCursor.getString(mCursor.getColumnIndex(documentdb.KEY_SAVEID));
                    String selectedDate = mCursor.getString(mCursor.getColumnIndex(documentdb.KEY_EXPIRE_DATE));
                    if (titleid.equalsIgnoreCase(titleIdDb) && !imagepa.equalsIgnoreCase("")) {
                        Picasso.with(getApplicationContext()).load(imagepa).into(uploadedDocumentIv);

                        uploadDocumentBtn.setText("Change Document");
                        if (isExpire.equalsIgnoreCase("yes")) {
                            documentHasExpire.setVisibility(View.VISIBLE);
                        } else {

                            documentHasExpire.setVisibility(View.GONE);
                        }
                        ((ImageView) findViewById(R.id.document_view_iv)).setVisibility(View.VISIBLE);
                        StrImage = imagepa;
                    }
                    if (titleid.equalsIgnoreCase(titleIdDb) && !selectedDate.equalsIgnoreCase("")) {
                        documentHasExpire.setText(selectedDate);
                    }
                } while (mCursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void clickEvents() {
        headerBackRl.setOnClickListener(this);
        uploadDocumentBtn.setOnClickListener(this);
        uploadedDocumentIv.setOnClickListener(this);
        documentUploadNextTv.setOnClickListener(this);
        documentHasExpire.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == uploadDocumentBtn) {
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkAccessFineLocationPermission() || !checkAccessCoarseLocationPermission() || !checkWriteExternalStoragePermission() || !checkREADExternalStoragePermission()) {
                    requestPermission();
                } else {
                    chooseimage();
                }
            } else {
                chooseimage();
            }
        } else if (view == headerBackRl) {
            finish();
            overridePendingTransition(R.anim.reg_pull_in_left, R.anim.reg_push_out_right);
        } else if (view == documentUploadNextTv) {

            if (documentHasExpire.getVisibility() == View.VISIBLE) {
                if (documentHasExpire.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Expiry date is needed", Toast.LENGTH_SHORT).show();
                } else {
                    documentdb mHelper = new documentdb(DocumentUploadAcitivy.this);
                    SQLiteDatabase dataBase = mHelper.getWritableDatabase();
                    ContentValues values = new ContentValues();
                    values.put(documentdb.KEY_EXPIRE_DATE, selectedDate);
                    dataBase.update(documentdb.TABLE_NAME, values, documentdb.KEY_SAVEID + "='" + titleid + "'", null);
                    finish();
                    overridePendingTransition(R.anim.reg_pull_in_left, R.anim.reg_push_out_right);
                }
            } else {
                documentdb mHelper = new documentdb(DocumentUploadAcitivy.this);
                SQLiteDatabase dataBase = mHelper.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(documentdb.KEY_EXPIRE_DATE, selectedDate);
                dataBase.update(documentdb.TABLE_NAME, values, documentdb.KEY_SAVEID + "='" + titleid + "'", null);
                finish();
                overridePendingTransition(R.anim.reg_pull_in_left, R.anim.reg_push_out_right);
            }


        } else if (view == documentHasExpire) {
            ShowDatePickerDialog();
        } else if (view == uploadedDocumentIv) {
            if (uploadDocumentBtn.getText().toString().equalsIgnoreCase("Change Document")) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
//                Uri uri = Uri.parse("file://" + StrImage);
                intent.setDataAndType(Uri.parse(StrImage), "image/*");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
    }

    private void ShowDatePickerDialog() {
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        pickerDialog = DatePickerDialog.newInstance(this, year, month, day);
        pickerDialog.setThemeDark(false);
        pickerDialog.showYearPickerFirst(false);
        pickerDialog.setAccentColor(getResources().getColor(R.color.app_color));
        pickerDialog.setMinDate(calendar);
        pickerDialog.show(getFragmentManager(), "DatePickerDialog");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                try {
                    final String picturePath = camera_FileUri.getPath();
                    File directory = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/milesdocument");
                    if (!directory.exists()) {
                        directory.mkdir();
                    }
                    File[] files = directory.listFiles();
                    for (int i = 0; i < files.length; i++) {
                        files[i].delete();
                    }


                    File curFile = new File(picturePath);
                    File compressedImage = new Compressor(this)
                            .setMaxWidth(640)
                            .setMaxHeight(480)
                            .setQuality(75)
                            .setCompressFormat(Bitmap.CompressFormat.JPEG)
                            .setDestinationDirectoryPath(directory.getAbsolutePath())
                            .compressToFile(curFile);


                    File[] filesnew = directory.listFiles();
                    File newcompressed = null;
                    for (int j = 0; j < filesnew.length; j++) {
                        newcompressed = new File(directory.getAbsolutePath() + "/" + filesnew[j].getName());
                    }
                    Uri picUri = Uri.fromFile(newcompressed);

                    UCrop.Options Uoptions = new UCrop.Options();
                    Uoptions.setStatusBarColor(getResources().getColor(R.color.app_color));
                    Uoptions.setToolbarColor(getResources().getColor(R.color.app_color));
                    Uoptions.setActiveWidgetColor(ContextCompat.getColor(this, R.color.app_color));
                    Uoptions.setCompressionFormat(Bitmap.CompressFormat.JPEG);
                    Uoptions.setCompressionQuality(70);
                    UCrop.of(picUri, picUri)
                            .withMaxResultSize(8000, 8000)
                            .withOptions(Uoptions)
                            .start(DocumentUploadAcitivy.this);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == REQUEST_GALLERY_PHOTO) {
                try {
                    Uri selectedImage = data.getData();
                    if (selectedImage.toString().startsWith("content://com.sec.android.gallery3d.provider")) {
                        String[] filePath = {MediaStore.Images.Media.DATA};
                        Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                        c.moveToFirst();
                        int columnIndex = c.getColumnIndex(filePath[0]);
                        final String picturePath = c.getString(columnIndex);
                        c.close();

                        File directory = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/milesdocument");
                        if (!directory.exists()) {
                            directory.mkdir();
                        }
                        File[] files = directory.listFiles();
                        for (int i = 0; i < files.length; i++) {
                            files[i].delete();
                        }


                        File curFile = new File(picturePath);
                        File compressedImage = new Compressor(this)
                                .setMaxWidth(640)
                                .setMaxHeight(480)
                                .setQuality(75)
                                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                                .setDestinationDirectoryPath(directory.getAbsolutePath())
                                .compressToFile(curFile);


                        File[] filesnew = directory.listFiles();
                        File newcompressed = null;
                        for (int j = 0; j < filesnew.length; j++) {
                            newcompressed = new File(directory.getAbsolutePath() + "/" + filesnew[j].getName());
                        }

                        Uri picUri = Uri.fromFile(newcompressed);

                        UCrop.Options Uoptions = new UCrop.Options();
                        Uoptions.setStatusBarColor(getResources().getColor(R.color.app_color));
                        Uoptions.setToolbarColor(getResources().getColor(R.color.app_color));
                        Uoptions.setActiveWidgetColor(ContextCompat.getColor(this, R.color.app_color));
                        Uoptions.setCompressionFormat(Bitmap.CompressFormat.JPEG);
                        Uoptions.setCompressionQuality(30);

                        UCrop.of(picUri, picUri)
                                .withMaxResultSize(8000, 8000)
                                .withOptions(Uoptions)
                                .start(DocumentUploadAcitivy.this);

                    } else {
                        String[] filePath = {MediaStore.Images.Media.DATA};
                        Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                        c.moveToFirst();
                        int columnIndex = c.getColumnIndex(filePath[0]);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        final String picturePath = c.getString(columnIndex);

                        File directory = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/milesdocument");
                        if (!directory.exists()) {
                            directory.mkdir();
                        }
                        File[] files = directory.listFiles();
                        for (int i = 0; i < files.length; i++) {
                            files[i].delete();
                        }


                        File curFile = new File(picturePath);
                        File compressedImage = new Compressor(this)
                                .setMaxWidth(640)
                                .setMaxHeight(480)
                                .setQuality(75)
                                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                                .setDestinationDirectoryPath(directory.getAbsolutePath())
                                .compressToFile(curFile);


                        File[] filesnew = directory.listFiles();
                        File newcompressed = null;
                        for (int j = 0; j < filesnew.length; j++) {
                            newcompressed = new File(directory.getAbsolutePath() + "/" + filesnew[j].getName());
                        }

                        Uri picUri = Uri.fromFile(newcompressed);
                        UCrop.Options Uoptions = new UCrop.Options();
                        Uoptions.setStatusBarColor(getResources().getColor(R.color.app_color));
                        Uoptions.setToolbarColor(getResources().getColor(R.color.app_color));
                        Uoptions.setActiveWidgetColor(ContextCompat.getColor(this, R.color.app_color));


                        UCrop.of(picUri, picUri)
                                .withMaxResultSize(8000, 8000)
                                .withOptions(Uoptions)
                                .start(DocumentUploadAcitivy.this);


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == UCrop.REQUEST_CROP) {

                final Uri resultUri = UCrop.getOutput(data);
                try {
                    selectedBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                byteArray = byteArrayOutputStream.toByteArray();
                cd = new ConnectionDetector(this);
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {
                    Driver_ID = user.get(SessionManager.KEY_DRIVERID);
                    gcmID = user.get(SessionManager.KEY_GCM_ID);
                    Agent_Name = user.get(SessionManager.KEY_ID_NAME);
                    language_code = user.get(SessionManager.KEY_Language_code);
//                        user_image = user.get(SessionManager.KEY_USERNAME);
                    if (flag.equalsIgnoreCase("3")) {
                        UploadResitrationImage(ServiceConstant.RegistrationDocumentUpload);
                    }
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                }

            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            System.out.println("========muruga cropError===========" + cropError);
        }

    }


    private void UploadResitrationImage(String url) {

        dialog = new Dialog(DocumentUploadAcitivy.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        final TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

                System.out.println("------------- UploadResitrationImage response-----------------" + response.data);

                String resultResponse = new String(response.data);
                System.out.println("-------------  UploadResitrationImage-----------------" + resultResponse);
                String sStatus = "", sResponse = "", Smsg = "", documentName = "";
                try {
                    JSONObject jsonObject = new JSONObject(resultResponse);
                    sStatus = jsonObject.getString("status");
                    if (sStatus.equalsIgnoreCase("1")) {
                        documentName = jsonObject.getString("document_name");
                        sResponse = jsonObject.getString("response");
                        if (jsonObject.has("document_path")) {
                            Smsg = jsonObject.getString("document_path");
                            Picasso.with(DocumentUploadAcitivy.this).load(Smsg).placeholder(R.drawable.file_upload_placeholder).into(uploadedDocumentIv);
                            uploadDocumentBtn.setText("Change Document");
                            if (isExpire.equalsIgnoreCase("yes")) {
                                documentHasExpire.setVisibility(View.VISIBLE);
                            } else {

                                documentHasExpire.setVisibility(View.GONE);
                            }

                            ((ImageView) findViewById(R.id.document_view_iv)).setVisibility(View.VISIBLE);
                            StrImage = Smsg;
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.edit_profile_success_label), Toast.LENGTH_SHORT).show();
                            if (flag.equalsIgnoreCase("3")) {
                                documentdb mHelper = new documentdb(DocumentUploadAcitivy.this);
                                SQLiteDatabase dataBase = mHelper.getWritableDatabase();
                                ContentValues values = new ContentValues();
                                values.put(documentdb.KEY_IMAGEADDED, Smsg);
                                values.put(documentdb.KEY_IMAGESNAME, documentName);
                                dataBase.update(documentdb.TABLE_NAME, values, documentdb.KEY_SAVEID + "='" + titleid + "'", null);

                            }
                        }
                    } else {
                        sResponse = jsonObject.getString("response");
                        Toast.makeText(getApplicationContext(), sResponse, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                System.out.println("------------Authkey------cabily---------" + Agent_Name);
                System.out.println("------------userid----------cabily-----" + Driver_ID);
                System.out.println("------------apptoken----------cabily-----" + gcmID);
                System.out.println("------------applanguage----------cabily-----" + language_code);
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authkey", Agent_Name);
                headers.put("isapplication", ServiceConstant.isapplication);
                headers.put("applanguage", language_code);
                headers.put("apptype", ServiceConstant.cabily_AppType);
                headers.put("driverid", Driver_ID);
                headers.put("apptoken", gcmID);

                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("document", new DataPart("document.jpeg" /*+ extension.trim()*/, byteArray, "image/jpeg"/*+extension.trim().replace(".","")*/));
                return params;
            }
        };

        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        multipartRequest.setRetryPolicy(retryPolicy);
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        multipartRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(multipartRequest);

    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(DocumentUploadAcitivy.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.alert_label_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    // --------------------Method for choose image to edit profileimage--------------------
    private void chooseimage() {
        photo_dialog = new Dialog(DocumentUploadAcitivy.this);
        photo_dialog.getWindow();
        photo_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        photo_dialog.setContentView(R.layout.image_upload_dialog);
        photo_dialog.setCanceledOnTouchOutside(true);
        photo_dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_photo_Picker;
        photo_dialog.show();
        photo_dialog.getWindow().setGravity(Gravity.CENTER);

        RelativeLayout camera = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromcamera);
        RelativeLayout gallery = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromgallery);

        TextView dialog_head = (TextView) photo_dialog.findViewById(R.id.howtotake);
        dialog_head.setText(getResources().getString(R.string.login_details_takedocument));

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
                photo_dialog.dismiss();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
                photo_dialog.dismiss();
            }
        });
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        camera_FileUri = getOutputMediaFileUri(1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, camera_FileUri);
        // start the image capture Intent
        startActivityForResult(intent, REQUEST_TAKE_PHOTO);
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQUEST_GALLERY_PHOTO);
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    private boolean checkAccessFineLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkAccessCoarseLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkREADExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String selectDay = String.valueOf(dayOfMonth);
        String selectMonth = String.valueOf(monthOfYear + 1);
        selectedYear = String.valueOf(year);
        if (selectMonth.length() > 1) {
            selectedMonth = selectMonth;
        } else {
            selectedMonth = 0 + "" + selectMonth;
        }

        if (selectDay.length() > 1) {
            selectedDay = selectDay;
        } else {
            selectedDay = 0 + "" + selectDay;
        }
        String selectDate = selectedDay + "-" + selectedMonth + "-" + selectedYear;
        selectedDate = selectedMonth + "-" + selectedDay + "-" + selectedYear;
        documentHasExpire.setText(selectDate);
    }

    private void CloseKeyBoard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

    }

}
