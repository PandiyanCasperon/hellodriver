package com.hellotaxidriver.driver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * Created by user144 on 8/31/2018.
 */

@SuppressLint("Registered")
public class UploadPhoto extends Activity {
    private static String TAG = "fast";
    final int PERMISSION_REQUEST_CODE = 111;
    private Dialog photo_dialog;
    Bitmap bitMapThumbnail;
    private byte[] byteArray;
    BroadcastReceiver logoutReciver;
    private int REQUEST_TAKE_PHOTO = 1;
    private int galleryRequestCode = 2;
    private Uri camera_FileUri;
    private static final String IMAGE_DIRECTORY_NAME = "FastDrive_image";
    private Bitmap selectedBitmap;
    private String mSelectedFilePath = "";


    private File temp_path;
    private final int COMPRESS = 100;
    String picturePath = "";
    Bitmap bitmap;
    Bitmap thumbnail = null;
    ByteArrayOutputStream byteArrayOutputStream = null;
    Cursor c = null;
    private String ImageUri = "";

    public UploadPhoto() {


    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.takephoto_dummy);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        UploadAndTakePhote();
    }

    public void UploadAndTakePhote() {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkAccessCoarseLocationPermission() || !checkWriteExternalStoragePermission()) {
                requestPermission();
            } else {
                chooseimage();
            }
        } else {
            chooseimage();
        }
    }

    private void chooseimage() {
        photo_dialog = new Dialog(UploadPhoto.this);
        photo_dialog.getWindow();
        photo_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        photo_dialog.setContentView(R.layout.image_upload_dialog);
        photo_dialog.setCancelable(true);
        photo_dialog.setCanceledOnTouchOutside(false);
        photo_dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_photo_Picker;
        photo_dialog.getWindow().setGravity(Gravity.CENTER);
        photo_dialog.show();


        RelativeLayout camera = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromcamera);
        RelativeLayout gallery = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromgallery);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
                if (photo_dialog != null) {
                    photo_dialog.dismiss();
                }
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
                if (photo_dialog != null) {
                    photo_dialog.dismiss();
                }
            }
        });
        photo_dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (photo_dialog != null) {
                    photo_dialog.dismiss();
                }
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }

        });
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        camera_FileUri = getOutputMediaFileUri(1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, camera_FileUri);
        startActivityForResult(intent, REQUEST_TAKE_PHOTO);
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, galleryRequestCode);
    }

    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {
        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("file_uri", camera_FileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        camera_FileUri = savedInstanceState.getParcelable("file_uri");
    }


    private boolean checkAccessCoarseLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    chooseimage();
                } else {
                    finish();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO || requestCode == UCrop.REQUEST_CROP) {
                try {
                    if (requestCode == REQUEST_TAKE_PHOTO) {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 8;

                        final Bitmap bitmap = BitmapFactory.decodeFile(camera_FileUri.getPath(), options);
                        Bitmap thumbnail = bitmap;
                        final String picturePath = camera_FileUri.getPath();
//                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

                        File curFile = new File(picturePath);
                        try {
                            ExifInterface exif = new ExifInterface(curFile.getPath());
                            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                            int rotationInDegrees = exifToDegrees(rotation);

                            Matrix matrix = new Matrix();
                            if (rotation != 0f) {
                                matrix.preRotate(rotationInDegrees);
                            }
                            //    thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
                        } catch (IOException ex) {
                            Log.e("TAG", "Failed to get Exif data", ex);
                        }
//                        System.out.println("edit-----" + ServiceConstant.Edit_profile_image_url);

                        Uri picUri = Uri.fromFile(curFile);

                        UCrop.Options Uoptions = new UCrop.Options();
                        Uoptions.setStatusBarColor(getResources().getColor(R.color.app_color));
                        Uoptions.setToolbarColor(getResources().getColor(R.color.app_color));
                        Uoptions.setActiveWidgetColor(ContextCompat.getColor(this, R.color.app_color));


                        UCrop.of(picUri, picUri)
                                .withAspectRatio(4, 4)
                                .withMaxResultSize(8000, 8000)
                                .start(UploadPhoto.this);

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == galleryRequestCode) {

                Uri selectedImage = data.getData();
                if (selectedImage.toString().startsWith("content://com.sec.android.gallery3d.provider")) {
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    final String picturePath = c.getString(columnIndex);
                    c.close();
                    File curFile = new File(picturePath);

                    Picasso.with(UploadPhoto.this).load(picturePath).resize(100, 100).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            Bitmap thumbnail = bitmap;
                            mSelectedFilePath = picturePath;
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);

                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                        }
                    });


                    Uri picUri = Uri.fromFile(curFile);

                    UCrop.Options Uoptions = new UCrop.Options();
                    Uoptions.setStatusBarColor(getResources().getColor(R.color.app_color));
                    Uoptions.setToolbarColor(getResources().getColor(R.color.app_color));
                    Uoptions.setActiveWidgetColor(ContextCompat.getColor(this, R.color.app_color));
                    Uoptions.setCropGridColor(getResources().getColor(R.color.app_color));
                    Uoptions.setCropFrameColor(getResources().getColor(R.color.app_color));

                    UCrop.of(picUri, picUri)
                            .withAspectRatio(4, 4)
                            .withMaxResultSize(8000, 8000)
                            .start(UploadPhoto.this);

                } else {

                    if (Build.VERSION.SDK_INT < 19) {
                        String[] filePath = {MediaStore.Images.Media.DATA};
                        c = getContentResolver().query(selectedImage, filePath, null, null, null);
                        c.moveToFirst();
                        int columnIndex = c.getColumnIndex(filePath[0]);
                        picturePath = c.getString(columnIndex);
                        bitmap = BitmapFactory.decodeFile(picturePath);
                        thumbnail = bitmap; //getResizedBitmap(bitmap, 600);
                        byteArrayOutputStream = new ByteArrayOutputStream();
                        c.close();
                    } else {
                        try {
                            InputStream imInputStream = getContentResolver().openInputStream(data.getData());
                            bitmap = BitmapFactory.decodeStream(imInputStream);
                            picturePath = saveGalaryImageOnLitkat(bitmap);

                            if (bitmap != null) {
                                bitmap.recycle();
                                bitmap = null;
                            }

                            bitmap = BitmapFactory.decodeFile(picturePath);
                            thumbnail = bitmap; //getResizedBitmap(bitmap, 600);
                            byteArrayOutputStream = new ByteArrayOutputStream();

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                    File curFile = null;

                    try {
                        curFile = new File(picturePath);
                        ExifInterface exif = new ExifInterface(curFile.getPath());
                        int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                        int rotationInDegrees = exifToDegrees(rotation);

                        Matrix matrix = new Matrix();
                        if (rotation != 0f) {
                            matrix.preRotate(rotationInDegrees);
                        }
                        thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
                    } catch (IOException ex) {
                        Log.e("TAG", "Failed to get Exif data", ex);
                    }
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                    //byteArray = byteArrayOutputStream.toByteArray();

                    Uri picUri;
                    picUri = Uri.fromFile(curFile);
                    UCrop.Options Uoptions = new UCrop.Options();
                    Uoptions.setStatusBarColor(getResources().getColor(R.color.app_color));
                    Uoptions.setToolbarColor(getResources().getColor(R.color.app_color));
                    Uoptions.setActiveWidgetColor(ContextCompat.getColor(this, R.color.app_color));
                    Uoptions.setCropGridColor(getResources().getColor(R.color.app_color));
                    Uoptions.setCropFrameColor(getResources().getColor(R.color.app_color));

                    UCrop.of(picUri, picUri)
                            .withAspectRatio(4, 4)
                            .withMaxResultSize(8000, 8000)
                            .start(UploadPhoto.this);


                }
            }


            if (requestCode == UCrop.REQUEST_CROP) {

                final Uri resultUri = UCrop.getOutput(data);
                try {
                    selectedBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                byteArray = byteArrayOutputStream.toByteArray();
                ImageUri = resultUri.toString();
                Intent returnIntent = new Intent();
                returnIntent.putExtra("imageByte", resultUri.toString());
                setResult(RESULT_OK, returnIntent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            } else if (resultCode == UCrop.RESULT_ERROR) {
                final Throwable cropError = UCrop.getError(data);
                System.out.println("========muruga cropError===========" + cropError);
            }


        } else {
            this.finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }


    }


    private String saveGalaryImageOnLitkat(Bitmap bitmap) {
        try {
            File cacheDir;
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
                cacheDir = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.app_name));
            else
                cacheDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            if (!cacheDir.exists())
                cacheDir.mkdirs();
            String filename = System.currentTimeMillis() + ".jpg";
            File file = new File(cacheDir, filename);
            temp_path = file.getAbsoluteFile();
            // if(!file.exists())
            file.createNewFile();
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, COMPRESS, out);
            return file.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);
        switch (keyCode) {

            case KeyEvent.KEYCODE_BACK:
                if (photo_dialog != null) {
                    photo_dialog.dismiss();
                }
                System.out.println("===destroyActivity====2");

                this.finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                return true;
        }

        return false;
    }

}