package com.hellotaxidriver.driver;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.service.ServiceConstant;
import com.app.service.ServiceRequest;
import com.hellotaxidriver.driver.Pojo.UserPojo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class waitingservice extends JobService {

    private Handler mHandler = new Handler();
    public static final long INTERVAL = 1000;
    int secondsstart = 0;
    Double comingsecond = 15.0;
    int hittdurl=0;
    String free_wait_time="";
    private Timer mTimer = null;
    String rideid;


    @Override
    public boolean onStartJob(JobParameters jobParameters)
    {
        return true;
    }

    @Override
    public void onCreate()
    {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        if (intent != null) {
            rideid = intent.getStringExtra("rideid");
            free_wait_time = intent.getStringExtra("free_wait_time");
            if(free_wait_time.equals(""))
            {
                comingsecond = 60.0;
            }
            else
            {
                comingsecond=(Double.parseDouble(free_wait_time)*60);
            }
            mTimer = new Timer(); // recreate new timer
            mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, INTERVAL);
        }
        return START_STICKY;
    }


    @Override
    public boolean onStopJob(JobParameters jobParameters)
    {
        if (mTimer != null)
        {
            mTimer.cancel();
        }
        stopSelf();
        return false;
    }




    @Override
    public void onRebind(Intent intent) {
        // Called when a client (MainActivity in case of this sample) returns to the foreground
        // and binds once again with this service. The service should cease to be a foreground
        // service when that happens.

        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {


        return true; // Ensures onRebind() is called when a client re-binds.
    }

    @Override
    public void onDestroy() {
        if (mTimer != null)
        {
            mTimer.cancel();
        }
    stopSelf();
    }

    private class TimeDisplayTimerTask extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @TargetApi(Build.VERSION_CODES.O)
                @Override
                public void run() {
                        secondsstart++;
                        if(secondsstart > comingsecond)
                        {
                            if (mTimer != null)
                            {
                                mTimer.cancel();
                            }
                            Toast.makeText(getApplicationContext(),"Waiting time notification alert sent.",Toast.LENGTH_LONG).show();
                            setServiceResponse(ServiceConstant.HITnotificationsendurl);
                        }

                }
            });
        }
    }



    public void setServiceResponse(String url)
    {
        ArrayList<UserPojo> userlist = new ArrayList<UserPojo>();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", rideid);
        ServiceRequest mRequest = new ServiceRequest(getApplicationContext());
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("sent"+response);
                try {
                    JSONObject object = new JSONObject(response);
                    String Str_status = object.getString("status");
                    if(Str_status.equals("1"))
                    {
                        stopSelf();
                    }
                    else
                    {
                        hittdurl++;
                        if(hittdurl > 4)
                        {
                            stopSelf();
                        }
                        else
                        {
                            setServiceResponse(ServiceConstant.HITnotificationsendurl);
                        }
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                    hittdurl++;
                    if(hittdurl > 4)
                    {
                        stopSelf();
                    }
                    else
                    {
                        setServiceResponse(ServiceConstant.HITnotificationsendurl);
                    }
                }
            }

            @Override
            public void onErrorListener() {
                hittdurl++;
                if(hittdurl > 4)
                {
                    stopSelf();
                }
                else
                {
                    setServiceResponse(ServiceConstant.HITnotificationsendurl);
                }
            }
        });

    }

}