package com.hellotaxidriver.driver;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.service.ServiceConstant;
import com.app.service.ServiceManager;
import com.app.service.ServiceRequest;
import com.hellotaxidriver.driver.Utils.ConnectionDetector;
import com.hellotaxidriver.driver.Utils.Extras;
import com.hellotaxidriver.driver.Utils.SessionManager;
import com.hellotaxidriver.driver.adapter.HomeMenuListAdapter;
import com.hellotaxidriver.driver.tripdetailspackage.tripsummarydetails;
import com.hellotaxidriver.driver.widgets.PkDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Prem Kumar and Anitha on 11/17/2016.
 */

public class NavigationDrawerNew extends AppCompatActivity {
    public static boolean sPushType = false;
    ActionBarDrawerToggle actionBarDrawerToggle;
    String currentVersion;
    private DrawerLayout drawerLayout;
    private RelativeLayout mDrawer;
    private HomeMenuListAdapter mMenuAdapter;
    private Context context;
    private ListView mDrawerList;
    private Extras extras;
    private String[] title;
    private int[] icon;
    private String Language_code = "";
    private boolean isAppInfoAvailable = false;
    private Dialog dialog;
    private SessionManager session;
    private ServiceRequest mRequest;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private TextView tv_version;
    private String sType = "", Trip_type = "", move_option = "";
    public static final int REQUEST_CODE_PERMISSIONS = 101;

    private ServiceManager.ServiceListener updateAvailablityServiceListener = new ServiceManager.ServiceListener() {
        @Override
        public void onCompleteListener(String object) {
            dismissDialog();
            postRequest_applaunch(ServiceConstant.app_launching_url);
        }

        @Override
        public void onErrorListener(Object error) {
            dismissDialog();
        }
    };

    public void openDrawer() {

        drawerLayout.openDrawer(mDrawer);
    }

    public void disableSwipeDrawer() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public void navigationNotifyChange() {
        mMenuAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_drawer);
        context = getApplicationContext();
        session = new SessionManager(NavigationDrawerNew.this);
        extras = new Extras(getApplicationContext());


        if (!extras.checkAccessFineLocationPermission()) {

            AlertPermission();

        } else {

        }

      /*  if (Build.VERSION.SDK_INT >= 21) {
            if (!extras.checkAccessFineLocationPermission() || !extras.checkAccessCoarseLocationPermission()) {

                AlertPermission();

            } else {
                //apiHit();
            }*/

      //  requestLocationPermission();

/*        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN |
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_FULLSCREEN |
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);*/

        drawerLayout = (DrawerLayout) findViewById(R.id.navigation_drawer);
        mDrawer = (RelativeLayout) findViewById(R.id.drawer);
        mDrawerList = (ListView) findViewById(R.id.drawer_listview);

        tv_version = (TextView) findViewById(R.id.verion_no);
        try {
            currentVersion = NavigationDrawerNew.this.getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        tv_version.setText("Version " + currentVersion);


        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("type")) {
                sType = intent.getStringExtra("type");
                if ("push".equalsIgnoreCase(sType)) {
                    sPushType = true;
                } else {
                    sPushType = false;
                }
            }

            if (intent.hasExtra("Spage")) {
                Trip_type = intent.getStringExtra("Spage");
            }


        }

        HashMap<String, String> move = session.getmoveupcomming();
//        move_option = move.get(SessionManager.KEY_MOVE);

     /*   if (move_option != null) {
            if (move_option.equals("1")) {
                FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
                tx.add(R.id.content_frame, new TripSummeryList());
                tx.addToBackStack(null);
                tx.commit();
            } else {
                FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
                tx.add(R.id.content_frame, new DashBoardDriver());
                tx.addToBackStack(null);
                tx.commit();
            }
        } else {
            FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
            tx.add(R.id.content_frame, new DashBoardDriver());
            tx.addToBackStack(null);
            tx.commit();
        }*/


         if (savedInstanceState == null) {
        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        tx.add(R.id.content_frame, new DashBoardDriver());
        tx.addToBackStack(null);
        tx.commit();
          }

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.app_name, R.string.app_name);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();


        title = new String[]{"username", getResources().getString(R.string.navigationDrawer_label_home),
                getResources().getString(R.string.navigationDrawer_label_tripSummary),
                getResources().getString(R.string.navigationDrawer_label_backAccount),
                getResources().getString(R.string.navigationDrawer_label_paymentDetail),
                getResources().getString(R.string.navigationDrawer_label_changePassword),
                getResources().getString(R.string.navgation_drawer_change_language),
                getResources().getString(R.string.navigation_label_Feedback),
                getResources().getString(R.string.navigation_label_aboutus),
                getResources().getString(R.string.navigationDrawer_label_logout),
        };

        icon = new int[]{R.drawable.placeholder_icon, R.drawable.home,
                R.drawable.profile, R.drawable.card,
                R.drawable.payment, R.drawable.pass_new, R.drawable.changelanguage, R.drawable.report, R.drawable.about_us, R.drawable.logout_new};
        if (mMenuAdapter == null) {
            mMenuAdapter = new HomeMenuListAdapter(context, title, icon);
            mDrawerList.setAdapter(mMenuAdapter);
            mMenuAdapter.notifyDataSetChanged();
        } else {
            mMenuAdapter.notifyDataSetChanged();

        }


        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                try {
                    cd = new ConnectionDetector(NavigationDrawerNew.this);
                    isInternetPresent = cd.isConnectingToInternet();

                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

                    switch (position) {

                        case 0:
                            break;
                        case 1:
                            ft.add(R.id.content_frame, new DashBoardDriver());
                            break;
                        case 2:
                            ft.add(R.id.content_frame, new TripSummeryList());

                            break;
                        case 3:
                            ft.add(R.id.content_frame, new BankDetails());
                            break;
                        case 4:
//                        ft.replace(R.id.content_frame, new PaymentDetails());
                      /*  Intent intent = new Intent(NavigationDrawerNew.this, tripsummarydetails.class);
                        startActivity(intent);*/
                            ft.add(R.id.content_frame, new tripsummarydetails());
                            break;
                        case 5:
                            ft.add(R.id.content_frame, new ChangePassWord());
                            break;
                        case 6:
                            ft.add(R.id.content_frame, new SettingsLanguageChange());
                            break;
                        case 7:
                       /* Intent feedback_intent = new Intent(NavigationDrawerNew.this, FeedBackPage.class);
                        startActivity(feedback_intent);*/
                            ft.add(R.id.content_frame, new FeedBackPage());

                            break;
                        case 8:
                      /*  Intent about_intent = new Intent(NavigationDrawerNew.this, AboutUs.class);
                        startActivity(about_intent);*/

                            ft.add(R.id.content_frame, new AboutUs());

                            break;
                        case 9:

                            showBackPressedDialog(true);
                            break;

                    }

                    ft.addToBackStack(null);
                    ft.commit();
                    mDrawerList.setItemChecked(position, true);
                    drawerLayout.closeDrawer(mDrawer);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
//        DemoSyncJob.scheduleJob(getApplicationContext());
    }

    public boolean checkAccessFineLocationPermission() {
        int result = ContextCompat.checkSelfPermission(NavigationDrawerNew.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void AlertPermission() {

 final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle("Alert Permission");
        builder.setMessage("this app collects location data to share my current location with user even when the app is closed or not in use.Click enable to allow location permission cancel to close..");
        builder.setPositiveButton("Enable",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                        requestLocationPermission();
                        // DO TASK
                    }
                });
        builder.setNegativeButton("Close",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                        // DO TASK
                    }
                });

        builder.show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
            /*int id = item.getItemId();

	        //noinspection SimplifiableIfStatement
	        if (id == R.id.action_settings) {
	            return true;
	        }*/

        return super.onOptionsItemSelected(item);
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(NavigationDrawerNew.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.alert_label_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        int fragmentCount = getSupportFragmentManager().getBackStackEntryCount();
        if (fragmentCount <= 1)
            showBackPressedDialog(false);
        else
            getSupportFragmentManager().popBackStack();
    }

    private void changeFragment(Fragment targetFragment) {

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    private void showBackPressedDialog(final boolean isLogout) {

        System.gc();
        if (isLogout) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.dialog_app_exit)
                    .setPositiveButton(getResources().getString(R.string.alert_label_ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (isLogout) {
                                logout();
                            }


                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.navigation_label_cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
            Dialog dialog = builder.create();
            dialog.show();


        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.dialog_app_exiting)
                    .setPositiveButton(getResources().getString(R.string.alert_label_ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();

                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.navigation_label_cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
            Dialog dialog = builder.create();
            dialog.show();
        }


    }

    private void logout() {
        showDialog("Logout");
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        HashMap<String, String> userDetails = session.getUserDetails();
        String driverId = userDetails.get("driverid");
        jsonParams.put("driver_id", "" + driverId);
        jsonParams.put("device", "" + "ANDROID");
        ServiceManager manager = new ServiceManager(this, updateAvailablityServiceListener);
        manager.makeServiceRequest(ServiceConstant.LOGOUT_REQUEST, Request.Method.POST, jsonParams);
    }

    private void postRequest_applaunch(String Url) {

        System.out.println("-------------Splash App Information Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_type", "driver");
        jsonParams.put("id", "");
        mRequest = new ServiceRequest(NavigationDrawerNew.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------Splash App Information Response----------------" + response);
                String Str_status = "", completed_rides_str = "", cancelled_rides_str = "", sContact_mail = "", sCustomerServiceNumber = "", sSiteUrl = "", sXmppHostUrl = "", sHostName = "", sFacebookId = "", sGooglePlusId = "", sPhoneMasking = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            JSONObject info_object = response_object.getJSONObject("info");
                            if (info_object.length() > 0) {
                                sContact_mail = info_object.getString("site_contact_mail");
                                sCustomerServiceNumber = info_object.getString("customer_service_number");
                                sSiteUrl = info_object.getString("site_url");
                                sXmppHostUrl = info_object.getString("xmpp_host_url");
                                sHostName = info_object.getString("xmpp_host_name");
                                Language_code = info_object.getString("lang_code");
                                completed_rides_str = info_object.getString("completed");
                                cancelled_rides_str = info_object.getString("cancelled");
                                int trips_all = Integer.parseInt(completed_rides_str + cancelled_rides_str);
                                session.setTripsAll(trips_all);
                                isAppInfoAvailable = true;
                            } else {
                                isAppInfoAvailable = false;
                            }
                        } else {
                            isAppInfoAvailable = false;
                        }
                    } else {
                        isAppInfoAvailable = false;
                    }
                    if (Str_status.equalsIgnoreCase("1") && isAppInfoAvailable) {
                        Locale locale = null;
                        switch (Language_code) {
                            case "en":
                                locale = new Locale("en");
                                session.setlamguage("en", "en");
                                break;
                            case "es":
                                locale = new Locale("es");
                                session.setlamguage("es", "es");
                                break;

                            default:
                                locale = new Locale("en");
                                session.setlamguage("en", "en");
                                break;
                        }
                        session.setApplogout("logout");

                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;
                        getApplicationContext().getResources().updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());
                        session.logoutUser();
                        finish();


                    } else {
                        Toast.makeText(NavigationDrawerNew.this, "BAD URL", Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {
                Toast.makeText(NavigationDrawerNew.this, ServiceConstant.baseurl, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showDialog(String message) {
        dialog = new Dialog(this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void dismissDialog() {
        try {
            if (dialog != null)
                dialog.dismiss();
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }

    }

    private void requestLocationPermission() {
        boolean foreground = ActivityCompat.checkSelfPermission(NavigationDrawerNew.this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        if (foreground) {
            boolean background = ActivityCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED;
            if (background) {
                handleLocationUpdates();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_BACKGROUND_LOCATION}, REQUEST_CODE_PERMISSIONS);
            }
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION}, REQUEST_CODE_PERMISSIONS);
        }
    }

    private void handleLocationUpdates() {
        //foreground and background
//        Toast.makeText(getApplicationContext(),"Start Foreground and Background Location Updates",Toast.LENGTH_SHORT).show();
    }

    private void handleForegroundLocationUpdates() {
        //handleForeground Location Updates
//        Toast.makeText(getApplicationContext(),"Start foreground location updates",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSIONS) {

            boolean foreground = false, background = false;

            for (int i = 0; i < permissions.length; i++) {
                if (permissions[i].equalsIgnoreCase(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    //foreground permission allowed
                    if (grantResults[i] >= 0) {
                        foreground = true;
                        Toast.makeText(getApplicationContext(), "Foreground location permission allowed", Toast.LENGTH_SHORT).show();
                        continue;
                    } else {
                        Toast.makeText(getApplicationContext(), "Location Permission denied", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }

                if (permissions[i].equalsIgnoreCase(Manifest.permission.ACCESS_BACKGROUND_LOCATION)) {
                    if (grantResults[i] >= 0) {
                        foreground = true;
                        background = true;
                        Toast.makeText(getApplicationContext(), "Background location location permission allowed", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Background location location permission denied", Toast.LENGTH_SHORT).show();
                    }

                }
            }

            if (foreground) {
                if (background) {
                    handleLocationUpdates();
                } else {
                    handleForegroundLocationUpdates();
                }
            }
        }
    }


}

