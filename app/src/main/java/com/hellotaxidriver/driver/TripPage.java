package com.hellotaxidriver.driver;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.latlnginterpolation.LatLngInterpolator;
import com.app.latlnginterpolation.MarkerAnimation;
import com.app.service.ServiceConstant;
import com.app.service.ServiceRequest;
import com.app.sqliteDb.ChatDatabaseHelper;
import com.hellotaxidriver.driver.Helper.GEODBHelper;
import com.hellotaxidriver.driver.Pojo.FarePojo;
import com.hellotaxidriver.driver.Pojo.MultipleLatLongPojo;
import com.hellotaxidriver.driver.Pojo.PoolRateCard;
import com.hellotaxidriver.driver.Pojo.UserPojo;
import com.hellotaxidriver.driver.Utils.ConnectionDetector;
import com.hellotaxidriver.driver.Utils.CurrencySymbolConverter;
import com.hellotaxidriver.driver.Utils.GPSTracker;
import com.hellotaxidriver.driver.Utils.GoogleNavigationService;
import com.hellotaxidriver.driver.Utils.HorizontalListView;
import com.hellotaxidriver.driver.Utils.RoundedImageView;
import com.hellotaxidriver.driver.Utils.SessionManager;
import com.hellotaxidriver.driver.adapter.FareAdaptewr;
import com.hellotaxidriver.driver.adapter.SelectSeatAdapter;
import com.hellotaxidriver.driver.adapter.UserListAdapter;
import com.hellotaxidriver.driver.foregroundservice.FcmTrackingForgroundServices;
import com.hellotaxidriver.driver.foregroundservice.GpsTripForgroundServices;
import com.hellotaxidriver.driver.foregroundservice.JobLocationUpdateForgroundService;
import com.hellotaxidriver.driver.foregroundservice.constant.Constant;
import com.hellotaxidriver.driver.foregroundservice.model.DataTripPage;
import com.hellotaxidriver.driver.googlemappath.GMapV2GetRouteDirection;
import com.hellotaxidriver.driver.mylocation.MyLocationGoogleMap;
import com.hellotaxidriver.driver.mylocation.sharelocation;
import com.hellotaxidriver.driver.subclass.SubclassActivity;
import com.hellotaxidriver.driver.tripdetailspackage.localdb.TripDetailsDbHelper;
import com.hellotaxidriver.driver.tripdetailspackage.ridesummarydetail;
import com.hellotaxidriver.driver.widgets.PkDialog;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerButton;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.jivesoftware.smack.chat.Chat;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import me.drakeet.materialdialog.MaterialDialog;

import static com.hellotaxidriver.driver.NewTripAlert.mediaPlayer1;

/**
 * Created by jayachandran on 7/6/2017.
 */

public class TripPage extends SubclassActivity implements sharelocation,LocationListener, GpsStatus.Listener, SeekBar.OnSeekBarChangeListener, GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnCameraMoveListener,
        GoogleMap.OnCameraMoveCanceledListener, GoogleMap.OnCameraIdleListener {

    //-------------------------------------------------Start and stop Location update foreground service Arun
    private static DataTripPage data;
    Timer timer;
    TimerTask timerTask;

    //we are going to use a handler to be able to run in our TimerTask
    final Handler handler = new Handler();
    private DataTripPage.onGpsServiceUpdate onGpsServiceUpdate;
    private SharedPreferences sharedPreferences;
    private String distance_trip = "", speed_trip = "";
    int  trackstaarted = 0;
    private LocationManager mLocationManager;
    private Location changelatLocations;
    String location_long = "", location_lat = "";
    Boolean isRun = false;
    MapFragment mapFragment;
    private LatLngBounds bounds;
    String free_wait_time = "";
    int jk = 1;
    private int reRouteCoute = 0;
    //-------------------------------------------------Start and stop Location update foreground service end Arun
    public static final int REQUEST_CODE_PERMISSIONS = 101;
    //----------------------------Local Db Start
    TripDetailsDbHelper tripDetailsDbHelper;

    //----------------------------Local Db end

    private LatLngBounds.Builder routePointBuilder;
    private EditText tollChargeEt, parkingChargeEt,SuitcaseEt;
    private String currency = "";

    private RefreshReceiver finishReceiver;
    private LocationRequest mLocationRequest;
    public static Location myLocation;
    private SessionManager session;
    GMapV2GetRouteDirection v2GetRouteDirection;
    LatLng fromPosition;
    LatLng toPosition;
    MarkerOptions markerOptions;
    Location location;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private GoogleMap googleMap;
    private GPSTracker gps;
    private RelativeLayout alert_layout, Rl_traffic;
    PendingResult<LocationSettingsResult> result;
    private Marker pickupmarker, dropmarker;
    MarkerOptions marker;
    private GEODBHelper myDBHelper;
    String traffic_status;
    String base64;
    Bitmap bmp;
    ImageButton traffic_button;

    private String droplocation[];
    private String startlocation[];
    private LatLng destlatlng, startlatlng;
    private double MyCurrent_lat = 0.0, MyCurrent_long = 0.0;
    private float distance_to = 0;
    private LatLng newLatLng, oldLatLng;

    double myMovingDistance = 0.0;
    private boolean isPathShowing;

    SeekBar sliderSeekBar;
    ShimmerButton Bt_slider;
    Shimmer shimmer;
    CardView destination_address_layout, driverDetail;
    private ServiceRequest mRequest;
    private Dialog dialog;
    private String driver_id = "", Str_Latitude = "", Str_longitude = "", active_ride = "", Str_user_phone_no = "", toll_park_status = "", suitcase_fee = "";
    private TextView Tv_Address, tv_start, tv_stop, tv_timer, tv_drop_address, tv_seat_count;
    private RelativeLayout Rl_layout_userinfo, Rl_layout_arrived, wait_time_layout, share_seat_layout;
    private ImageView phone_call, Rl_layout_enable_voicenavigation, wait_time_start, user_info, wait_time_stop;
    ArrayList<UserPojo> Userlist = new ArrayList<UserPojo>();
    ArrayList<MultipleLatLongPojo> multiple_latlon_list = new ArrayList<MultipleLatLongPojo>();
    private HorizontalListView listview;
    UserListAdapter adapter;

    private ArrayList<LatLng> wayPointList;
    private LatLngBounds.Builder wayPointBuilder;
    private List<Polyline> polyLines;
    final int PERMISSION_REQUEST_CODE = 111;
    final int PERMISSION_REQUEST_NAVIGATION_CODE_WAZE = 223;
    final int PERMISSION_REQUEST_NAVIGATION_CODE = 222;
    public static int OVERLAY_PERMISSION_REQ_CODE = 1234;
    private Dialog cantact_dialog, sms_popup;
    ImageButton refresh_button;

    public static final int DropLocationRequestCode = 5000;
    private String str_drop_location = "";
    private String str_drop_Latitude = "";
    private String str_drop_Longitude = "";
    private String Str_Interrupt = "", Spage = "", back_option = "";

    private long startTime = 0L;
    private Handler customHandler = new Handler();
    private long timeInMilliseconds = 0L;
    private long timeSwapBuff = 0L;
    private long updatedTime = 0L;
    private boolean waitingStatus = false;


    private int mins;
    private int secs;
    private int hours;

    private int milliseconds;

    private Boolean idFareAvailable = false;

    ArrayList<FarePojo> farearray = new ArrayList<FarePojo>();
    ArrayList<PoolRateCard> poolRateCardList = new ArrayList<PoolRateCard>();
    private String carIcon = "";
    private String ride_type = "";
    String oldtime="";
    LatLngInterpolator mLatLngInterpolator;
    Marker drivermarker;
    Location oldLocation;
    private TextView user_name;
    private RoundedImageView user_icon;
    private RelativeLayout rl, touchview;
    private Marker currentMarker;

    boolean isMapToched = false;
    public static int OVERLAY_PERMISSION_WAZE_REQ_CODE = 555;

    Chat chat;

    private RelativeLayout myridetrack_your_ride_Rl;
    int height = 100;
    int width = 100;
    ChatDatabaseHelper db;
    int UnviewedCount;
    private TextView Chat_unviewed_TV;
    private String currentRideId = "";

    private Dialog toll_method_dialog;

    MyLocationGoogleMap My_mps;

    @Override
    public void onGpsStatusChanged(int i) {

    }

    @Override
    public void callback(LatLng lat, float bearing,String datetime) {
        System.out.println("--->>"+lat.latitude);
        oldtime = datetime;
        if (lat != null) {
            moveMarkerBackground("" + lat.latitude, "" + lat.longitude);
        }
    }

    public class RefreshReceiver extends BroadcastReceiver {
        String rideIdChatBroadcast;

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.package.ACTION_CLASS_TrackYourRide_REFRESH_page")) {

              /* Intent broadcastIntent_trip = new Intent();
               broadcastIntent_trip.setAction("com.finish.tripPage");
               context.sendBroadcast(broadcastIntent_trip);

               Intent trip_intent = new Intent(context, TripPage.class);
               context.startActivity(trip_intent);*/
//                if (intent.getExtras() != null) {

                session = new SessionManager(TripPage.this);
                HashMap<String, String> user = session.getUserDetails();
                driver_id = user.get(SessionManager.KEY_DRIVERID);
                System.out.println("--------------Jai Refresh-------------------------");

                if (isInternetPresent) {
                    postRequest(ServiceConstant.trip_Track_Driver);
                } else {
                    Alert1(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));
                }


//                }

            } else if (intent.getAction().equals("com.app.finish.ArrivedTrip")) {
                finish();
            }

            if (intent.getAction().equals("com.app.conversation.chat.send")) {
                try {
                    rideIdChatBroadcast = intent.getStringExtra("Rideidvalue");
                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (ride_type.equalsIgnoreCase("Normal")) {
                    UnviewedCountChat();
                } else {
                    if (rideIdChatBroadcast.equalsIgnoreCase(currentRideId)) {
                        UnviewedCountChatShare();
                    } else {
                        for (int i = 0; i < Userlist.size(); i++) {
                            if (rideIdChatBroadcast.equalsIgnoreCase(Userlist.get(i).getRide_id())) {
                         /*     UserPojo pojo=new UserPojo();
                              pojo.setChatNotify("1");*/
                                Userlist.get(i).setChatNotify("1");
                            }
                        }
                        adapter.notifyDataSetChanged();
                    }

                }

            }


        }
    }


    private void UnviewedCountChat() {
        UnviewedCount = db.ChatUnviewedCount();
        Chat_unviewed_TV.setText(String.valueOf(UnviewedCount));
        if (UnviewedCount == 0) {
            Chat_unviewed_TV.setVisibility(View.GONE);
            System.out.println("************************chat view tv gone*************");

        } else {
            Chat_unviewed_TV.setVisibility(View.VISIBLE);
            System.out.println("************************chat view tv visible*************");

        }
        System.out.println("***************************Timer Started*****************");
    }

    private void UnviewedCountChatShare() {
        UnviewedCount = db.ChatUnviewedCountShare(currentRideId);
        Chat_unviewed_TV.setText(String.valueOf(UnviewedCount));
        if (UnviewedCount == 0) {
            Chat_unviewed_TV.setVisibility(View.GONE);
            System.out.println("************************chat view tv gone*************");

        } else {
            Chat_unviewed_TV.setVisibility(View.VISIBLE);
            System.out.println("************************chat view tv visible*************");

        }
        System.out.println("***************************Timer Started*****************");
    }

    private void clearChatNotifyDot() {
        if (!ride_type.equalsIgnoreCase("Normal")) {

            for (int i = 0; i < Userlist.size(); i++) {
                if (currentRideId.equalsIgnoreCase(Userlist.get(i).getRide_id())) {
                         /*     UserPojo pojo=new UserPojo();
                              pojo.setChatNotify("1");*/
                    Userlist.get(i).setChatNotify("0");
                }
            }
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }


        }
    }

    private void ShowChatNotifyDot() {
        if (!ride_type.equalsIgnoreCase("Normal")) {

            for (int i = 0; i < Userlist.size(); i++) {
                if (!currentRideId.equalsIgnoreCase(Userlist.get(i).getRide_id())) {
                    int Unviewed = db.ChatUnviewedCountShare(currentRideId);
                    if (Unviewed != 0) {
                        Userlist.get(i).setChatNotify("1");

                    }
                         /*     UserPojo pojo=new UserPojo();
                              pojo.setChatNotify("1");*/

                }
            }
            adapter.notifyDataSetChanged();


        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_layout_final);
        requestLocationPermission();
        bmp = BitmapFactory.decodeResource(getResources(),
                R.drawable.taxiucar);
        My_mps=new MyLocationGoogleMap(TripPage.this,this);
        requestLocationPermission();
        finishReceiver = new RefreshReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.app.finish.ArrivedTrip");
        filter.addAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_page");
        filter.addAction("com.app.conversation.chat.send");
        registerReceiver(finishReceiver, filter);
        jk = 1;
//        dbInitialize();
        initialize();
        startTimer();




   /*     HashMap<String, String> chat = session.getchatNotify();
        String chat_notify = chat.get(SessionManager.KEY_CHAT_NOTIFY);
        if (chat_notify.equalsIgnoreCase("1")) {
            session.setchatNotify("0");
            Intent chatintent = new Intent(TripPage.this, ChatActivity.class);
            chatintent.putExtra("Ride_id", currentRideId);
            startActivity(chatintent);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            Chat_unviewed_TV.setVisibility(View.GONE);
            clearChatNotifyDot();
        }*/

        try {
            setLocationRequest();
//            buildGoogleApiClient();
            initilizeMap();
        } catch (Exception e) {
        }


        myridetrack_your_ride_Rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chatintent = new Intent(TripPage.this, ChatActivity.class);
                chatintent.putExtra("Ride_id", currentRideId);
                startActivity(chatintent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                Chat_unviewed_TV.setVisibility(View.GONE);
                clearChatNotifyDot();
            }
        });

        destination_address_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GPSTracker gps = new GPSTracker(TripPage.this);
                if (gps.canGetLocation() && gps.isgpsenabled()) {
                    Intent intent = new Intent(TripPage.this, DropLocationSelect.class);
                    startActivityForResult(intent, DropLocationRequestCode);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else {
                    Toast.makeText(TripPage.this, "Enable Gps", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });


        user_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("-----------Userlist----------------" + Userlist.size());

                System.out.println("-----------active_ride----------------" + active_ride);
                for (int i = 0; i < Userlist.size(); i++) {
                    if (active_ride.equals(Userlist.get(i).getRide_id())) {
                        Intent intent = new Intent(TripPage.this, UserInfo.class);
                        intent.putExtra("user_name", Userlist.get(i).getUser_name());
                        intent.putExtra("user_phoneno", Userlist.get(i).getPhone_number());
                        intent.putExtra("user_rating", Userlist.get(i).getUser_review());
                        intent.putExtra("user_image", Userlist.get(i).getUser_image());
                        intent.putExtra("RideId", Userlist.get(i).getRide_id());
                        intent.putExtra("Btn_group", Userlist.get(i).getBtn_group());
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        break;
                    }

                }


            }

        });


        tv_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wait_time_stop.setVisibility(View.VISIBLE);
                wait_time_start.setVisibility(View.INVISIBLE);
                tv_stop.setVisibility(View.VISIBLE);
                tv_start.setVisibility(View.GONE);
                //    layout_timer.setVisibility(View.VISIBLE);
                startTime = SystemClock.uptimeMillis();
                customHandler.postDelayed(updateTimerThread, 0);
                waitingStatus = true;
                session.setWaitingStatus("true");
                Str_Interrupt = "Yes";
            }
        });
        tv_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wait_time_stop.setVisibility(View.INVISIBLE);
                wait_time_start.setVisibility(View.VISIBLE);
                tv_start.setVisibility(View.VISIBLE);
                tv_stop.setVisibility(View.GONE);
                session.setWaitingStatus("false");
                timeSwapBuff += timeInMilliseconds;
                customHandler.removeCallbacks(updateTimerThread);
                waitingStatus = false;

            }
        });


        wait_time_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wait_time_stop.setVisibility(View.VISIBLE);
                wait_time_start.setVisibility(View.INVISIBLE);
                tv_stop.setVisibility(View.VISIBLE);
                tv_start.setVisibility(View.GONE);
                //    layout_timer.setVisibility(View.VISIBLE);
                startTime = SystemClock.uptimeMillis();
                customHandler.postDelayed(updateTimerThread, 0);
                waitingStatus = true;
                session.setWaitingStatus("true");
                Str_Interrupt = "Yes";
            }
        });
        wait_time_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wait_time_stop.setVisibility(View.INVISIBLE);
                wait_time_start.setVisibility(View.VISIBLE);
                tv_start.setVisibility(View.VISIBLE);
                tv_stop.setVisibility(View.GONE);
                session.setWaitingStatus("false");
                timeSwapBuff += timeInMilliseconds;
                customHandler.removeCallbacks(updateTimerThread);
                waitingStatus = false;
            }
        });
        phone_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

/*                System.out.println("-----------Userlist----------------" + Userlist.size());

                for (int i = 0; i < Userlist.size(); i++) {
                    System.out.println("-----------active_ride--------getRide_id--------" + active_ride + Userlist.get(i).getRide_id());
                    if (active_ride.equals(Userlist.get(i).getRide_id())) {
                        chooseContactOptions(Userlist.get(i).getRide_id(), Userlist.get(i).getPhone_number());
                    }

                }*/
                Intent i = new Intent(TripPage.this, NavigationDrawerNew.class);
                i.putExtra("availability", "Yes");
                startActivity(i);
                finish();


            }
        });

        refresh_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //feb 1
                //  ChatingService.startDriverAction(DriverMapActivity.this);
                setLocationRequest();
//                buildGoogleApiClient();


                if (gps.canGetLocation() && gps.isgpsenabled()) {
                    double Dlatitude = gps.getLatitude();
                    double Dlongitude = gps.getLongitude();

                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Dlatitude, Dlongitude)).zoom(17).build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                }

            }
        });
        share_seat_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < Userlist.size(); i++) {
                    if (active_ride.equals(Userlist.get(i).getRide_id())) {
                        //  no_of_seat=Userlist.get(i).getNo_of_seat();

                        select_Shareseat_Dialog(Userlist.get(i).getMax_no_of_seat(), i);
                    }
                }
            }
        });

        Rl_layout_enable_voicenavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MapNavicationChooseDialog();
            }
        });


        traffic_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HashMap<String, String> user = session.getTrafficImage();
                traffic_status = user.get(SessionManager.KEY_Traffic);
                if ("1".equals(traffic_status)) {
                    googleMap.setTrafficEnabled(false);
                    session.setTrafficImage("0");
                    traffic_button.setBackgroundResource(R.drawable.traffic_off_new);
                    Rl_traffic.setBackgroundResource(R.drawable.curveblack);
                } else {
                    googleMap.setTrafficEnabled(true);
                    session.setTrafficImage("1");
                    traffic_button.setBackgroundResource(R.drawable.traffic_on_new);
                    Rl_traffic.setBackgroundResource(R.drawable.curveblack);
                }
            }
        });


        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                currentRideId = Userlist.get(position).getRide_id();
                session.setCurrentRideId(currentRideId);
                if (ride_type != null && !ride_type.equalsIgnoreCase("") && !currentRideId.equalsIgnoreCase("")) {
                    if (ride_type.equalsIgnoreCase("Normal")) {
                        UnviewedCountChat();
                    } else {
                        UnviewedCountChatShare();
//                        Userlist.get(position).setChatNotify("0");
                 /*       UserPojo pojo=new UserPojo();
                        pojo.setChatNotify("0");
                        Userlist.set(position,pojo);*/

                    }
                }
                System.out.println("-----------active_ride---------position-----------" + Userlist.get(position).getRide_id() + "postion" + position);
                active_ride = Userlist.get(position).getRide_id();
                for (int i = 0; i < Userlist.size(); i++) {
                    if (position == i) {
                        Userlist.get(position).setActive_ride_id(active_ride);
                    } else {
                        Userlist.get(i).setActive_ride_id("");
                    }
                }
                if (Userlist.get(position).getBtn_group().equalsIgnoreCase("2")) {
                    Arrived(Userlist.get(position).getPickup_location());
                } else if (Userlist.get(position).getBtn_group().equalsIgnoreCase("3")) {
                    if (!Userlist.get(position).getDrop_location().equals("")) {
                        Begin(Userlist.get(position).getDrop_location(), Userlist.get(position).getNo_of_seat(), Userlist.get(position).getMax_no_of_seat(), Userlist.get(position).getRide_type());
                    } else {
                        Begin("", Userlist.get(position).getNo_of_seat(), Userlist.get(position).getMax_no_of_seat(), Userlist.get(position).getRide_type());
                    }

                } else if (Userlist.get(position).getBtn_group().equalsIgnoreCase("4")) {
                    if (!Userlist.get(position).getDrop_location().equals("")) {
                        End(Userlist.get(position).getDrop_location(), Userlist.get(position).getRide_type());
                    }
                } else if (Userlist.get(position).getBtn_group().equalsIgnoreCase("5")) {

                    if (isMyServiceRunning(GoogleNavigationService.class)) {
                        Intent serviceIntent = new Intent(getApplicationContext(), GoogleNavigationService.class);
                        stopService(serviceIntent);
                    }
                    idFareAvailable = true;
                    farePopup(Userlist.get(position).getTotal_payable_amount(), Userlist.get(position).getNeed_payment(), Userlist.get(position).getReceive_cash(), Userlist.get(position).getReq_payment(), Userlist.get(position).getFarelist(), Userlist.get(position).getRide_id());
                } else if (Userlist.get(position).getBtn_group().equalsIgnoreCase("8")) {

                    Intent intent = new Intent(TripPage.this, RatingsPage.class);
                    intent.putExtra("rideid", Userlist.get(position).getRide_id());
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }

                adapter.notifyDataSetChanged();


            }
        });

    }


    private void initialize() {
        session = new SessionManager(TripPage.this);

        db = new ChatDatabaseHelper(getApplicationContext());
        gps = new GPSTracker(TripPage.this);
        v2GetRouteDirection = new GMapV2GetRouteDirection();

        wayPointList = new ArrayList<LatLng>();
        polyLines = new ArrayList<Polyline>();


        myDBHelper = new GEODBHelper(getApplicationContext());
        myDBHelper.insertDriverStatus("1");
        HashMap<String, String> user = session.getUserDetails();
        HashMap<String, String> bitmap = session.getBitmapCode();
        base64 = bitmap.get(SessionManager.KEY_VEHICLE_BitMap_IMAGE);


        bmp = Bitmap.createScaledBitmap(bmp, width, height, false);

        driver_id = user.get(SessionManager.KEY_DRIVERID);
        Rl_traffic = (RelativeLayout) findViewById(R.id.traffic_btn_layout);
        refresh_button = (ImageButton) findViewById(R.id.refresh);
        traffic_button = (ImageButton) findViewById(R.id.traffic);
        phone_call = (ImageView) findViewById(R.id.call_image);
        user_info = (ImageView) findViewById(R.id.info_image);
        Tv_Address = (TextView) findViewById(R.id.trip_user_address);
        Rl_layout_userinfo = (RelativeLayout) findViewById(R.id.info_layout);
        Rl_layout_arrived = (RelativeLayout) findViewById(R.id.layout_begintrip);
        wait_time_layout = (RelativeLayout) findViewById(R.id.navi_layout);
        share_seat_layout = (RelativeLayout) findViewById(R.id.share_seat_layout);
        listview = (HorizontalListView) findViewById(R.id.user_listview);
        Rl_layout_enable_voicenavigation = (ImageView) findViewById(R.id.google_navigation);
        wait_time_start = (ImageView) findViewById(R.id.wait_img);
        wait_time_stop = (ImageView) findViewById(R.id.wait_img1);
        destination_address_layout = (CardView) findViewById(R.id.book_cardview_destination_address_layout);
        driverDetail = (CardView) findViewById(R.id.driver_details);
        tv_start = (TextView) findViewById(R.id.start_time);
        tv_stop = (TextView) findViewById(R.id.stop_time);
        tv_timer = (TextView) findViewById(R.id.wait_time);
        tv_seat_count = (TextView) findViewById(R.id.seat_label_count);
        tv_drop_address = (TextView) findViewById(R.id.location_drop_address);
        touchview = (RelativeLayout) findViewById(R.id.map_view);
        rl = (RelativeLayout) findViewById(R.id.user_list_normal_layout);
        user_name = (TextView) findViewById(R.id.user_title);
        user_icon = (RoundedImageView) findViewById(R.id.user_icon);
        myridetrack_your_ride_Rl = (RelativeLayout) findViewById(R.id.arrived_trip_floatingchatButton_Rl);
        Chat_unviewed_TV = (TextView) findViewById(R.id.arrived_trip_txt_complaint_notification);


   /*     UnviewedCount = db.ChatUnviewedCount();

        if (UnviewedCount != 0) {
            Chat_unviewed_TV.setVisibility(View.VISIBLE);
            Chat_unviewed_TV.setText(String.valueOf(UnviewedCount));

        }*/

        shimmer = new Shimmer();
        sliderSeekBar = (SeekBar) findViewById(R.id.Trip_seek);
        Bt_slider = (ShimmerButton) findViewById(R.id.Trip_slider_button);
        shimmer.start(Bt_slider);

        System.out.println("");
        sliderSeekBar.setOnSeekBarChangeListener(this);


        Intent i = getIntent();
        Str_Interrupt = i.getStringExtra("interrupted");

        HashMap<String, String> back = session.getupcomming_backoption();
        back_option = back.get(SessionManager.KEY_UPCOMMING_BACK);


        if (back_option != null) {
            if (back_option.equals("1")) {

                phone_call.setVisibility(View.VISIBLE);
            } else {
                phone_call.setVisibility(View.GONE);

            }

        }


        if (mediaPlayer1 != null && mediaPlayer1.isPlaying()) {
            mediaPlayer1.stop();
        }


        cd = new ConnectionDetector(TripPage.this);
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            postRequest(ServiceConstant.trip_Track_Driver);
        } else {
            Alert1(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));
        }

        session.setTripStatus("1");
        initDistance();
    }


    private Runnable updateTimerThread = new Runnable() {

        public void run() {
            waitingStatus = true;
            HashMap<String, String> wait_status = session.getWaitingStatus();
            String waitstatus = wait_status.get(SessionManager.KEY_WAIT_STATUS);
            if (!wait_status.equals("")) {
                if (waitstatus.equals("true")) {

                    timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

                    updatedTime = timeSwapBuff + timeInMilliseconds;
/*
            secs = (int) (updatedTime / 1000);
            mins = secs / 60;
            secs = secs % 60;
            milliseconds = (int) (updatedTime % 1000);
            timerValue.setText("" + mins + ":"
                    + String.format("%02d", secs));
            */

                    secs = (int) (updatedTime / 1000);
                    hours = secs / (60 * 60);
                    mins = secs / 60;
                    secs = secs % 60;
                    if (mins >= 60) {
                        mins = 0;
                    }
                    session.setWaitingTime(String.valueOf(updatedTime));
                    milliseconds = (int) (updatedTime % 1000);
            /*timerValue.setText(hours + ":" + mins + ":"
                    + String.format("%02d", secs));*/
                    tv_timer.setText(String.format("%02d", hours) + ":" + String.format("%02d", mins) + ":"
                            + String.format("%02d", secs));
                    //          System.out.println("thread----------------" + timerValue);

                    customHandler.postDelayed(this, 3 * 1000);
                }
            }
        }

    };


    //-------------    ------Show Share Seat Method--------------------
    private void select_Shareseat_Dialog(final String num, final int pos) {
        final MaterialDialog dialog = new MaterialDialog(TripPage.this);
        View view = LayoutInflater.from(TripPage.this).inflate(R.layout.select_share_seat, null);

        ListView car_listview = (ListView) view.findViewById(R.id.seat_type_dialog_listView);
        RelativeLayout ok = (RelativeLayout) view.findViewById(R.id.select_seatype_single_Bottm_layout);
        poolRateCardList.clear();
        for (int i = 0; i < Integer.parseInt(num); i++) {
            PoolRateCard pojo = new PoolRateCard();
            pojo.setSeat(String.valueOf(i + 1));
            pojo.setSelect("no");
            poolRateCardList.add(pojo);
        }

        //poolRateCardList.get(0).setSelect("yes");
        final SelectSeatAdapter seat_adapter = new SelectSeatAdapter(TripPage.this, poolRateCardList);
        car_listview.setAdapter(seat_adapter);
        seat_adapter.notifyDataSetChanged();

        dialog.setTitle(TripPage.this.getResources().getString(R.string.car_type_select_dialog_label_shareSeat));
        /*dialog.setPositiveButton(getActivity().getResources().getString(R.string.estimate_detail_label_ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.dismiss();
                    }
                }
        );*/

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        car_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //         dialog.dismiss();
                System.out.println("poolRateCardList--------------------jai" + poolRateCardList.size());

                for (int j = 0; j < poolRateCardList.size(); j++) {
                    System.out.println("poolRateCardList---------------j-----jai" + j);
                    System.out.println("poolRateCardList-------------pos-------jai" + position);

                    if (j == position) {
                        poolRateCardList.get(position).setSelect("yes");
                        System.out.println("poolRateCardList-----------bm----j-----jai" + poolRateCardList.get(position).getSeat());
                        String no_of_seat = poolRateCardList.get(position).getSeat();
                        Userlist.get(pos).setNo_of_seat(no_of_seat);
                        tv_seat_count.setText(no_of_seat + "/" + num);

                    } else {
                        poolRateCardList.get(j).setSelect("no");
                    }
                }
                seat_adapter.notifyDataSetChanged();
                dialog.dismiss();

            }
        });
        dialog.setView(view).show();
    }


    private void chooseContactOptions(final String ride_id, final String Str_user_phoneno) {
        cantact_dialog = new Dialog(TripPage.this);
        cantact_dialog.getWindow();
        cantact_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cantact_dialog.setContentView(R.layout.choose_contact_popup);
        cantact_dialog.setCanceledOnTouchOutside(true);
        cantact_dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_photo_Picker;
        cantact_dialog.show();
        cantact_dialog.getWindow().setGravity(Gravity.CENTER);

        RelativeLayout call = (RelativeLayout) cantact_dialog
                .findViewById(R.id.call_layout);
        RelativeLayout message = (RelativeLayout) cantact_dialog
                .findViewById(R.id.message_layout);
        RelativeLayout bottom_layout = (RelativeLayout) cantact_dialog
                .findViewById(R.id.bottom_layout);

        bottom_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cantact_dialog.dismiss();
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                HashMap<String, String> mask = session.getPhoneMasking();
                String mask_status = mask.get(SessionManager.KEY_PHONE_MASKING_STATUS);


                System.out.println("=========PHONEMASKINGSTATUS track ur ride get ==========>= " + mask_status);


                if (mask_status.equalsIgnoreCase("Yes")) {

                    System.out.println("=========PHONEMASKINGSTATUS track ur ride YES ==========>= " + mask_status);

                    cd = new ConnectionDetector(TripPage.this);
                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        phonemask_Call(ServiceConstant.phoneMasking, ride_id);
                    } else {
                        Alert1(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                    }
                } else {
                    if (Str_user_phoneno != null) {
                        Str_user_phone_no = Str_user_phoneno;
                        System.out.println("=========PHONEMASKINGSTATUS NO==========>= " + mask_status);

                        if (Build.VERSION.SDK_INT >= 23) {
                            // Marshmallow+

                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + Str_user_phoneno));
                            startActivity(intent);

                        } else {
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + Str_user_phoneno));
                            startActivity(intent);

                        }
                    } else {
                        Alert1(TripPage.this.getResources().getString(R.string.alert_label_title), TripPage.this.getResources().getString(R.string.arrived_alert_content1));
                    }
                }


                cantact_dialog.dismiss();


            }
        });


        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                cantact_dialog.dismiss();
                HashMap<String, String> mask = session.getPhoneMasking();
                String mask_status = mask.get(SessionManager.KEY_PHONE_MASKING_STATUS);


                /*System.out.println("=========PHONEMASKINGSTATUS track ur ride get ==========>= " + mask_status);

                Intent n = new Intent(Intent.ACTION_VIEW);
                n.setType("vnd.android-dir/mms-sms");
                n.putExtra("address", driverMobile);
                startActivity(n);*/

                if (mask_status.equalsIgnoreCase("Yes")) {
                    showMessagePopup(ride_id);


                } else {

                    try {

                        Intent intent = new Intent(Intent.ACTION_SENDTO);
                        intent.setData(Uri.parse("smsto:" + Uri.encode(Str_user_phoneno)));
                        startActivity(intent);
                    } catch (Exception e) {

                    }


                    /*Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setType("vnd.android-dir/mms-sms");
                    i.putExtra("address", Str_user_phoneno);
                    startActivity(i);*/


                }


            }
        });
    }


    private boolean isGoogleMapsInstalled() {
        try {
            ApplicationInfo info = getPackageManager().getApplicationInfo("com.google.android.apps.maps", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void moveNavigationPickup(String plat, String plong) {

        if (isGoogleMapsInstalled()) {

            if (!isMyServiceRunning(GoogleNavigationService.class)) {
                startService(new Intent(getApplicationContext(), GoogleNavigationService.class));
            }
            try {
                //      session.setGoogleNavicationValueArrived(Str_address,Str_RideId,Str_pickUp_Lat,Str_pickUp_Long,Str_username,Str_user_rating,Str_user_phoneno,Str_user_img,Suser_Id,Str_droplat,Str_droplon,str_drop_location);
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(String.format("google.navigation:ll=%s,%s%s", Double.parseDouble(plat), Double.parseDouble(plong), "&mode=c")));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            } catch (Exception e) {
            }

        } else {

            Toast.makeText(getApplicationContext(), getResources().getString(R.string.alert_label_no_google_map_installed), Toast.LENGTH_LONG).show();

        }

//        addBackLayout();
    }


    private void moveNavigationdrop(String Dlat, String Dlong) {


        if (isGoogleMapsInstalled()) {


            if (!Dlat.equalsIgnoreCase("") && !Dlong.equalsIgnoreCase("")) {

                if (!isMyServiceRunning(GoogleNavigationService.class)) {
                    startService(new Intent(getApplicationContext(), GoogleNavigationService.class));
                }

                //    session.setGoogleNavicationValue(Str_name,Str_rideid,Str_mobilno,address,beginAddress,Str_User_Id,Str_Interrupt,Str_profilpic);

                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(String.format("google.navigation:ll=%s,%s%s", Double.parseDouble(Dlat), Double.parseDouble(Dlong), "&mode=c")));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
                System.out.println("----jai--1-----------");
//            addBackLayout();
            } else {

                Toast.makeText(getApplicationContext(), getResources().getString(R.string.endtrip_alert_lbl), Toast.LENGTH_LONG).show();
            }

        } else {

            Toast.makeText(getApplicationContext(), getResources().getString(R.string.alert_label_no_google_map_installed), Toast.LENGTH_LONG).show();

        }


        System.out.println("lat and long ------jai-------------" + droplocation);
   /* Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(String.format("google.navigation:ll=%s,%s%s", Double.parseDouble(droplocation[0]), Double.parseDouble(droplocation[1]), "&mode=c")));
    startActivity(intent);
    addBackLayout();*/
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        boolean b = false;
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                System.out.println("1 already running");
                b = true;
                break;
            } else {
                System.out.println("2 not running");
                b = false;
            }
        }
        System.out.println("3 not running");
        return b;
    }

    private void AlertNavigation(String title, String message) {
        final PkDialog mDialog = new PkDialog(TripPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.alert_label_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    public void showMessagePopup(final String str_ride_id) {

        sms_popup = new Dialog(TripPage.this);
        sms_popup.getWindow();
        sms_popup.requestWindowFeature(Window.FEATURE_NO_TITLE);
        sms_popup.setContentView(R.layout.sms_popup);
        sms_popup.setCanceledOnTouchOutside(true);
        sms_popup.getWindow().getAttributes().windowAnimations = R.style.Animations_photo_Picker;
        sms_popup.show();
        sms_popup.getWindow().setGravity(Gravity.CENTER);

        TextView cancel = (TextView) sms_popup
                .findViewById(R.id.cancel);
        final EditText ed_msg = (EditText) sms_popup
                .findViewById(R.id.text_editview);
        TextView send = (TextView) sms_popup
                .findViewById(R.id.send);

        //     ed_msg.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        //       ed_msg.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        // tFields.addView(inputs[i]);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                sms_popup.dismiss();


            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sms = ed_msg.getText().toString();
                sms_popup.dismiss();
                if (sms.trim().length() > 0) {
                    phonemask_sms(ServiceConstant.phoneMasking_sms, sms, str_ride_id);
                } else {
                    Alert1(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.sms_masking_text));
                }

            }
        });
    }

    private void phonemask_sms(String Url, String msg, final String Str_RideId) {
        dialog = new Dialog(TripPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));
        System.out.println("-------------phonemask_sms----------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", Str_RideId);
        jsonParams.put("user_type", "driver");
        jsonParams.put("sms_content", msg);
        System.out.println("ride_id---------" + Str_RideId);
        System.out.println("user_type---------" + "user");
        System.out.println("sms_content---------" + msg);
        mRequest = new ServiceRequest(TripPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println(response);
                Log.e("phonemask_sms", response);
                String Sstatus = "", SResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    SResponse = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Alert1(getResources().getString(R.string.action_loading_sucess), SResponse);
                    } else {
                        Alert1(getResources().getString(R.string.alert_sorry_label_title), SResponse);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void phonemask_Call(String Url, String Str_RideId) {
        dialog = new Dialog(TripPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));
        System.out.println("-------------phone Url----------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", Str_RideId);
        jsonParams.put("user_type", "driver");

        System.out.println("ride_id---------" + Str_RideId);
        System.out.println("user_type---------" + "driver");

        mRequest = new ServiceRequest(TripPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println(response);
                Log.e("phonemask", response);
                String Sstatus = "", SResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    SResponse = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Alert1(getResources().getString(R.string.action_loading_sucess), SResponse);
                    } else {
                        Alert1(getResources().getString(R.string.alert_sorry_label_title), SResponse);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });


    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }


    private void postRequest(String Url) {
        if (dialog == null) {
            dialog = new Dialog(TripPage.this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_loading);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.show();

            TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
            dialog_title.setText(getResources().getString(R.string.action_loading));
        }


        System.out.println("-------------Trip_Page----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driver_id);


        System.out.println("-------------dashboard--Trip_Page--------------" + jsonParams);
        mRequest = new ServiceRequest(TripPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                String Str_status = "", Str_response = "", duty_id = "";
                ;
                Log.e("Trip Page", response);
                try {

                    JSONObject jobject = new JSONObject(response);

                    Str_status = jobject.getString("status");

                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject object = jobject.getJSONObject("response");
                        ride_type = object.getString("ride_type");
                        session.setrideType(ride_type);
                        duty_id = object.getString("duty_id");
                        active_ride = object.getString("active_ride");
                        toll_park_status = object.getString("toll_parking_status");
                        suitcase_fee = object.getString("suitcase_fee");
                        currentRideId = active_ride;
                        session.setCurrentRideId(currentRideId);
                       /* if (ride_type.equalsIgnoreCase("Normal")) {
                            UnviewedCountChat();
                        } else {

                            UnviewedCountChatShare();

                        }*/

                        JSONArray jarray = object.getJSONArray("rides");
                        Userlist.clear();
                        if (jarray.length() > 0) {
                            for (int i = 0; i < jarray.length(); i++) {
                                JSONObject j_ride_object = jarray.getJSONObject(i);
                                UserPojo u_pojo = new UserPojo();
                                u_pojo.setBtn_group(j_ride_object.getString("btn_group"));
                                session.setbngrp(j_ride_object.getString("btn_group"));
                                currency = j_ride_object.getString("currency");
                                u_pojo.setRide_id(j_ride_object.getString("ride_id"));
                                u_pojo.setRide_status(j_ride_object.getString("ride_status"));
                                u_pojo.setUser_id(j_ride_object.getString("user_id"));
                                u_pojo.setUser_name(j_ride_object.getString("user_name"));
                                u_pojo.setUser_email(j_ride_object.getString("user_email"));
                                u_pojo.setPhone_number(j_ride_object.getString("phone_number"));
                                u_pojo.setUser_image(j_ride_object.getString("user_image"));
                                u_pojo.setUser_review(j_ride_object.getString("user_review"));
                                u_pojo.setPickup_location(j_ride_object.getString("pickup_location"));
                                u_pojo.setPickup_lat(j_ride_object.getString("pickup_lat"));
                                u_pojo.setPickup_lon(j_ride_object.getString("pickup_lon"));
                                u_pojo.setPickup_time(j_ride_object.getString("pickup_time"));
                                u_pojo.setDrop_location(j_ride_object.getString("drop_loc"));
                                u_pojo.setDrop_lat(j_ride_object.getString("drop_lat"));
                                u_pojo.setDrop_lon(j_ride_object.getString("drop_lon"));
                                carIcon = j_ride_object.getString("car_icon");


                                if (j_ride_object.has("free_wait_time")) {
                                    free_wait_time = j_ride_object.getString("free_wait_time");
                                }


                                if (j_ride_object.has("no_of_seat")) {
                                    u_pojo.setNo_of_seat(j_ride_object.getString("no_of_seat"));
                                }
                                if (j_ride_object.has("max_no_of_seat")) {
                                    u_pojo.setMax_no_of_seat(j_ride_object.getString("max_no_of_seat"));
                                }

                                if (!ride_type.equalsIgnoreCase("Normal") && !currentRideId.equalsIgnoreCase(j_ride_object.getString("ride_id"))) {
                                    int Unviewed = db.ChatUnviewedCountShare(j_ride_object.getString("ride_id"));
                                    if (Unviewed == 0) {
                                        u_pojo.setChatNotify("0");
                                        System.out.println("************************Unviewed 0 *************");

                                    } else {
                                        u_pojo.setChatNotify("1");
                                        System.out.println("************************Unviewed 1*************");

                                    }
                                    System.out.println("***************************Timer Started*****************");
                                } else {
                                    u_pojo.setChatNotify("0");
                                }

                                u_pojo.setActive_ride_id(active_ride);
                                u_pojo.setRide_type(ride_type);
                                //  Userlist.add(u_pojo);
                                String sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(currency);
                                if (j_ride_object.getString("btn_group").equals("5")) {
                                    JSONArray triparray = j_ride_object.getJSONArray("trip_summary");
                                    u_pojo.setNeed_payment(j_ride_object.getString("need_payment"));
                                    u_pojo.setReceive_cash(j_ride_object.getString("receive_cash"));
                                    u_pojo.setReq_payment(j_ride_object.getString("req_payment"));
                                    u_pojo.setTotal_payable_amount(sCurrencySymbol + j_ride_object.getString("total_payable_amount"));


                                    if (triparray.length() > 0) {
                                        farearray.clear();
                                        for (int j = 0; j < triparray.length(); j++) {
                                            JSONObject fare_obj = triparray.getJSONObject(j);
                                            FarePojo fpojo = new FarePojo();
                                            fpojo.setTitle(fare_obj.getString("title"));
                                            fpojo.setValue(fare_obj.getString("value") + " " + fare_obj.getString("unit"));
                                            farearray.add(fpojo);
                                            idFareAvailable = true;

                                        }


                                    }
                                    u_pojo.setFarelist(farearray);


                                }
                                Userlist.add(u_pojo);

                            }
                        }
                        Object check_driver_object = object.get("map_locations");
                        if (check_driver_object instanceof JSONArray) {
                            JSONArray j_latlon_array = object.getJSONArray("map_locations");
                            multiple_latlon_list.clear();
                            if (j_latlon_array.length() > 0) {
                                for (int i = 0; i < j_latlon_array.length(); i++) {
                                    JSONObject j_ride_object = j_latlon_array.getJSONObject(i);
                                    MultipleLatLongPojo u_pojo = new MultipleLatLongPojo();
                                    u_pojo.setLat(j_ride_object.getString("lat"));
                                    u_pojo.setLon(j_ride_object.getString("lon"));
                                    u_pojo.setTxt(j_ride_object.getString("txt"));
                                    multiple_latlon_list.add(u_pojo);
                                }
                            }
                        }

                        if (Userlist.size() > 0) {
                            if (googleMap != null) {

                                googleMap.clear();
                                Bitmap icon = BitmapFactory.decodeResource(getResources(),
                                        R.drawable.ic_booking_prime_play_map_topview);

                                if (currentMarker != null) {
                                    currentMarker.remove();
                                }

                                try {
                                    drivermarker = null;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                            myDBHelper.deleteUser();
                            for (int i = 0; i < Userlist.size(); i++) {
                                if (Userlist.get(i).getRide_id().equalsIgnoreCase(active_ride)) {
                                    if (Userlist.get(i).getBtn_group().equalsIgnoreCase("2")) {
                                        Arrived(Userlist.get(i).getPickup_location());
                                        locationstartservice();
                                    } else if (Userlist.get(i).getBtn_group().equalsIgnoreCase("3")) {
                                        locatiostopservice();
                                        meterstartservice();
                                        if (!Userlist.get(i).getDrop_location().equals("")) {
                                            Begin(Userlist.get(i).getDrop_location(), Userlist.get(i).getNo_of_seat(), Userlist.get(i).getMax_no_of_seat(), Userlist.get(i).getRide_type());
                                        } else {
                                            Begin("", Userlist.get(i).getNo_of_seat(), Userlist.get(i).getMax_no_of_seat(), Userlist.get(i).getRide_type());
                                        }

                                    } else if (Userlist.get(i).getBtn_group().equalsIgnoreCase("4")) {
                                        locatiostopservice();
                                        meterstartservice();
                                        if (!Userlist.get(i).getDrop_location().equals("")) {
                                            End(Userlist.get(i).getDrop_location(), Userlist.get(i).getRide_type());
                                        }
                                    } else if (Userlist.get(i).getBtn_group().equalsIgnoreCase("5")) {
                                        locatiostopservice();

                                        data.setRunning(false);
                                        resetData();
                                        if (isMyServiceRunning(GoogleNavigationService.class)) {
                                            Intent serviceIntent = new Intent(getApplicationContext(), GoogleNavigationService.class);
                                            stopService(serviceIntent);
                                        }

                                        farePopup(Userlist.get(i).getTotal_payable_amount(), Userlist.get(i).getNeed_payment(), Userlist.get(i).getReceive_cash(), Userlist.get(i).getReq_payment(), Userlist.get(i).getFarelist(), Userlist.get(i).getRide_id());
                                    } else if (Userlist.get(i).getBtn_group().equalsIgnoreCase("8")) {
                                        locatiostopservice();
                                        data.setRunning(false);
                                        resetData();

                                        Intent intent = new Intent(TripPage.this, RatingsPage.class);
                                        intent.putExtra("rideid", Userlist.get(i).getRide_id());
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    }
                                }
                                System.out.println("insert user details db");
                                myDBHelper.insertUserDetails(Userlist.get(i).getUser_id(), Userlist.get(i).getBtn_group(), Userlist.get(i).getRide_id(), duty_id);
                                myDBHelper.userCount();
                            }

                            if (Userlist.size() > 1) {
                                rl.setVisibility(View.GONE);
                                listview.setVisibility(View.VISIBLE);
                                adapter = new UserListAdapter(TripPage.this, Userlist);
                                listview.setAdapter(adapter);
                            } else {
                                listview.setVisibility(View.GONE);
                                rl.setVisibility(View.VISIBLE);
                                Picasso.with(TripPage.this).load(String.valueOf(Userlist.get(0).getUser_image())).placeholder(R.drawable.placeholder_icon).into(user_icon);
                                user_name.setText(Userlist.get(0).getUser_name());
                            }
                        }


                        if (multiple_latlon_list.size() >= 2) {
                            reRouteCoute = 0;
                            GetRouteTask getRoute = new GetRouteTask(multiple_latlon_list, String.valueOf(MyCurrent_lat), String.valueOf(MyCurrent_long));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                                getRoute.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            else
                                getRoute.execute();
                        }

                        if (currentMarker != null) {
                            currentMarker.remove();
                        }
                   /*     currentMarker = googleMap.
                                addMarker(new MarkerOptions()
                                        .position(new LatLng(MyCurrent_lat, MyCurrent_long))
                                        .icon(BitmapDescriptorFactory
                                                .fromBitmap(bmp)));
*/
                        if (!ride_type.equals("Share")) {

                            if ("Confirmed".equalsIgnoreCase(Userlist.get(0).getRide_status())) {

                                MarkerOptions marker = new MarkerOptions().position(new LatLng(MyCurrent_lat, MyCurrent_long)).title("Pickup");
                                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pickup_marker));
                                googleMap.addMarker(marker);

                                MarkerOptions marker1 = new MarkerOptions().position(new LatLng(Double.parseDouble(Userlist.get(0).getPickup_lat()), Double.parseDouble(Userlist.get(0).getPickup_lon()))).title("Drop");
                                marker1.icon(BitmapDescriptorFactory.fromResource(R.drawable.drop_marker));
                                googleMap.addMarker(marker1);

                            } else {
                                MarkerOptions marker = new MarkerOptions().position(new LatLng(Double.parseDouble(Userlist.get(0).getPickup_lat()), Double.parseDouble(Userlist.get(0).getPickup_lon()))).title("Pickup");
                                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pickup_marker));
                                googleMap.addMarker(marker);

                                if (!"".equalsIgnoreCase(Userlist.get(0).getDrop_lat()) || !"".equalsIgnoreCase(Userlist.get(0).getDrop_lon())) {

                                    MarkerOptions marker1 = new MarkerOptions().position(new LatLng(Double.parseDouble(Userlist.get(0).getDrop_lat()), Double.parseDouble(Userlist.get(0).getDrop_lon()))).title("Drop");
                                    marker1.icon(BitmapDescriptorFactory.fromResource(R.drawable.drop_marker));
                                    googleMap.addMarker(marker1);
                                }


                            }


                        }

                        if ("Yes".equalsIgnoreCase(Str_Interrupt)) {
                            // waitingStatus=true;
                            //   Str_Interrupt="Yes";
                            startTime = SystemClock.uptimeMillis();

                            HashMap<String, String> wait = session.getWaitingTime();

                            String waittime = wait.get(SessionManager.KEY_WAIT);
                            HashMap<String, String> wait_status = session.getWaitingStatus();
                            String waitstatus = wait_status.get(SessionManager.KEY_WAIT_STATUS);

                            if (!waitstatus.equals("0")) {
                                if (waitstatus.equals("true")) {

                                    waitingStatus = true;
                                    wait_time_stop.setVisibility(View.VISIBLE);
                                    wait_time_start.setVisibility(View.INVISIBLE);
                                    tv_stop.setVisibility(View.VISIBLE);
                                    tv_start.setVisibility(View.GONE);
                                    customHandler.postDelayed(updateTimerThread, 0);
                                    //    layout_timer.setVisibility(View.VISIBLE);
                                } else {

                                    waitingStatus = false;
                                    wait_time_stop.setVisibility(View.INVISIBLE);
                                    wait_time_start.setVisibility(View.VISIBLE);
                                    System.out.println("jai 2");
                                    tv_stop.setVisibility(View.GONE);
                                    tv_start.setVisibility(View.VISIBLE);
                                    //      layout_timer.setVisibility(View.VISIBLE);

                                }
                            } else {
                                //    layout_timer.setVisibility(View.GONE);
                            }

                            if (!waittime.equals("0")) {
                                if (!waittime.isEmpty()) {
                                    System.out.println("jai 3");
                                    timeSwapBuff = Long.parseLong(waittime);
                                    secs = (int) (timeSwapBuff / 1000);
                                    hours = secs / (60 * 60);
                                    mins = secs / 60;
                                    secs = secs % 60;
                                    if (mins >= 60) {
                                        mins = 00;
                                    }

                                    milliseconds = (int) (timeSwapBuff % 1000);
                                    tv_timer.setText(String.format("%02d", hours) + ":" + String.format("%02d", mins) + ":"
                                            + String.format("%02d", secs));
                                } else {
                                    session.setWaitingStatus("0");
                                    //      layout_timer.setVisibility(View.GONE);
                                    tv_timer.setText("00:00:00");
                                }
                            }

                        } else {
                            session.setWaitingStatus("0");
                            //    layout_timer.setVisibility(View.GONE);
                            tv_timer.setText("00:00:00");
                        }


                        if (ride_type.equals("Share")) {
                            wait_time_layout.setVisibility(View.GONE);
                        }

                    } else {
                        Str_response = jobject.getString("response");
                        Alert1(getResources().getString(R.string.alert_sorry_label_title), Str_response);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
                trking_start();


            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
                /*HashMap<String, String> chat = session.getchatNotify();
                String chat_notify = chat.get(SessionManager.KEY_CHAT_NOTIFY);
                if (chat_notify.equalsIgnoreCase("1")) {
                    session.setchatNotify("0");
                    Intent chatintent = new Intent(TripPage.this, ChatActivity.class);
                    chatintent.putExtra("Ride_id", currentRideId);
                    startActivity(chatintent);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    Chat_unviewed_TV.setVisibility(View.GONE);
                    clearChatNotifyDot();
                }*/
            }
        });
    }

    public void farePopup(final String amt, String need_payment, String receive_cash, String req_payment, ArrayList<FarePojo> farelist, final String ride_id) {
        Rl_layout_arrived.setVisibility(View.GONE);
        wait_time_layout.setVisibility(View.GONE);
        share_seat_layout.setVisibility(View.GONE);
        destination_address_layout.setVisibility(View.GONE);


        if (idFareAvailable) {


            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.fare_popup, null);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setView(view);
            builder.create().setCanceledOnTouchOutside(false);
            builder.setCancelable(false);
            builder.show();
            final TextView Tv_amt = (TextView) view.findViewById(R.id.my_rides_detail_Total_fare);
            final TextView Tv_requst = (TextView) view.findViewById(R.id.requst);
            final ExpandableHeightListView listview = (ExpandableHeightListView) view.findViewById(R.id.my_rides_payment_detail_listView);

            RelativeLayout layout_request_payment = (RelativeLayout) view.findViewById(R.id.layout_faresummery_requstpayment);
            RelativeLayout layout_receive_cash = (RelativeLayout) view.findViewById(R.id.fare_summery_receive_cash_layout);

            Tv_amt.setText(amt);

            FareAdaptewr adaptewr = new FareAdaptewr(TripPage.this, farelist);
            listview.setAdapter(adaptewr);
            listview.setExpanded(true);


            //if (Str_need_payment.equalsIgnoreCase("YES")){
            if (need_payment.equals("1")) {
                if (receive_cash.equals("1") && req_payment.equals("1")) {
                    Tv_requst.setText(getResources().getString(R.string.fare_summary_ride_request_payment));
                    layout_receive_cash.setVisibility(View.VISIBLE);
                    layout_request_payment.setVisibility(View.VISIBLE);
                } else if (receive_cash.equals("0") && req_payment.equals("1")) {
                    layout_receive_cash.setVisibility(View.GONE);
                    Tv_requst.setText(getResources().getString(R.string.fare_summary_ride_request_payment));
                    layout_request_payment.setVisibility(View.VISIBLE);
                } else if (receive_cash.equals("1") && req_payment.equals("0")) {
                    layout_receive_cash.setVisibility(View.VISIBLE);
                    //  Tv_requst.setText(getResources().getString(R.string.fare_summary_ride_request_payment));
                    layout_request_payment.setVisibility(View.GONE);
                } else {
                    Tv_requst.setText(getResources().getString(R.string.lbel_notification_ok));
                    layout_receive_cash.setVisibility(View.GONE);
                    layout_request_payment.setVisibility(View.VISIBLE);
                }

            } else {
                if (receive_cash.equals("0") && req_payment.equals("0")) {
                    Tv_requst.setText(getResources().getString(R.string.lbel_notification_ok));
                    layout_receive_cash.setVisibility(View.GONE);
                    layout_request_payment.setVisibility(View.VISIBLE);
                }
            }


            layout_receive_cash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent intent = new Intent(TripPage.this, PaymentPage.class);
                    intent.putExtra("amount", amt);
                    intent.putExtra("rideid", ride_id);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


                }
            });

            layout_request_payment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cd = new ConnectionDetector(TripPage.this);
                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {

                        if (Tv_requst.getText().toString().equalsIgnoreCase(getResources().getString(R.string.fare_summary_ride_request_payment))) {
                            postRequest_Reqqustpayment(ServiceConstant.request_paymnet_url, ride_id);
                            System.out.println("arrived------------------" + ServiceConstant.request_paymnet_url);
                        } else {
                            Intent intent = new Intent(TripPage.this, RatingsPage.class);
                            intent.putExtra("rideid", ride_id);
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        }

                    } else {
                        Alert1(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));
                    }
                }
            });


        }


    }


    //-----------------------Code for arrived post request-----------------
    private void postRequest_Reqqustpayment(String Url, final String ride_id) {
        dialog = new Dialog(TripPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        final TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);

        dialog_title.setText(getResources().getString(R.string.action_loading));
     /*  LinearLayout main = (LinearLayout)findViewById(R.id.main_layout);
        View view = getLayoutInflater().inflate(R.layout.waiting, main,false);
        main.addView(view);
*/

        System.out.println("-------------endtrip----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driver_id);
        jsonParams.put("ride_id", ride_id);

        System.out
                .println("--------------driver_id-------------------"
                        + driver_id);


        System.out
                .println("--------------ride_id-------------------"
                        + ride_id);

        mRequest = new ServiceRequest(TripPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                Log.e("requestpayment", response);

                System.out.println("response---------" + response);

                String Str_status = "", Str_response = "", Str_currency = "", Str_rideid = "", Str_action = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Str_response = object.getString("response");
                    Str_status = object.getString("status");

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (Str_status.equalsIgnoreCase("0")) {
                    dialog.dismiss();
                    Alert_req(getResources().getString(R.string.alert_sorry_label_title), Str_response, ride_id);

                } else {
                    Alert_req(getResources().getString(R.string.label_pushnotification_cashreceived), Str_response, ride_id);
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {

                dialog.dismiss();
            }

        });

    }

    //--------------Alert Method-----------
    private void Alert_req(String title, String message, final String ride_id) {
        final PkDialog mDialog = new PkDialog(TripPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.alert_label_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

               /* Intent broadcastIntent_begintrip = new Intent();
                broadcastIntent_begintrip.setAction("com.finish.com.finish.BeginTrip");
                sendBroadcast(broadcastIntent_begintrip);

                Intent broadcastIntent_arrivedtrip = new Intent();
                broadcastIntent_arrivedtrip.setAction("com.finish.ArrivedTrip");
                sendBroadcast(broadcastIntent_arrivedtrip);

                Intent broadcastIntent_endtrip = new Intent();
                broadcastIntent_endtrip.setAction("com.finish.EndTrip");
                sendBroadcast(broadcastIntent_endtrip);

                Intent broadcastIntent_trip = new Intent();
                broadcastIntent_trip.setAction("com.finish.tripPage");
                sendBroadcast(broadcastIntent_trip);*/


                Intent intent = new Intent(TripPage.this, LoadingPage.class);
                intent.putExtra("Driverid", driver_id);
                intent.putExtra("RideId", ride_id);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                //finish();
            }
        });
        mDialog.show();
    }


    private void PostRequest(String Url, final String btn_stat, final String ride_id, final String str_drop_Latitude, final String str_drop_Longitude, String ride_type1, String no_of_seat) {
        dialog = new Dialog(TripPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        System.out.println("-------------Trip_Page----------------" + Url);
        System.out.println("---------ride_type---------------" + ride_type1);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driver_id);
        jsonParams.put("ride_id", ride_id);

        if (btn_stat.equals("2")) {
            jsonParams.put("driver_lat", String.valueOf(MyCurrent_lat));
            jsonParams.put("driver_lon", String.valueOf(MyCurrent_long));
        } else if (btn_stat.equals("3")) {
            if (ride_type1.equals("Share")) {
                jsonParams.put("no_of_seat", no_of_seat);
            }
            jsonParams.put("pickup_lat", String.valueOf(MyCurrent_lat));
            jsonParams.put("pickup_lon", String.valueOf(MyCurrent_long));
            jsonParams.put("drop_lat", String.valueOf(str_drop_Latitude));
            jsonParams.put("drop_lon", String.valueOf(str_drop_Longitude));
            jsonParams.put("distance", "0");
            startStopServices();

        } else if (btn_stat.equals("4")) {

            ChatDatabaseHelper db = new ChatDatabaseHelper(getApplicationContext());
            db.clearTable();

   /*         ArrayList<String> travel_history = myDBHelper.getDataEndTrip(ride_id);
            System.out.println("-----------jai---total_distance-------------------" + travel_history.toString().replace("[", "").replace("]", "").replace(" ", ""));
            StringBuilder builder = new StringBuilder();
            for (String string : travel_history) {
                builder.append("," + string);
            }
*/
            Calendar aCalendar = Calendar.getInstance();
            SimpleDateFormat aDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String aCurrentDate = aDateFormat.format(aCalendar.getTime());

            gps = new GPSTracker(TripPage.this);
            if (gps.canGetLocation()) {
                location_lat = "" + gps.getLatitude();
                location_long = "" + gps.getLongitude();
            }
            if (location_lat != null) {
                if (location_lat.length() > 3) {
                    myDBHelper.insertLatLong(ride_id, Double.parseDouble(location_lat), Double.parseDouble(location_long), aCurrentDate);
                }
            }
            ArrayList<String> travel_history = myDBHelper.getDataEndTrip(ride_id);
            System.out.println("-----------jai---total_distance-------------------" + travel_history.toString().replace("[", "").replace("]", "").replace(" ", ""));
            StringBuilder builder = new StringBuilder();
            for (String string : travel_history) {
                builder.append("," + string);
            }
            jsonParams.put("wait_time_frame", String.valueOf(tv_timer.getText().toString()));
            jsonParams.put("travel_history", builder.toString());
            if (location_lat != null || !location_lat.isEmpty() || !location_lat.equals("null")) {
                if (location_lat.length() > 3) {
                    jsonParams.put("drop_lat", location_lat);
                    jsonParams.put("drop_lon", location_long);
                }
            }
      /*      jsonParams.put("distance", "" + distance_trip);
            jsonParams.put("wait_time_frame", String.valueOf(tv_timer.getText().toString()));
            jsonParams.put("travel_history", builder.toString());
            jsonParams.put("drop_lat", String.valueOf(MyCurrent_lat));
            jsonParams.put("drop_lon", String.valueOf(MyCurrent_long));
            jsonParams.put("distance", "0");*/

            if (toll_park_status.equals("1")) {
                jsonParams.put("toll_charge", tollChargeEt.getText().toString());
                jsonParams.put("parking_charge", parkingChargeEt.getText().toString());
            }
//             jsonParams.put("extra_fee",SuitcaseEt.getText().toString().trim());
               jsonParams.put("extra_fee","0");
        }

        if (btn_stat.equals("3")) {
            if (isMyServiceRunning(waitingservice.class)) {
                try {
                    Intent startIntent = new Intent(getApplicationContext(), waitingservice.class);
                    stopService(startIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }

        System.out.println("---------------Trip_Page jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(TripPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                String Str_status = "", Str_response = "", duty_id = "", currency = "", Zero_response = "";
                Log.e("Trip Page", response);
                try {

                    JSONObject jobject = new JSONObject(response);

                    Str_status = jobject.getString("status");

                    if (Str_status.equalsIgnoreCase("0")) {
                        Zero_response = jobject.getString("ride_view");
                        Str_response = jobject.getString("response");
                    }

                    if (Str_status.equalsIgnoreCase("1")) {
                        if (btn_stat.equals("2")) {
                            if (isMyServiceRunning(waitingservice.class)) {
                            } else {
                                try {
                                    Intent startIntent = new Intent(getApplicationContext(), waitingservice.class);
                                    startIntent.putExtra("rideid", ride_id);
                                    startIntent.putExtra("free_wait_time", free_wait_time);
                                    startService(startIntent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }


                        JSONObject object = jobject.getJSONObject("response");

                        ride_type = object.getString("ride_type");
                        duty_id = object.getString("duty_id");
                        active_ride = object.getString("active_ride");
                        suitcase_fee = object.getString("suitcase_fee");
                        currentRideId = active_ride;
                        session.setCurrentRideId(currentRideId);
                        JSONArray jarray = object.getJSONArray("rides");

                        Userlist.clear();
                        if (jarray.length() > 0) {
                            for (int i = 0; i < jarray.length(); i++) {
                                JSONObject j_ride_object = jarray.getJSONObject(i);
                                UserPojo u_pojo = new UserPojo();
                                u_pojo.setBtn_group(j_ride_object.getString("btn_group"));
                                currency = j_ride_object.getString("currency");
                                u_pojo.setRide_id(j_ride_object.getString("ride_id"));
                                u_pojo.setRide_status(j_ride_object.getString("ride_status"));
                                u_pojo.setUser_id(j_ride_object.getString("user_id"));
                                u_pojo.setUser_name(j_ride_object.getString("user_name"));
                                u_pojo.setUser_email(j_ride_object.getString("user_email"));
                                u_pojo.setPhone_number(j_ride_object.getString("phone_number"));
                                u_pojo.setUser_image(j_ride_object.getString("user_image"));
                                u_pojo.setUser_review(j_ride_object.getString("user_review"));
                                u_pojo.setPickup_location(j_ride_object.getString("pickup_location"));
                                u_pojo.setPickup_lat(j_ride_object.getString("pickup_lat"));
                                u_pojo.setPickup_lon(j_ride_object.getString("pickup_lon"));
                                u_pojo.setPickup_time(j_ride_object.getString("pickup_time"));
                                u_pojo.setDrop_location(j_ride_object.getString("drop_loc"));
                                u_pojo.setDrop_lat(j_ride_object.getString("drop_lat"));
                                u_pojo.setDrop_lon(j_ride_object.getString("drop_lon"));
                                carIcon = j_ride_object.getString("car_icon");
                                if (j_ride_object.has("no_of_seat")) {
                                    u_pojo.setNo_of_seat(j_ride_object.getString("no_of_seat"));
                                }
                                if (j_ride_object.has("max_no_of_seat")) {
                                    u_pojo.setMax_no_of_seat(j_ride_object.getString("max_no_of_seat"));
                                }

                                if (!ride_type.equalsIgnoreCase("Normal") && !currentRideId.equalsIgnoreCase(j_ride_object.getString("ride_id"))) {
                                    int Unviewed = db.ChatUnviewedCountShare(j_ride_object.getString("ride_id"));
                                    if (Unviewed == 0) {
                                        u_pojo.setChatNotify("0");
                                        System.out.println("************************Unviewed 0 *************");

                                    } else {
                                        u_pojo.setChatNotify("1");
                                        System.out.println("************************Unviewed 1*************");

                                    }
                                    System.out.println("***************************Timer Started*****************");
                                } else {
                                    u_pojo.setChatNotify("0");
                                }
                                u_pojo.setActive_ride_id(active_ride);
                                u_pojo.setRide_type(ride_type);
//                                Userlist.add(u_pojo);
                                String sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(currency);
                                if (j_ride_object.getString("btn_group").equals("5")) {
                                    JSONArray triparray = j_ride_object.getJSONArray("trip_summary");
                                    u_pojo.setNeed_payment(j_ride_object.getString("need_payment"));
                                    u_pojo.setReceive_cash(j_ride_object.getString("receive_cash"));
                                    u_pojo.setReq_payment(j_ride_object.getString("req_payment"));
                                    u_pojo.setTotal_payable_amount(sCurrencySymbol + j_ride_object.getString("total_payable_amount"));


                                    if (triparray.length() > 0) {
                                        farearray.clear();
                                        for (int j = 0; j < triparray.length(); j++) {
                                            JSONObject fare_obj = triparray.getJSONObject(j);
                                            FarePojo fpojo = new FarePojo();
                                            fpojo.setTitle(fare_obj.getString("title"));
                                            fpojo.setValue(fare_obj.getString("value") + " " + fare_obj.getString("unit"));
                                            farearray.add(fpojo);
                                            idFareAvailable = true;

                                        }


                                    }
                                    u_pojo.setFarelist(farearray);


                                }
                                Userlist.add(u_pojo);

                            }
                        }

                        JSONArray j_latlon_array = object.getJSONArray("map_locations");
                        multiple_latlon_list.clear();
                        if (j_latlon_array.length() > 0) {
                            for (int i = 0; i < j_latlon_array.length(); i++) {
                                JSONObject j_ride_object = j_latlon_array.getJSONObject(i);
                                MultipleLatLongPojo u_pojo = new MultipleLatLongPojo();
                                u_pojo.setLat(j_ride_object.getString("lat"));
                                u_pojo.setLon(j_ride_object.getString("lon"));
                                u_pojo.setTxt(j_ride_object.getString("txt"));
                                multiple_latlon_list.add(u_pojo);
                            }
                        }

                        if (Userlist.size() > 0) {
                            if (googleMap != null) {

                                googleMap.clear();
                                Bitmap icon = BitmapFactory.decodeResource(getResources(),
                                        R.drawable.ic_booking_prime_play_map_topview);

                                if (currentMarker != null) {
                                    currentMarker.remove();
                                }

                                try {
                                    drivermarker = null;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            myDBHelper.deleteUser();
                            for (int i = 0; i < Userlist.size(); i++) {
                                if (Userlist.get(i).getRide_id().equalsIgnoreCase(active_ride)) {
                                    if (Userlist.get(i).getBtn_group().equalsIgnoreCase("2")) {
                                        Arrived(Userlist.get(i).getPickup_location());
                                    } else if (Userlist.get(i).getBtn_group().equalsIgnoreCase("3")) {
                                        if (!Userlist.get(i).getDrop_location().equals("")) {
                                            Begin(Userlist.get(i).getDrop_location(), Userlist.get(i).getNo_of_seat(), Userlist.get(i).getMax_no_of_seat(), Userlist.get(i).getRide_type());
                                        } else {
                                            Begin("", Userlist.get(i).getNo_of_seat(), Userlist.get(i).getMax_no_of_seat(), Userlist.get(i).getRide_type());
                                        }

                                    } else if (Userlist.get(i).getBtn_group().equalsIgnoreCase("4")) {
                                        if (!Userlist.get(i).getDrop_location().equals("")) {
                                            End(Userlist.get(i).getDrop_location(), Userlist.get(i).getRide_type());

                                        }
                                    } else if (Userlist.get(i).getBtn_group().equalsIgnoreCase("5")) {
                                        data.setRunning(false);
                                        resetData();
                                        idFareAvailable = true;
                                        session.setWaitingStatus("0");
                                        session.setWaitingTime("0");

                                        if (isMyServiceRunning(GoogleNavigationService.class)) {
                                            Intent serviceIntent = new Intent(getApplicationContext(), GoogleNavigationService.class);
                                            stopService(serviceIntent);
                                        }

                                        farePopup(Userlist.get(i).getTotal_payable_amount(), Userlist.get(i).getNeed_payment(), Userlist.get(i).getReceive_cash(), Userlist.get(i).getReq_payment(), Userlist.get(i).getFarelist(), Userlist.get(i).getRide_id());
                                    } else if (Userlist.get(i).getBtn_group().equalsIgnoreCase("8")) {
                                        data.setRunning(false);
                                        resetData();

                                        Intent intent = new Intent(TripPage.this, RatingsPage.class);
                                        intent.putExtra("rideid", Userlist.get(i).getRide_id());
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    }
                                }

                                System.out.println("insert user details db");

                                myDBHelper.insertUserDetails(Userlist.get(i).getUser_id(), Userlist.get(i).getBtn_group(), Userlist.get(i).getRide_id(), duty_id);
                                myDBHelper.userCount();
                                System.out.println("---------jai multiple_latlon_list-----------" + multiple_latlon_list.size());


                                if (multiple_latlon_list.size() >= 2) {
                                    reRouteCoute = 0;
                                    GetRouteTask getRoute = new GetRouteTask(multiple_latlon_list, String.valueOf(MyCurrent_lat), String.valueOf(MyCurrent_long));
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                                        getRoute.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                    else
                                        getRoute.execute();
                                }
                                if (bmp != null) {


                                    if (currentMarker != null) {
                                        currentMarker.remove();
                                    }
                                 /*   currentMarker = googleMap.
                                            addMarker(new MarkerOptions()
                                                    .position(new LatLng(MyCurrent_lat, MyCurrent_long))
                                                    .icon(BitmapDescriptorFactory
                                                            .fromBitmap(bmp)));
*/
                                }

                                if (!ride_type.equals("Share")) {

                                    if ("Confirmed".equalsIgnoreCase(Userlist.get(0).getRide_status())) {

                                        MarkerOptions marker = new MarkerOptions().position(new LatLng(MyCurrent_lat, MyCurrent_long)).title("Pickup");
                                        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pickup_marker));
                                        googleMap.addMarker(marker);

                                        MarkerOptions marker1 = new MarkerOptions().position(new LatLng(Double.parseDouble(Userlist.get(0).getPickup_lat()), Double.parseDouble(Userlist.get(0).getPickup_lon()))).title("Drop");
                                        marker1.icon(BitmapDescriptorFactory.fromResource(R.drawable.drop_marker));
                                        googleMap.addMarker(marker1);

                                    } else {
                                        MarkerOptions marker = new MarkerOptions().position(new LatLng(Double.parseDouble(Userlist.get(0).getPickup_lat()), Double.parseDouble(Userlist.get(0).getPickup_lon()))).title("Pickup");
                                        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pickup_marker));
                                        googleMap.addMarker(marker);

                                        if (!"".equalsIgnoreCase(Userlist.get(0).getDrop_lat()) || !"".equalsIgnoreCase(Userlist.get(0).getDrop_lon())) {

                                            MarkerOptions marker1 = new MarkerOptions().position(new LatLng(Double.parseDouble(Userlist.get(0).getDrop_lat()), Double.parseDouble(Userlist.get(0).getDrop_lon()))).title("Drop");
                                            marker1.icon(BitmapDescriptorFactory.fromResource(R.drawable.drop_marker));
                                            googleMap.addMarker(marker1);
                                        }


                                    }


                                }


                            }

                            adapter = new UserListAdapter(TripPage.this, Userlist);
                            listview.setAdapter(adapter);

                            if (dialog != null) {
                                dialog.dismiss();
                            }

                        }

                        if (dialog != null) {
                            dialog.dismiss();
                        }

                    } else {
                        if (dialog != null) {
                            dialog.dismiss();
                        }

                        if (Str_status.equalsIgnoreCase("0")) {

                            if (Zero_response.equals("stay")) {
                                Alert1(getResources().getString(R.string.alert_sorry_label_title), Str_response);
                            }
                            if (Zero_response.equals("next")) {
                                Intent broadcastIntent1 = new Intent();
                                broadcastIntent1.setAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_page");
                                sendBroadcast(broadcastIntent1);

                            }
                            if (Zero_response.equals("detail")) {
                                finish();
                                Intent intent = new Intent(TripPage.this, ridesummarydetail.class);
                                intent.putExtra("rideid", ride_id);
                                intent.putExtra("type", "trip");
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                               /* Intent intent = new Intent(TripPage.this, TripSummaryDetail.class);
                                intent.putExtra("ride_id", ride_id);
                                intent.putExtra("type", "trip");
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/
                                finish();
                            }
                            if (Zero_response.equals("home")) {
                                Intent broadcastIntent = new Intent();
                                broadcastIntent.setAction("com.finish.ArrivedTrip");
                                sendBroadcast(broadcastIntent);
                                Intent broadcastIntent1 = new Intent();
                                broadcastIntent1.setAction("com.finish.BeginTrip");
                                sendBroadcast(broadcastIntent1);
                                Intent broadcastIntent_userinfo = new Intent();
                                broadcastIntent_userinfo.setAction("com.finish.UserInfo");
                                sendBroadcast(broadcastIntent_userinfo);

                                Intent broadcastIntent_tripdetail = new Intent();
                                broadcastIntent_tripdetail.setAction("com.finish.tripsummerydetail");
                                sendBroadcast(broadcastIntent_tripdetail);

                                Intent broadcastIntent_drivermap = new Intent();
                                broadcastIntent_drivermap.setAction("com.finish.canceltrip.DriverMapActivity");
                                sendBroadcast(broadcastIntent_drivermap);

                                DashBoardDriver.isOnline = true;
                                Intent i = new Intent(TripPage.this, NavigationDrawerNew.class);
                                i.putExtra("availability", "Yes");
                                startActivity(i);
                                finish();

                            }
                        }

                        // Alert(getResources().getString(R.string.alert_sorry_label_title), Str_response);
                    } /*else {
                        Str_response = jobject.getString("response");
                        Alert1(getResources().getString(R.string.alert_sorry_label_title), Str_response);
                    }*/


                } catch (Exception e) {
                    e.printStackTrace();
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    public void Arrived(String loc) {
        Rl_layout_enable_voicenavigation.setVisibility(View.VISIBLE);
        wait_time_layout.setVisibility(View.GONE);
        Rl_layout_arrived.setVisibility(View.VISIBLE);
        share_seat_layout.setVisibility(View.GONE);
        Tv_Address.setText(loc);
        Bt_slider.setText(getResources().getString(R.string.arrivedtrip_arrivedtriptv_label));
        sliderSeekBar.setProgress(0);
        sliderSeekBar.setBackgroundResource(R.color.app_color);
        Bt_slider.setVisibility(View.VISIBLE);
        shimmer.start(Bt_slider);

    }

    public void End(String Loc, String type) {
        if (type.equals("Share")) {
            wait_time_layout.setVisibility(View.GONE);
        } else {
            wait_time_layout.setVisibility(View.VISIBLE);
        }
        Rl_layout_enable_voicenavigation.setVisibility(View.VISIBLE);
        share_seat_layout.setVisibility(View.GONE);
        destination_address_layout.setVisibility(View.GONE);
        driverDetail.setVisibility(View.VISIBLE);
        Rl_layout_arrived.setVisibility(View.VISIBLE);
        Tv_Address.setText(Loc);
        Bt_slider.setText(getResources().getString(R.string.lbel_endtrip));
        phone_call.setVisibility(View.GONE);
        sliderSeekBar.setProgress(0);
        sliderSeekBar.setBackgroundResource(R.color.app_color);
        Bt_slider.setVisibility(View.VISIBLE);

        shimmer.start(Bt_slider);
    }

    public void Begin(String Drop_loc, String no_seat, String max_no_seat, String type) {
        if (!Drop_loc.equals("")) {
            driverDetail.setVisibility(View.VISIBLE);
            Tv_Address.setText(Drop_loc);
            Rl_layout_arrived.setVisibility(View.VISIBLE);
            destination_address_layout.setVisibility(View.GONE);
        } else {
            driverDetail.setVisibility(View.GONE);
            destination_address_layout.setVisibility(View.VISIBLE);
            Rl_layout_arrived.setVisibility(View.GONE);
            tv_drop_address.setText(getResources().getString(R.string.action_enter_drop_location));
        }
        if (type.equals("Share")) {
            share_seat_layout.setVisibility(View.VISIBLE);
        } else {
            share_seat_layout.setVisibility(View.GONE);
        }
        phone_call.setVisibility(View.GONE);

        Rl_layout_enable_voicenavigation.setVisibility(View.INVISIBLE);
        wait_time_layout.setVisibility(View.GONE);

        tv_seat_count.setText(no_seat + "/" + max_no_seat);
        Bt_slider.setText(getResources().getString(R.string.lbel_begintrip));
        sliderSeekBar.setProgress(0);
        sliderSeekBar.setBackgroundResource(R.color.app_color);
        Bt_slider.setVisibility(View.VISIBLE);
        shimmer.start(Bt_slider);
    }

    public class GetRouteTask extends AsyncTask<String, Void, String> {

        String response = "";
        private ArrayList<LatLng> wayLatLng;
        private String dLat, dLong;
        private ArrayList<MultipleLatLongPojo> multipleDropList;

        GetRouteTask(ArrayList<MultipleLatLongPojo> multipleDropList, String lat, String lon) {

            this.multipleDropList = multipleDropList;
            dLat = lat;
            dLong = lon;
            wayLatLng = addWayPointPoint(multipleDropList, dLat, dLong);
            if (wayLatLng.size() < 2) {
                wayLatLng.clear();
                wayLatLng = addWayPointPoint(multiple_latlon_list, dLat, dLong);
            }

        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... urls) {

            try {

                if (wayLatLng.size() >= 2) {
                    Routing routing = new Routing.Builder()
                            .travelMode(AbstractRouting.TravelMode.DRIVING)
                            .withListener(listner)
                            .key(ServiceConstant.routeKey)
                            .alternativeRoutes(true)
                            .waypoints(wayLatLng)
                            .build();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                        routing.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    else
                        routing.execute();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            response = "Success";
            return response;

        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equalsIgnoreCase("Success")) {
            }
        }
    }

    private ArrayList<LatLng> addWayPointPoint(ArrayList<MultipleLatLongPojo> mMultipleDropLatLng, String lat, String lon) {

        try {
            if (googleMap != null) {
//                googleMap.clear();
                wayPointList.clear();

                wayPointBuilder = new LatLngBounds.Builder();
                if (mMultipleDropLatLng != null) {

                    for (int i = 0; i < mMultipleDropLatLng.size(); i++) {

                        String sLat = mMultipleDropLatLng.get(i).getLat();
                        String sLng = mMultipleDropLatLng.get(i).getLon();
                        String sTxt = mMultipleDropLatLng.get(i).getTxt();

                        double Dlatitude = Double.parseDouble(sLat);
                        double Dlongitude = Double.parseDouble(sLng);

                        System.out.println("------jai----lat and long-----------" + Dlatitude + "sfsdfsdfdsd" + Dlongitude);

                        wayPointList.add(new LatLng(Dlatitude, Dlongitude));
                        wayPointBuilder.include(new LatLng(Dlatitude, Dlongitude));

                        if (ride_type.equals("Share")) {

                            if (sTxt != null && !"".equalsIgnoreCase(sTxt)) {

                                if (sTxt.contains("Pickup")) {
                                    MarkerOptions marker = new MarkerOptions().position(new LatLng(Dlatitude, Dlongitude)).title(sTxt);
                                    marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pickup_marker));
                                    googleMap.addMarker(marker);
                                } else if (sTxt.contains("Drop")) {
                                    MarkerOptions marker = new MarkerOptions().position(new LatLng(Dlatitude, Dlongitude)).title(sTxt);
                                    marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.drop_marker));
                                    googleMap.addMarker(marker);
                                } else {
                                    MarkerOptions marker = new MarkerOptions().position(new LatLng(Dlatitude, Dlongitude)).title(sTxt);
                                    marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pickup_map_pointer_pin));
                                    googleMap.addMarker(marker);
                                }
                            } else {

                                if (i == 0) {
                                    MarkerOptions marker = new MarkerOptions().position(new LatLng(Dlatitude, Dlongitude));
                                    marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pickup_marker));
                                    googleMap.addMarker(marker);
                                } else if (i == mMultipleDropLatLng.size() - 1) {

                                    MarkerOptions marker = new MarkerOptions().position(new LatLng(Dlatitude, Dlongitude));
                                    marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.drop_marker));
                                    googleMap.addMarker(marker);
                                } else {
                                    MarkerOptions marker = new MarkerOptions().position(new LatLng(Dlatitude, Dlongitude));
                                    marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pickup_map_pointer_pin));
                                    googleMap.addMarker(marker);
                                }
                            }
                        }

                    }

                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return wayPointList;


    }


    RoutingListener listner = new RoutingListener() {
        @Override
        public void onRoutingFailure(RouteException e) {

            System.out.println("-----------jai onRoutingFailure-----------------" + e);
            if (reRouteCoute < 2) {
                reRouteCoute += 1;
                if (multiple_latlon_list.size() >= 2) {
                    GetRouteTask getRoute = new GetRouteTask(multiple_latlon_list, String.valueOf(MyCurrent_lat), String.valueOf(MyCurrent_long));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                        getRoute.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    else
                        getRoute.execute();
                } else {
                    Intent broadcastIntent1 = new Intent();
                    broadcastIntent1.setAction("com.package.ACTION_CLASS_TrackYourRide_REFRESH_page");
                    sendBroadcast(broadcastIntent1);
                }
            }

        }

        @Override
        public void onRoutingStart() {

        }

        @Override
        public void onRoutingSuccess(ArrayList<Route> arrayList, int i) {
            if (polyLines.size() > 0) {
                for (Polyline poly : polyLines) {
                    poly.remove();
                }
            }

            polyLines = new ArrayList<>();

            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(R.color.app_color));
            polyOptions.width(7);
            polyOptions.addAll(arrayList.get(0).getPoints());
            Polyline polyline = googleMap.addPolyline(polyOptions);
            polyLines.add(polyline);


            System.out.println("------------route--------------jai----" + arrayList);
            if (wayPointBuilder != null) {
                System.out.println("-------bounds----srt--");
                int padding, paddingH, paddingW;
                routePointBuilder = new LatLngBounds.Builder();
                for (int r = 0; r < arrayList.get(0).getPoints().size(); r++) {
                    routePointBuilder.include(arrayList.get(0).getPoints().get(r));
                }
                bounds = routePointBuilder.build();
//                bounds = wayPointBuilder.build();

                try {
                    final View mapview = mapFragment.getView();
                    float maxX = mapview.getMeasuredWidth();
                    float maxY = mapview.getMeasuredHeight();

                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                    int height = displayMetrics.heightPixels;
                    int width = displayMetrics.widthPixels;

                    System.out.println("***************************height and width" + height + "," + width);

                    float percentageH = 80.0f;
                    float percentageW = 90.0f;
                    paddingH = (int) (maxY * (percentageH / 100.0f));
                    paddingW = (int) (maxX * (percentageW / 100.0f));


                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, paddingW, paddingH, 100);
                    googleMap.animateCamera(cu);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }

        @Override
        public void onRoutingCancelled() {

        }
    };


    private void initilizeMap() {
        if (googleMap == null) {
            mapFragment = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.arrived_trip_view_map));
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap arg) {
                    loadMap(arg);
                }
            });
        }

    }

    private void trking_start() {

        trackstaarted++;
        Bitmap driver_marker_resized = null;

        int height = 100;
        int width = 100;
        BitmapDrawable bitmapdraw = (BitmapDrawable) ContextCompat.getDrawable(TripPage.this, R.drawable.taxiucar);

        Bitmap b = bitmapdraw.getBitmap();
        driver_marker_resized = Bitmap.createScaledBitmap(b, height, width, false);
        My_mps.removeFrom();
        My_mps.addTo(googleMap, driver_marker_resized);

    }

    public void loadMap(GoogleMap arg) {
        googleMap = arg;

        googleMap.setOnCameraIdleListener(this);
        googleMap.setOnCameraMoveStartedListener(this);
        googleMap.setOnCameraMoveListener(this);
        googleMap.setOnCameraMoveCanceledListener(this);

        googleMap.setMyLocationEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        // Enable / Disable zooming functionality
        googleMap.getUiSettings().setZoomGesturesEnabled(true);

        googleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                        TripPage.this, R.raw.mapstyle));

        googleMap.getUiSettings().setTiltGesturesEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(true);
        //   googleMap.setTrafficEnabled(true);
        HashMap<String, String> user1 = session.getTrafficImage();
        traffic_status = user1.get(SessionManager.KEY_Traffic);

        if ("1".equals(traffic_status)) {
            googleMap.setTrafficEnabled(true);
            //  session.setTrafficImage("1");
            traffic_button.setBackgroundResource(R.drawable.traffic_on_new);
            Rl_traffic.setBackgroundResource(R.drawable.curveblack);
        }
        if (gps.canGetLocation() && gps.isgpsenabled()) {
            double Dlatitude = gps.getLatitude();
            double Dlongitude = gps.getLongitude();

            MyCurrent_lat = Dlatitude;
            MyCurrent_long = Dlongitude;
            // Move the camera to last position with a zoom level
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Dlatitude, Dlongitude)).zoom(17).build();
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        } else {
            Alert1(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_gpsEnable));

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        /*if (mGoogleApiClient != null)
            mGoogleApiClient.connect();*/
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (progress > 95) {
            seekBar.setThumb(getResources().getDrawable(R.drawable.slidetounlock_arrow));
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        Bt_slider.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

        shimmer = new Shimmer();
        if (seekBar.getProgress() < 70) {
            seekBar.setProgress(0);
            sliderSeekBar.setBackgroundResource(R.color.app_color);
            Bt_slider.setVisibility(View.VISIBLE);
            shimmer.start(Bt_slider);
        } else if (seekBar.getProgress() > 70) {
            seekBar.setProgress(100);
            Bt_slider.setVisibility(View.VISIBLE);
            //        Bt_slider.setText(getResources().getString(R.string.arrivedtrip_arrivedtriptv_label));
            shimmer.start(Bt_slider);
            sliderSeekBar.setVisibility(View.VISIBLE);

            cd = new ConnectionDetector(TripPage.this);
            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {

                for (int i = 0; i < Userlist.size(); i++) {
                    if (active_ride.equals(Userlist.get(i).getRide_id())) {
                        if (Userlist.get(i).getBtn_group().equals("2")) {
                            Bt_slider.setText(getResources().getString(R.string.arrivedtrip_arrivedtriptv_label));
                            PostRequest(ServiceConstant.arrivedtrip_url, "2", Userlist.get(i).getRide_id(), Userlist.get(i).getDrop_lat(), Userlist.get(i).getDrop_lon(), Userlist.get(i).getRide_type(), "");
                        } else if (Userlist.get(i).getBtn_group().equals("3")) {
                            Bt_slider.setText(getResources().getString(R.string.lbel_begintrip));
                            PostRequest(ServiceConstant.begintrip_url, "3", Userlist.get(i).getRide_id(), Userlist.get(i).getDrop_lat(), Userlist.get(i).getDrop_lon(), Userlist.get(i).getRide_type(), Userlist.get(i).getNo_of_seat());
                        } else if (Userlist.get(i).getBtn_group().equals("4")) {
                            if (!waitingStatus) {
                                Bt_slider.setText(getResources().getString(R.string.lbel_endtrip));

                                if (toll_park_status.equals("1")) {
                                    GenderSelectMethodDialog(i, seekBar);
                                } else {
//                                    SuitcaseMethodDialog(i,seekBar);
                                    PostRequest(ServiceConstant.endtrip_url, "4", Userlist.get(i).getRide_id(), Userlist.get(i).getDrop_lat(), Userlist.get(i).getDrop_lon(), Userlist.get(i).getRide_type(), "");

                                }
//                                PostRequest(ServiceConstant.endtrip_url, "4", Userlist.get(i).getRide_id(), Userlist.get(i).getDrop_lat(), Userlist.get(i).getDrop_lon(), Userlist.get(i).getRide_type(), "");
                                if (isMyServiceRunning(GoogleNavigationService.class)) {
                                    Intent serviceIntent = new Intent(getApplicationContext(), GoogleNavigationService.class);
                                    stopService(serviceIntent);
                                }
                            } else {

                                AlertNavigation(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.voice_navigationlabel_continue));

                            }
                        }
                    }

                }


                //    PostRequest(ServiceConstant.arrivedtrip_url);

            } else {
                Alert1(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));
            }
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        if (jk == 1) {
            if (location != null) {
               // moveMarkerBackground("" + location.getLatitude(), "" + location.getLongitude());
            }
        }

        fcmstartservice();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    static public void rotateMarker(final Marker marker, final float toRotation, GoogleMap map) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final float startRotation = marker.getRotation();
        final long duration = 1555;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);

                float rot = t * toRotation + (1 - t) * startRotation;

                marker.setRotation(-rot > 180 ? rot / 2 : rot);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
//        startLocationUpdates();
        try {
            if (!data.isRunning()) {
                Gson gson = new Gson();
                String json = sharedPreferences.getString("data", "");
                data = gson.fromJson(json, DataTripPage.class);
            }
            if (data == null) {
                data = new DataTripPage(onGpsServiceUpdate);
            } else {
                data.setOnGpsServiceUpdate(onGpsServiceUpdate);
            }
            data.setRunning(true);
            data.setFirstTime(true);
            if (mLocationManager.getAllProviders().indexOf(LocationManager.GPS_PROVIDER) >= 0) {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, this);
            } else {
                Log.w("MainActivity", "No GPS location provider found. GPS data display will not be available.");
            }

            mLocationManager.addGpsStatusListener(this);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("trip page****************************" + e);
        }
        if (ride_type != null && !ride_type.equalsIgnoreCase("") && !currentRideId.equalsIgnoreCase("")) {
            if (ride_type.equalsIgnoreCase("Normal")) {
                UnviewedCountChat();
            } else {
                UnviewedCountChatShare();

            }
        }

        HashMap<String, String> chat = session.getchatNotify();
        String chat_notify = chat.get(SessionManager.KEY_CHAT_NOTIFY);

        if (chat_notify.equalsIgnoreCase("1")) {
            session.setchatNotify("0");
            Intent chatintent = new Intent(TripPage.this, ChatActivity.class);
            chatintent.putExtra("Ride_id", currentRideId);
            startActivity(chatintent);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            Chat_unviewed_TV.setVisibility(View.GONE);
            clearChatNotifyDot();
        }

    }

    private void setLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void Alert1(String title, String message) {
        final PkDialog mDialog = new PkDialog(TripPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.alert_label_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

/*
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }*/


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == DropLocationRequestCode) {


                String sAddress = data.getStringExtra("Selected_Location");
                String Slattitude = data.getStringExtra("Selected_Latitude");
                String Slongitude = data.getStringExtra("Selected_Longitude");
                tv_drop_address.setText(sAddress);

                str_drop_location = sAddress;
                str_drop_Latitude = Slattitude;
                str_drop_Longitude = Slongitude;

                if (tv_drop_address.getText().toString().length() > 0) {
                    Rl_layout_arrived.setVisibility(View.VISIBLE);


                    GPSTracker gps = new GPSTracker(TripPage.this);
                    if (gps.canGetLocation() && gps.isgpsenabled()) {
                        double dLatitude = gps.getLatitude();
                        double dLongitude = gps.getLongitude();
                        MyCurrent_lat = dLatitude;
                        MyCurrent_long = dLongitude;
                        if (Userlist.size() == 1) {

                            System.out.println("-jai----------normal ride ------------ lat long----------" + str_drop_Latitude + " " + str_drop_Longitude);
                            Userlist.get(0).setDrop_lat(str_drop_Latitude);
                            Userlist.get(0).setDrop_lon(str_drop_Longitude);
                            Userlist.get(0).setDrop_location(str_drop_location);
                            Begin(str_drop_location, "", "", "Normal");

                        }

                        LatLng fromLat = new LatLng(MyCurrent_lat, MyCurrent_long);
                        LatLng toLat = new LatLng(Double.parseDouble(str_drop_Latitude), Double.parseDouble(str_drop_Longitude));

                        if (googleMap != null) {

                            googleMap.clear();
                            Bitmap icon = BitmapFactory.decodeResource(this.getResources(),
                                    R.drawable.ic_booking_prime_play_map_topview);


                            MarkerOptions marker = new MarkerOptions().position(fromLat).title("Pickup");
                            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pickup_marker));
                            googleMap.addMarker(marker);

                            MarkerOptions marker1 = new MarkerOptions().position(toLat).title("Drop");
                            marker1.icon(BitmapDescriptorFactory.fromResource(R.drawable.drop_marker));
                            googleMap.addMarker(marker1);
                        } else {
                            Toast.makeText(TripPage.this, "Prabu", Toast.LENGTH_LONG);
                        }


                        multiple_latlon_list.clear();
                        MultipleLatLongPojo nor_u_pojo = new MultipleLatLongPojo();
                        nor_u_pojo.setLat(String.valueOf(MyCurrent_lat));
                        nor_u_pojo.setLon(String.valueOf(MyCurrent_long));
                        nor_u_pojo.setTxt(Userlist.get(0).getUser_name() + " " + "Pickup");
                        multiple_latlon_list.add(nor_u_pojo);

                        MultipleLatLongPojo nor_u_pojo1 = new MultipleLatLongPojo();
                        nor_u_pojo1.setLat(String.valueOf(str_drop_Latitude));
                        nor_u_pojo1.setLon(String.valueOf(str_drop_Longitude));
                        nor_u_pojo.setTxt(Userlist.get(0).getUser_name() + " " + "Drop");
                        multiple_latlon_list.add(nor_u_pojo1);

                      /*  if (!"".equalsIgnoreCase(carIcon)) {

                            Picasso.with(TripPage.this)
                                    .load(carIcon)
                                    .into(new Target() {
                                        @Override
                                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                                            if (bitmap != null) {
                                                String s = BitMapToString(bitmap);
                                                System.out.println("session bitmap" + s);
                                                session.setVehicle_BitmapImage(s);

                                                double Dlatitude = MyCurrent_lat;
                                                double Dlongitude = MyCurrent_long;


                                                if (currentMarker != null) {
                                                    currentMarker.remove();
                                                }

                                                bmp = Bitmap.createScaledBitmap(bmp, width, height, false);
                                                currentMarker = googleMap.
                                                        addMarker(new MarkerOptions()
                                                                .position(new LatLng(Dlatitude, Dlongitude))
                                                                .icon(BitmapDescriptorFactory
                                                                        .fromBitmap(bmp)));


                                            }

                                        }

                                        @Override
                                        public void onBitmapFailed(Drawable errorDrawable) {

                                        }

                                        @Override
                                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                                        }
                                    });
                        }
*/
                        if (multiple_latlon_list.size() >= 2) {
                            reRouteCoute = 0;
                            GetRouteTask getRoute = new GetRouteTask(multiple_latlon_list, String.valueOf(MyCurrent_lat), String.valueOf(MyCurrent_long));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                                getRoute.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            else
                                getRoute.execute();
                        }

                    }
                } else {
                    Rl_layout_arrived.setVisibility(View.GONE);
                }

            }

        }
    }

    private boolean isWazeMapsInstalled() {
        try {
            ApplicationInfo info = getPackageManager().getApplicationInfo("com.waze", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case PERMISSION_REQUEST_NAVIGATION_CODE_WAZE:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!Settings.canDrawOverlays(TripPage.this)) {
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                Uri.parse("package:" + getPackageName()));
                        startActivityForResult(intent, OVERLAY_PERMISSION_WAZE_REQ_CODE);
                    } else {
                        for (int i = 0; i < Userlist.size(); i++) {
                            System.out.println("-----------active_ride--------getRide_id--------" + active_ride + Userlist.get(i).getRide_id());
                            if (active_ride.equals(Userlist.get(i).getRide_id())) {
                                if (Userlist.get(i).getBtn_group().equals("2")) {
                                    Moviewaze_map(Userlist.get(i).getPickup_lat(), Userlist.get(i).getPickup_lon());
                                } else {
                                    Moviewaze_map(Userlist.get(i).getDrop_lat(), Userlist.get(i).getDrop_lon());

                                }
                            }

                        }

                    }
                }
                break;
            case REQUEST_CODE_PERMISSIONS:
                boolean foreground = false, background = false;
                for (int i = 0; i < permissions.length; i++) {
                    if (permissions[i].equalsIgnoreCase(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        //foreground permission allowed
                        if (grantResults[i] >= 0) {
                            foreground = true;
                            continue;
                        } else {
                            break;
                        }
                    }
                    if (permissions[i].equalsIgnoreCase(Manifest.permission.ACCESS_BACKGROUND_LOCATION)) {
                        if (grantResults[i] >= 0) {
                            foreground = true;
                            background = true;
                        } else {
//                            Toast.makeText(getApplicationContext(), "Background location location permission denied", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                if (foreground) {
                    if (background) {
                        handleLocationUpdates();
                    } else {
                        handleForegroundLocationUpdates();
                    }
                }

        }
    }


    //-----------------Move Back on  phone pressed  back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            // nothing
            return true;
        }
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        My_mps.removeFrom();
        stoptimertask();
        try {
            if (customHandler != null) {
                customHandler.removeCallbacks(updateTimerThread);
            }
            locatiostopservice();
            meterstopservice();
            fcmstopservice();
            unregisterReceiver(finishReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onCameraIdle() {
        isMapToched = false;
        System.out.println("===========Muruga isMapTouched Idle=========" + "The usercancel the map.  " + isMapToched);

    }

    @Override
    public void onCameraMoveCanceled() {
        isMapToched = false;
        System.out.println("===========Muruga isMapTouched cancelled=========" + "The usercancel the map.  " + isMapToched);

    }

    @Override
    public void onCameraMove() {
        isMapToched = true;

    }

    @Override
    public void onCameraMoveStarted(int reason) {
        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            isMapToched = true;

        } else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_API_ANIMATION) {
            isMapToched = true;


        } else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_DEVELOPER_ANIMATION) {
            isMapToched = true;

        }
    }


    private void MapNavicationChooseDialog() {

        System.out.println("************Select Navication**************");

        final Dialog dialog = new Dialog(TripPage.this, R.style.DialogAnimation2);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.map_navication_select);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        final MaterialDialog navication_dialog = new MaterialDialog(ArrivedTrip.this);
//        View view = LayoutInflater.from(ArrivedTrip.this).inflate(R.layout.map_navication_select, null);

        RelativeLayout Rl_google_map_navication = (RelativeLayout) dialog.findViewById(R.id.google_map_navication);
        RelativeLayout Rl_waze_map_navication = (RelativeLayout) dialog.findViewById(R.id.waze_map_navication);
        RelativeLayout Rl_cancel_dialog = (RelativeLayout) dialog.findViewById(R.id.navigation_header_cancel_layout);


        Rl_google_map_navication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (Build.VERSION.SDK_INT >= 23) {
                    if (!Settings.canDrawOverlays(TripPage.this)) {
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                Uri.parse("package:" + getPackageName()));
                        startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);
                    } else {
                        moveNavigation();
                    }

                } else {
                    moveNavigation();
                }


                dialog.dismiss();

            }
        });


        Rl_waze_map_navication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= 23) {
                    if (!Settings.canDrawOverlays(TripPage.this)) {
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                Uri.parse("package:" + getPackageName()));
                        startActivityForResult(intent, OVERLAY_PERMISSION_WAZE_REQ_CODE);
                    } else {

                        for (int i = 0; i < Userlist.size(); i++) {
                            System.out.println("-----------active_ride--------getRide_id--------" + active_ride + Userlist.get(i).getRide_id());
                            if (active_ride.equals(Userlist.get(i).getRide_id())) {
                                if (Userlist.get(i).getBtn_group().equals("2")) {
                                    Moviewaze_map(Userlist.get(i).getPickup_lat(), Userlist.get(i).getPickup_lon());
                                } else {
                                    Moviewaze_map(Userlist.get(i).getDrop_lat(), Userlist.get(i).getDrop_lon());

                                }
                            }

                        }
                    }
                } else {
                    for (int i = 0; i < Userlist.size(); i++) {
                        System.out.println("-----------active_ride--------getRide_id--------" + active_ride + Userlist.get(i).getRide_id());
                        if (active_ride.equals(Userlist.get(i).getRide_id())) {
                            if (Userlist.get(i).getBtn_group().equals("2")) {
                                Moviewaze_map(Userlist.get(i).getPickup_lat(), Userlist.get(i).getPickup_lon());
                            } else {
                                Moviewaze_map(Userlist.get(i).getDrop_lat(), Userlist.get(i).getDrop_lon());

                            }
                        }

                    }
                }

                dialog.dismiss();

            }
        });


        Rl_cancel_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

//        navication_dialog.setView(view).show();

    }


    private void Moviewaze_map(String Str_pickUp_Lat, String Str_pickUp_Long) {

        if (isWazeMapsInstalled()) {


            if (!isMyServiceRunning(GoogleNavigationService.class)) {
                startService(new Intent(getApplicationContext(), GoogleNavigationService.class));
            }
            String url = String.format("waze://?ll=" + Double.parseDouble(Str_pickUp_Lat) + "," + Double.parseDouble(Str_pickUp_Long) + "&navigate=yes");
            System.out.println("************WAZE MAP NAVIGATION URL*************" + url);
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        } else {

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.waze"));
            startActivity(intent);
        }

    }

    private void moveNavigation() {

        if (Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(TripPage.this)) {

                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);

            } else {

                System.out.println("-----------Userlist----------------" + Userlist.size());

                for (int i = 0; i < Userlist.size(); i++) {
                    System.out.println("-----------active_ride--------getRide_id--------" + active_ride + Userlist.get(i).getRide_id());
                    if (active_ride.equals(Userlist.get(i).getRide_id())) {
                        if (Userlist.get(i).getBtn_group().equals("2")) {
                            moveNavigationPickup(Userlist.get(i).getPickup_lat(), Userlist.get(i).getPickup_lon());
                        } else {
                            moveNavigationdrop(Userlist.get(i).getDrop_lat(), Userlist.get(i).getDrop_lon());
                        }
                    }

                }
            }

        } else {

            System.out.println("-----------Userlist----------------" + Userlist.size());

            for (int i = 0; i < Userlist.size(); i++) {
                System.out.println("-----------active_ride--------getRide_id--------" + active_ride + Userlist.get(i).getRide_id());
                if (active_ride.equals(Userlist.get(i).getRide_id())) {
                    if (Userlist.get(i).getBtn_group().equals("2")) {
                        moveNavigationPickup(Userlist.get(i).getPickup_lat(), Userlist.get(i).getPickup_lon());
                    } else {
                        moveNavigationdrop(Userlist.get(i).getDrop_lat(), Userlist.get(i).getDrop_lon());
                    }
                }

            }


        }

//        addBackLayout();
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(TripPage.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }


    // --------------------Method for Toll--------------------
    private void GenderSelectMethodDialog(final int i, final SeekBar seekbar) {
        toll_method_dialog = new Dialog(TripPage.this);
        toll_method_dialog.getWindow();
        toll_method_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        toll_method_dialog.setContentView(R.layout.dialog_toll_select_method);
        toll_method_dialog.setCanceledOnTouchOutside(true);
        toll_method_dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        toll_method_dialog.show();
        toll_method_dialog.getWindow().setGravity(Gravity.CENTER);
        toll_method_dialog.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);


        TextView cancel_tv = (TextView) toll_method_dialog.findViewById(R.id.dialog_toll_cancel_tv);
        TextView confirm_tv = (TextView) toll_method_dialog.findViewById(R.id.dialog_toll_confirm_tv);
        tollChargeEt = (EditText) toll_method_dialog.findViewById(R.id.toll_charge_et);
        parkingChargeEt = (EditText) toll_method_dialog.findViewById(R.id.parking_charge_et);

        TextView currency_parking_tv = (TextView) toll_method_dialog.findViewById(R.id.toll_parking_currency_tv);
        TextView currency_toll_tv = (TextView) toll_method_dialog.findViewById(R.id.toll_currency_tv);
        String tollCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(currency);

        currency_parking_tv.setText(tollCurrencySymbol);
        currency_toll_tv.setText(tollCurrencySymbol);


        confirm_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toll_method_dialog.dismiss();

                cd = new ConnectionDetector(TripPage.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {

                    PostRequest(ServiceConstant.endtrip_url, "4", Userlist.get(i).getRide_id(), Userlist.get(i).getDrop_lat(), Userlist.get(i).getDrop_lon(), Userlist.get(i).getRide_type(), "");

                    //    PostRequest(ServiceConstant.arrivedtrip_url);

                } else {
                    Alert1(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));
                }


            }
        });
        cancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toll_method_dialog.dismiss();
                seekbar.setProgress(0);
                toll_method_dialog.getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                TripPage.this.getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                if (isInternetPresent) {

                    parkingChargeEt.setText("");
                    tollChargeEt.setText("");
                    PostRequest(ServiceConstant.endtrip_url, "4", Userlist.get(i).getRide_id(), Userlist.get(i).getDrop_lat(), Userlist.get(i).getDrop_lon(), Userlist.get(i).getRide_type(), "");

                    //    PostRequest(ServiceConstant.arrivedtrip_url);

                } else {
                    Alert1(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));
                }


            }
        });


    }

    // ----------------suitcase ---------------------
    private void SuitcaseMethodDialog(final int i, final SeekBar seekbar) {
        toll_method_dialog = new Dialog(TripPage.this);
        toll_method_dialog.getWindow();
        toll_method_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        toll_method_dialog.setContentView(R.layout.dialog_suitcase_method);
        toll_method_dialog.setCanceledOnTouchOutside(false);
        toll_method_dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        toll_method_dialog.show();
        toll_method_dialog.getWindow().setGravity(Gravity.CENTER);
        toll_method_dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        TextView suicase_fare_txt = (TextView) toll_method_dialog.findViewById(R.id.suicase_fare_txt);
        TextView cancel_tv = (TextView) toll_method_dialog.findViewById(R.id.suicase_cancel_tv);
        TextView confirm_tv = (TextView) toll_method_dialog.findViewById(R.id.suicase_confirm_tv);
        SuitcaseEt = (EditText) toll_method_dialog.findViewById(R.id.suitcase_charge_et);

        String tollCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(currency);
        suicase_fare_txt.setText(getResources().getString(R.string.note_suitcase)+" "+tollCurrencySymbol+suitcase_fee);

        seekbar.setProgress(0);

        confirm_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SuitcaseEt.getText().toString().trim().length()>0){
                    toll_method_dialog.dismiss();

                    cd = new ConnectionDetector(TripPage.this);
                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {

                        PostRequest(ServiceConstant.endtrip_url, "4", Userlist.get(i).getRide_id(), Userlist.get(i).getDrop_lat(), Userlist.get(i).getDrop_lon(), Userlist.get(i).getRide_type(), "");
                    } else {
                        Alert1(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));
                    }
                } else {
                    Toast.makeText(TripPage.this, getResources().getString(R.string.enter_suicases_amount), Toast.LENGTH_SHORT).show();
                }



            }
        });
        cancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toll_method_dialog.dismiss();
                toll_method_dialog.getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                TripPage.this.getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                if (isInternetPresent) {
                    SuitcaseEt.setText("");
                    PostRequest(ServiceConstant.endtrip_url, "4", Userlist.get(i).getRide_id(), Userlist.get(i).getDrop_lat(), Userlist.get(i).getDrop_lon(), Userlist.get(i).getRide_type(), "");

                } else {
                    Alert1(getResources().getString(R.string.alert_sorry_label_title), getResources().getString(R.string.alert_nointernet));
                }


            }
        });


    }

    //-------------------------------------------------Start and stop Location update foreground service by Arun


    private void locationstartservice() {
        if (isMyServiceRunning(JobLocationUpdateForgroundService.class)) {

        } else {
            Intent startIntent = new Intent(getApplicationContext(), JobLocationUpdateForgroundService.class);
            startIntent.setAction(Constant.ACTION.STARTFOREGROUND_ACTION);
            startService(startIntent);
        }

    }

    private void locatiostopservice() {

        if (isMyServiceRunning(JobLocationUpdateForgroundService.class)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Intent startIntent = new Intent(getApplicationContext(), JobLocationUpdateForgroundService.class);
                startIntent.setAction(Constant.ACTION.STOPFOREGROUND_ACTION);
                startService(startIntent);
            } else {
                stopService(new Intent(getBaseContext(), JobLocationUpdateForgroundService.class));
            }
        } else {

        }

    }

    private void meterstartservice() {
        if (isMyServiceRunning(GpsTripForgroundServices.class)) {

        } else {
            startService(new Intent(getBaseContext(), GpsTripForgroundServices.class));
        }

    }

    private void meterstopservice() {
        if (isMyServiceRunning(GpsTripForgroundServices.class)) {
            stopService(new Intent(getBaseContext(), GpsTripForgroundServices.class));
        }

    }

    private void fcmstartservice() {
        if (isMyServiceRunning(FcmTrackingForgroundServices.class)) {

        } else {
            startService(new Intent(getBaseContext(), FcmTrackingForgroundServices.class));
        }

    }

    private void fcmstopservice() {
        if (isMyServiceRunning(FcmTrackingForgroundServices.class)) {
            stopService(new Intent(getBaseContext(), FcmTrackingForgroundServices.class));
        }

    }

    public void resetData() {
        data = new DataTripPage(onGpsServiceUpdate);
    }


    private void startStopServices() {
        locatiostopservice();
        meterstartservice();
    }

    public static DataTripPage getData() {
        return data;
    }

    private void initDistance() {
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        data = new DataTripPage(onGpsServiceUpdate);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        onGpsServiceUpdate = new DataTripPage.onGpsServiceUpdate() {
            @Override
            public void update() {
                double distanceTemp = data.getDistance();
                double currentSpeedTemp = data.getCurSpeed();
                location_lat = String.valueOf(data.getCurrentLat());
                location_long = String.valueOf(data.getCurrentLon());
                jk = 0;
                distanceTemp /= 1000.0;
                distance_trip = String.format("%.2f", distanceTemp);
                speed_trip = String.format("%.0f", currentSpeedTemp);
                String dis = String.format("%.2f", distanceTemp);
//                data.setTotalDistance(distance_trip);
              /*  if (!isRun) {
                    if (location_lat == null || location_lat.equals("") || location_lat.equals(" ") || location_lat.equals("0.0") || location_lat.equals("0") || (location_long == null || location_long.equals("") || location_long.equals(" ") || location_long.equals("0.0") || location_long.equals("0"))) {
                    } else {
                       // moveMarkerBackground(location_lat, location_long);
                    }
                }*/
            }
        };
    }


    @Override
    protected void onPause() {
        super.onPause();
        mLocationManager.removeUpdates(this);
        mLocationManager.removeGpsStatusListener(this);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(data);
        prefsEditor.putString("data", json);
        prefsEditor.commit();
    }

    private void moveMarkerBackground(String lat, String longg) {

        isRun = true;

        try {
            MyCurrent_lat = Double.parseDouble(lat);
            MyCurrent_long = Double.parseDouble(longg);

            final LatLng latLng = new LatLng(MyCurrent_lat, MyCurrent_long);

            if (oldLatLng == null) {
                oldLatLng = latLng;
            }
            newLatLng = latLng;
            if (mLatLngInterpolator == null) {
                mLatLngInterpolator = new LatLngInterpolator.Linear();
            }
            oldLocation = new Location("");
            oldLocation.setLatitude(oldLatLng.latitude);
            oldLocation.setLongitude(oldLatLng.longitude);

            final LatLng changelat = new LatLng(MyCurrent_lat, MyCurrent_long);

            changelatLocations = new Location("");
            changelatLocations.setLatitude(changelat.latitude);
            changelatLocations.setLongitude(changelat.longitude);

            final float bearingValue = oldLocation.bearingTo(changelatLocations);
            myMovingDistance = oldLocation.distanceTo(changelatLocations);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        if (currentMarker != null) {
                            currentMarker.remove();
                        }

                        if (googleMap != null) {
                            if (drivermarker != null) {
                                if (!String.valueOf(bearingValue).equalsIgnoreCase("NaN")) {
                                    rotateMarker(drivermarker, bearingValue, googleMap);
                                    MarkerAnimation.animateMarkerToGB(drivermarker, latLng, mLatLngInterpolator);
                                    float zoom = googleMap.getCameraPosition().zoom;
                                    CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(zoom).build();
                                    CameraUpdate camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                                    googleMap.animateCamera(camUpdate);
                                }
                            } else {
                                if (currentMarker != null) {
                                    currentMarker.remove();
                                }


                                bmp = Bitmap.createScaledBitmap(bmp, width, height, false);

                                drivermarker = googleMap.addMarker(new MarkerOptions()
                                        .position(latLng)
                                        .icon(BitmapDescriptorFactory.fromBitmap(bmp))
                                        .anchor(0.5f, 0.5f)
                                        .rotation(changelatLocations.getBearing())
                                        .flat(true));
                            }


                        }
                        oldLatLng = newLatLng;

                    }
                });



            isRun = false;
            //      sendLocationToUser(myLocation);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //-------------------------------------------------Start and stop Location update foreground service end Arun


    //----------------------------Local Db Start
    private void dbInitialize() {
        tripDetailsDbHelper = new TripDetailsDbHelper(getApplicationContext());
    }
    //----------------------------Local Db end



    private void requestLocationPermission() {
        boolean foreground = ActivityCompat.checkSelfPermission(TripPage.this,
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        if (foreground) {
            boolean background = ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED;
            if (background) {
                handleLocationUpdates();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, REQUEST_CODE_PERMISSIONS);
            }
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION}, REQUEST_CODE_PERMISSIONS);
        }
    }

    private void handleLocationUpdates() {
        //foreground and background
//        Toast.makeText(getApplicationContext(),"Start Foreground and Background Location Updates",Toast.LENGTH_SHORT).show();
    }

    private void handleForegroundLocationUpdates() {
        //handleForeground Location Updates
//        Toast.makeText(getApplicationContext(),"Start foreground location updates",Toast.LENGTH_SHORT).show();
    }
    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 5000, 2000); //
    }

    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        GPSTracker gps = new GPSTracker(TripPage.this);
                        if (gps.canGetLocation() && gps.isgpsenabled()) {
                            System.out.println("-->>>"+gps.getLatitude());

                            if(oldtime.equals(""))
                            {
                                if (gps.getLatitude() != 0.0 ) {
                                    moveMarkerBackground("" + gps.getLatitude(), "" + gps.getLongitude());
                                }
                            }
                            else
                            {
                                System.out.println("-->>>"+oldtime);

                                Calendar aCalendar = Calendar.getInstance();
                                SimpleDateFormat aDateFormat = new SimpleDateFormat("hh:mm:ss aa");
                                String currentitme = aDateFormat.format(aCalendar.getTime());
                                System.out.println("-->>>"+currentitme);


                                try {
                                    Date date1 = aDateFormat.parse(oldtime);
                                    Date date2 = aDateFormat.parse(currentitme);
                                    long different = date2.getTime() - date1.getTime();
                                  /* int days = (int) (difference / (1000*60*60*24));
                                    hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
                                    int  min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
*/
                                    long secondsInMilli = 1000;
                                    long minutesInMilli = secondsInMilli * 60;
                                    long hoursInMilli = minutesInMilli * 60;
                                    long daysInMilli = hoursInMilli * 24;

                                    long elapsedDays = different / daysInMilli;
                                    different = different % daysInMilli;

                                    long elapsedHours = different / hoursInMilli;
                                    different = different % hoursInMilli;

                                    long elapsedMinutes = different / minutesInMilli;
                                    different = different % minutesInMilli;

                                    long elapsedSeconds = different / secondsInMilli;
                                    System.out.println("elapsedMinutes--"+elapsedMinutes+"-elapsedSeconds-"+elapsedSeconds);
                                    if(elapsedMinutes == 0 && elapsedSeconds > 10)
                                    {
                                        if (gps.getLatitude() != 0.0 ) {
                                            moveMarkerBackground("" + gps.getLatitude(), "" + gps.getLongitude());
                                        }
                                    }
                                    else  if(elapsedMinutes >= 1)
                                    {
                                        if (gps.getLatitude() != 0.0 ) {
                                            moveMarkerBackground("" + gps.getLatitude(), "" + gps.getLongitude());
                                        }
                                    }

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                            }

                        }
                    }
                });
            }
        };
    }

    }
