package com.hellotaxidriver.driver.tripdetailspackage

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.hellotaxidriver.driver.R




class passengerlistsummaryModelAdapter(val context: Context, val listModelArrayList: ArrayList<passengerlistsummaryModel>) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val view: View?
        val vh: passengerlistsummaryModelAdapterViewHolder1

        if (convertView == null)
        {
            val layoutInflater = LayoutInflater.from(context)
            view = layoutInflater.inflate(R.layout.farelistadapterkotlin, parent, false)
            vh = passengerlistsummaryModelAdapterViewHolder1(view)
            view.tag = vh
        }
        else
        {
            view = convertView
            vh = view.tag as passengerlistsummaryModelAdapterViewHolder1
        }

        vh.tvTitle.text = listModelArrayList[position].title
        vh.tvTitle.setTextColor(context.getResources().getColor(R.color.lite_black));
        var value:String=""+listModelArrayList[position].content;
        if(value.startsWith("-"))
        {
            vh.tvContent.text = listModelArrayList[position].content
            vh.tvContent.setTextColor(context.getResources().getColor(R.color.red));
        }
        else
        {
            vh.tvContent.text = listModelArrayList[position].content
            vh.tvContent.setTextColor(context.getResources().getColor(R.color.lite_black));
        }


        return view
    }

    override fun getItem(position: Int): Any {
        return listModelArrayList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return listModelArrayList.size
    }
}

private class passengerlistsummaryModelAdapterViewHolder1(view: View?)
{
    val tvTitle: TextView = view?.findViewById<TextView>(R.id.faretile) as TextView
    val tvContent: TextView = view?.findViewById<TextView>(R.id.farevalue) as TextView

}