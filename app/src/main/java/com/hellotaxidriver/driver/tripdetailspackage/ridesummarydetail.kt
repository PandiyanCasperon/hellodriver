package com.hellotaxidriver.driver.tripdetailspackage

import android.app.Dialog
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.Window
import android.widget.*
import com.android.volley.Request
import com.app.service.ServiceConstant
import com.app.service.ServiceManager
import com.hellotaxidriver.driver.R
import com.hellotaxidriver.driver.Utils.CurrencySymbolConverter
import com.hellotaxidriver.driver.Utils.SessionManager
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView
import com.google.android.gms.maps.GoogleMap
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class ridesummarydetail : AppCompatActivity() {
    var sCurrencySymbol: String = ""
    var trip_deatils_json: String = ""
    var isJsonAvail: Boolean = false
    lateinit var pickup_time: TextView
    lateinit var drop_time: TextView
    lateinit var my_rides_detail_cabname: TextView
    lateinit var total_amount_txt: TextView
    lateinit var pickup_address_txt: TextView
    lateinit var drop_address_txt: TextView
    lateinit var Tv_tripdetail_rideId: TextView
    lateinit var my_ride_detail_driverImage: CircleImageView
    lateinit var tripsummry_back: ImageView
    lateinit var driver_dashboard_ratting: RatingBar
    lateinit var mFareListView: ExpandableHeightListView
    lateinit var passengerfarelist: ExpandableHeightListView
    lateinit var finalamt: TextView
    lateinit var paymenttypee: TextView

    lateinit var googleMap: GoogleMap

    lateinit var mapFragment: MyMapFragment

    lateinit var my_rides_detail_mapview_layout: RelativeLayout


    lateinit var my_rides_detail_route_map_imageview: ImageView
    lateinit var passengeramt: TextView

    lateinit var sm: SessionManager
    lateinit var km: TextView
    lateinit var symbolblack: TextView
    lateinit var splitzeros: TextView
    lateinit var viewfarebreakup: TextView
    lateinit var ride_status: String


    lateinit var layoutsummery_and_bill_details: RelativeLayout

    lateinit var dialog: Dialog
    lateinit var scrollView_tripsummry: ScrollView
    var price: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ridefullsummary)
        sm = SessionManager(applicationContext)

        val ss: String = intent.getStringExtra("rideid")
/*        if (intent.hasExtra("trip_details_json")) {
            trip_deatils_json = intent.getStringExtra("trip_details_json")
            ride_status = intent.getStringExtra("ride_status")
            isJsonAvail = true
        } else {
            isJsonAvail = false

        }*/

        //view intilization will be called here
        intilizationofviews()

        viewfarebreakup.setOnClickListener {


            if (layoutsummery_and_bill_details.getVisibility() == View.VISIBLE) {
                layoutsummery_and_bill_details.visibility = View.GONE
//                layoutsummery_and_bill_details.visibility = View.VISIBLE

            } else {
                layoutsummery_and_bill_details.visibility = View.VISIBLE
                scrollView_tripsummry.postDelayed(Runnable {
                    //replace this line to scroll up or down
                    scrollView_tripsummry.fullScroll(ScrollView.FOCUS_DOWN);

                }, 100L)
            }
        }

        tripsummry_back.setOnClickListener {
            finish();
        }


        Tv_tripdetail_rideId.text = getString(R.string.payment_list_ridesid) + "-" + ss
        showdialog()
        if (isJsonAvail) {
//            tripDetailsParserMethod(trip_deatils_json)
            val jsonParams = HashMap<String, String>()
            val userDetails = sm.getUserDetails()
            var driverId: String? = userDetails[SessionManager.KEY_DRIVERID]
            jsonParams["driver_id"] = "" + driverId
            jsonParams["ride_id"] = "" + ss
            val manager = ServiceManager(applicationContext, acceptServicelistener)
            manager.makeServiceRequest(ServiceConstant.tripsummery_view_url, Request.Method.POST, jsonParams)
        } else {
            val jsonParams = HashMap<String, String>()
            val userDetails = sm.getUserDetails()
            var driverId: String? = userDetails[SessionManager.KEY_DRIVERID]
            jsonParams["driver_id"] = "" + driverId
            jsonParams["ride_id"] = "" + ss
            val manager = ServiceManager(applicationContext, acceptServicelistener)
            manager.makeServiceRequest(ServiceConstant.tripsummery_view_url, Request.Method.POST, jsonParams)
        }
    }

    fun intilizationofviews() {

        mapFragment =
                supportFragmentManager.findFragmentById(R.id.tripsummery_view_map) as MyMapFragment
        layoutsummery_and_bill_details = findViewById<RelativeLayout>(R.id.layoutsummery_and_bill_details)
        my_rides_detail_mapview_layout = findViewById<RelativeLayout>(R.id.my_rides_detail_mapview_layout)
        scrollView_tripsummry = findViewById<ScrollView>(R.id.scrollView_tripsummry)
        pickup_time = findViewById<TextView>(R.id.pickup_time)
        viewfarebreakup = findViewById<TextView>(R.id.viewfarebreakup)
        km = findViewById<TextView>(R.id.km)
        finalamt = findViewById<TextView>(R.id.finalamt)
        drop_time = findViewById<TextView>(R.id.drop_time)
        my_rides_detail_cabname = findViewById<TextView>(R.id.my_rides_detail_cabname)
        total_amount_txt = findViewById<TextView>(R.id.total_amount_txt)
        pickup_address_txt = findViewById<TextView>(R.id.pickup_address_txt)
        drop_address_txt = findViewById<TextView>(R.id.drop_address_txt)
        Tv_tripdetail_rideId = findViewById<TextView>(R.id.tripsummry_rideidTv)
        symbolblack = findViewById<TextView>(R.id.symbolblack)
        paymenttypee = findViewById<TextView>(R.id.paymenttypee)
        splitzeros = findViewById<TextView>(R.id.splitzeros)
        passengeramt = findViewById<TextView>(R.id.passengeramt)
        tripsummry_back = findViewById<ImageView>(R.id.tripsummry_back)
        my_rides_detail_route_map_imageview = findViewById<ImageView>(R.id.my_rides_detail_route_map_imageview)
        my_ride_detail_driverImage = findViewById<CircleImageView>(R.id.my_ride_detail_driverImage)
        driver_dashboard_ratting = findViewById<RatingBar>(R.id.driver_dashboard_ratting)
        mFareListView = findViewById<ExpandableHeightListView>(R.id.my_rides_payment_detail_listView)
        passengerfarelist = findViewById<ExpandableHeightListView>(R.id.passengerfarelist)
        if (isJsonAvail) {
            if (ride_status.equals("0")) {
                paymenttypee.visibility = View.GONE
                viewfarebreakup.visibility = View.GONE
            } else {
                paymenttypee.visibility = View.VISIBLE
                viewfarebreakup.visibility = View.VISIBLE
            }
        } else {
            paymenttypee.visibility = View.VISIBLE
            viewfarebreakup.visibility = View.VISIBLE
        }

    }

    internal var acceptServicelistener: ServiceManager.ServiceListener = object : ServiceManager.ServiceListener {
        override fun onCompleteListener(`object`: String) {
            dismissdialog()
            try {
                try {
                    val jobject = JSONObject(`object`)
                    val Str_status = jobject.getString("status")
                    if (Str_status == "1") {

                        var PRIVATE_MODE = 0
                        val PREF_NAME = "mapdattoshow"
                        val sharedPref: SharedPreferences = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
                        val editor = sharedPref.edit()
                        scrollView_tripsummry.visibility = View.VISIBLE
                        val `object` = jobject.getJSONObject("response")
                        //  val jarray = `object`.getJSONObject("details")
                        val user_profileobj = `object`.getJSONObject("user_profile")
                        if (`object`.has("currency")) {
                            val currency = `object`.getString("currency")
                            sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(currency)
                            symbolblack.text = sCurrencySymbol
                        }
                        if (`object`.has("payment_method")) {
                            val payment_method = `object`.getString("payment_method")
                            paymenttypee.text = payment_method;

                        }
                        if (`object`.has("driver_revenue")) {
                            val driver_revenue = `object`.getString("driver_revenue")
                            if (driver_revenue.contains(".")) {
                                val first = driver_revenue.split("\\.".toRegex())[0]
                                val second = driver_revenue.split("\\.".toRegex())[1]
                                total_amount_txt.text = first
                                splitzeros.text = "." + second
                                if(first.equals("0"))
                                {
                                    viewfarebreakup.visibility = View.GONE
                                }
                            } else {
                                total_amount_txt.text = driver_revenue
                            }
                        }
                        else
                        {
                            viewfarebreakup.visibility = View.GONE
                        }




                        if (`object`.has("invoice_src"))
                        {
                            my_rides_detail_route_map_imageview.visibility = View.GONE
                            my_rides_detail_mapview_layout.visibility = View.VISIBLE

                             val invoice_src = `object`.getString("invoice_src")
                              if(invoice_src.length>3)
                              {
                                  Picasso.with(applicationContext)
                                          .load(invoice_src)
                                          .into(my_rides_detail_route_map_imageview)
                                   my_rides_detail_route_map_imageview.visibility = View.VISIBLE
                                    my_rides_detail_mapview_layout.visibility = View.GONE


                                }
                                else
                                {
                                    my_rides_detail_route_map_imageview.visibility = View.VISIBLE
                                    my_rides_detail_mapview_layout.visibility = View.GONE

                                }
                        }

                        val user_name = user_profileobj.getString("user_name")
                        my_rides_detail_cabname.text = user_name
                        if (user_profileobj.has("user_review")) {
                            driver_dashboard_ratting.setRating(java.lang.Float.parseFloat(user_profileobj.getString("user_review")))
                        }
                        if (user_profileobj.has("user_image")) {
                            var user_image: String = "";
                            try {
                                user_image = user_profileobj.getString("user_image")
                            } catch (e: Exception) {
                            }
                            if (user_image.length > 3) {
                                Picasso.with(applicationContext)
                                        .load(user_image)
                                        .into(my_ride_detail_driverImage)
                            }
                        }
                        val pickup = `object`.getJSONObject("pickup")
                        if (pickup.has("location")) {
                            pickup_address_txt.text = pickup.getString("location")
                            val pickuplatlong = pickup.getJSONObject("latlong")
                            if (pickuplatlong.has("lat")) {
                                var lat: String = pickuplatlong.getString("lat")
                                editor.putString("pickuplat", lat)
                            }
                            if (pickuplatlong.has("lon")) {
                                var lon: String = pickuplatlong.getString("lon")
                                editor.putString("pickuplong", lon)
                            }
                        }


                        if (`object`.has("driver_revenue")) {
                            val driver_revenue = `object`.getString("driver_revenue")
                            finalamt.text = sCurrencySymbol + driver_revenue

                        }

                        val drop = `object`.getJSONObject("drop")
                        if (drop.has("location")) {
                            drop_address_txt.text = drop.getString("location")

                            val droplatlong = drop.getJSONObject("latlong")
                            if (droplatlong.has("lat")) {
                                var lat: String = droplatlong.getString("lat")
                                editor.putString("droplatt", lat)
                            }
                            if (droplatlong.has("lon")) {
                                var lon: String = droplatlong.getString("lon")
                                editor.putString("droplongg", lon)
                            }
                        }
                        editor.apply()
                        mapFragment.getMapAsync(mapFragment)


                        if (`object`.has("pickup_date")) {
                            val pickup_date = `object`.getString("pickup_date")
                            pickup_time.text = pickup_date
                        } else {
                            pickup_time.text = "--:--"

                        }
                        if (`object`.has("drop_date")) {
                            val drop_date = `object`.getString("drop_date")
                            drop_time.text = drop_date
                            if(drop_date.equals(""))
                            {
                                drop_time.visibility = View.INVISIBLE
                            }
                            else
                            {
                                drop_time.visibility = View.VISIBLE
                            }
                        } else {
                            drop_time.text = "--:--"
                            drop_time.visibility = View.INVISIBLE

                        }


                        val summary = `object`.getJSONObject("summary")
                        if (summary.has("ride_distance")) {
                            var distance_unit: String = ""
                            if (`object`.has("distance_unit")) {
                                distance_unit = `object`.getString("distance_unit")
                            }
                            km.text = summary.getString("ride_distance") + "\n" + "" + distance_unit
                        }
                        val driver_earning = `object`.getJSONArray("driver_earning")
                        val farelist = ArrayList<ridelistsummaryModel>()
                        if (driver_earning.length() > 0) {
                            for (k in 0 until driver_earning.length()) {
                                val job = driver_earning.getJSONObject(k)
                                var title: String = job.getString("title");
                                var value: String = sCurrencySymbol + job.getString("value");
                                var positive: String = job.getString("positive");
                                if (positive.equals("0")) {
                                    farelist.add(ridelistsummaryModel(1, title, " " + value))
                                }
                                else if(positive.equals("2")){
                                    farelist.add(ridelistsummaryModel(1, title, "+" + value))
                                }
                                else {
                                    farelist.add(ridelistsummaryModel(1, title, "-" + value))
                                }
                            }
                            var listViewAdapter = ridelistsummaryModelAdapter(applicationContext, farelist)
                            mFareListView.setAdapter(listViewAdapter)
                            mFareListView.isExpanded = true
                            mFareListView.visibility = View.VISIBLE
                        } else {
                            mFareListView.visibility = View.GONE
                        }


                        val passenger_fare = `object`.getJSONArray("passenger_fare")
                        val farelists = ArrayList<passengerlistsummaryModel>()
                        if (passenger_fare.length() > 0) {
                            for (k in 0 until passenger_fare.length()) {
                                val job = passenger_fare.getJSONObject(k)
                                var title: String = job.getString("title");
                                var value: String = sCurrencySymbol + job.getString("value");
                                var positive: String = job.getString("positive");
                                if (positive.equals("0")) {
                                    farelists.add(passengerlistsummaryModel(1, title, " " + value))
                                } else {
                                    farelists.add(passengerlistsummaryModel(1, title, "-" + value))
                                }
                            }
                            var listViewAdapter = passengerlistsummaryModelAdapter(applicationContext, farelists)
                            passengerfarelist.setAdapter(listViewAdapter)
                            passengerfarelist.isExpanded = true
                            passengerfarelist.visibility = View.VISIBLE
                        } else {
                            passengerfarelist.visibility = View.GONE
                        }


                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                dismissdialog()
                e.printStackTrace()
            } catch (e: Exception) {
                dismissdialog()
            }

        }

        override fun onErrorListener(error: Any) {
            dismissdialog()
        }
    }

    fun showdialog() {
        dialog = Dialog(this, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()
    }

    fun dismissdialog() {
        try {
            if (dialog != null) {
                dialog.dismiss()
            }
        } catch (e: Exception) {
        }
    }

    //----------------------------Local Db Start
    fun tripDetailsParserMethod(trip_deatils_json: String) {
        dismissdialog()
        try {
            try {
                val jobject = JSONObject(trip_deatils_json)
                val ride_info_object = jobject.getJSONObject("ride_info")
                var PRIVATE_MODE = 0
                val PREF_NAME = "mapdattoshow"
                val sharedPref: SharedPreferences = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
                val editor = sharedPref.edit()
                scrollView_tripsummry.visibility = View.VISIBLE
                val `object` = ride_info_object.getJSONObject("response")
                //  val jarray = `object`.getJSONObject("details")
                val user_profileobj = `object`.getJSONObject("user_profile")
                if (`object`.has("currency")) {
                    val currency = `object`.getString("currency")
                    sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(currency)
                    symbolblack.text = sCurrencySymbol
                }
                if (`object`.has("payment_method")) {
                    val payment_method = `object`.getString("payment_method")
                    paymenttypee.text = payment_method;

                }
                if (`object`.has("driver_revenue")) {
                    val driver_revenue = `object`.getString("driver_revenue")
                    if (driver_revenue.contains(".")) {
                        val first = driver_revenue.split("\\.".toRegex())[0]
                        val second = driver_revenue.split("\\.".toRegex())[1]
                        total_amount_txt.text = first
                        splitzeros.text = "." + second
                    } else {
                        total_amount_txt.text = driver_revenue
                    }
                }


                if (`object`.has("invoice_src")) {
                    my_rides_detail_route_map_imageview.visibility = View.VISIBLE
                    my_rides_detail_mapview_layout.visibility = View.GONE

                    val invoice_src = `object`.getString("invoice_src")
                    if(invoice_src.length>3) {
                        Picasso.with(applicationContext)
                                .load(invoice_src)
                                .into(my_rides_detail_route_map_imageview)
                    }
                }

                val user_name = user_profileobj.getString("user_name")
                my_rides_detail_cabname.text = user_name
                if (user_profileobj.has("user_review")) {
                    driver_dashboard_ratting.setRating(java.lang.Float.parseFloat(user_profileobj.getString("user_review")))
                }
                if (user_profileobj.has("user_image")) {
                    var user_image: String = "";
                    try {
                        user_image = user_profileobj.getString("user_image")
                    } catch (e: Exception) {
                    }
                    if (user_image.length > 3) {
                        Picasso.with(applicationContext)
                                .load(user_image)
                                .into(my_ride_detail_driverImage)
                    }
                }
                val pickup = `object`.getJSONObject("pickup")
                if (pickup.has("location")) {
                    pickup_address_txt.text = pickup.getString("location")
                    val pickuplatlong = pickup.getJSONObject("latlong")
                    if (pickuplatlong.has("lat")) {
                        var lat: String = pickuplatlong.getString("lat")
                        editor.putString("pickuplat", lat)
                    }
                    if (pickuplatlong.has("lon")) {
                        var lon: String = pickuplatlong.getString("lon")
                        editor.putString("pickuplong", lon)
                    }
                } else {
                    pickup_address_txt.text = getString(R.string.ride_summary_ride_cancelled_pickup_loc)
                }


                if (`object`.has("driver_revenue")) {
                    val driver_revenue = `object`.getString("driver_revenue")
                    finalamt.text = sCurrencySymbol + driver_revenue

                }

                val drop = `object`.getJSONObject("drop")
                if (drop.has("location")) {
                    drop_address_txt.text = drop.getString("location")

                    val droplatlong = drop.getJSONObject("latlong")
                    if (droplatlong.has("lat")) {
                        var lat: String = droplatlong.getString("lat")
                        editor.putString("droplatt", lat)
                    }
                    if (droplatlong.has("lon")) {
                        var lon: String = droplatlong.getString("lon")
                        editor.putString("droplongg", lon)
                    }
                } else {
                    drop_address_txt.text = getString(R.string.ride_summary_ride_cancelled_drop_loc)

                }
                editor.apply()
                mapFragment.getMapAsync(mapFragment)


                if (`object`.has("pickup_date")) {
                    val pickup_date = `object`.getString("pickup_date")
                    pickup_time.text = pickup_date
                } else {
                    pickup_time.text = "--:--"

                }
                if (`object`.has("drop_date") && !`object`.getString("drop_date").equals("")) {
                    val drop_date = `object`.getString("drop_date")
                    drop_time.text = drop_date
                    if(drop_date.equals(""))
                    {
                        drop_time.visibility = View.INVISIBLE
                    }
                    else
                    {
                        drop_time.visibility = View.VISIBLE
                    }
                } else {
                    drop_time.text = "--:--"
                    drop_time.visibility = View.INVISIBLE

                }
                if (`object`.has("summary")) {
                    val summarypasenger = `object`.getJSONObject("summary")
                    if (summarypasenger.has("ride_fare")) {
                        passengeramt.text = sCurrencySymbol + summarypasenger.getString("ride_fare")
                    }
                    if (summarypasenger.has("ride_distance")) {
                        var distance_unit: String = ""
                        if (`object`.has("distance_unit")) {
                            distance_unit = `object`.getString("distance_unit")
                        }
                        km.text = summarypasenger.getString("ride_distance") + "\n" + "" + distance_unit
                    }
                }

                val driver_earning = `object`.getJSONArray("driver_earning")
                val farelist = ArrayList<ridelistsummaryModel>()
                if (driver_earning.length() > 0) {
                    for (k in 0 until driver_earning.length()) {
                        val job = driver_earning.getJSONObject(k)
                        var title: String = job.getString("title");
                        var value: String = sCurrencySymbol + job.getString("value");
                        var positive: String = job.getString("positive");
                        if (positive.equals("0")) {
                            farelist.add(ridelistsummaryModel(1, title, " " + value))
                        } else {
                            farelist.add(ridelistsummaryModel(1, title, "-" + value))
                        }
                    }
                    var listViewAdapter = ridelistsummaryModelAdapter(applicationContext, farelist)
                    mFareListView.setAdapter(listViewAdapter)
                    mFareListView.isExpanded = true
                    mFareListView.visibility = View.VISIBLE
                } else {
                    mFareListView.visibility = View.GONE
                }


                val passenger_fare = `object`.getJSONArray("passenger_fare")
                val farelists = ArrayList<passengerlistsummaryModel>()
                if (passenger_fare.length() > 0) {
                    for (k in 0 until passenger_fare.length()) {
                        val job = passenger_fare.getJSONObject(k)
                        var title: String = job.getString("title");
                        var value: String = sCurrencySymbol + job.getString("value");
                        var positive: String = job.getString("positive");
                        if (positive.equals("0")) {
                            farelists.add(passengerlistsummaryModel(1, title, " " + value))
                        } else {
                            farelists.add(passengerlistsummaryModel(1, title, "-" + value))
                        }
                    }
                    var listViewAdapter = passengerlistsummaryModelAdapter(applicationContext, farelists)
                    passengerfarelist.setAdapter(listViewAdapter)
                    passengerfarelist.isExpanded = true
                    passengerfarelist.visibility = View.VISIBLE
                } else {
                    passengerfarelist.visibility = View.GONE
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        } catch (e: JSONException) {
            // TODO Auto-generated catch block
            dismissdialog()
            e.printStackTrace()
        } catch (e: Exception) {
            dismissdialog()
        }
    }
    //----------------------------Local Db end


}


