package com.hellotaxidriver.driver.tripdetailspackage.tripsummaryutils

import android.app.Dialog
import android.content.Context
import android.util.Log
import android.view.Window
import android.widget.TextView
import com.android.volley.Request
import com.app.service.ServiceConstant
import com.app.service.ServiceRequest
import com.hellotaxidriver.driver.R
import com.hellotaxidriver.driver.tripdetailspackage.localdb.TripDetailsDbHelper
import org.json.JSONObject
import java.util.HashMap

class TripSummarCancelRidesDetailsUtil(context: Context, driverId: String, rideId: String) {
    private lateinit var dialog: Dialog
    private lateinit var mContext: Context
    private lateinit var driver_id: String
    private lateinit var ride_id: String
    private lateinit var mRequest: ServiceRequest
    private lateinit var tripDetailsDbHelper: TripDetailsDbHelper

    init {
        mContext = context
        tripDetailsDbHelper = TripDetailsDbHelper(mContext)
        driver_id = driverId
        ride_id = rideId
        postRequestRetrieveTripDetails(ServiceConstant.tripsummery_view_url)
    }


    private fun postRequestRetrieveTripDetails(Url: String) {
        dialog = Dialog(mContext)
        dialog.getWindow()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.setText(mContext.getResources().getString(R.string.action_loading))

        println("-------------triplist----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id
        jsonParams["ride_id"] = ride_id


        mRequest = ServiceRequest(mContext)
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {
                println("--------------reponse-------------------$response")
                Log.e("trip", response)
                var status = ""
                val total_rides = ""
                val type_group = ""
                var Str_response = ""

                try {
                    val `object` = JSONObject(response)
                    status = `object`.getString("status")

                    println("triplist--status----$status")

                    if (status.equals("1", ignoreCase = true)) {

                        val jsonObject = `object`.getJSONObject("response")

                        val finalresponse = "{" + "\"ride_info\": {" + "" + "\"response\": " + jsonObject.toString() + "}}"
                        var ride_status = ""
                        if (jsonObject.has("ride_status")) {
                            ride_status = jsonObject.getString("ride_status")
                        }
                        val rideid = jsonObject.getString("ride_id")

                        if (ride_status.equals("Cancelled", ignoreCase = true)) {
                            tripDetailsDbHelper.insertTripdeatils(rideid, driver_id, finalresponse, "0")
                        }
                        dialog.dismiss()

                    } else {
                        Str_response = `object`.getString("response")

                    }
                    dialog.dismiss()
                } catch (e: Exception) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                    dialog.dismiss()
                }

            }

            override fun onErrorListener() {
                dialog.dismiss()
            }

        })

    }
}