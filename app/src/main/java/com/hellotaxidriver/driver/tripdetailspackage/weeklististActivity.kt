package com.hellotaxidriver.driver.tripdetailspackage

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.*


import android.view.View
import android.view.Window
import com.android.volley.Request
import com.app.service.ServiceConstant
import com.app.service.ServiceManager
import com.hellotaxidriver.driver.R
import com.hellotaxidriver.driver.Utils.CurrencySymbolConverter
import com.hellotaxidriver.driver.Utils.SessionManager

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.HashMap


class weeklististActivity : AppCompatActivity() {
    lateinit var dialog: Dialog

    lateinit var nodatatodiaplsy: TextView
    lateinit var listView: ListView

    lateinit var sm: SessionManager
    lateinit var listViewModelArrayList: ArrayList<weeklistModel>
    lateinit var listViewAdapter: weelistadapter
    var sCurrencySymbol: String = ""
    var dateto: String = ""
    var amountto: String = ""
    var datetosspass: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.weeklist_kotlin_summary)
        sm = SessionManager(applicationContext)



        if (intent.hasExtra("dateto")) {
            dateto = intent.getStringExtra("dateto")
            datetosspass = intent.getStringExtra("dateto")
        }
        if (intent.hasExtra("amount")) {
            amountto = intent.getStringExtra("amount")
        }


        try {
            if (intent.hasExtra("dateto")) {
                dateto = intent.getStringExtra("dateto")

                var spf = SimpleDateFormat("yyyy-MM-dd")
                val newDate = spf.parse(datetosspass)
                spf = SimpleDateFormat("MMM dd")
                datetosspass = "" + spf.format(newDate)


            }
        } catch (e: Exception) {
        }
        listViewModelArrayList = ArrayList<weeklistModel>()
        listViewAdapter = weelistadapter(this, listViewModelArrayList)

        //intlize view in kotlin
        nodatatodiaplsy = findViewById<TextView>(R.id.nodatatodiaplsy)
        val activity_trip_txt = findViewById<TextView>(R.id.activity_trip_txt)
        listView = findViewById<ListView>(R.id.recyclerView)

        val activity_back_image = findViewById<ImageView>(R.id.activity_back_image)
        activity_trip_txt.text = getString(R.string.weeklyreport)
        activity_back_image.setOnClickListener {
            finish();
        }
        listViewModelArrayList.clear()

        showdialog()
        val jsonParams = HashMap<String, String>()
        val userDetails = sm.getUserDetails()
        var driverId: String? = userDetails[SessionManager.KEY_DRIVERID]
        jsonParams["driver_id"] = "" + driverId
        val manager = ServiceManager(applicationContext, acceptServicelistener)
        manager.makeServiceRequest(ServiceConstant.WEEKLIST, Request.Method.POST, jsonParams)

        listView.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->


            val intent = Intent(this, rideslistActivity::class.java)
            intent.putExtra("date", "" + listViewModelArrayList.get(position).from_date)
            intent.putExtra("dateto", "" + listViewModelArrayList.get(position).to_date)
            intent.putExtra("amount",  listViewModelArrayList.get(position).amount)
            startActivity(intent)


        }


    }

    internal var acceptServicelistener: ServiceManager.ServiceListener = object : ServiceManager.ServiceListener {
        override fun onCompleteListener(`object`: String) {
            var Sstatus = ""
            var response = ""

            try {
                val object1 = JSONObject(`object`)
                Sstatus = object1.getString("status")
                if (Sstatus.equals("1", ignoreCase = true)) {
                    dismissdialog()
                    response = object1.getString("response")
                    val responseobject1 = JSONObject(response)
                    if (responseobject1.has("currency")) {
                        var currency: String = responseobject1.getString("currency")
                        sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(currency)
                    }
                    if (responseobject1.has("payments")) {
                        val day_earning = JSONArray(responseobject1.getString("payments"))
                        for (i in 0 until day_earning.length()) {
                            val day_earningObject = day_earning.getJSONObject(i)
                            val billing_period = day_earningObject.getString("billing_period")
                            val amount = day_earningObject.getString("amount")
                            val total_rides = day_earningObject.getString("total_rides")
                            val from_date = day_earningObject.getString("pay_duration_from")
                            val to_date = day_earningObject.getString("pay_duration_to")
                            listViewModelArrayList.add(weeklistModel(1, billing_period, sCurrencySymbol + "" + amount, total_rides, from_date, to_date))


                        }
                        listViewAdapter = weelistadapter(applicationContext, listViewModelArrayList)
                        listView.setAdapter(listViewAdapter)

                        if (day_earning.length() == 0) {
                            nodatatodiaplsy.visibility = View.VISIBLE
                        } else {
                            nodatatodiaplsy.visibility = View.GONE
                        }


                    }
                } else {
                    nodatatodiaplsy.visibility = View.VISIBLE
                    dismissdialog()
                }
            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                dismissdialog()
                e.printStackTrace()
            } catch (e: Exception) {
                dismissdialog()
            }

        }

        override fun onErrorListener(error: Any) {
            dismissdialog()
        }
    }

    fun snackbar() {
        /*var snackbar:Snackbar= Snackbar.make(listView, getString(R.string.nodatatodisplay).toString(), Snackbar.LENGTH_LONG)
        val message = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text) as TextView
        var font:Typeface=Typeface.createFromAsset(applicationContext.assets,"verdanab.ttf")
        message.typeface=font
        snackbar.show()*/
    }

    fun showdialog() {
        dialog = Dialog(this, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()
    }

    fun dismissdialog() {
        try {
            if (dialog != null) {
                dialog.dismiss()
            }
        } catch (e: Exception) {
        }
    }

}
