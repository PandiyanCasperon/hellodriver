package com.hellotaxidriver.driver.tripdetailspackage

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper


val DATBASE_NAME="ridesdb1234";
val TABLE_NAME="ridestable1234";

//fields for local db
val COLID="id";

//fields for user details
val COLUSERID="userid";
val COLUSERNAME="username";
val COLUSEREMAIL="useremail";
val COLUSERPHNO="userphno";
val COLUSERIMAGE="userimage";
val COLUSERREVIEW="userreview";

val COLRIDETYPE="ridetype";


//fields for rides details
val COLRIDEID="rideid";
val COLPICKUP_ADDRESS="pickupaddress";
val COLDROP_ADDRESS="dropaddress";
val COLTIMESTAMP="timestamp";
val COLESTIMATEKM="estimatekm";
val COLESTIMATETIME="estimatetime";
val COLETSIMATEFARE="estimatefare";
val COLRIDEDATE="ridedate";
val COLFARESUMMARY="faresummary";
val COLPICKUPTIME="pickuptime";
val COLDROPTIME="droptime";
val COLMAPIMAGE="mapiamge";
val COLAUTOTYPE="autotype";
val COLDISTANCE="distance";
val COLTIMETAKEN="timetaken";
val COLWAITINGTIME="waitingtime";
val COLCANCELLATIONREASON="cancellationreason";
val COLSTATUSOFRIDE="statusofride";


class databasecreation(var context : Context) : SQLiteOpenHelper(context, DATBASE_NAME,null,1)
{
    override fun onCreate(db: SQLiteDatabase?)
    {
        val createTable="CREATE TABLE "+ TABLE_NAME+" ("+ COLID +" INTEGER PRIMARY KEY AUTOINCREMENT,"+
                COLRIDEID+" VARCHAR,"+
                COLPICKUP_ADDRESS+" VARCHAR,"+
                COLDROP_ADDRESS+" VARCHAR,"+
                COLTIMESTAMP+" VARCHAR,"+
                COLESTIMATEKM+" VARCHAR,"+
                COLESTIMATETIME+" VARCHAR,"+
                COLETSIMATEFARE+" VARCHAR,"+
                COLRIDEDATE+" VARCHAR,"+
                COLFARESUMMARY+" VARCHAR,"+
                COLPICKUPTIME+" VARCHAR,"+
                COLDROPTIME+" VARCHAR,"+
                COLMAPIMAGE+" VARCHAR,"+
                COLAUTOTYPE+" VARCHAR,"+
                COLDISTANCE+" VARCHAR,"+
                COLTIMETAKEN+" VARCHAR,"+
                COLRIDETYPE+" VARCHAR,"+
                COLWAITINGTIME+" VARCHAR,"+
                COLUSERID+" VARCHAR,"+
                COLUSERNAME+" VARCHAR,"+
                COLUSEREMAIL+" VARCHAR,"+
                COLUSERPHNO+" VARCHAR,"+
                COLUSERIMAGE+" VARCHAR,"+
                COLUSERREVIEW+" VARCHAR,"+
                COLSTATUSOFRIDE+" VARCHAR,"+
                COLCANCELLATIONREASON+" VARCHAR)";
        db?.execSQL(createTable);
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int)
    {
    }


    fun InserData(USERID:String,USERNAME:String,USEREMAIL:String,USERPHNO:String,USERIMAGE:String,USERREVIEW:String,RIDEID:String,PICKUP_ADDRESS:String
                  ,DROP_ADDRESS:String
                  ,TIMESTAMP:String,ESTIMATEKM:String,ESTIMATETIME:String
                  ,ETSIMATEFARE:String,RIDEDATE:String,FARESUMMARY:String,PICKUPTIME:String,DROPTIME:String,MAPIMAGE:String
                  ,AUTOTYPE:String,DISTANCE:String,TIMETAKEN:String,WAITINGTIME:String,CANCELLATIONREASON:String)
    {

        val totalrow = "SELECT  * FROM " + TABLE_NAME;
        val dbs = this.readableDatabase
        val cursors = dbs.rawQuery(totalrow, null);
        var totalrowcount: Int = cursors.count;
        if (totalrowcount >= 300)
        {
            var jsonresponse:String=deletelatid()
            dbs.delete(TABLE_NAME,COLID + "='"+ jsonresponse+"'", null);
        }

        val rowsinstring = "SELECT  * FROM " + TABLE_NAME+" WHERE "+ COLRIDEID+"='"+RIDEID+"'";
        val cursor = dbs.rawQuery(rowsinstring, null);
        var rowcount: Int = cursor.count;
        if (rowcount == 0)
        {
            val db = this.writableDatabase
            val cv = ContentValues()
            cv.put(COLUSERID, USERID)
            cv.put(COLUSERNAME, USERNAME)
            cv.put(COLUSEREMAIL, USEREMAIL)
            cv.put(COLUSERPHNO, USERPHNO)
            cv.put(COLUSERIMAGE, USERIMAGE)
            cv.put(COLUSERREVIEW, USERREVIEW)
            cv.put(COLRIDEID, RIDEID)
            cv.put(COLRIDETYPE, "")
            cv.put(COLPICKUP_ADDRESS, PICKUP_ADDRESS)
            cv.put(COLDROP_ADDRESS, DROP_ADDRESS)
            cv.put(COLTIMESTAMP, TIMESTAMP)
            cv.put(COLESTIMATEKM, ESTIMATEKM)
            cv.put(COLESTIMATETIME, ESTIMATETIME)
            cv.put(COLETSIMATEFARE, ETSIMATEFARE)
            cv.put(COLRIDEDATE, RIDEDATE)
            cv.put(COLFARESUMMARY, FARESUMMARY)
            cv.put(COLPICKUPTIME, PICKUPTIME)
            cv.put(COLDROPTIME, DROPTIME)
            cv.put(COLMAPIMAGE, MAPIMAGE)
            cv.put(COLAUTOTYPE, AUTOTYPE)
            cv.put(COLDISTANCE, DISTANCE)
            cv.put(COLTIMETAKEN, TIMETAKEN)
            cv.put(COLWAITINGTIME, WAITINGTIME)
            cv.put(COLCANCELLATIONREASON, CANCELLATIONREASON)
            cv.put(COLSTATUSOFRIDE, "")
            db.insert(TABLE_NAME, null, cv);
        }
    }


    //for ride based count
    fun returncount(RIDEIDD:String): Int
    {
        val rowsinstring = "SELECT  * FROM " + TABLE_NAME+" WHERE "+ COLRIDEID+"='"+RIDEIDD+"'";
        val dbs = this.readableDatabase
        val cursor = dbs.rawQuery(rowsinstring, null);
        var rowcount: Int = cursor.count;
        return rowcount;
    }


    //for cancel ride process count and updating cancel status
    fun returncancelridecount(): Int
    {
        val rowsinstring = "SELECT  * FROM " + TABLE_NAME+" WHERE "+ COLSTATUSOFRIDE+"='cancel'";
        val dbs = this.readableDatabase
        val cursor = dbs.rawQuery(rowsinstring, null);
        var rowcount: Int = cursor.count;
        return rowcount;
    }

    //Updating data for cancel ride
    fun UpdateDataforcancelride(RIDEID:String,CANCELLATIONREASON: String)
    {
        val rowsinstring = "SELECT  * FROM " + TABLE_NAME+" WHERE "+ COLRIDEID+"='"+RIDEID+"'";
        val dbs = this.readableDatabase
        val cursor = dbs.rawQuery(rowsinstring, null);
        var rowcount: Int = cursor.count;
        if (rowcount != 0)
        {
            val db = this.writableDatabase
            val cv = ContentValues()
            cv.put(COLCANCELLATIONREASON, CANCELLATIONREASON)
            cv.put(COLSTATUSOFRIDE, "cancel")
            db.update(TABLE_NAME,cv, COLRIDEID+"='"+RIDEID+"'", null);
        }
    }


    //Updating data for completed ride
    fun UpdateDataforcompleteride(RIDEID:String,RIDETYPE:String,successrep:String,paidamount:String,dropaddress:String)
    {
        val rowsinstring = "SELECT  * FROM " + TABLE_NAME+" WHERE "+ COLRIDEID+"='"+RIDEID+"'";
        val dbs = this.readableDatabase
        val cursor = dbs.rawQuery(rowsinstring, null);
        var rowcount: Int = cursor.count;
        if (rowcount != 0)
        {
            val db = this.writableDatabase
            val cv = ContentValues()
            cv.put(COLFARESUMMARY, successrep)
            cv.put(COLETSIMATEFARE, paidamount)
            cv.put(COLDROP_ADDRESS, dropaddress)
            cv.put(COLRIDETYPE, RIDETYPE)
            cv.put(COLSTATUSOFRIDE, "1")
            db.update(TABLE_NAME,cv, COLRIDEID+"='"+RIDEID+"'", null);
        }
    }







    fun deletelatid(): String
    {
        var colid:String=""
        val rowsinstring = "SELECT  * FROM " + TABLE_NAME;
        val dbs = this.readableDatabase
        val cursor = dbs.rawQuery(rowsinstring, null);
        if(cursor.moveToLast())
        {
                colid=cursor.getString(cursor.getColumnIndex(COLID));

        }
        return colid;
    }


    fun comparesqlitecount(): Int
    {
        val rowsinstring = "SELECT  * FROM " + TABLE_NAME;
        val dbs = this.readableDatabase
        val cursor = dbs.rawQuery(rowsinstring, null);
        var rowcount: Int = cursor.count;
        return rowcount;
    }
}