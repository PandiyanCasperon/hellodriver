package com.hellotaxidriver.driver.tripdetailspackage

class  summaryinsideModel
{
    var itemList:List<String> = ArrayList()

    constructor(itemList: List<String>)
    {
        this.itemList = itemList
    }

    fun set(itemLists: List<String>)
    {
        this.itemList = itemLists
    }

    fun get (): List<String>
    {
        return itemList
    }
}