package com.hellotaxidriver.driver.tripdetailspackage

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.hellotaxidriver.driver.R
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView



class summarylistModelAdapter(val context: Context, val listModelArrayList: ArrayList<summarylistModel>,val secondaryadapter: List<summaryinsideModel>) : BaseAdapter()
{

    lateinit var tripSummaryModel:summaryinsideModel
    internal var list: List<String> = java.util.ArrayList()
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View?
    {
        val view: View?
        val vh: ViewHoldersummary
        if (convertView == null)
        {
            val layoutInflater = LayoutInflater.from(context)
            view = layoutInflater.inflate(R.layout.summarytrip_kotlin_summary_list, parent, false)
            vh = ViewHoldersummary(view)
            view.tag = vh
        }
        else
        {
            view = convertView
            vh = view.tag as ViewHoldersummary
        }
        tripSummaryModel = secondaryadapter.get(position)
        list = tripSummaryModel.get()



        var listViewAdapter = sublistModelAdapter(context, list)
        vh.summaryfareexp.setAdapter(listViewAdapter)
        vh.summaryfareexp.isExpanded = true
        vh.summaryfareexp.isEnabled  = false

        if(listModelArrayList[position].title.equals(""))
        {
            vh.tvTitle.text = context.getString(R.string.ridetypecancelled)
        }
        else
        {
            vh.tvTitle.text = listModelArrayList[position].title
        }
        vh.tvContent.text = listModelArrayList[position].content


        return view
    }
    override fun getItem(position: Int): Any
    {
        return listModelArrayList[position]
    }
    override fun getItemId(position: Int): Long
    {
        return position.toLong()
    }
    override fun getCount(): Int
    {
        return listModelArrayList.size
    }
}
private class ViewHoldersummary(view: View?)
{
    val tvTitle: TextView = view?.findViewById<TextView>(R.id.trip_summary_list_address) as TextView
    val tvContent: TextView = view?.findViewById<TextView>(R.id.trip_summary_list_date_and_time) as TextView
    val summaryfareexp: ExpandableHeightListView = view?.findViewById<ExpandableHeightListView>(R.id.summaryfareexp) as ExpandableHeightListView
}