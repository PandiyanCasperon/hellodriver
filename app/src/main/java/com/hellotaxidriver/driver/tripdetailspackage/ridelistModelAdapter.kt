package com.hellotaxidriver.driver.tripdetailspackage

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import android.widget.TextView
import com.hellotaxidriver.driver.R


class ListViewModelAdapter(val context: Context, val listModelArrayList: ArrayList<ridelistModel>) : BaseAdapter() {

    var typeface: Typeface = Typeface.createFromAsset(context.assets, "fonts/POPPINS-MEDIUM.TTF")
    var typefacebold: Typeface = Typeface.createFromAsset(context.assets, "fonts/POPPINS-MEDIUM.TTF")
    var textsize_12: Float = 12f
    var textsize_14: Float = 16f


    lateinit var tripSummaryModel: summaryinsideModel
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val view: View?
        val vh: ViewHolder
        if (convertView == null) {
            val layoutInflater = LayoutInflater.from(context)
            view = layoutInflater.inflate(R.layout.trip_kotlin_summary_list, parent, false)
            vh = ViewHolder(view)
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as ViewHolder
        }
        if (listModelArrayList[position].title.equals("")) {
            vh.tvTitle.text = context.getString(R.string.ridetypecancelled)
        } else {
            vh.tvTitle.text = listModelArrayList[position].title
        }
        vh.vehicletypee.text = listModelArrayList[position].vehicletype
        vh.dropadd.text = listModelArrayList[position].rideidd
        vh.tvContent.text = listModelArrayList[position].content

        vh.tvTitle.setTextColor(context.getResources().getColor(R.color.black_color));
        vh.dropaddress.text = context.getString(R.string.rideeid) + listModelArrayList[position].dropaddress

        if (listModelArrayList[position].statustype.equals("1")) {
            vh.cancelreason.setTextColor(context.getResources().getColor(R.color.dark_red));
            vh.dropadd.visibility = View.VISIBLE
            vh.cancelreason.text = listModelArrayList[position].cancelreason
            vh.cancelreason.textSize = textsize_14
            vh.offlineclass.visibility = View.VISIBLE
            vh.cardviw.setBackgroundResource(R.drawable.tripcard)
            vh.cancelreason.visibility = View.VISIBLE
            vh.tvTitle.typeface = typefacebold
        } else if (listModelArrayList[position].statustype.equals("3")) {

            vh.cancelreason.setTextColor(context.getResources().getColor(R.color.lite_black));
            vh.dropadd.visibility = View.VISIBLE
            vh.offlineclass.visibility = View.GONE
            vh.cancelreason.text = listModelArrayList[position].content
            vh.cancelreason.textSize = textsize_12

            if (listModelArrayList[position].title.equals("Online")) {
                vh.cardviw.setBackgroundResource(R.drawable.lightgreenforonline)
                vh.tvTitle.setTextColor(context.getResources().getColor(R.color.green));
                vh.tvTitle.typeface = typeface
            } else {
                vh.cardviw.setBackgroundResource(R.drawable.offlinered)
                vh.tvTitle.setTextColor(context.getResources().getColor(R.color.red));
                vh.tvTitle.typeface = typeface
            }
        } else {
            vh.tvTitle.typeface = typefacebold
            vh.cancelreason.setTextColor(context.getResources().getColor(R.color.red));
            vh.dropadd.visibility = View.GONE
            vh.cancelreason.textSize = textsize_14
            vh.cancelreason.visibility = View.VISIBLE
            vh.cardviw.setBackgroundResource(R.drawable.tripcard)
            vh.cancelreason.text = listModelArrayList[position].cancelreason
            vh.offlineclass.visibility = View.VISIBLE
        }
        return view
    }

    override fun getItem(position: Int): Any {
        return listModelArrayList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return listModelArrayList.size
    }
}

private class ViewHolder(view: View?) {
    val tvTitle: TextView = view?.findViewById<TextView>(R.id.trip_summary_list_address) as TextView
    val tvContent: TextView = view?.findViewById<TextView>(R.id.trip_summary_list_date_and_time) as TextView
    val dropaddress: TextView = view?.findViewById<TextView>(R.id.dropaddress) as TextView
    val dropadd: TextView = view?.findViewById<TextView>(R.id.dropadd) as TextView
    val cancelreason: TextView = view?.findViewById<TextView>(R.id.cancelreason) as TextView
    val offlineclass: LinearLayout = view?.findViewById<LinearLayout>(R.id.offlineclass) as LinearLayout
    val cardviw: LinearLayout = view?.findViewById<LinearLayout>(R.id.cardviw) as LinearLayout
    val vehicletypee: TextView = view?.findViewById<TextView>(R.id.vehicletypee) as TextView

}