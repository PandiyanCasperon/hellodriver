package com.hellotaxidriver.driver.tripdetailspackage.localdb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.hellotaxidriver.driver.Utils.SessionManager;

import java.util.ArrayList;

/**
 * Created by anandan on 10-09-2016.
 */
public class TripDetailsDbHelper implements TripDbConstansts {

    private String TAG = TripDetailsDbHelper.class.getSimpleName();
    private TripDetailsDataBaseHelper myDBHelper;
    private SQLiteDatabase myDataBase;
    private Context myContext;
    private int DATABASE_VERSION = 1;
    private SessionManager manager;

    private static final String CREATE_TABLE_USER = "CREATE TABLE IF NOT EXISTS " + TABLE_TRIP_DETAILS + "(" + "trip_ride_id" + " INTEGER," + "trip_driver_id" + " INTEGER," + "trip_details_json" + " TEXT," + "trip_details_status" + " INTEGER" + ")";
    private String tag = "TripDetailsDbHelper";
//    private static final String CREATE_TABLE_DISTANCE = "CREATE TABLE IF NOT EXISTS " + LATLONG_TABLE_DISTANCE + "(" + "status" + " INTEGER," + "geo_lat" + " TEXT," + "geo_long" + " TEXT," + "geo_time" + " TEXT" + ")";

    public TripDetailsDbHelper(Context context) {
        myContext = context;
        myDBHelper = new TripDetailsDataBaseHelper(context);
        myDataBase = myDBHelper.getWritableDatabase();
        manager = new SessionManager(context);
        open();
    }

    public class TripDetailsDataBaseHelper extends SQLiteOpenHelper {

        public TripDetailsDataBaseHelper(Context context) {

            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }


    /**
     * Function to make DB as readable and writable
     */
    private void open() {

        try {
            if (myDataBase == null) {
                myDataBase = myDBHelper.getWritableDatabase();
            }
            myDataBase.execSQL(CREATE_TABLE_USER);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Function to Close the database
     */
    public void close() {

        try {
            Log.d(TAG, "mySQLiteDatabase Closed");
            // ---Closing the database---
            myDBHelper.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void insertTripdeatils(String ride_id, String driver_id, String trip_details_json, String trip_details_status) {
        try {
            String selectQuery = "SELECT  * FROM " + TABLE_TRIP_DETAILS;
            SQLiteDatabase db = myDataBase;
            Cursor cursor = db.rawQuery(selectQuery, null);
            boolean lastnamePresent = false;
            while (cursor.moveToNext()) {
                String db_ride_id = cursor.getString(cursor.getColumnIndex("trip_ride_id"));
                if (db_ride_id.equals(ride_id)) {
                    lastnamePresent = true;
                    break;
                }
            }


            if (!lastnamePresent) {
                Log.e(tag, "trip Info inserted in DB");
                ContentValues values = new ContentValues();
                values.put("trip_ride_id", ride_id);
                values.put("trip_driver_id", driver_id);
                values.put("trip_details_json", trip_details_json);
                values.put("trip_details_status", trip_details_status);
                myDataBase.insert(TABLE_TRIP_DETAILS, null, values);
            } else {
                Log.e(tag, "trip Info already inserted in DB");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteTripdeatils() {
        SQLiteDatabase db = myDataBase;
        db.delete(TABLE_TRIP_DETAILS, null, null);

    }

    public int getTripDetailCount() {
        String countQuery = "SELECT  * FROM " + TABLE_TRIP_DETAILS;
        SQLiteDatabase db = myDataBase;
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
//        cursor.close();
        return cnt;
    }

    public ArrayList<String> getTripDetailsJson() {
        String selectQuery = "SELECT  * FROM " + TABLE_TRIP_DETAILS;
        SQLiteDatabase db = myDataBase;
        Cursor cursor = db.rawQuery(selectQuery, null);
        String[] data = null;
        ArrayList<String> userDbData = new ArrayList<String>();
        if (cursor.moveToLast()) {
            do {
                String trip_details_ride_id = cursor.getString(0);
                String trip_details_driver_id = cursor.getString(1);
                String trip_details_json = cursor.getString(2);
                String trip_details_ride_status = cursor.getString(3);
                userDbData.add(trip_details_json + ";" + trip_details_ride_status);
            } while (cursor.moveToPrevious());
        }
        return userDbData;
    }


    public String getTripDriverId() {
        String selectQuery = "SELECT  * FROM " + TABLE_TRIP_DETAILS;
        SQLiteDatabase db = myDataBase;
        Cursor cursor = db.rawQuery(selectQuery, null);
        String trip_details_driver_id = "";
        if (cursor.moveToLast()) {
            trip_details_driver_id = cursor.getString(1);
        }
        return trip_details_driver_id;
    }
}
