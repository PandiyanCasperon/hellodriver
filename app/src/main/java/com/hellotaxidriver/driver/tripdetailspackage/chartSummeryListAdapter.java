package com.hellotaxidriver.driver.tripdetailspackage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.hellotaxidriver.driver.R;

public class chartSummeryListAdapter extends RecyclerView.Adapter<chartSummeryListAdapter.MyViewHolder> {
    private Activity activity;
    int pos = -1,chart_count=-1;
    String value,datetopass;


    public chartSummeryListAdapter(Activity activity, Integer pos,String value,String datetopass,Integer chart_count) {
        this.pos = pos;
        this.value = value;
        this.datetopass = datetopass;
        this.activity = activity;
        this.chart_count=chart_count;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView dayEarningsTv;
        View lineView,bg_view;

        ImageView right_arrow_iv;

        public MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            dayEarningsTv = (TextView) itemView.findViewById(R.id.day_earnings_tv);
            lineView= (View) itemView.findViewById(R.id.line_view);
            dayEarningsTv = (TextView) itemView.findViewById(R.id.day_earnings_tv);
            lineView= (View) itemView.findViewById(R.id.line_view);
            bg_view= (View) itemView.findViewById(R.id.bg_view);
            right_arrow_iv = (ImageView) itemView.findViewById(R.id.right_arrow_iv);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.chartearning, viewGroup, false);

        itemView.getLayoutParams().width = (int) (getScreenWidth() / 7.3); /// THIS LINE WILL DIVIDE OUR VIEW INTO NUMBERS OF PARTS

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, final int position) {
        /*if (pos == position)
        {
            myViewHolder.dayEarningsTv.setVisibility(View.VISIBLE);
            myViewHolder.lineView.setVisibility(View.VISIBLE);
            myViewHolder.dayEarningsTv.setText(value);
        }
        else
        {
            myViewHolder.dayEarningsTv.setVisibility(View.INVISIBLE);
            myViewHolder.lineView.setVisibility(View.INVISIBLE);
        }*/

        if (pos == position) {
            myViewHolder.right_arrow_iv.setVisibility(View.VISIBLE);
            myViewHolder.dayEarningsTv.setVisibility(View.VISIBLE);
            myViewHolder.lineView.setVisibility(View.VISIBLE);
            myViewHolder.bg_view.setVisibility(View.VISIBLE);
            myViewHolder.dayEarningsTv.setText(value);
        } else {
            myViewHolder.right_arrow_iv.setVisibility(View.INVISIBLE);
            myViewHolder.dayEarningsTv.setVisibility(View.INVISIBLE);
            myViewHolder.lineView.setVisibility(View.INVISIBLE);
            myViewHolder.bg_view.setVisibility(View.INVISIBLE);
        }

        myViewHolder.bg_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, rideslistActivity.class);
                intent.putExtra("date",""+datetopass);
                intent.putExtra("amount",""+value);
                activity.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return chart_count;
    }

    public int getScreenWidth() {
        WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }
}
