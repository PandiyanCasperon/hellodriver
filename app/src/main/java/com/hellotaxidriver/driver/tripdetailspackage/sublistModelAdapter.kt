package com.hellotaxidriver.driver.tripdetailspackage

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.hellotaxidriver.driver.R


class sublistModelAdapter(val context: Context, val listModelArrayList: List<String>) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View?
    {
        val view: View?
        val vh: ViewHolder3
        var typeface: Typeface = Typeface.createFromAsset(context.assets, "fonts/SF-Pro-Display-Regular.ttf")
        var typefacebold: Typeface = Typeface.createFromAsset(context.assets, "fonts/verdanab.ttf")

        if (convertView == null)
        {
            val layoutInflater = LayoutInflater.from(context)
            view = layoutInflater.inflate(R.layout.farelistadapterkotlin, parent, false)
            vh = ViewHolder3(view)
            view.tag = vh
        }
        else
        {
            view = convertView
            vh = view.tag as ViewHolder3
        }


        val title = listModelArrayList[position].split("\\,".toRegex())[0]
        val pricee = listModelArrayList[position].split("\\,".toRegex())[1]


        if(position==0)
        {
            vh.tvTitle.setTextColor(context.getResources().getColor(R.color.grey_color));
            vh.tvContent.setTextColor(context.getResources().getColor(R.color.grey_color));
            vh.tvTitle.typeface = typefacebold
            vh.tvContent.typeface = typefacebold
        }
        else
        {
            vh.tvTitle.setTextColor(context.getResources().getColor(R.color.grey_light_summary));
            vh.tvContent.setTextColor(context.getResources().getColor(R.color.grey_light_summary));
            vh.tvTitle.typeface = typeface
            vh.tvContent.typeface = typeface
        }



        // vh.tvTitle.setTextColor(context.getResources().getColor(R.color.lite_black));
        //  vh.tvContent.setTextColor(context.getResources().getColor(R.color.lite_black));
        vh.tvTitle.text = title
        vh.tvContent.text = pricee

        return view
    }

    override fun getItem(position: Int): Any {
        return listModelArrayList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return listModelArrayList.size
    }
}

private class ViewHolder3(view: View?)
{
    val tvTitle: TextView = view?.findViewById<TextView>(R.id.faretile) as TextView
    val tvContent: TextView = view?.findViewById<TextView>(R.id.farevalue) as TextView
}