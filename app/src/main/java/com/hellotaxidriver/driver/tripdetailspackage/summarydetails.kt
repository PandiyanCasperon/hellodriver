package com.hellotaxidriver.driver.tripdetailspackage

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.Window
import android.widget.*
import com.android.volley.Request
import com.app.service.ServiceConstant
import com.app.service.ServiceManager
import com.hellotaxidriver.driver.R
import com.hellotaxidriver.driver.Utils.CurrencySymbolConverter
import com.hellotaxidriver.driver.Utils.SessionManager
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap
import kotlin.collections.ArrayList
import kotlin.collections.set


class summarydetails : AppCompatActivity() {


    lateinit var billing_periodT: TextView
    lateinit var amountT: TextView
    var sCurrencySymbol: String = ""
    lateinit var dialog: Dialog
    lateinit var sm: SessionManager
    lateinit var list: ArrayList<String>
    lateinit var minusoffset: LinearLayout
    lateinit var addoffset: LinearLayout
    lateinit var fullclick: LinearLayout

    lateinit var hidescroll: LinearLayout

    lateinit var summodel: List<summaryinsideModel>
    var offset: Int = 0
    lateinit var listViewModelArrayList: ArrayList<summarylistModel>
    lateinit var listViewAdapter: summarylistModelAdapter
    lateinit var recyclerView: ListView
    lateinit var nodatatodiaplsy: TextView

    var billing_from: String = ""
    var billing_to: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layoutforsummary)

        listViewModelArrayList = ArrayList<summarylistModel>()

        sm = SessionManager(applicationContext)
        val getoffset: String = intent.getStringExtra("offset")
        offset = 0
        list = ArrayList<String>()

        val activity_back_image = findViewById<ImageView>(R.id.activity_back_image)
        minusoffset = findViewById<LinearLayout>(R.id.minusoffset)
        fullclick = findViewById<LinearLayout>(R.id.fullclick)
        addoffset = findViewById<LinearLayout>(R.id.addoffset)
        billing_periodT = findViewById<TextView>(R.id.fromto)
        amountT = findViewById<TextView>(R.id.amount)
        recyclerView = findViewById<ListView>(R.id.recyclerView)
        hidescroll = findViewById<LinearLayout>(R.id.hidescroll)
        nodatatodiaplsy = findViewById<TextView>(R.id.nodatatodiaplsy)




        amountT.setOnClickListener {
            val intent = Intent(this, rideslistActivity::class.java)
            intent.putExtra("date", "" + billing_from)
            intent.putExtra("dateto", "" + billing_to)
            intent.putExtra("amount", "" + amountT.text.toString())
            startActivity(intent)
        }
        fullclick.setOnClickListener {
            val intent = Intent(this, rideslistActivity::class.java)
            intent.putExtra("date", "" + billing_from)
            intent.putExtra("dateto", "" + billing_to)
            intent.putExtra("amount", "" + amountT.text.toString())
            startActivity(intent)
        }
        if (offset == 0) {
            minusoffset.visibility = View.INVISIBLE;
        } else {
            minusoffset.visibility = View.VISIBLE;
        }
        activity_back_image.setOnClickListener {
            finish();
        }
        addoffset.setOnClickListener {
            offset++
            if (offset == 0) {
                minusoffset.visibility = View.INVISIBLE
            } else {
                minusoffset.visibility = View.VISIBLE
            }
            showdialog()
            val jsonParams = HashMap<String, String>()
            val userDetails = sm.getUserDetails()
            var driverId: String? = userDetails[SessionManager.KEY_DRIVERID]
            jsonParams["driver_id"] = "" + driverId
            jsonParams["offset"] = "" + offset
            val manager = ServiceManager(applicationContext, acceptServicelistener)
            manager.makeServiceRequest(ServiceConstant.SUMMARYY, Request.Method.POST, jsonParams)
        }
        minusoffset.setOnClickListener {
            offset--
            if (offset == 0) {
                minusoffset.visibility = View.INVISIBLE
                showdialog()
                val jsonParams = HashMap<String, String>()
                val userDetails = sm.getUserDetails()
                var driverId: String? = userDetails[SessionManager.KEY_DRIVERID]
                jsonParams["driver_id"] = "" + driverId
                jsonParams["offset"] = "" + offset
                val manager = ServiceManager(applicationContext, acceptServicelistener)
                manager.makeServiceRequest(ServiceConstant.SUMMARYY, Request.Method.POST, jsonParams)
            } else {
                minusoffset.visibility = View.VISIBLE
                showdialog()
                val jsonParams = HashMap<String, String>()
                val userDetails = sm.getUserDetails()
                var driverId: String? = userDetails[SessionManager.KEY_DRIVERID]
                jsonParams["driver_id"] = "" + driverId
                jsonParams["offset"] = "" + offset
                val manager = ServiceManager(applicationContext, acceptServicelistener)
                manager.makeServiceRequest(ServiceConstant.SUMMARYY, Request.Method.POST, jsonParams)
            }
        }
        showdialog()
        val jsonParams = HashMap<String, String>()
        val userDetails = sm.getUserDetails()
        var driverId: String? = userDetails[SessionManager.KEY_DRIVERID]
        jsonParams["driver_id"] = "" + driverId
        jsonParams["offset"] = "" + offset
        val manager = ServiceManager(applicationContext, acceptServicelistener)
        manager.makeServiceRequest(ServiceConstant.SUMMARYY, Request.Method.POST, jsonParams)
    }

    internal var acceptServicelistener: ServiceManager.ServiceListener = object : ServiceManager.ServiceListener {
        override fun onCompleteListener(`object`: String) {
            var Sstatus = ""
            var response = ""
            try {
                val object1 = JSONObject(`object`)
                Sstatus = object1.getString("status")
                if (Sstatus.equals("1", ignoreCase = true)) {
                    hidescroll.visibility = View.VISIBLE
                    listViewModelArrayList.clear()
                    summodel = ArrayList<summaryinsideModel>()

                    dismissdialog()
                    response = object1.getString("response")
                    val responseobject1 = JSONObject(response)
                    if (responseobject1.has("currency")) {
                        var currency: String = responseobject1.getString("currency")
                        sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(currency)
                    }
                    if (responseobject1.has("net_breakup")) {
                        var data1: List<String> = ArrayList()
                        val day_earning = JSONArray(responseobject1.getString("net_breakup"))
                        for (i in 0 until day_earning.length()) {
                            val day_earningObject = day_earning.getJSONObject(i)
                            val title = day_earningObject.getString("title")
                            val value = sCurrencySymbol + day_earningObject.getString("value")
                            data1 += title + "," + value
                        }
                        var tripSummaryModel: summaryinsideModel = summaryinsideModel(data1)
                        summodel += tripSummaryModel
                        if (day_earning.length() > 0) {
                            listViewModelArrayList.add(summarylistModel(1, getString(R.string.netfasre), "", "", "", "1", "", ""))
                        }


                    }
                    if (responseobject1.has("trip_breakup")) {
                        var data1: List<String> = ArrayList()
                        val day_earning = JSONArray(responseobject1.getString("trip_breakup"))
                        for (i in 0 until day_earning.length()) {
                            val day_earningObject = day_earning.getJSONObject(i)
                            val title = day_earningObject.getString("title")
                            val value = day_earningObject.getString("value")

                            data1 += title + "," + value

                        }
                        var tripSummaryModel: summaryinsideModel = summaryinsideModel(data1)
                        summodel += tripSummaryModel
                        if (day_earning.length() > 0) {
                            listViewModelArrayList.add(summarylistModel(1, getString(R.string.tripss), "", "", "", "1", "", ""))
                        }

                    }
                    if (responseobject1.has("ratting_stats")) {
                        var data1: List<String> = ArrayList()
                        val day_earning = JSONArray(responseobject1.getString("ratting_stats"))
                        for (i in 0 until day_earning.length()) {
                            val day_earningObject = day_earning.getJSONObject(i)
                            val title = day_earningObject.getString("title")
                            val value = day_earningObject.getString("value")
                            data1 += title + "," + value

                        }
                        var tripSummaryModel: summaryinsideModel = summaryinsideModel(data1)
                        summodel += tripSummaryModel
                        if (day_earning.length() > 0) {
                            listViewModelArrayList.add(summarylistModel(1, getString(R.string.ratings), "", "", "", "1", "", ""))
                        }
                    }
                    listViewAdapter = summarylistModelAdapter(applicationContext, listViewModelArrayList, summodel)
                    listViewAdapter.notifyDataSetChanged()
                    recyclerView.setAdapter(listViewAdapter)


                    if (responseobject1.has("billing_period")) {
                        var billing_period: String = responseobject1.getString("billing_period")
                        billing_periodT.text = billing_period
                    }
                    if (responseobject1.has("net_fare")) {
                        var amount: String = responseobject1.getString("net_fare")
                        amountT.text = sCurrencySymbol + amount
                        if (amount.equals("0") || amount.equals("0.0") || amount.equals("0.00")) {
                            nodatatodiaplsy.visibility = View.VISIBLE
                            recyclerView.visibility = View.GONE
                        } else {
                            nodatatodiaplsy.visibility = View.GONE
                            recyclerView.visibility = View.VISIBLE
                        }
                    }

                    if (responseobject1.has("billing_from")) {
                        billing_from = responseobject1.getString("billing_from")
                    }
                    if (responseobject1.has("billing_to")) {
                        billing_to = responseobject1.getString("billing_to")
                    }
                } else {
                    dismissdialog()
                }
            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                dismissdialog()
                e.printStackTrace()
            } catch (e: Exception) {
                dismissdialog()
            }

        }

        override fun onErrorListener(error: Any) {
            dismissdialog()
        }
    }

    fun showdialog() {
        dialog = Dialog(this, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()
    }

    fun dismissdialog() {
        try {
            if (dialog != null) {
                dialog.dismiss()
            }
        } catch (e: Exception) {
        }
    }


}