package com.hellotaxidriver.driver.tripdetailspackage

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.AttributeSet
import com.hellotaxidriver.driver.R
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions


class MyMapFragment : SupportMapFragment(), OnMapReadyCallback {
    private lateinit var mMap: GoogleMap

    var initial_latitude  = -34.0
    var initial_longitude = 151.0
    var initial_marker    = "Pick up"
    var drop_marker    = "Drop"

    override fun onInflate(context: Context?, attrs: AttributeSet?, savedInstanceState: Bundle?) {
        super.onInflate(context, attrs, savedInstanceState)
        attrs ?: return
        context ?: return

        val typed_array = context.obtainStyledAttributes(attrs,
                R.styleable.MyyMap, 0, 0)

        try {
            val provided_latitude =
                    typed_array.getFloat(R.styleable.MyyMap_latitude, initial_latitude.toFloat())
            initial_latitude  = provided_latitude.toDouble()

            val provided_longitude =
                    typed_array.getFloat(R.styleable.MyyMap_longitude, initial_longitude.toFloat())
            initial_longitude = provided_longitude.toDouble()

            val provided_marker =
                    typed_array.getString(R.styleable.MyyMap_marker)
            provided_marker?.apply { initial_marker = provided_marker }

        }
        catch(e: Exception) {

        }

        typed_array.recycle()

    }
    override fun onMapReady(map: GoogleMap?)
    {

        var PRIVATE_MODE = 0
        val PREF_NAME = "mapdattoshow"
        val sharedPref: SharedPreferences? = context?.getSharedPreferences(PREF_NAME, PRIVATE_MODE)


        var pickuplat:String = ""+sharedPref?.getString("pickuplat","");
        var picklongg:String = ""+sharedPref?.getString("pickuplong","");
        var droplatt:String = ""+sharedPref?.getString("droplatt","");
        var droplongg:String = ""+sharedPref?.getString("droplongg","");

        mMap = map as GoogleMap;

        val sydney = LatLng(pickuplat.toDouble(), picklongg.toDouble());
        val dropp = LatLng(droplatt.toDouble(), droplongg.toDouble());

       // val dropp = LatLng(13.0500, 80.2121);


        mMap.addMarker(MarkerOptions()
                .position(sydney)
                .title(initial_marker)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.pickup_pointer_small))
        );
        mMap.addMarker(MarkerOptions()
                .position(dropp)
                .title(drop_marker)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.drop_pointer_small))
        );

        mMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                        this.context, R.raw.silver))

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 12.0f))


    }


}
