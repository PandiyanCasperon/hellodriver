package com.hellotaxidriver.driver.tripdetailspackage

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.*


import android.view.View
import android.view.Window
import com.android.volley.Request
import com.app.service.ServiceConstant
import com.app.service.ServiceManager
import com.hellotaxidriver.driver.R
import com.hellotaxidriver.driver.Utils.CurrencySymbolConverter
import com.hellotaxidriver.driver.Utils.SessionManager

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.HashMap


class rideslistActivity : AppCompatActivity() {
    lateinit var dialog: Dialog
    lateinit var date: TextView
    lateinit var amount: TextView
    lateinit var nodatatodiaplsy: TextView
    lateinit var listView: ListView
    var gettingdate: String = ""
    lateinit var sm: SessionManager
    lateinit var listViewModelArrayList: ArrayList<ridelistModel>
    lateinit var listViewAdapter: ListViewModelAdapter
    var sCurrencySymbol: String = ""
    var dateto: String = ""
    var amountto: String = ""
    var datetosspass: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.trip_kotlin_summary)
        sm = SessionManager(applicationContext)
        gettingdate = intent.getStringExtra("date")
        var datetopass: String = intent.getStringExtra("date")

        if (intent.hasExtra("dateto")) {
            dateto = intent.getStringExtra("dateto")
            datetosspass = intent.getStringExtra("dateto")
        }
        if (intent.hasExtra("amount")) {
            amountto = intent.getStringExtra("amount")
        }


        try {
            var spf = SimpleDateFormat("yyyy-MM-dd")
            val newDate = spf.parse(datetopass)
            spf = SimpleDateFormat("MMM dd")
            datetopass = "" + spf.format(newDate)

            if (intent.hasExtra("dateto")) {
                dateto = intent.getStringExtra("dateto")

                var spf = SimpleDateFormat("yyyy-MM-dd")
                val newDate = spf.parse(datetosspass)
                spf = SimpleDateFormat("MMM dd")
                datetosspass = "" + spf.format(newDate)

                datetopass = datetopass + "-" + datetosspass
            }
        } catch (e: Exception) {
        }
        listViewModelArrayList = ArrayList<ridelistModel>()
        listViewAdapter = ListViewModelAdapter(this, listViewModelArrayList)

        //intlize view in kotlin
        nodatatodiaplsy = findViewById<TextView>(R.id.nodatatodiaplsy)
        val activity_trip_txt = findViewById<TextView>(R.id.activity_trip_txt)
        listView = findViewById<ListView>(R.id.recyclerView)
        date = findViewById<TextView>(R.id.date)
        amount = findViewById<TextView>(R.id.amount)
        amount.text = amountto
        date.text = datetopass
        val activity_back_image = findViewById<ImageView>(R.id.activity_back_image)
        activity_trip_txt.text = getString(R.string.trip_summary)
        activity_back_image.setOnClickListener {
            finish();
        }
        listViewModelArrayList.clear()
        showdialog()
        val jsonParams = HashMap<String, String>()
        val userDetails = sm.getUserDetails()
        var driverId: String? = userDetails[SessionManager.KEY_DRIVERID]
        jsonParams["driver_id"] = "" + driverId
        jsonParams["date"] = "" + gettingdate
        jsonParams["date_to"] = "" + dateto

        val manager = ServiceManager(applicationContext, acceptServicelistener)
        manager.makeServiceRequest(ServiceConstant.ACTIVITYOFRIDE, Request.Method.POST, jsonParams)
        listView.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val cancelstatus = listViewModelArrayList.get(position).statustype;
            if (cancelstatus.equals("cancel")) {
                snackbar()
            } else if (cancelstatus.equals("3")) {
                snackbar()
            } else {
                val intent = Intent(this, ridesummarydetail::class.java)
                intent.putExtra("rideid", "" + listViewModelArrayList.get(position).dropaddress)
                intent.putExtra("pickupaddress", "" + listViewModelArrayList.get(position).title)
                intent.putExtra("dropaddress", "" + listViewModelArrayList.get(position).dropaddress)
                intent.putExtra("price", "" + listViewModelArrayList.get(position).cancelreason)
                startActivity(intent)
            }
        }
    }

    internal var acceptServicelistener: ServiceManager.ServiceListener = object : ServiceManager.ServiceListener {
        override fun onCompleteListener(`object`: String) {
            var Sstatus = ""
            var response = ""

            try {
                val object1 = JSONObject(`object`)
                Sstatus = object1.getString("status")
                if (Sstatus.equals("1", ignoreCase = true)) {
                    dismissdialog()
                    response = object1.getString("response")
                    val responseobject1 = JSONObject(response)
                    if (responseobject1.has("currency")) {
                        var currency: String = responseobject1.getString("currency")
                        sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(currency)
                    }
                    if (responseobject1.has("activity_list")) {
                        val day_earning = JSONArray(responseobject1.getString("activity_list"))
                        for (i in 0 until day_earning.length()) {
                            val day_earningObject = day_earning.getJSONObject(i)
                            val activity = day_earningObject.getString("activity")
                            if (activity.equals("Ride")) {
                                val ride_id = day_earningObject.getString("ride_id")
                                val ride_type = day_earningObject.getString("ride_type")
                                val driver_earning = sCurrencySymbol + day_earningObject.getString("driver_earning")
                                val payment_type = day_earningObject.getString("payment_type")
                                val ratting = day_earningObject.getString("ratting")
                                val category = day_earningObject.getString("category")
                                val activity_time = day_earningObject.getString("activity_time")
                                listViewModelArrayList.add(ridelistModel(1, ride_type, activity_time, ride_id, driver_earning, "1", payment_type, category))
                            } else {
                                val activity = day_earningObject.getString("activity")
                                val activity_time = day_earningObject.getString("activity_time")
                                listViewModelArrayList.add(ridelistModel(1, activity, activity_time, "", "", "3", "", ""))
                            }
                        }
                        listViewAdapter = ListViewModelAdapter(applicationContext, listViewModelArrayList)
                        listView.setAdapter(listViewAdapter)

                        if (day_earning.length() == 0) {
                            nodatatodiaplsy.visibility = View.VISIBLE
                        } else {
                            nodatatodiaplsy.visibility = View.GONE
                        }


                    }
                } else {
                    nodatatodiaplsy.visibility = View.VISIBLE
                    dismissdialog()
                }
            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                dismissdialog()
                e.printStackTrace()
            } catch (e: Exception) {
                dismissdialog()
            }

        }

        override fun onErrorListener(error: Any) {
            dismissdialog()
        }
    }

    fun snackbar() {
        /*var snackbar:Snackbar= Snackbar.make(listView, getString(R.string.nodatatodisplay).toString(), Snackbar.LENGTH_LONG)
        val message = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text) as TextView
        var font:Typeface=Typeface.createFromAsset(applicationContext.assets,"verdanab.ttf")
        message.typeface=font
        snackbar.show()*/
    }

    fun showdialog() {
        dialog = Dialog(this, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()
    }

    fun dismissdialog() {
        try {
            if (dialog != null) {
                dialog.dismiss()
            }
        } catch (e: Exception) {
        }
    }

}
