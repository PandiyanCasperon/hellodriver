package com.hellotaxidriver.driver.tripdetailspackage;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.constraint.Guideline;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;


import com.hellotaxidriver.driver.R;

import java.util.ArrayList;
import java.util.List;

public class chartdowndataAdapter extends RecyclerView.Adapter<chartdowndataAdapter.MyViewHolder> {
    List<String> tripSummaryModels = new ArrayList<>();
    ChartClickListener chartClickListener;
    int pos = -1;
    Float guideLineSpaceValue;
    private Activity activity;

    public chartdowndataAdapter(Activity activity, List<String> tripSummaryList, ChartClickListener chartClickListener) {
        this.tripSummaryModels = tripSummaryList;
        this.activity = activity;
        this.chartClickListener = chartClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.chart_down_row, viewGroup, false);

        itemView.getLayoutParams().width = (int) (getScreenWidth() / 7.3); /// THIS LINE WILL DIVIDE OUR VIEW INTO NUMBERS OF PARTS

        return new chartdowndataAdapter.MyViewHolder(itemView);
//        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, final int position) {
        String content = tripSummaryModels.get(position);
        String[] splitString = content.split(",");
        myViewHolder.dayTv.setText(splitString[0]);
        guideLineSpaceValue = 0.9f - Float.valueOf(splitString[1]) / 100.0f;
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(myViewHolder.constraintLayout);
        constraintSet.setGuidelinePercent(R.id.height_guideline, guideLineSpaceValue);
        constraintSet.connect(R.id.seperator_view2, ConstraintSet.TOP, R.id.height_guideline, ConstraintSet.TOP, 0);
        constraintSet.applyTo(myViewHolder.constraintLayout);


        myViewHolder.view_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("*********************onBindViewHolder" + position);
                pos = position;
                notifyDataSetChanged();
            }
        });

        if (pos == position) {
            myViewHolder.blackLineView.setVisibility(View.VISIBLE);
            myViewHolder.view_test.setBackground(activity.getResources().getDrawable(R.drawable.bg_chart_select_bar));
//            myViewHolder.view_test.setBackgroundColor(activity.getResources().getColor(R.color.app_color));
            chartClickListener.clickPosition(pos, getItemCount());
        } else {
            myViewHolder.blackLineView.setVisibility(View.GONE);
            myViewHolder.view_test.setBackgroundColor(activity.getResources().getColor(R.color.chat_header_bg_gray));
        }
    }

    @Override
    public int getItemCount() {
        return tripSummaryModels.size();
    }

    public int getScreenWidth() {

        WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        return size.x;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //        ProgressBar VerticalProgressBar;
        TextView dayTv;
        View seperatorView, view_test, blackLineView;
        Guideline height_guideLine;
        ConstraintLayout constraintLayout;
        private RecyclerView listView;

        public MyViewHolder(@NonNull final View itemView) {
            super(itemView);
//            VerticalProgressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
            constraintLayout = (ConstraintLayout) itemView.findViewById(R.id.constraint_layout);
            dayTv = (TextView) itemView.findViewById(R.id.day_tv);
            seperatorView = (View) itemView.findViewById(R.id.seperator_view);
            view_test = (View) itemView.findViewById(R.id.seperator_view2);
            blackLineView = (View) itemView.findViewById(R.id.black_line_view);
            height_guideLine = (Guideline) itemView.findViewById(R.id.height_guideline);

        }
    }
}
