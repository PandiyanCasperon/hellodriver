package com.hellotaxidriver.driver.tripdetailspackage

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.*
import com.android.volley.Request
import com.app.service.ServiceConstant
import com.app.service.ServiceManager

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import android.support.v7.widget.LinearLayoutManager
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.set
import android.support.v7.widget.RecyclerView
import android.view.*
import android.view.inputmethod.InputMethodManager
import com.hellotaxidriver.driver.NavigationDrawerNew
import com.hellotaxidriver.driver.R
import com.hellotaxidriver.driver.Utils.CurrencySymbolConverter
import com.hellotaxidriver.driver.Utils.SessionManager

class tripsummarydetails : Fragment(), ChartClickListener {


    lateinit var billing_periodT: TextView
    lateinit var amountT: TextView
    lateinit var nodatatodiaplsy: TextView
    lateinit var total_ridesT: TextView
    lateinit var total_onlinehoursT: TextView
    var sCurrencySymbol: String = ""
    lateinit var typeface: Typeface

    lateinit var dialog: Dialog
    lateinit var sm: SessionManager
    lateinit var list: ArrayList<String>
    lateinit var dateaddlist: ArrayList<String>
    lateinit var storefloatvalue: ArrayList<String>


    lateinit var minusoffset: LinearLayout
    lateinit var addoffset: LinearLayout
    lateinit var summarydetailsd: LinearLayout
    lateinit var hidescroll: ScrollView
    var offset: Int = 0
    lateinit var BarEntryLabels: ArrayList<String>
    var DEFAULT_DATA: Int = 0
    var SUBCOLUMNS_DATA: Int = 1
    var STACKED_DATA: Int = 2
    var NEGATIVE_SUBCOLUMNS_DATA: Int = 3
    var NEGATIVE_STACKED_DATA: Int = 4

    var hasAxes: Boolean = true
    var hasAxesNames: Boolean = true
    var hasLabels: Boolean = false
    var hasLabelForSelected: Boolean = true
    var dataType: Int = DEFAULT_DATA

    lateinit var largeamount: TextView
    lateinit var hideview: View
    lateinit var clickmonth: LinearLayout

    lateinit var tripSummaryRecyclerView: RecyclerView
    lateinit var price_list: RecyclerView
    lateinit var data1: ArrayList<String>
    lateinit var listViewAdapter: chartdowndataAdapter

    lateinit var listViewAdapter2: chartSummeryListAdapter

    private var rootview: View? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            if (rootview == null) {
                rootview = inflater.inflate(R.layout.layoutforchart, container, false)
            } else {
                val parent = rootview!!.getParent() as ViewGroup
                parent?.removeView(rootview)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

 /*       if (rootview != null) {
            val parent = rootview!!.getParent() as ViewGroup
            parent?.removeView(rootview)
        }

        try {
            rootview = inflater!!.inflate(R.layout.layoutforchart, container, false)
        } catch (e: InflateException) {
        }*/

        initization(rootview)
        (activity as NavigationDrawerNew).disableSwipeDrawer()
        /*  NavigationDrawerNew navigationDrawerNew=new NavigationDrawerNew();
        navigationDrawerNew.disableSwipeDrawer();*/
//        NavigationDrawerNew.disableSwipeDrawer();

        rootview!!.findViewById<View>(R.id.activity_back_image).setOnClickListener { view ->
            val imm = (activity as NavigationDrawerNew).getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

            if (imm.isAcceptingText) {
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
            (activity as NavigationDrawerNew).openDrawer()
        }


        return rootview
    }

    private fun initization(rootview: View?){
        sm = SessionManager(activity)
        list = ArrayList<String>()
        dateaddlist = ArrayList<String>()
        data1 = ArrayList<String>()
        storefloatvalue = ArrayList<String>()
        BarEntryLabels = ArrayList<String>()




        hidescroll = rootview!!.findViewById<ScrollView>(R.id.hidescroll)
        typeface = Typeface.createFromAsset(activity?.assets, "fonts/verdanab.ttf")

        tripSummaryRecyclerView = rootview.findViewById<RecyclerView>(R.id.recyclervi)
        price_list = rootview.findViewById<RecyclerView>(R.id.price_list)

        clickmonth = rootview.findViewById<LinearLayout>(R.id.clickmonth)
        hideview = rootview.findViewById<TextView>(R.id.hideview)
        nodatatodiaplsy = rootview.findViewById<TextView>(R.id.nodatatodiaplsy)
        largeamount = rootview.findViewById<TextView>(R.id.largeamount)
        val activity_back_image = rootview.findViewById<ImageView>(R.id.activity_back_image)
        minusoffset = rootview.findViewById<LinearLayout>(R.id.minusoffset)
        addoffset = rootview.findViewById<LinearLayout>(R.id.addoffset)
        summarydetailsd = rootview.findViewById<LinearLayout>(R.id.summarydetails)
        billing_periodT = rootview.findViewById<TextView>(R.id.fromto)
        amountT = rootview.findViewById<TextView>(R.id.amount)
        total_ridesT = rootview.findViewById<TextView>(R.id.trips)
        total_onlinehoursT = rootview.findViewById<TextView>(R.id.tripsonline)
        if (offset == 0) {
            minusoffset.visibility = android.view.View.INVISIBLE;
        } else {
            minusoffset.visibility = android.view.View.VISIBLE;
        }


        amountT.setOnClickListener {
            val intent = Intent(activity, summarydetails::class.java)
            intent.putExtra("offset", "" + offset)
            startActivity(intent)
        }

        clickmonth.setOnClickListener {
            /*  val intent = Intent(this, summarydetails::class.java)
              intent.putExtra("offset", "" + offset)
              startActivity(intent)*/

            val intent = Intent(activity, weeklististActivity::class.java)
            intent.putExtra("offset", "" + offset)
            startActivity(intent)
        }





        addoffset.setOnClickListener {
            offset++
            if (offset == 0) {
                minusoffset.visibility = android.view.View.INVISIBLE
            } else {
                minusoffset.visibility = android.view.View.VISIBLE
            }
            showdialog()
            val jsonParams = HashMap<String, String>()
            val userDetails = sm.getUserDetails()
            var driverId: String? = userDetails[SessionManager.KEY_DRIVERID]
            jsonParams["driver_id"] = "" + driverId
            jsonParams["offset"] = "" + offset
            val manager = ServiceManager(activity, acceptServicelistener)
            manager.makeServiceRequest(ServiceConstant.PAYMENTSUMMARY, Request.Method.POST, jsonParams)
        }

        minusoffset.setOnClickListener {
            offset--
            if (offset == 0) {
                minusoffset.visibility = android.view.View.INVISIBLE
                showdialog()
                val jsonParams = HashMap<String, String>()
                val userDetails = sm.getUserDetails()
                var driverId: String? = userDetails[SessionManager.KEY_DRIVERID]
                jsonParams["driver_id"] = "" + driverId
                jsonParams["offset"] = "" + offset
                val manager = ServiceManager(activity, acceptServicelistener)
                manager.makeServiceRequest(ServiceConstant.PAYMENTSUMMARY, Request.Method.POST, jsonParams)
            } else {
                minusoffset.visibility = android.view.View.VISIBLE
                showdialog()
                val jsonParams = HashMap<String, String>()
                val userDetails = sm.getUserDetails()
                var driverId: String? = userDetails[SessionManager.KEY_DRIVERID]
                jsonParams["driver_id"] = "" + driverId
                jsonParams["offset"] = "" + offset
                val manager = ServiceManager(activity, acceptServicelistener)
                manager.makeServiceRequest(ServiceConstant.PAYMENTSUMMARY, Request.Method.POST, jsonParams)
            }
        }

        showdialog()
        val jsonParams = HashMap<String, String>()
        val userDetails = sm.getUserDetails()
        var driverId: String? = userDetails[SessionManager.KEY_DRIVERID]
        jsonParams["driver_id"] = "" + driverId
        jsonParams["offset"] = "" + offset
        val manager = ServiceManager(activity, acceptServicelistener)
        manager.makeServiceRequest(ServiceConstant.PAYMENTSUMMARY, Request.Method.POST, jsonParams)

    }


    override fun clickPosition(position: Int, chart_count: Int) {
        var datetopass: String = dateaddlist[position]
        var bac: Double = list[position].toDouble()
//        var roundtoint: Double = Math.ceil(bac)
        var roundtoint: Double = bac

        price_list.setVisibility(View.VISIBLE)

        listViewAdapter2 = chartSummeryListAdapter(activity, position, sCurrencySymbol + roundtoint, datetopass, chart_count)
        val mLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        price_list.setLayoutManager(mLayoutManager)
        price_list.setAdapter(listViewAdapter2)
    }


    internal var acceptServicelistener: ServiceManager.ServiceListener = object : ServiceManager.ServiceListener {
        override fun onCompleteListener(`object`: String) {
            var Sstatus = ""
            var response = ""

            try {
                val object1 = JSONObject(`object`)
                Sstatus = object1.getString("status")
                if (Sstatus.equals("1", ignoreCase = true)) {

                    tripSummaryRecyclerView.visibility = View.VISIBLE
                    hidescroll.visibility = View.VISIBLE
                    dismissdialog()
                    response = object1.getString("response")
                    val responseobject1 = JSONObject(response)

                    if (responseobject1.has("currency")) {
                        var currency: String = responseobject1.getString("currency")
                        sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(currency)
                    }


                    if (responseobject1.has("day_earning")) {
                        val day_earning = JSONArray(responseobject1.getString("day_earning"))
                        list.clear()
                        dateaddlist.clear()
                        storefloatvalue.clear()
                        data1.clear()

                        for (i in 0 until day_earning.length()) {
                            val day_earningObject = day_earning.getJSONObject(i)
                            val `val` = day_earningObject.getString("amount").toFloat()
                            storefloatvalue.add(`val`.toString())
                        }
                        var maxvalue: Float = 0f

                        if (storefloatvalue.size > 0) {
                            maxvalue = storefloatvalue[0].toFloat()
                            for (i in 0 until storefloatvalue.size) {
                                if (storefloatvalue[i].toFloat() > maxvalue) {
                                    maxvalue = storefloatvalue[i].toFloat();
                                }
                            }
                        }
                        for (i in 0 until day_earning.length()) {
                            if (i < 7) {
                                val day_earningObject = day_earning.getJSONObject(i)
                                val `val` = day_earningObject.getString("amount").toFloat()
                                val convertedper = (`val` / maxvalue) * 100;
                                val date = day_earningObject.getString("date")
                                val chart_date = day_earningObject.getString("chart_date")
                                list.add(`val`.toString())
                                dateaddlist.add(date)
                                if (convertedper > 0 && convertedper < 10) {
                                    data1.add(chart_date.substring(0, 2) + ",10")
                                } else {
                                    data1.add(chart_date.substring(0, 2) + "," + convertedper)
                                }
                            }
                        }
                        if (day_earning.length() > 0) {
                            nodatatodiaplsy.visibility = View.INVISIBLE
                            hideview.visibility = View.VISIBLE
                            largeamount.visibility = View.GONE
                            var maxvalue: Float = storefloatvalue[0].toFloat()
                            for (i in 0 until storefloatvalue.size) {
                                if (storefloatvalue[i].toFloat() > maxvalue) {
                                    maxvalue = storefloatvalue[i].toFloat();
                                }
                            }
                            largeamount.text = sCurrencySymbol + maxvalue
                        } else {
                            nodatatodiaplsy.visibility = View.VISIBLE
                            hideview.visibility = View.INVISIBLE
                            largeamount.visibility = View.GONE
                        }
                    }


                    if (responseobject1.has("billing_period")) {
                        var billing_period: String = responseobject1.getString("billing_period")
                        billing_periodT.text = billing_period
                    }
                    if (responseobject1.has("amount")) {
                        var amount: String = responseobject1.getString("amount")
                        amountT.text = sCurrencySymbol + amount
                        showlist(amount)
                        if (amount.equals("0")) {
                            nodatatodiaplsy.visibility = View.VISIBLE
                            hideview.visibility = View.INVISIBLE
                            largeamount.visibility = View.GONE
                        } else {
                            nodatatodiaplsy.visibility = View.INVISIBLE
                            hideview.visibility = View.VISIBLE
                            largeamount.visibility = View.GONE
                        }
                    }
                    if (responseobject1.has("total_rides")) {
                        var total_rides: String = responseobject1.getString("total_rides")
                        total_ridesT.text = total_rides
                    }
                    if (responseobject1.has("total_onlinehours")) {
                        var total_onlinehours: String = responseobject1.getString("total_onlinehours")
                        total_onlinehoursT.text = total_onlinehours
                    }
                } else {
                    tripSummaryRecyclerView.visibility = View.GONE
                    nodatatodiaplsy.visibility = View.VISIBLE
                    dismissdialog()
                }
            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                dismissdialog()
                e.printStackTrace()
            } catch (e: Exception) {
                dismissdialog()
            }

        }

        override fun onErrorListener(error: Any) {
            dismissdialog()
        }
    }

    fun showdialog() {
        dialog = Dialog(activity!!, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()
    }

    fun dismissdialog() {
        try {
            if (dialog != null) {
                dialog.dismiss()
            }
        } catch (e: Exception) {
        }
    }

    fun showlist(amount: String) {

        if (amount.equals("0")) {
            tripSummaryRecyclerView.visibility = View.GONE
            price_list.visibility = View.GONE
            val mLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            listViewAdapter = chartdowndataAdapter(activity, data1, this)
            tripSummaryRecyclerView.setLayoutManager(mLayoutManager)
            tripSummaryRecyclerView.setAdapter(listViewAdapter)
        } else {
            tripSummaryRecyclerView.visibility = View.VISIBLE
            price_list.visibility = View.VISIBLE

            val mLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            listViewAdapter = chartdowndataAdapter(activity, data1, this)
            tripSummaryRecyclerView.setLayoutManager(mLayoutManager)
            tripSummaryRecyclerView.setAdapter(listViewAdapter)

            var datetopass: String = dateaddlist[0]
            var bac: Float = list[0].toFloat()
            var roundtoint: Int = bac.toInt()
            price_list.setVisibility(View.VISIBLE)
            listViewAdapter2 = chartSummeryListAdapter(activity, 0, sCurrencySymbol + roundtoint, datetopass, 0)
            val mLayoutManagerd = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            price_list.setLayoutManager(mLayoutManagerd)
            price_list.setAdapter(listViewAdapter2)


        }

    }
}


