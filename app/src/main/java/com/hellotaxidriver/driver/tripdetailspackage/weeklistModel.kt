package com.hellotaxidriver.driver.tripdetailspackage

class  weeklistModel
{
    var id: Int? = null
    var billing_period: String? = null
    var amount: String? = null
    var total_rides: String? = null
    var from_date: String? = null
    var to_date: String? = null


    constructor(id: Int, billing_period: String, amount: String, total_rides: String, from_date: String, to_date: String)
    {
        this.id = id
        this.billing_period = billing_period
        this.amount = amount
        this.total_rides = total_rides
        this.from_date = from_date
        this.to_date = to_date

    }
}