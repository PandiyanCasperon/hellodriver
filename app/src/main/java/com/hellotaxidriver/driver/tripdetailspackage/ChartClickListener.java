package com.hellotaxidriver.driver.tripdetailspackage;

public interface ChartClickListener {

    public void clickPosition(int position, int chart_count);

}
