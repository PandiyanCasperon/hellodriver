package com.hellotaxidriver.driver.tripdetailspackage

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.hellotaxidriver.driver.R


class weelistadapter(val context: Context, val listModelArrayList: ArrayList<weeklistModel>) : BaseAdapter()
{

    var typeface: Typeface = Typeface.createFromAsset(context.assets, "fonts/verdanab.ttf")
    var typefacebold: Typeface = Typeface.createFromAsset(context.assets, "fonts/verdanab.ttf")

    lateinit var tripSummaryModel:summaryinsideModel

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View?
    {
        val view: View?
        val vh: weekViewHolder
        if (convertView == null)
        {
            val layoutInflater = LayoutInflater.from(context)
            view = layoutInflater.inflate(R.layout.weektrip_kotlin_summary_list, parent, false)
            vh = weekViewHolder(view)
            view.tag = vh
        }
        else
        {
            view = convertView
            vh = view.tag as weekViewHolder
        }

        vh.month_tv.text=listModelArrayList[position].billing_period
        vh.net_fare_tv.text=listModelArrayList[position].amount
        vh.trip_tv.text=listModelArrayList[position].total_rides
        return view
    }
    override fun getItem(position: Int): Any
    {
        return listModelArrayList[position]
    }
    override fun getItemId(position: Int): Long
    {
        return position.toLong()
    }
    override fun getCount(): Int
    {
        return listModelArrayList.size
    }
}
private class weekViewHolder(view: View?)
{
    val month_tv: TextView = view?.findViewById<TextView>(R.id.week_list_month_tv) as TextView
    val net_fare_tv: TextView = view?.findViewById<TextView>(R.id.week_list_net_fare_tv) as TextView
    val trip_tv: TextView = view?.findViewById<TextView>(R.id.week_list_trip_tv) as TextView


}