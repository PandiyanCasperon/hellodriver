package com.hellotaxidriver.driver.tripdetailspackage.localdb;

/**
 * Created by anandan on 10-09-2016.
 */
public interface TripDbConstansts {

    String DATABASE_NAME = "trip_details_db";
    String TABLE_TRIP_DETAILS = "trip_details_table";

}
