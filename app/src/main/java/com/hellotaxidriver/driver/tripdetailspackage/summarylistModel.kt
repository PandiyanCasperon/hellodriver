package com.hellotaxidriver.driver.tripdetailspackage

class  summarylistModel
{
    var id: Int? = null
    var title: String? = null
    var content: String? = null
    var dropaddress: String? = null
    var cancelreason: String? = null
    var statustype: String? = null
    var rideidd: String? = null
    var vehicletype: String? = null

    constructor(id: Int, title: String, content: String, dropaddress: String, cancelreason: String, statustype: String, rideidd: String, vehicletype:String)
    {
        this.id = id
        this.title = title
        this.content = content
        this.dropaddress = dropaddress
        this.cancelreason = cancelreason
        this.statustype = statustype
        this.rideidd = rideidd
        this.vehicletype = vehicletype
    }
}