package com.hellotaxidriver.driver.tripdetailspackage

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Location
import android.os.*
import com.hellotaxidriver.driver.R
import com.hellotaxidriver.driver.foregroundservice.constant.Constant
import com.hellotaxidriver.driver.foregroundservice.jobserviceLocationUpdatesComponent
import java.text.SimpleDateFormat
import java.util.*


class locationretrievekotlin : Service(), jobserviceLocationUpdatesComponent.ILocationProvider
{
    override fun onLocationUpdate(location: Location?)
    {
        if (location != null)
        {
            sendMessage( location)
        }
    }

     var TAG:String = locationretrievekotlin::class.java.simpleName;
     var mcontext: Context? = null
     var currentLat:Double=0.0;
     var currentLong:Double=0.0;
     var notify_string:String="";
     var NOTIFICATION_CHANNEL_ID:String="";
     var builder: Notification.Builder?=null;
     var myContext: Context? = null
     var locationUpdatesComponent: jobserviceLocationUpdatesComponent? = null
     var mNotificationManager: NotificationManager? = null
     var isServiceDestroyed = false

    override fun onBind(intent: Intent): IBinder?
    {
        throw UnsupportedOperationException("Not yet implemented")
    }


    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int
    {
        if (intent != null)
        {
            locationUpdatesComponent = jobserviceLocationUpdatesComponent(this)
            locationUpdatesComponent?.onCreate(this)
            myContext = applicationContext
            if (intent.action == Constant.ACTION.STARTFOREGROUND_ACTION)
            {
                builder = Notification.Builder(this)
                mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                {
                    val cal = Calendar.getInstance()
                    val date = cal.time
                    val dateFormat = SimpleDateFormat("hh:mm:ss a")
                    val formattedDate = dateFormat.format(date)
                    notify_string = getString(R.string.location_update_notify_label) + " " + formattedDate
                    NOTIFICATION_CHANNEL_ID = "LOCATION_UPDATE_FOREGROUND_SERVICES_ID"
                    val channelName = "LOCATION_UPDATE_FOREGROUND_SERVICE_NAME"
                    val channel = NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_LOW)
                    channel.lightColor = Color.BLUE
                    channel.enableVibration(false)
                    channel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
                    mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    assert(mNotificationManager != null)
                    mNotificationManager?.createNotificationChannel(channel)
                    builder?.setContentTitle(resources.getString(R.string.go_online_string))
                            ?.setBadgeIconType(R.drawable.applogo)
                            ?.setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.applogo))
                            ?.setTicker(notify_string)
                            ?.setContentText(notify_string)
                            ?.setAutoCancel(true)
                            ?.setChannelId(NOTIFICATION_CHANNEL_ID)
                            ?.setSmallIcon(R.drawable.applogo)
                            ?.setOngoing(false)
                            ?.setCategory(Notification.CATEGORY_SERVICE)?.build()
                    startForeground(1, builder?.build())
                }
                else
                {
                    val builder = Notification.Builder(baseContext)
                            .setContentTitle(getString(R.string.go_online_string))
                            .setSmallIcon(R.drawable.applogo)
                    builder.setContentText(notify_string)
                    val notification = builder.build()
                    startForeground(1, notification)
                }
            }
            else if (intent.action == Constant.ACTION.STOPFOREGROUND_ACTION)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                {
                    locationUpdatesComponent?.onStop()
                    isServiceDestroyed = true
                    stopForeground(true)
                    stopSelf()
                    if (mNotificationManager != null)
                        mNotificationManager?.deleteNotificationChannel(NOTIFICATION_CHANNEL_ID)
                }
                else
                {
                    locationUpdatesComponent?.onStop()
                    isServiceDestroyed = true
                    stopForeground(true)
                }
            }
        }
        return Service.START_STICKY
    }




    override fun onDestroy()
    {
        super.onDestroy()
        if (locationUpdatesComponent != null)
        {
            locationUpdatesComponent?.onStop()
        }
        stopForeground(true)
        isServiceDestroyed = true
    }



    //location update to main page
    private fun sendMessage(location: Location?)
    {
        if (location != null)
        {
            currentLat = location.latitude
            currentLong = location.longitude

            var intent = Intent()
            intent.setAction("GET_SIGNAL_STRENGTH")
            intent.putExtra("latitude", currentLat)
            intent.putExtra("longitude", currentLong)
            intent.putExtra("LEVEL_DATA", "1")
            sendBroadcast(intent)
        }
    }

}