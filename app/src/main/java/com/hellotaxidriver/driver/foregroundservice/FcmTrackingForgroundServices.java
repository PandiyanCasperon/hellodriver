package com.hellotaxidriver.driver.foregroundservice;


import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.app.xmpp.MyXMPP;
import com.hellotaxidriver.driver.Helper.GEODBHelper;
import com.hellotaxidriver.driver.Pojo.FcmTrackingPojo;
import com.hellotaxidriver.driver.Pojo.UserPojo;
import com.hellotaxidriver.driver.R;
import com.hellotaxidriver.driver.TripPage;
import com.hellotaxidriver.driver.Utils.ConnectionDetector;
import com.hellotaxidriver.driver.Utils.SessionManager;
import com.hellotaxidriver.driver.foregroundservice.model.DataTripPage;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class FcmTrackingForgroundServices extends Service implements LocationListener, GpsStatus.Listener {
    protected String chatID;
    public int SERVER_HIT_INTERVAL = 3;//variable to execute services every 35 second
    Location lastlocation = new Location("last");
    private static final String TAG = FcmTrackingForgroundServices.class.getSimpleName();
    DataTripPage data;
    double currentLon = 0;
    double currentLat = 0;
    double lastLon = 0;
    String hostURL = "", hostName = "";
    double lastLat = 0;
    float bearingValue;
    int insertDB = 0;
    String NOTIFICATION_CHANNEL_ID;
    int jk = 0;
    int tracking = 0;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;

    PendingIntent contentIntent;
    private LocationManager mLocationManager;
    private SessionManager session;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private float total_amount;
    private Context myContext;
    private NotificationManager mNotificationManager;
    private GEODBHelper myDBHelper;
    private JSONObject job;

    public static MyXMPP xmpp;
    private String ServiceName = "", HostAddress = "";
    private String USERNAME = "";
    private String PASSWORD = "";
    private String userId="";


    @SuppressLint("MissingPermission")
    @Override
    public void onCreate() {

        if (myContext == null)
            myContext = getApplicationContext();
        if (session == null)
            session = new SessionManager(myContext);


        myDBHelper = new GEODBHelper(myContext);
        Intent notificationIntent = new Intent(this, TripPage.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        contentIntent = PendingIntent.getActivity(
                this, 0, notificationIntent, 0);

//        updateNotification(false);

        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        mLocationManager.addGpsStatusListener(this);
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, this);
    }

    public void onLocationChanged(Location location) {

        if (location != null) {

            data = TripPage.getData();
            if (data.isRunning()) {
                currentLat = location.getLatitude();
                currentLon = location.getLongitude();

                double distance = lastlocation.distanceTo(location);
                bearingValue = lastlocation.bearingTo(location);

                if (location.getAccuracy() < distance) {
                    if (lastLat != 0.0) {

                        Calendar aCalendar = Calendar.getInstance();
                        SimpleDateFormat aDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String aCurrentDate = aDateFormat.format(aCalendar.getTime());
                        String ride_id_str = session.getCurrentRideId();


                        ArrayList<UserPojo> userlist = new ArrayList<UserPojo>();
                        userlist = myDBHelper.getUserData();

                        mFirebaseInstance = FirebaseDatabase.getInstance();

                        // get reference to 'users' node
                        mFirebaseDatabase = mFirebaseInstance.getReference();

                        // store app title to 'app_title' node
                      /*  mFirebaseInstance.getReference("app_title").setValue("Realtime Database");*/


                         if (tracking ==0){

                             // Creating new user node, which returns the unique key value
                             // new user node would be /users/$userid/
                             userId = mFirebaseDatabase.push().getKey();

                            // creating user object
                            FcmTrackingPojo trackingdata = new FcmTrackingPojo(ride_id_str,currentLat,currentLon,bearingValue,aCurrentDate,userlist.get(0).getBtn_group());

                            // pushing user to 'users' node using the userId
                             mFirebaseDatabase.child(ride_id_str).setValue(trackingdata);

                            tracking++;

//                             addUserChangeListener(ride_id_str);

                        }
                        else {

                             // pushing user to 'users' node using the userId
                             mFirebaseDatabase.child(ride_id_str).child("currentLat").setValue(currentLat);
                             mFirebaseDatabase.child(ride_id_str).child("currentLon").setValue(currentLon);
                             mFirebaseDatabase.child(ride_id_str).child("bearingValue").setValue(bearingValue);
                             mFirebaseDatabase.child(ride_id_str).child("aCurrentDate").setValue(aCurrentDate);
                             mFirebaseDatabase.child(ride_id_str).child("ridestatus").setValue(userlist.get(0).getBtn_group());

//                             addUserChangeListener(ride_id_str);
                         }


                        lastLat = currentLat;
                        lastLon = currentLon;
                    } else {
                        lastLat = currentLat;
                        lastLon = currentLon;
                    }
                }

//                updateNotification(true);
            }
        }

    }


    /**
     * User data change listener FCm
     */
    private void addUserChangeListener(String userId) {
        // User data change listener
        mFirebaseDatabase.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               /* FcmTrackingPojo track = dataSnapshot.getValue(FcmTrackingPojo.class);*/

                // Check for null
                if (dataSnapshot == null) {
                    Log.e(TAG, "Tracking data is null!");
                    return;
                }
                Log.e(TAG, "Tracking data is changed!" + dataSnapshot.toString());

            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }


    public void updateNotification(boolean asData) {

        Notification.Builder builder = new Notification.Builder(getBaseContext())
                .setContentTitle(getString(R.string.meter_notify_trip_label))
                .setSmallIcon(R.drawable.applogo)
                .setContentIntent(contentIntent);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NOTIFICATION_CHANNEL_ID = "OTPMETER_ID";
            String channelName = "OTPMETER";

            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_LOW);
            channel.setLightColor(Color.BLUE);
            channel.enableVibration(false);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            assert mNotificationManager != null;
            mNotificationManager.createNotificationChannel(channel);

            if (asData) {
                double distanceTemp = data.getDistance();
                distanceTemp /= 1000.0;
                String dis = String.format("%.2f", distanceTemp);
                builder.setContentText(String.format(getString(R.string.notificationlowerend), "", dis));
                builder.setContentTitle(getResources().getString(R.string.meter_notify_trip_label))
                        .setBadgeIconType(R.drawable.applogo)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.applogo))
                        .setTicker("")
                        .setContentText(String.format(getString(R.string.notificationlowerend), "", dis))
                        .setAutoCancel(true)
                        .setChannelId(NOTIFICATION_CHANNEL_ID)
                        .setSmallIcon(R.drawable.applogo)
                        .setOngoing(false)
                        .setCategory(Notification.CATEGORY_SERVICE).build();
            } else {
                builder.setContentTitle(getResources().getString(R.string.meter_notify_trip_label))
                        .setBadgeIconType(R.drawable.applogo)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.applogo))
                        .setTicker("")
                        .setContentText(String.format(getString(R.string.notificationlowerend), '-', "-0"))
                        .setAutoCancel(true)
                        .setChannelId(NOTIFICATION_CHANNEL_ID)
                        .setSmallIcon(R.drawable.applogo)
                        .setOngoing(false)
                        .setCategory(Notification.CATEGORY_SERVICE).build();
            }
            Notification notification = builder.build();
            startForeground(R.string.noti_id, notification);

        } else {
            if (asData) {
                double distanceTemp = data.getDistance();
                distanceTemp /= 1000.0;
                String dis = String.format("%.2f", distanceTemp);
                builder.setContentText(String.format(getString(R.string.notificationlowerend), "", dis));
            } else {
                builder.setContentText(String.format(getString(R.string.notificationlowerend), '-', "-0"));
            }
            Notification notification = builder.build();
            startForeground(R.string.noti_id, notification);
        }


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // If we get killed, after returning from here, restart
        if (myContext == null)
            myContext = getApplicationContext();
        if (session == null)
            session = new SessionManager(myContext);

//        doBindService();
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
        mLocationManager.removeUpdates(this);
        mLocationManager.removeGpsStatusListener(this);
        stopForeground(true);
//        doUnbindService();
    }

    @Override
    public void onGpsStatusChanged(int event) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }




    class isStillStopped extends AsyncTask<Void, Integer, String> {
        int timer = 0;

        @Override
        protected String doInBackground(Void... unused) {
            try {
                while (data.getCurSpeed() == 0) {
                    Thread.sleep(1000);
                    timer++;
                }
            } catch (InterruptedException t) {
                return ("The sleep operation failed");
            }
            return ("return object when task is finished");
        }

        @Override
        protected void onPostExecute(String message) {
            data.setTimeStopped(timer);
        }
    }

}