package com.hellotaxidriver.driver.foregroundservice;


import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.service.ServiceConstant;
import com.app.service.ServiceRequest;
import com.app.xmpp.MyXMPP;
import com.hellotaxidriver.driver.Helper.GEODBHelper;
import com.hellotaxidriver.driver.Pojo.UserPojo;
import com.hellotaxidriver.driver.R;
import com.hellotaxidriver.driver.Utils.ConnectionDetector;
import com.hellotaxidriver.driver.Utils.GPSTracker;
import com.hellotaxidriver.driver.Utils.SessionManager;
import com.hellotaxidriver.driver.foregroundservice.constant.Constant;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class JobLocationUpdateForgroundService extends Service implements LocationListener, GpsStatus.Listener {

    private LocationManager mLocationManager;
    private Location last_location, currentLocation;

    public static final int LOCATION_MESSAGE = 9999;
    public static final long INTERVAL = 1000;//variable to execute services every 1 second
    public int SERVER_HIT_INTERVAL = 3;//variable to execute services every 35 second
    private static final String TAG = JobLocationUpdateForgroundService.class.getSimpleName();
    private static final String LOG_TAG = "ForegroundService";
    public static Context mcontext;
    protected String chatID;
    protected double currentLat = 0.0;
    protected double currentLong = 0.0;
    float bearingValue;
    int insertDB = 0;
    int forseconds = 0;
    String hostURL = "", hostName = "";
    String notify_string = "";
    String NOTIFICATION_CHANNEL_ID;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    Notification.Builder builder;
    Handler gpsHandler;
    private PowerManager.WakeLock mWakeLock;

    private Context myContext;
    private GEODBHelper myDBHelper;
    private SessionManager session;

    public static MyXMPP xmpp;
    private String ServiceName = "", HostAddress = "";
    private String USERNAME = "";
    private String PASSWORD = "";
    private JSONObject job;
    private float total_amount;
    private Handler mHandler = new Handler(); // run on another Thread to avoid crash
    private Timer mTimer = null;
    //    private jobserviceLocationUpdatesComponent locationUpdatesComponent;
    private NotificationManager mNotificationManager;
    private boolean isServiceDestroyed = false;

    public JobLocationUpdateForgroundService() {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopForeground(true);
        if(mLocationManager!=null)
        {
            mLocationManager.removeUpdates(this);
            mLocationManager.removeGpsStatusListener(this);
        }

        if (mTimer != null)
            mTimer.cancel();
        Log.i(TAG, "onDestroy....");
//        locationUpdatesComponent.onStop();
        isServiceDestroyed = true;
//        doUnbindService();
/*        if (DashBoardDriver.isOnline) {
            Intent broadcastIntent = new Intent(this, ServiceStarter.class);
            sendBroadcast(broadcastIntent);
        }*/

        //  doUnbindService();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // calling super is required when extending from LocationBaseService


        super.onStartCommand(intent, flags, startId);
        Log.i(TAG, "created...............");
    /*    locationUpdatesComponent = new jobserviceLocationUpdatesComponent(this);
        locationUpdatesComponent.onCreate(this);*/
//        doBindService();
        myContext = getApplicationContext();
        session = new SessionManager(myContext);
        myDBHelper = new GEODBHelper(myContext);
        session.createServiceStatus("1");

        if (session != null && session.getUserDetails() != null) {
            hostURL = session.getXmpp().get(SessionManager.KEY_HOST_URL);
            hostName = session.getXmpp().get(SessionManager.KEY_HOST_NAME);

            HashMap<String, String> domain = session.getXmpp();
            ServiceName = domain.get(SessionManager.KEY_HOST_NAME);
            HostAddress = domain.get(SessionManager.KEY_HOST_URL);

            HashMap<String, String> user = session.getUserDetails();
            USERNAME = user.get(SessionManager.KEY_DRIVERID);
            PASSWORD = user.get(SessionManager.KEY_SEC_KEY);

            if (session.isLoggedIn()) {
                try {
                    xmpp = MyXMPP.getInstance(this, ServiceName, HostAddress, USERNAME, PASSWORD);
                    xmpp.connect("onCreate");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        if (session != null && session.getUserDetails() != null) {
            hostURL = session.getXmpp().get(SessionManager.KEY_HOST_URL);
            hostName = session.getXmpp().get(SessionManager.KEY_HOST_NAME);
        }
        if (intent != null) {
            if (intent.getAction().equals(Constant.ACTION.STARTFOREGROUND_ACTION)) {

                builder = new Notification.Builder(this);
                mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    Calendar cal = Calendar.getInstance();
                    Date date = cal.getTime();
                    DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss a");
                    String formattedDate = dateFormat.format(date);
                    notify_string = getString(R.string.location_update_notify_label) + " " + formattedDate;
                    NOTIFICATION_CHANNEL_ID = "LOCATION_UPDATE_FOREGROUND_SERVICES_ID";
                    String channelName = "LOCATION_UPDATE_FOREGROUND_SERVICE_NAME";
                    NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_LOW);
                    channel.setLightColor(Color.BLUE);
                    channel.enableVibration(false);
                    channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
                    mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    assert mNotificationManager != null;
                    mNotificationManager.createNotificationChannel(channel);
                    builder.setContentTitle(getResources().getString(R.string.app_name))
                            .setBadgeIconType(R.drawable.applogo)
                            .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.applogo))
                            .setTicker(notify_string)
                            .setContentText(notify_string)
                            .setAutoCancel(true)
                            .setChannelId(NOTIFICATION_CHANNEL_ID)
                            .setSmallIcon(R.drawable.applogo)
                            .setOngoing(false)
                            .setCategory(Notification.CATEGORY_SERVICE).build();
                    startForeground(1, builder.build());
                }

                if (mTimer != null) {
                    mTimer.cancel();
                    mTimer = null;
                } else {
                    forseconds = 0;
                    mTimer = new Timer(); // recreate new timer
                    mTimer.scheduleAtFixedRate(new JobLocationUpdateForgroundService.TimeDisplayTimerTask(), 0, INTERVAL);
                }
                Log.i(LOG_TAG, "Received Start Foreground Intent ");

            } else if (intent.getAction().equals(Constant.ACTION.STOPFOREGROUND_ACTION)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    if (mTimer != null)
                        mTimer.cancel();
                    Log.i(TAG, "onDestroy....");
//                    locationUpdatesComponent.onStop();
                    isServiceDestroyed = true;
                    //  doUnbindService();

                    stopForeground(true);
                    stopSelf();
                    if (mNotificationManager != null)
                        mNotificationManager.deleteNotificationChannel(NOTIFICATION_CHANNEL_ID);
                } else {
                    if (mTimer != null)
                        mTimer.cancel();
                    Log.i(TAG, "onDestroy....");
//                    locationUpdatesComponent.onStop();
                    isServiceDestroyed = true;
                    //  doUnbindService();
                    stopForeground(true);
                }

            }

        }

        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        mLocationManager.addGpsStatusListener(this);
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, this);


        return START_STICKY;
    }

    public void setServiceResponse(String url, String lat, String lon, String id) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        HashMap<String, String> user = session.getUserDetails();
        String driver_id = user.get(SessionManager.KEY_DRIVERID);
        String ride_id = id;
        jsonParams.put("ride_id", ride_id);
        jsonParams.put("latitude", lat);
        jsonParams.put("longitude", lon);
        jsonParams.put("driver_id", driver_id);
        ServiceRequest mRequest = new ServiceRequest(myContext);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println(response);
            }

            @Override
            public void onErrorListener() {
            }
        });

    }

/*    @Override
    public void onLocationUpdate(Location location) {
        if (location != null) {
            bearingValue = location.bearingTo(location);
            currentLat = location.getLatitude();
            currentLong = location.getLongitude();
        }
    }*/

    public void setxmppServiceResponse(String url, String lat, String lon, String id, Float bearing) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        HashMap<String, String> user = session.getUserDetails();
        String driver_id = user.get(SessionManager.KEY_DRIVERID);
        String ride_id = id;
        jsonParams.put("ride_id", ride_id);
        jsonParams.put("lat", lat);
        jsonParams.put("lon", lon);
        jsonParams.put("bearing", String.valueOf(bearingValue));
        jsonParams.put("driver_id", driver_id);
        ServiceRequest mRequest = new ServiceRequest(myContext);
        System.out.println("------XMPP TO SERVER URL------------------------jaicheck---------" + url);
        System.out.println("------XMPP TO SERVER jsonParams------------------------jaicheck---------" + jsonParams);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                // System.out.println(response);
                try {
                    JSONObject object = new JSONObject(response);
                    // JSONObject respomse = object.getJSONObject("response");
                    String status = object.getString("status");
                    String message = object.getString("response");
                    if (status.equalsIgnoreCase("1")) {
                        System.out.println("------XMPP TO SERVER response------------------------jaicheck---------" + message);
                    }

                    // Toast.makeText(myContext, message, Toast.LENGTH_LONG);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
            }
        });
    }

    /*void doBindService() {
        System.out.println("---------jai-----service bind------");
        bindService(new Intent(this, XmppService.class), mConnection,
                Context.BIND_AUTO_CREATE);
    }

    void doUnbindService() {
        if (mConnection != null) {
            unbindService(mConnection);
        }
    }*/

    @Override
    public void onGpsStatusChanged(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null && location.getLatitude() != 0.0) {
            currentLat = location.getLatitude();
            currentLong = location.getLongitude();
        } else {
            currentLat = 0.0;
            currentLong = 0.0;
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private class TimeDisplayTimerTask extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @TargetApi(Build.VERSION_CODES.O)
                @Override
                public void run() {
                    if (session.isLoggedIn()) {
                        try {
                            cd = new ConnectionDetector(getBaseContext());
                            isInternetPresent = cd.isConnectingToInternet();
                            if (isInternetPresent) {
                                try {
                                    xmpp = MyXMPP.getInstance(getBaseContext(), ServiceName, HostAddress, USERNAME, PASSWORD);
                                    xmpp.connect("onCreate");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    System.out.println("--------Serivce running with timer------" + currentLat);
                    Calendar cal = Calendar.getInstance();
                    Date date = cal.getTime();
                    DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss a");
                    String formattedDate = dateFormat.format(date);
                    notify_string = getString(R.string.location_update_notify_label) + " " + formattedDate;


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        if (mNotificationManager != null) {
                            builder.setTicker(notify_string)
                                    .setBadgeIconType(R.drawable.applogo)
                                    .setSmallIcon(R.drawable.applogo)
                                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.applogo))
                                    .setContentText(notify_string)
                                    .setTicker(notify_string)
                                    .setContentText(notify_string)
                                    .setAutoCancel(true)
                                    .setOngoing(false)
                                    .setCategory(Notification.CATEGORY_SERVICE).build();
                            mNotificationManager.notify(1, builder.build());
                        }
                    } else {
                        Notification.Builder builder = new Notification.Builder(getBaseContext())
                                .setContentTitle(getString(R.string.app_name))
                                .setSmallIcon(R.drawable.applogo);

                        builder.setContentText(notify_string);
                        Notification notification = builder.build();
                        startForeground(1, notification);
                    }

                    if (forseconds > SERVER_HIT_INTERVAL) {
                        forseconds = 0;

                        cd = new ConnectionDetector(getBaseContext());
                        isInternetPresent = cd.isConnectingToInternet();

                        if (isInternetPresent) {

                            LocationManager locationManager;
                            boolean isGPSEnabled = false;
                            boolean isNetworkEnabled = false;
                            locationManager = (LocationManager) getBaseContext().getSystemService(LOCATION_SERVICE);
                            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                            if (!isGPSEnabled && !isNetworkEnabled) {
                                //no provider
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.alert_gpsEnable), Toast.LENGTH_LONG).show();
                            } else {
                                if (currentLat != 0.0) {
                                    serverhitlocation();
                                }

                            }

                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.alert_nointernet), Toast.LENGTH_LONG).show();
                        }


                    } else {
                        forseconds++;
                    }


                }
            });
        }
    }


    private void serverhitlocation() {
        String status = myDBHelper.retriveStatus();
        //verfify driver status when onduty
        if (("1".equalsIgnoreCase(status))) {
            double lat = currentLat;
            double longitude = currentLong;
            currentLocation = new Location("");
            currentLocation.setLatitude(currentLat);
            currentLocation.setLongitude(currentLong);
            ArrayList<UserPojo> userlist = new ArrayList<UserPojo>();
            userlist = myDBHelper.getUserData();
            for (int i = 0; i < userlist.size(); i++) {
                if (userlist.get(i).getBtn_group().equals("2") || userlist.get(i).getBtn_group().equals("3") || userlist.get(i).getBtn_group().equals("4")) {
                    SERVER_HIT_INTERVAL = 3;
                    if (last_location == null) {
                        last_location = currentLocation;
                    }
                    bearingValue = last_location.bearingTo(currentLocation);
                    chatID = userlist.get(i).getUser_id() + "@" + hostName;
                    if (chatID != null) {
                        try {
                            if (job == null) {
                                job = new JSONObject();
                            }
                            job.put("action", "driver_loc");
                            job.put("latitude", lat);
                            job.put("longitude", longitude);
                            job.put("bearing", bearingValue);
                            job.put("ride_id", userlist.get(i).getRide_id());
                            job.put("user_id", userlist.get(i).getUser_id());
                            HashMap<String, String> user = session.getUserDetails();
                            String driver_id = user.get(SessionManager.KEY_DRIVERID);
                            job.put("driver_id", driver_id);
                            job.put("chatID", chatID);

                            xmpp.chat_created = false;
                            String data = URLEncoder.encode(job.toString(), "UTF-8");
                            System.out.println("----jaiURLEncoder data----------" + data);
                            try {
                                int return_status1 = xmpp.sendMessageServer("trackuser" + "@" + hostName, data);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            int return_status = xmpp.sendMessage(chatID, job.toString());
                            if (return_status == 0) {

                                setxmppServiceResponse(ServiceConstant.XmppServerUpdate, "" + currentLat, "" + currentLong, userlist.get(i).getRide_id(), bearingValue);

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    SERVER_HIT_INTERVAL = 35;
                    if ((currentLat == 0.0) || (currentLong == 0.0)) {
                    } else {
                        GPSTracker gps;
                        gps = new GPSTracker(getBaseContext());
                        if (gps.canGetLocation()) {
                            if (gps.getLatitude() > 0.0) {
                                currentLat = gps.getLatitude();
                                currentLong = gps.getLongitude();
                            }
                        }
                        setServiceResponse(ServiceConstant.UPDATE_CURRENT_LOCATION, "" + currentLat, "" + currentLong, "");
                    }

                }
            }
        } else {
            insertDB = 0;
            total_amount = 0;
            if ((currentLat == 0.0) || (currentLong == 0.0)) {
            } else {
                GPSTracker gps;
                gps = new GPSTracker(getBaseContext());
                if (gps.canGetLocation()) {
                    if (gps.getLatitude() > 0.0) {
                        currentLat = gps.getLatitude();
                        currentLong = gps.getLongitude();
                    }
                }

                setServiceResponse(ServiceConstant.UPDATE_CURRENT_LOCATION, "" + currentLat, "" + currentLong, "");
            }
        }
        last_location = currentLocation;
    }

   /* private final ServiceConnection mConnection = new ServiceConnection() {
        @SuppressWarnings("unchecked")
        @Override
        public void onServiceConnected(final ComponentName name,
                                       final IBinder service) {
            xmppService = ((LocalBinder<XmppService>) service).getService();
            Log.d(TAG, "-jai---onServiceConnected");
        }

        @Override
        public void onServiceDisconnected(final ComponentName name) {
            xmppService = null;
        }
    };*/
}