package com.hellotaxidriver.driver.foregroundservice;


import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.app.service.ServiceConstant;
import com.app.service.ServiceRequest;
import com.app.xmpp.MyXMPP;
import com.hellotaxidriver.driver.Helper.GEODBHelper;
import com.hellotaxidriver.driver.Pojo.UserPojo;
import com.hellotaxidriver.driver.R;
import com.hellotaxidriver.driver.TripPage;
import com.hellotaxidriver.driver.Utils.ConnectionDetector;
import com.hellotaxidriver.driver.Utils.GPSTracker;
import com.hellotaxidriver.driver.Utils.SessionManager;
import com.hellotaxidriver.driver.foregroundservice.model.DataTripPage;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class GpsTripForgroundServices extends Service implements LocationListener, GpsStatus.Listener {
    protected String chatID;
    public int SERVER_HIT_INTERVAL = 3;//variable to execute services every 35 second
    Location lastlocation = new Location("last");
    private static final String TAG = GpsTripForgroundServices.class.getSimpleName();
    DataTripPage data;
    double currentLon = 0;
    double currentLat = 0;
    double lastLon = 0;
    String hostURL = "", hostName = "";
    double lastLat = 0;
    float bearingValue;
    int insertDB = 0;
    String NOTIFICATION_CHANNEL_ID;
    int jk = 0;
    int tracking = 0;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;

    PendingIntent contentIntent;
    private LocationManager mLocationManager;
    private SessionManager session;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private float total_amount;
    private Context myContext;
    private NotificationManager mNotificationManager;
    private GEODBHelper myDBHelper;
    private JSONObject job;

    public static MyXMPP xmpp;
    private String ServiceName = "", HostAddress = "";
    private String USERNAME = "";
    private String PASSWORD = "";
    private String userId="";

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate() {

        if (myContext == null)
            myContext = getApplicationContext();
        if (session == null)
            session = new SessionManager(myContext);

        if (session != null && session.getUserDetails() != null) {
            hostURL = session.getXmpp().get(SessionManager.KEY_HOST_URL);
            hostName = session.getXmpp().get(SessionManager.KEY_HOST_NAME);
            HashMap<String, String> domain = session.getXmpp();
            ServiceName = domain.get(SessionManager.KEY_HOST_NAME);
            HostAddress = domain.get(SessionManager.KEY_HOST_URL);
            HashMap<String, String> user = session.getUserDetails();
            USERNAME = user.get(SessionManager.KEY_DRIVERID);
            PASSWORD = user.get(SessionManager.KEY_SEC_KEY);
            if (session.isLoggedIn()) {
                try {
                    xmpp = MyXMPP.getInstance(this, ServiceName, HostAddress, USERNAME, PASSWORD);
                    xmpp.connect("onCreate");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        myDBHelper = new GEODBHelper(myContext);
        Intent notificationIntent = new Intent(this, TripPage.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        contentIntent = PendingIntent.getActivity(
                this, 0, notificationIntent, 0);

        updateNotification(false);

        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        mLocationManager.addGpsStatusListener(this);
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, this);


    }

    public void onLocationChanged(Location location) {

        if (location != null) {

            data = TripPage.getData();
            if (data.isRunning()) {
                currentLat = location.getLatitude();
                currentLon = location.getLongitude();

                if (data.isFirstTime()) {
                    lastLat = currentLat;
                    lastLon = currentLon;
                    data.setFirstTime(false);
                }

                lastlocation.setLatitude(lastLat);
                lastlocation.setLongitude(lastLon);
                double distance = lastlocation.distanceTo(location);
                bearingValue = lastlocation.bearingTo(location);

                if (location.getAccuracy() < distance) {
                    if (lastLat != 0.0) {

                        Calendar aCalendar = Calendar.getInstance();
                        SimpleDateFormat aDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String aCurrentDate = aDateFormat.format(aCalendar.getTime());
                        String ride_id_str = session.getCurrentRideId();
                        if (!ride_id_str.equals("")) {
                            myDBHelper = new GEODBHelper(myContext);
                            myDBHelper.insertLatLong(ride_id_str, currentLat, currentLon, aCurrentDate);

                        }

                       /* mFirebaseInstance = FirebaseDatabase.getInstance();

                        // get reference to 'users' node
                        mFirebaseDatabase = mFirebaseInstance.getReference();

                        // store app title to 'app_title' node
                      *//*  mFirebaseInstance.getReference("app_title").setValue("Tracking DataBase");*//*


                         if (tracking ==0){

                             // Creating new user node, which returns the unique key value
                             // new user node would be /users/$userid/
                             *//*userId = mFirebaseDatabase.push().getKey();*//*

                            // creating user object
                            FcmTrackingPojo trackingdata = new FcmTrackingPojo(ride_id_str,currentLat,currentLon,bearingValue,aCurrentDate,"");

                            // pushing user to 'users' node using the userId
                             mFirebaseDatabase.child(ride_id_str).setValue(trackingdata);

                            tracking++;

                             addUserChangeListener(ride_id_str);

                        }
                        else {

                             // pushing user to 'users' node using the userId
                             mFirebaseDatabase.child(ride_id_str).child("currentLat").setValue(currentLat);
                             mFirebaseDatabase.child(ride_id_str).child("currentLon").setValue(currentLon);
                             mFirebaseDatabase.child(ride_id_str).child("bearingValue").setValue(bearingValue);
                             mFirebaseDatabase.child(ride_id_str).child("aCurrentDate").setValue(aCurrentDate);

                             addUserChangeListener(ride_id_str);
                         }*/




                        data.addDistance(distance);
                        lastLat = currentLat;
                        lastLon = currentLon;
                    } else {
                        lastLat = currentLat;
                        lastLon = currentLon;
                    }
                    data.setCurrentLat(currentLat);
                    data.setCurrentLon(currentLon);
                }

                if (location.hasSpeed()) {
                    data.setCurSpeed(location.getSpeed() * 3.6);
                    if (location.getSpeed() == 0) {
                        new isStillStopped().execute();
                    }
                }
                data.update();
                updateNotification(true);
            }
        }

        if (jk > SERVER_HIT_INTERVAL) {
            jk = 0;
            cd = new ConnectionDetector(getBaseContext());
            isInternetPresent = cd.isConnectingToInternet();

            if (isInternetPresent) {

                LocationManager locationManager;
                boolean isGPSEnabled = false;
                boolean isNetworkEnabled = false;
                locationManager = (LocationManager) getBaseContext().getSystemService(LOCATION_SERVICE);
                isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                if (!isGPSEnabled && !isNetworkEnabled) {
                    //no provider
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.alert_gpsEnable), Toast.LENGTH_LONG).show();

                } else {
                    serverhitlocation();
                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.alert_nointernet), Toast.LENGTH_LONG).show();
            }

        } else {
            jk++;
        }

    }




    public void updateNotification(boolean asData) {

        Notification.Builder builder = new Notification.Builder(getBaseContext())
                .setContentTitle(getString(R.string.meter_notify_trip_label))
                .setSmallIcon(R.drawable.applogo)
                .setContentIntent(contentIntent);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NOTIFICATION_CHANNEL_ID = "OTPMETER_ID";
            String channelName = "OTPMETER";

            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_LOW);
            channel.setLightColor(Color.BLUE);
            channel.enableVibration(false);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            assert mNotificationManager != null;
            mNotificationManager.createNotificationChannel(channel);

            if (asData) {
                double distanceTemp = data.getDistance();
                distanceTemp /= 1000.0;
                String dis = String.format("%.2f", distanceTemp);
                builder.setContentText(String.format(getString(R.string.notificationlowerend), "", dis));
                builder.setContentTitle(getResources().getString(R.string.meter_notify_trip_label))
                        .setBadgeIconType(R.drawable.applogo)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.applogo))
                        .setTicker("")
                        .setContentText(String.format(getString(R.string.notificationlowerend), "", dis))
                        .setAutoCancel(true)
                        .setChannelId(NOTIFICATION_CHANNEL_ID)
                        .setSmallIcon(R.drawable.applogo)
                        .setOngoing(false)
                        .setCategory(Notification.CATEGORY_SERVICE).build();
            } else {
                builder.setContentTitle(getResources().getString(R.string.meter_notify_trip_label))
                        .setBadgeIconType(R.drawable.applogo)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.applogo))
                        .setTicker("")
                        .setContentText(String.format(getString(R.string.notificationlowerend), '-', "-0"))
                        .setAutoCancel(true)
                        .setChannelId(NOTIFICATION_CHANNEL_ID)
                        .setSmallIcon(R.drawable.applogo)
                        .setOngoing(false)
                        .setCategory(Notification.CATEGORY_SERVICE).build();
            }
            Notification notification = builder.build();
            startForeground(R.string.noti_id, notification);

        } else {
            if (asData) {
                double distanceTemp = data.getDistance();
                distanceTemp /= 1000.0;
                String dis = String.format("%.2f", distanceTemp);
                builder.setContentText(String.format(getString(R.string.notificationlowerend), "", dis));
            } else {
                builder.setContentText(String.format(getString(R.string.notificationlowerend), '-', "-0"));
            }
            Notification notification = builder.build();
            startForeground(R.string.noti_id, notification);
        }


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // If we get killed, after returning from here, restart
        if (myContext == null)
            myContext = getApplicationContext();
        if (session == null)
            session = new SessionManager(myContext);

//        doBindService();
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    /* Remove the locationlistener updates when Services is stopped */
    @Override
    public void onDestroy() {
        mLocationManager.removeUpdates(this);
        mLocationManager.removeGpsStatusListener(this);
        stopForeground(true);
//        doUnbindService();
    }

    @Override
    public void onGpsStatusChanged(int event) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    private void serverhitlocation() {
        myDBHelper = new GEODBHelper(myContext);
        session.createServiceStatus("1");
        if (session != null && session.getUserDetails() != null) {
            hostURL = session.getXmpp().get(SessionManager.KEY_HOST_URL);
            hostName = session.getXmpp().get(SessionManager.KEY_HOST_NAME);
        }


        double lat = currentLat;
        double longitude = currentLon;
        ArrayList<UserPojo> userlist = new ArrayList<UserPojo>();
        userlist = myDBHelper.getUserData();
        for (int i = 0; i < userlist.size(); i++) {
            if (userlist.get(i).getBtn_group().equals("2") || userlist.get(i).getBtn_group().equals("3") || userlist.get(i).getBtn_group().equals("4")) {
                chatID = userlist.get(i).getUser_id() + "@" + hostName;
                if (chatID != null) {
                    try {
                        if (job == null) {
                            job = new JSONObject();
                        }
                        job.put("action", "driver_loc");
                        job.put("latitude", lat);
                        job.put("longitude", longitude);
                        job.put("bearing", bearingValue);
                        job.put("ride_id", userlist.get(i).getRide_id());
                        job.put("user_id", userlist.get(i).getUser_id());
                        HashMap<String, String> user = session.getUserDetails();
                        String driver_id = user.get(SessionManager.KEY_DRIVERID);
                        job.put("driver_id", driver_id);
                        job.put("chatID", chatID);

                        xmpp.chat_created = false;
                        String data = URLEncoder.encode(job.toString(), "UTF-8");
                        System.out.println("----jaiURLEncoder data----------" + data);
                        try {
                            int return_status1 = xmpp.sendMessageServer("trackuser" + "@" + hostName, data);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        int return_status = xmpp.sendMessage(chatID, job.toString());
                        if (return_status == 0) {

                            setxmppServiceResponse(ServiceConstant.XmppServerUpdate, "" + currentLat, "" + currentLon, userlist.get(i).getRide_id(), bearingValue);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    public void setxmppServiceResponse(String url, String lat, String lon, String id, Float bearing) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        HashMap<String, String> user = session.getUserDetails();
        String driver_id = user.get(SessionManager.KEY_DRIVERID);
        String ride_id = id;
        jsonParams.put("ride_id", ride_id);
        jsonParams.put("lat", lat);
        jsonParams.put("lon", lon);
        jsonParams.put("bearing", String.valueOf(bearingValue));
        jsonParams.put("driver_id", driver_id);
        ServiceRequest mRequest = new ServiceRequest(myContext);
        System.out.println("------XMPP TO SERVER URL------------------------jaicheck---------" + url);
        System.out.println("------XMPP TO SERVER jsonParams------------------------jaicheck---------" + jsonParams);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                // System.out.println(response);
                try {
                    JSONObject object = new JSONObject(response);
                    // JSONObject respomse = object.getJSONObject("response");
                    String status = object.getString("status");
                    String message = object.getString("response");
                    if (status.equalsIgnoreCase("1")) {
                        System.out.println("------XMPP TO SERVER response------------------------jaicheck---------" + message);
                    }

                    // Toast.makeText(myContext, message, Toast.LENGTH_LONG);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
            }
        });
    }

    class isStillStopped extends AsyncTask<Void, Integer, String> {
        int timer = 0;

        @Override
        protected String doInBackground(Void... unused) {
            try {
                while (data.getCurSpeed() == 0) {
                    Thread.sleep(1000);
                    timer++;
                }
            } catch (InterruptedException t) {
                return ("The sleep operation failed");
            }
            return ("return object when task is finished");
        }

        @Override
        protected void onPostExecute(String message) {
            data.setTimeStopped(timer);
        }
    }


}