package com.hellotaxidriver.driver;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.app.service.ServiceConstant;
import com.app.service.VolleyMultipartRequest;
import com.hellotaxidriver.driver.Pojo.RegistrationInfoModel;
import com.hellotaxidriver.driver.Utils.AppController;
import com.hellotaxidriver.driver.Utils.SessionManager;
import com.hellotaxidriver.driver.widgets.SnackFlashBar;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import id.zelory.compressor.Compressor;

public class RegistrationLoginDetailsActivity extends FragmentActivity {

    private static final int PERMISSION_REQUEST_CODE = 100;
    private static final String TAG = "imageupload";
    private static final String IMAGE_DIRECTORY_NAME = "Cabily_image";
    private static final int REQUEST_TAKE_PHOTO = 101;
    private static final int REQUEST_GALLERY_PHOTO = 102;

    private View headerView;
    private RelativeLayout headerBackRl, mainRl;
    private SnackFlashBar snackFlashBar;
    private CollapsingToolbarLayout headerCollapseTL;

    private TextView loginDetailsNextTv;
    private EditText driverNameEt, driverEmailEt, passwordEt, confrmPasswordEt;
    private ImageView driverImageAddIv;
    private CircularImageView driverImageIv;
    private RadioGroup genderRg;
    //    private RegistrationInfoModel registrationInfoModel;
    //Image upload
    private Dialog photo_dialog;
    private Uri camera_FileUri;
    private Bitmap selectedBitmap;
    private byte[] byteArray;
    Dialog dialog;
    private SessionManager sessionManager;
    private String driverID = "", gcmID = "", language_code = "", Agent_Name = "";
    private RegistrationInfoModel registrationInfoModel;
    private BroadcastReceiver broadcastReceiver;


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            finish();
            CloseKeyBoard();
            overridePendingTransition(R.anim.reg_pull_in_left, R.anim.reg_push_out_right);

            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    chooseimage();
                }
                else
                    {
                    chooseimage();
                }
                break;
            default:
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                try {
                    final String picturePath = camera_FileUri.getPath();


                    File directory = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/milesdocument");
                    if(!directory.exists())
                    {
                        directory.mkdir();
                    }
                    File[] files = directory.listFiles();
                    for (int i = 0; i < files.length; i++)
                    {
                        files[i].delete();
                    }



                    File curFile = new File(picturePath);
                    File compressedImage = new Compressor(this)
                            .setMaxWidth(640)
                            .setMaxHeight(480)
                            .setQuality(75)
                            .setCompressFormat(Bitmap.CompressFormat.JPEG)
                            .setDestinationDirectoryPath(directory.getAbsolutePath())
                            .compressToFile(curFile);


                    File[] filesnew = directory.listFiles();
                    File newcompressed = null;
                    for (int j = 0; j < filesnew.length; j++)
                    {
                        newcompressed = new File(directory.getAbsolutePath()+"/"+filesnew[j].getName());
                    }
                    Uri picUri = Uri.fromFile(newcompressed);

                    UCrop.Options Uoptions = new UCrop.Options();
                    Uoptions.setStatusBarColor(getResources().getColor(R.color.app_color));
                    Uoptions.setToolbarColor(getResources().getColor(R.color.app_color));
                    Uoptions.setActiveWidgetColor(ContextCompat.getColor(this, R.color.app_color));
                    Uoptions.setCompressionFormat(Bitmap.CompressFormat.JPEG);
                    Uoptions.setCompressionQuality(70);


                    UCrop.of(picUri, picUri)
                            .withMaxResultSize(8000, 8000)
                            .withOptions(Uoptions)
                            .start(RegistrationLoginDetailsActivity.this);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == REQUEST_GALLERY_PHOTO) {
                try {
                    Uri selectedImage = data.getData();
                    if (selectedImage.toString().startsWith("content://com.sec.android.gallery3d.provider")) {
                        String[] filePath = {MediaStore.Images.Media.DATA};
                        Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                        c.moveToFirst();
                        int columnIndex = c.getColumnIndex(filePath[0]);
                        final String picturePath = c.getString(columnIndex);
                        c.close();


                        File directory = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/milesdocument");
                        if(!directory.exists())
                        {
                            directory.mkdir();
                        }
                        File[] files = directory.listFiles();
                        for (int i = 0; i < files.length; i++)
                        {
                            files[i].delete();
                        }



                        File curFile = new File(picturePath);
                        File compressedImage = new Compressor(this)
                                .setMaxWidth(640)
                                .setMaxHeight(480)
                                .setQuality(75)
                                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                                .setDestinationDirectoryPath(directory.getAbsolutePath())
                                .compressToFile(curFile);


                        File[] filesnew = directory.listFiles();
                        File newcompressed = null;
                        for (int j = 0; j < filesnew.length; j++)
                        {
                            newcompressed = new File(directory.getAbsolutePath()+"/"+filesnew[j].getName());
                        }

                        Uri picUri = Uri.fromFile(newcompressed);
                        UCrop.Options Uoptions = new UCrop.Options();
                        Uoptions.setStatusBarColor(getResources().getColor(R.color.app_color));
                        Uoptions.setToolbarColor(getResources().getColor(R.color.app_color));
                        Uoptions.setActiveWidgetColor(ContextCompat.getColor(this, R.color.app_color));
                        Uoptions.setCompressionFormat(Bitmap.CompressFormat.JPEG);
                        Uoptions.setCompressionQuality(70);

                        UCrop.of(picUri, picUri)
                                .withMaxResultSize(8000, 8000)
                                .withOptions(Uoptions)
                                .start(RegistrationLoginDetailsActivity.this);

                    } else {
                        String[] filePath = {MediaStore.Images.Media.DATA};
                        Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                        c.moveToFirst();
                        int columnIndex = c.getColumnIndex(filePath[0]);
                        final String picturePath = c.getString(columnIndex);

                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();


                        File directory = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/milesdocument");
                        if(!directory.exists())
                        {
                            directory.mkdir();
                        }
                        File[] files = directory.listFiles();
                        for (int i = 0; i < files.length; i++)
                        {
                            files[i].delete();
                        }



                        File curFile = new File(picturePath);
                        File compressedImage = new Compressor(this)
                                .setMaxWidth(640)
                                .setMaxHeight(480)
                                .setQuality(75)
                                .setCompressFormat(Bitmap.CompressFormat.JPEG)
                                .setDestinationDirectoryPath(directory.getAbsolutePath())
                                .compressToFile(curFile);


                        File[] filesnew = directory.listFiles();
                        File newcompressed = null;
                        for (int j = 0; j < filesnew.length; j++)
                        {
                             newcompressed = new File(directory.getAbsolutePath()+"/"+filesnew[j].getName());
                        }

                        Uri picUri = Uri.fromFile(newcompressed);
                        UCrop.Options Uoptions = new UCrop.Options();
                        Uoptions.setStatusBarColor(getResources().getColor(R.color.app_color));
                        Uoptions.setToolbarColor(getResources().getColor(R.color.app_color));
                        Uoptions.setActiveWidgetColor(ContextCompat.getColor(this, R.color.app_color));


                        UCrop.of(picUri, picUri)
                                .withMaxResultSize(8000, 8000)
                                .withOptions(Uoptions)
                                .start(RegistrationLoginDetailsActivity.this);


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == UCrop.REQUEST_CROP) {

                final Uri resultUri = UCrop.getOutput(data);
                try {
                    selectedBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                byteArray = byteArrayOutputStream.toByteArray();
               /* driverImageIv.setImageBitmap(selectedBitmap);
                driverImageIv.setImageURI(resultUri);*/
                UploadDriverImage(ServiceConstant.RegistrationDriverImageUpload);

            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            System.out.println("========muruga cropError===========" + cropError);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_login_details);
//for android.os.FileUriExposedException target version>24
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Init();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.finish.LoginDetails");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("com.finish.LoginDetails")) {
                    finish();
                }
            }
        };
        try {
            registerReceiver(broadcastReceiver, filter);
        } catch (Exception e) {

        }
        headerBackRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                CloseKeyBoard();
                overridePendingTransition(R.anim.reg_pull_in_left, R.anim.reg_push_out_right);
            }
        });

        genderRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = genderRg.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                RadioButton radioButton = (RadioButton) findViewById(selectedId);
                radioButton.setTextColor(getResources().getColor(R.color.white_color));
                registrationInfoModel.setGender(radioButton.getText().toString());
            }
        });
        driverImageAddIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkAccessFineLocationPermission() || !checkAccessCoarseLocationPermission() || !checkWriteExternalStoragePermission()|| !checkREADExternalStoragePermission()) {
                        requestPermission();
                    } else {
                        chooseimage();
                    }
                } else {
                    chooseimage();
                }

            }
        });

        driverImageIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkAccessFineLocationPermission() || !checkAccessCoarseLocationPermission() || !checkWriteExternalStoragePermission() || !checkREADExternalStoragePermission()) {
                        requestPermission();
                    } else {
                        chooseimage();
                    }
                } else {
                    chooseimage();
                }

            }
        });

        loginDetailsNextTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (driverNameEt.getText().toString().trim().length() == 0) {
                    snackFlashBar.SnackBar(getResources().getString(R.string.app_name), getResources().getString(R.string.login_details_name_empty_error), mainRl, "error");
                } else if (driverEmailEt.getText().toString().trim().length() == 0) {
                    snackFlashBar.SnackBar(getResources().getString(R.string.app_name), getResources().getString(R.string.login_details_email_empty_error), mainRl, "error");
                } else if (!isValidEmail(driverEmailEt.getText().toString().replace(" ", ""))) {
                    snackFlashBar.SnackBar(getResources().getString(R.string.app_name), getResources().getString(R.string.login_details_email_invalid_error), mainRl, "error");
                } else if (passwordEt.getText().toString().length() == 0) {
                    snackFlashBar.SnackBar(getResources().getString(R.string.app_name), getResources().getString(R.string.login_details_password_empty_error), mainRl, "error");

                } else if (passwordEt.getText().toString().length() < 6) {
                    snackFlashBar.SnackBar(getResources().getString(R.string.app_name), getResources().getString(R.string.login_details_password_invalid_error), mainRl, "error");

                } else if (!confrmPasswordEt.getText().toString().equalsIgnoreCase(passwordEt.getText().toString())) {
                    snackFlashBar.SnackBar(getResources().getString(R.string.app_name), getResources().getString(R.string.login_details_confirm_password_mismatch_error), mainRl, "error");
                } else {

                    registrationInfoModel.setLoginDetailsAttr(driverNameEt.getText().toString(), driverEmailEt.getText().toString(), passwordEt.getText().toString());

                    Intent nextIntent = new Intent(RegistrationLoginDetailsActivity.this, RegistrationDriverDetailsActivity.class);
//                    nextIntent.putExtra("modelObject", registrationInfoModel);
                    CloseKeyBoard();
                    startActivity(nextIntent);
                    overridePendingTransition(R.anim.reg_pull_in_left, R.anim.reg_push_out_right);
                }

            }
        });
    }

    private void Init() {

        headerView = findViewById(R.id.header_registration);
        headerBackRl = (RelativeLayout) headerView.findViewById(R.id.back_layout);
        snackFlashBar = new SnackFlashBar(RegistrationLoginDetailsActivity.this);
        mainRl = (RelativeLayout) findViewById(R.id.main_layout);
        headerCollapseTL = (CollapsingToolbarLayout) headerView.findViewById(R.id.collapsing_toolbar);
        headerCollapseTL.setTitleEnabled(true);
        headerCollapseTL.setCollapsedTitleTextColor(getResources().getColor(R.color.white_color));
        headerCollapseTL.setTitle(getResources().getString(R.string.login_details_header_label));
        headerCollapseTL.setCollapsedTitleTextAppearance(R.style.coll_toolbar_title);
        headerCollapseTL.setExpandedTitleTextAppearance(R.style.exp_toolbar_title);

        sessionManager = new SessionManager(getApplicationContext());
        registrationInfoModel = RegistrationLoginTypeActivity.registrationInfoModel;
        loginDetailsNextTv = (TextView) findViewById(R.id.registration_login_details_next_tv);
        driverNameEt = (EditText) findViewById(R.id.registration_login_name_editText);
        driverEmailEt = (EditText) findViewById(R.id.registration_login_email_editText);
        passwordEt = (EditText) findViewById(R.id.registration_login_password_editText);
        confrmPasswordEt = (EditText) findViewById(R.id.registration_login_confrm_passwrd_editText);
        driverImageAddIv = (ImageView) findViewById(R.id.registration_login_iv_add_profile);
        driverImageIv = (CircularImageView) findViewById(R.id.registration_login_iv_profile);
        genderRg = (RadioGroup) findViewById(R.id.gender);
//        Intent in = getIntent();
//        registrationInfoModel = (RegistrationInfoModel) in.getSerializableExtra("modelObject");

        passwordEt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        confrmPasswordEt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        registrationInfoModel.setGender("Male");
        sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> language = sessionManager.getLanaguageCode();
        HashMap<String, String> user = sessionManager.getUserDetails();
        driverID = user.get(SessionManager.KEY_DRIVERID);
        gcmID = user.get(SessionManager.KEY_GCM_ID);
        Agent_Name = user.get(SessionManager.KEY_ID_NAME);
        language_code = user.get(SessionManager.KEY_Language_code);
    }

    // --------------------Method for choose image to edit profileimage--------------------
    private void chooseimage() {
        photo_dialog = new Dialog(RegistrationLoginDetailsActivity.this);
        photo_dialog.getWindow();
        photo_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        photo_dialog.setContentView(R.layout.image_upload_dialog);
        photo_dialog.setCanceledOnTouchOutside(true);
        photo_dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_photo_Picker;
        photo_dialog.show();
        photo_dialog.getWindow().setGravity(Gravity.CENTER);

        RelativeLayout camera = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromcamera);
        RelativeLayout gallery = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromgallery);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
                photo_dialog.dismiss();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
                photo_dialog.dismiss();
            }
        });
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        camera_FileUri = getOutputMediaFileUri(1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, camera_FileUri);
        // start the image capture Intent
        startActivityForResult(intent, REQUEST_TAKE_PHOTO);
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQUEST_GALLERY_PHOTO);
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    private void CloseKeyBoard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

    }

    //-------------------------code to Check Email Validation-----------------------
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void UploadDriverImage(String url) {

        dialog = new Dialog(RegistrationLoginDetailsActivity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

                System.out.println("------------- image response-----------------" + response.data);
                String resultResponse = new String(response.data);
                System.out.println("-------------  response-----------------" + resultResponse);
                String sStatus = "", sResponse = "", driver_image = "", Smsg = "", picture_name = "";
                try {
                    JSONObject jsonObject = new JSONObject(resultResponse);
                    sStatus = jsonObject.getString("status");
                    if (sStatus.equalsIgnoreCase("1")) {

                        driver_image = jsonObject.getString("picture_path");
                        picture_name = jsonObject.getString("picture_name");
                        registrationInfoModel.setDriverImageName(picture_name);
                        Picasso.with(RegistrationLoginDetailsActivity.this).load(driver_image).placeholder(R.drawable.no_profile_avatar_ic).into(driverImageIv);
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.edit_profile_photo_success_label), Toast.LENGTH_SHORT).show();

                    } else {
                        sResponse = jsonObject.getString("response");
                        Toast.makeText(getApplicationContext(), sResponse, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Something happened , try again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                System.out.println("-----------Authkey----------------" + Agent_Name);
                System.out.println("-----------apptype----------------" + ServiceConstant.cabily_AppType);
                System.out.println("-----------driverid----------------" + driverID);
                System.out.println("-----------apptoken----------------" + gcmID);
                System.out.println("-----------applanguage----------------" + language_code);
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authkey", Agent_Name);
                headers.put("isapplication", ServiceConstant.isapplication);
                headers.put("applanguage", language_code);
                headers.put("apptype", ServiceConstant.cabily_AppType);
                headers.put("driverid", driverID);
                headers.put("apptoken", gcmID);
                System.out.println("app header------------" + headers.toString());

                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("driver_id", driverID);
                System.out.println("driverID---------------" + driverID);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("picture", new DataPart("cabily_driver.jpg", byteArray));

                System.out.println("user_image--------edit------" + byteArray);

                return params;
            }
        };

        //to avoid repeat request Multiple Time
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        multipartRequest.setRetryPolicy(retryPolicy);
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        multipartRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(multipartRequest);

    }


    private boolean checkAccessFineLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkAccessCoarseLocationPermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkREADExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }


}
