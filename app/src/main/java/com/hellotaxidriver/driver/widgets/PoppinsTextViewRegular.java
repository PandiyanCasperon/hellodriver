package com.hellotaxidriver.driver.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class PoppinsTextViewRegular extends TextView {

    public PoppinsTextViewRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public PoppinsTextViewRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PoppinsTextViewRegular(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/POPPINS-REGULAR.TTF");
        setTypeface(tf);
    }
}