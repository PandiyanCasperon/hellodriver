package com.hellotaxidriver.driver.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextViewnormall extends TextView {

    public CustomTextViewnormall(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTextViewnormall(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTextViewnormall(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/SF-Pro-Display-Regular.ttf");
        setTypeface(tf);
    }
}