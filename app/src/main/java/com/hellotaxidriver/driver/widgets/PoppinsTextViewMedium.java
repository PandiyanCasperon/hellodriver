package com.hellotaxidriver.driver.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class PoppinsTextViewMedium extends TextView {

    public PoppinsTextViewMedium(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public PoppinsTextViewMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PoppinsTextViewMedium(Context context) {
        super(context);
        init();
    }


    public void init() {
       /* Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/POPPINS-MEDIUM.TTF");
        setTypeface(tf);*/
    }
}