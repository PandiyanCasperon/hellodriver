package com.hellotaxidriver.driver.widgets;


import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.romainpiel.shimmer.ShimmerButton;

public class shimmertextview extends ShimmerButton {

    public shimmertextview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public shimmertextview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public shimmertextview(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/POPPINS-MEDIUM.TTF");
        setTypeface(tf);
    }
}