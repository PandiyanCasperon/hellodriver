package com.hellotaxidriver.driver.mylocation;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.TypedValue;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hellotaxidriver.driver.Helper.GEODBHelper;
import com.hellotaxidriver.driver.Utils.SessionManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class MyLocationGoogleMap {
    public static final String LOGTAG = "Tracking";
    public static final String ACCURACY_COLOR = "#1800ce5b";
    public static final String ACCURACY_COLOR_BORDERS = "#8000ce5b";
    private final Handler handler = new Handler(Looper.getMainLooper());
    private Context mContext;
    private MyLocationProvider mLocationProvider;
    private boolean isMyLocationCentered = false;
    private boolean zoomed = false;
    private final float accuracyStrokeWidth;
    SessionManager mSessionManager;
    private Circle accuracyCircle;
    private Marker locationMarker;
    private Marker bearingMarker;
    private GEODBHelper myDBHelper;
    sharelocation ml;


    public MyLocationGoogleMap(Context context,sharelocation ml) {
        mContext = context;
        Resources r = context.getResources();
        float density = r.getDisplayMetrics().density;
        int size = (int) (256 * density);
        TileSystem.setTileSize(size);

        accuracyStrokeWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1,
                r.getDisplayMetrics()
        );
        this.ml = ml;
    }

    @SuppressLint("MissingPermission")
    public void moveToMyLocation(GoogleMap googleMap) {
        if (mLocationProvider != null) {
            Location location = mLocationProvider.getLastKnownLocation();
            if (googleMap != null && location != null) {
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 13);
                googleMap.animateCamera(cameraUpdate, 1000, null);
            }
        }
    }

    public void addTo(final GoogleMap googleMap, Bitmap driver_marker_resized)
    {
        myDBHelper = new GEODBHelper(mContext);
        addTo(googleMap, new GpsMyLocationProvider(mContext), driver_marker_resized);
    }



    public void addTo(final GoogleMap googleMap, MyLocationProvider myCustomLocationProvider, final Bitmap driver_marker_resized) {
        mLocationProvider = myCustomLocationProvider;
        mSessionManager = new SessionManager(mContext);
        mLocationProvider.startLocationProvider(new MyLocationConsumer() {
            @Override
            public void onLocationChanged(final Location location, MyLocationProvider source) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        Log.e("VSDK.onLocationChanged",
                                location.getLatitude() + " | " +
                                        location.getLongitude() + " | " +
                                        location.getBearing());

                        LatLng center = new LatLng(location.getLatitude(), location.getLongitude());
                        Calendar aCalendar = Calendar.getInstance();
                        SimpleDateFormat aDateFormat = new SimpleDateFormat("hh:mm:ss aa");
                        String aCurrentDate = aDateFormat.format(aCalendar.getTime());
                        ml.callback(center, location.getBearing(),aCurrentDate);


/*
//                        mSessionManager.currentlatlong(location.getLatitude(),location.getLongitude(),location.getBearing());

                        final float radius = location.getAccuracy()
                                / (float) TileSystem.GroundResolution(location.getLatitude(),
                                googleMap.getCameraPosition().zoom);


                        if (accuracyCircle == null) {
                            accuracyCircle = googleMap.addCircle(new CircleOptions()
                                    .center(center)
                                    .radius(radius)
                                    .fillColor(Color.parseColor(ACCURACY_COLOR))
                                    .strokeColor(Color.parseColor(ACCURACY_COLOR_BORDERS))
                                    .strokeWidth(accuracyStrokeWidth)
                            );
                        } else {
                            accuracyCircle.setCenter(center);
                            accuracyCircle.setRadius(radius);
                        }

                        

                        if (locationMarker == null) {
                           locationMarker = googleMap.addMarker(new MarkerOptions()
                                    .position(center)
                                    .anchor(0.5f, 0.5f)
                                    .icon(BitmapDescriptorFactory.fromBitmap(driver_marker_resized))
                            );
                        } else {
                            locationMarker.setPosition(center);
                        }

                        if (location.getBearing() == 0.0) {
                            if (bearingMarker != null) {
                                bearingMarker.remove();
                                bearingMarker = null;
                            }
                        } else {
                            if (bearingMarker == null)
                            {
                            if (locationMarker != null) {
                                locationMarker.remove();
                                locationMarker = null;
                            }
                            bearingMarker = googleMap.addMarker(new MarkerOptions()
                                    .position(center)
                                    .flat(true)
                                    .anchor(0.5f, 0.5f)
                                    .rotation(location.getBearing())
                                    .icon(BitmapDescriptorFactory.fromBitmap(driver_marker_resized))
                            );
                                if (locationMarker != null) {
                                    locationMarker.remove();
                                    locationMarker = null;
                                }
                        } else {
                            if (locationMarker != null) {
                                locationMarker.remove();
                                locationMarker = null;
                            }
                            bearingMarker.setPosition(center);
                            bearingMarker.setRotation(location.getBearing());
                            googleMap.animateCamera(CameraUpdateFactory.newLatLng(center));
                        }
                        }
                        if (!isMyLocationCentered)
                        {
                            isMyLocationCentered = true;
                            moveToMyLocation(googleMap);
                        }*/
                    }
                });
            }
        });
    }

    public void removeFrom() {
        Log.e("removed",
                "map removed");
        if (mLocationProvider != null) {
            mLocationProvider.destroy();
            mLocationProvider = null;
            locationMarker = null;
            bearingMarker = null;
        }
    }


}
