package com.hellotaxidriver.driver.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.hellotaxidriver.driver.R;

public class Welcome_slide1  extends Fragment
{
    Context context;
    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";
    ImageView Iv_Slide;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.welcome_slide1, container, false);
        context = getActivity();
        init(rootView);


        return rootView;
    }

    private void init(View rootView)
    {
		Iv_Slide = rootView.findViewById(R.id.iv_slide);
       // Glide.with(context).load(R.drawable.slider2).into(Iv_Slide);
    }

    public static Fragment newInstance(String string)
    {
        Welcome_slide1 f = new Welcome_slide1();

        return f;
    }
}

