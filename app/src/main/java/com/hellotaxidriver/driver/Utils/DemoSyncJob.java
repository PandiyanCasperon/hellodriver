package com.hellotaxidriver.driver.Utils;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.app.gcm.MyFirebaseMessagingService;
import com.app.xmpp.MyXMPP;
import com.hellotaxidriver.driver.R;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;

import java.util.HashMap;

public class DemoSyncJob extends Job {
    private static int jobId = -1;
    public static final String TAG = ">>>> job_demo_tag";
    public static SessionManager sessionManager;
    static Context context;
    public static MyXMPP xmpp;
    @Override
    protected Result onRunJob(Params params) {
        // run your job here
        Log.d(TAG, "onRunJob: ");
        if (sessionManager == null) {
            sessionManager = new SessionManager(this.getContext());
        }
        context = this.getContext();
        scheduleJob(context);
        if (sessionManager.isLoggedIn() || !sessionManager.getDriverId().equalsIgnoreCase("")) {
            if (MyXMPP.connected) {
                Log.d(TAG, "onRunJob: Xmpp connected");
            } else {
                Log.d(TAG, "onRunJob:  Xmpp not connected");
                String ServiceName = "", HostAddress = "";
                String USERNAME = "";
                String PASSWORD = "";
                SessionManager sessionManager = new SessionManager(this.getContext());
                // get user data from session
                HashMap<String, String> domain = sessionManager.getXmpp();
                ServiceName = domain.get(SessionManager.KEY_HOST_NAME);
                HostAddress = domain.get(SessionManager.KEY_HOST_URL);

                HashMap<String, String> user = sessionManager.getUserDetails();
                USERNAME = user.get(SessionManager.KEY_DRIVERID);
                PASSWORD = user.get(SessionManager.KEY_SEC_KEY);

                xmpp = MyXMPP.getInstance(this.getContext(), ServiceName, HostAddress, USERNAME, PASSWORD);
                xmpp.connect("onCreate");
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId  = "1";
            String channelName = getContext().getResources().getString(R.string.app_name);
            NotificationManager notificationManager =getContext().getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_LOW));
        }else{
            getContext().startService(new Intent(getContext(), MyFirebaseMessagingService.class));
        }
        return Result.SUCCESS;
    }

    public static void scheduleJob(Context context) {

        if (sessionManager == null) {
            sessionManager = new SessionManager(context);
        }
        int demoSyncJob = new JobRequest.Builder(DemoSyncJob.TAG)
                .setExecutionWindow(1_000L, 30_000L)
                .setUpdateCurrent(true)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .build()
                .schedule();
        sessionManager.setjobid(demoSyncJob);
    }


    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        try {
            ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "isMyServiceRunning: ", e);
        }
        return false;
    }
}