package com.hellotaxidriver.driver.Utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;


import com.hellotaxidriver.driver.appfloatingicon.service.FloatingViewService;

import java.util.HashMap;
import java.util.List;


public class MyActivityLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {

    private String checkLifeCycleStatus = "foreground";

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
    }

    public void onActivityResumed(Activity activity) {
        if (!isAppIsInBackground(activity)) {
            if (checkLifeCycleStatus.equalsIgnoreCase("foreground")) {
                available(activity);
                checkLifeCycleStatus = "background";
            }
        }
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityStopped(Activity activity) {
        if (isAppIsInBackground(activity)) {
            if (checkLifeCycleStatus.equalsIgnoreCase("background")) {
                unAvailable(activity);
                checkLifeCycleStatus = "foreground";
            }
            SessionManager sessionManager = new SessionManager(activity);
            HashMap<String, String> onlinedetails = sessionManager.getOnlineDetails();
            if (sessionManager.isLoggedIn() && onlinedetails.get(SessionManager.KEY_ONLINE).equalsIgnoreCase("1") && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(activity)) {
                activity.startService(new Intent(activity, FloatingViewService.class));
            }
        }
    }


    private void available(Activity activity) {
        SessionManager sessionManager = new SessionManager(activity);
        sessionManager.setAppStatus("resume");

        AppOpenCheck_Session appSession = new AppOpenCheck_Session(activity);
        appSession.setAppOpenStatus("open");

        ChatAvailabilityCheck chatAvailability = new ChatAvailabilityCheck(activity, "unavailable");
        chatAvailability.postChatRequest();

        activity.stopService(new Intent(activity, FloatingViewService.class));
  /*      if (!isMyServiceRunning(FloatingViewService.class, activity)) {
            activity.stopService(new Intent(activity, FloatingViewService.class));
        }*/

/*
        if (!isMyServiceRunning(IdentifyAppKilled.class, activity)) {
            activity.startService(new Intent(activity, IdentifyAppKilled.class));
        }*/

    }

    private void unAvailable(Activity activity) {
        SessionManager sessionManager = new SessionManager(activity);
        sessionManager.setAppStatus("pause");

        AppOpenCheck_Session appSession = new AppOpenCheck_Session(activity);
        appSession.setAppOpenStatus("close");

        ChatAvailabilityCheck chatAvailability = new ChatAvailabilityCheck(activity, "unavailable");
        chatAvailability.postChatRequest();
    }


    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            if (runningProcesses != null) {
                if (runningProcesses.size() > 0) {
                    for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                        if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                            for (String activeProcess : processInfo.pkgList) {
                                if (activeProcess.equals(context.getPackageName())) {
                                    isInBackground = false;
                                }
                            }
                        }
                    }
                }
            }

        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }


}
